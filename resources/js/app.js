require('./bootstrap');


window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('inscription-steps', require('./components/InscriptionSteps.vue').default);
Vue.component('inscription-group', require('./components/InscriptionGroup.vue').default);
Vue.component('inscription-modify', require('./components/InscriptionModify.vue').default);
// Vue.component('step-app', require('./components/StepApp.vue').default);

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import 'bootstrap-vue/dist/bootstrap-vue.css';
//import 'bootstrap/dist/css/bootstrap.css'

Vue.use(BootstrapVue);

const app = new Vue();

window.onload = function(){
    app.$mount('#app');
}