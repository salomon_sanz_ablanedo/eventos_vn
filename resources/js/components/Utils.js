const Utils = {

    getAge : function(date1,date2)
    {
        if (typeof date2 == 'undefined')
            var today = new Date();
        else
            var today = new Date(date2);

        var birthDate = new Date(date1);
        var age       = today.getFullYear() - birthDate.getFullYear();
        var m         = today.getMonth() - birthDate.getMonth();

        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
            age--;

        return age;
    },

    getDayDiff : function(date1,date2)
    {
        var date1 = new Date(date1);
        var date2 = new Date(date2);

        return (date2-date1)/(1000*60*60*24);
    },

    standar2MysqlDate : function(date){
        var date = date.split('/');
        return date[2]+'-'+date[1]+'-'+date[0];
    },

    mysqlDate2standar : function(date){
        var date = date.split('-');
        return date[2]+'/'+date[1]+'/'+date[0];
    },

    getDateSimple : function(date){
        var days = ['domingo','lunes','martes','miércoles','jueves','viernes','sábado'];
        date = new Date(date);
        return days[date.getDay()]+ ' ' + date.getDate();       
    },

    getRandomString : function(){
        return Math.random().toString(36).substr(2, 5)
    },

    validateDNI : function (value)
    {
        var validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
        var nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
        var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
        var str = value.toString().toUpperCase();

        if (!nifRexp.test(str) && !nieRexp.test(str)) return false;

        var nie = str
        .replace(/^[X]/, '0')
        .replace(/^[Y]/, '1')
        .replace(/^[Z]/, '2');

        var letter = str.substr(-1);
        var charIndex = parseInt(nie.substr(0, 8)) % 23;

        if (validChars.charAt(charIndex) === letter) return true;

        return false;
    },

    isEmpty : function (d) {
        return d == null || (typeof d == 'string' && d.trim() == '');
    },


    isEmail: function (e) {
        return /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(e);
    },
};

export default Utils;