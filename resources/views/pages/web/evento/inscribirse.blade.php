@extends('layouts.app')

@section('content')
    <inscription-group
        event_id="{{ $event_id }}"
        session="{{ Session::getId() }}"
        url_cart_line="{{ route('api.cart.line', ['line'=>'']) }}"
        logged_user="{{ Auth::user() }}"
    ></inscription-group>
    <input type="hidden" id="refreshbrowser" value="no">
@endsection

@push('scripts')

@include('partials/common_js')

@endpush