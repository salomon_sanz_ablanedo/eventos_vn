@extends('layouts.app')

@push('css')
<style>
body{
  background-image:url({{asset('img/eventos/'.$event->id.'/g.jpg')}}) !important;
  background-repeat:no-repeat;
  background-size:cover;
}
footer{
  display:none;
}
.jumbotron{
  background-color:rgba(256,256,256,0.7);
}
</style>
@endpush

@section('content')
<div class="container">
    <div class="jumbotron p-0">
        <div class="row">

            <div class="col-md-5">
                <img src="{{ asset('img/eventos/'.$event->id.'/screen.jpg') }}" width="100%" />
            </div>
            <div class="col-md-7 p-4 text-center" style="padding-left:40px;">
                <h1 style="font-size: 46px;">{{$event->name}}</h1>
                <hr></hr>
                <blockquote>
                    <p class="lead"><i>{{$event->slogan}}</i></p>
                    <footer><cite title="Source Title">{{$event->slogan_versicule}}</cite></footer>
                </blockquote>
                <hr/>
                <p class="lead" style="font-size: 18px;"><i class="fa fa-calendar"></i> del {{ DateUtils::getMonthDayFromDate($event->date_from) }} al {{ DateUtils::getMonthDayFromDate($event->date_to) }} de {{ DateUtils::getMonthNameByDate($event->date_from)}}</p>
                <p class="lead" style="font-size: 18px;"><i class="fa fa-map-marker"></i> {{ $event->address }}</p>
                <br>
                @if ($event_status != 'PENDING')
                <div class="row">
                    <div class="col-md-12">
                        @if ($event_status == 'PROGRESS')
                        Evento en curso
                        @else
                        Evento finalizado
                        @endif
                    </div>
                </div>
                @endif


                @if ($inscription_status == 'OPENED')
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            @if (!empty($event_info))
                            <a class="btn btn-lg btn-success" href="javascript:scrollToAnchor('eventinfo')" role="button">
                                <i class="fa fa-info"></i> &nbsp; Información
                            </a>
                            @endif
                            <a class="btn btn-lg btn-primary" href="{{route('web.evento.inscribirse',$event->id)}}" role="button">
                                <i class="fa fa-pencil-square-o"></i> &nbsp; Inscripción Online 
                            </a>
                        </p>
                    </div>
                    <div class="col-md-12">Plazo de inscripci&oacute;n online abiertas <u><b>hasta el {{ DateUtils::getMonthDayFromDate($event->date_inscription_end) }} de {{ DateUtils::getMonthNameByDate($event->date_inscription_end)}}</b></u>
                    </div>
                </div>
                @elseif ($inscription_status == 'CLOSED')
                    <div class="row">
                
                    @if ($event_status != 'ENDED')
                        Finalizado el plazo de inscripciones
                        
                        @if (Auth::check())
                            <div class="col-md-6">
                                <p class="text-center">
                                    <a class="btn btn-lg btn-success" href="{{route('web.evento.inscribirse',$event->id)}}" role="button"><i class="fa fa-pencil-square-o"></i> &nbsp; Inscripción Online</a>
                                <br>
                                </p>
                            </div>
                        @endif

                    @endif
                    </div>
                @else
                    <div class="row">
                    
                  {{--   @if (!empty($event_info))
                    <div class="col-12 text-center">
                        <a class="btn btn-lg btn-success" href="javascript:scrollToAnchor('eventinfo')" role="button">
                            <i class="fa fa-info"></i> &nbsp; Información
                        </a>
                    </div>
                    @endif --}}

                    <div class="col-12 text-center mt-2">
                        <span class="text-info">
                            <i class="fa fa-info-circle"></i> Apertura de inscripciones próximamente
                        </span>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    @if (!empty($event_info))
        <a name="eventinfo">
        </a>
        {!! $event_info !!}
        <br/>
        <br/>
        <br/>
        <br/>
        @endif

        @endsection
</div>

@push('scripts')
<script>
function scrollToAnchor(aid){
    var aTag = $("a[name='"+ aid +"']");
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}
</script>
@endpush