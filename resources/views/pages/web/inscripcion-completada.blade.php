@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row" id="inscription_complete">
    <div class="col-md-12">                        
        <div class="card">        
            <div class="card-body p-5">
                <div class="float-right pb-3">
                    <button class="btn btn-primary btn-lg col-xs-6 pull-right d-print-none" id="btn_print" onclick="window.print()"><li class="far fa-print"></li> &nbsp;Imprimir</button> 
                    <a href="{{route('web.evento.informacion',$event->id)}}" class="btn btn-primary col-xs-6 btn-lg pull-right d-print-none" id="btn_print"><li class="far fa-arrow-circle-left"></li> &nbsp;Volver </a>
                </div>

                <div class="form-group">
                <h1 class="page-header on-print-no-margin-top">
                    <li class="far fa-check fa-1x"></li> Inscripción 
                    @if (isset($sm) && $sm == 'UPDATE')
                        actualizada
                    @else
                        completada  
                    @endif
                </h1>
                </div>
                <div class="d-print-none">
                  <small class="text-danger"><b>¡Por favor! Imprime esta hoja y preséntala al llegar al evento en la recepción. </b></small>
                  <br>
                </div>
                <br>
                <div class="form-group">
                  <label class="text-muted">Evento:</label>
                  <p>{{ $event->name }}</p>
                </div>

                <div class="form-group">
                  <label class="text-muted">Nº Inscripci&oacute;n:</label>
                  <p><b>{{ $inscription->inscription_id }}</b></p>
                </div>

                <div class="form-group">
                  <label class="text-muted">Código de inscripci&oacute;n:</label>
                  <p> {{ $inscription->public_id }} </p>
                </div>

                <div class="form-group">
                  <label class="text-muted">Datos de contacto: </label>
                  <p> 
                    {{ $inscription->contact_name }}<br>
                    <small class="text-muted">
                      <li class="far fa-envelope-o"></li> {{ $inscription->contact_email }} &nbsp;&nbsp; 
                      <li class="far fa-phone"></li> {{ $inscription->contact_phone }}
                    </small>
                  </p>
                </div>

                <div class="form-group">
                  <label class="text-muted">Personas: </label>
                  <p> 
                    @foreach($inscription->lines as $insc)
                    <small>
                      {{ $insc->name}} {{ $insc->surname }} ({{ DateUtils::getAge($insc->birthday) }}),
                    </small>
                    @endforeach
                  </p>
                </div>

                <div class="form-group">
                  <label class="text-muted"> Importe total:</label>
                  <p> {{ $inscription->total_price }}&euro;</p>
                </div>

                @if ($inscription->total_price != 0)
                <div class="form-group">
                  <label class="text-muted"> <b>Forma de pago:</b></label>
                  <p> 
                    {{ $inscription::$PAYMENT_DESCRIPTIONS[$inscription->payment_method]['label'] }} 
                    @if (!empty($inscription::$PAYMENT_DESCRIPTIONS[$inscription->payment_method]['info']))
                      {{-- <small class="text-muted">{{ $inscription::$PAYMENT_DESCRIPTIONS[$inscription->payment_method]['info'] }}</small> --}}
                    @endif
                    @if ($paid)
                      <span class="text-success"><i class="far fa-check"></i> PAGADO </span>
                    @else
                      <span class="text-danger"><i class="far fa-warning"></i> PENDIENTE DE PAGO</span>
                      <!--<p class="text-danger text-bold"> <b>PENDIENTE</b></p>-->  &nbsp;&nbsp; 
                      {{-- <small class="text-muted">(siga las instrucciones de más abajo)</small> --}}
                    @endif
                  </p>
                </div>
                @endif

                @if (!$paid)
                <div class="row">
                    <div class="alert alert-secondary col-12">

                        @if ($inscription->payment_method !== 'CHURCH')
                        
                        <label class="text-muted"> Nº de Cuenta:</label>
                        <p> {{ $event->IBAN_bank }} <small class="text-muted">El titular de la cuenta es &quot;Asociación Jóvenes para Cristo&quot;)</small></p>

                        <label class="text-muted"> Importe:</label>
                        <p> {{  $inscription->total_price }}&euro;</p>
                        <label class="text-muted"> Concepto:</label>
                        <p class="text-italic"> <em>{{ $inscription->inscription_id}} {{ $inscription->contact_name}}</p>

                        <small class="text-danger"><b>¡Es muy importante especificar el concepto del ingreso o transferencia tal cual se indica!</b></small>
                        @else
                        {{-- Pida información en su Iglesia de como realizarla. --}}
                        {{ $inscription::$PAYMENT_DESCRIPTIONS[$inscription->payment_method]['info'] }}
                        @endif
                    </div>
                </div>
                @endif
                <div class="show-on-print">
                  <img id="barcode" src="{!! $barcode_src !!}">
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection


@push('styles')
<style>
label{

}
</style>
@endpush
@push('scripts')

@endpush
