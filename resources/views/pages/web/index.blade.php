@extends('layouts.app')

@section('content')
    <div id="page-content" class="p-4">
        <h2 style="margin-top: 0px;"> Próximos eventos</h2>
        <hr/>
        <div class="row">
        @foreach($events as $event)
            <div class="col-sm-6 col-xs-12 col-md-4 col-lg-3">                                        
                <div class="card p-0 m-3" style="width:18rem;">  
                    <a href="{{ route('web.evento.informacion', $event->id) }}"><img src="img/eventos/{{ $event->id }}/screen.jpg" width="100%" class="card-img-top"></a>
                    <div class="card-body" style="margin:12px;">
                        <h3> {{ $event->name }}</h3>
                        <p>del {{ DateUtils::getMonthDayFromDate($event->date_from) }} al {{ DateUtils::getMonthDayFromDate($event->date_to) }} de {{ DateUtils::getMonthNameByDate($event->date_from)}}</p>
                        <a href="{{ route('web.evento.informacion', $event->id) }}" class="btn btn-primary form form-control">+ info</a>
                    </div>
                </div>                
            </div>        
        @endforeach
        </div>
    </div>
@endsection