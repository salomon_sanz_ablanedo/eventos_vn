<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login | Eventos Iglesias en Restauracion</title>


	<!--STYLESHEET-->
	<!--=================================================-->

	<!--Open Sans Font [ OPTIONAL ] -->
 	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


	<!--Bootstrap Stylesheet [ REQUIRED ]-->
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">


	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="{{asset('css/nifty.min.css')}}" rel="stylesheet">


	<!--Font Awesome [ OPTIONAL ]-->
	<link href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
	<div id="container" class="cls-container">
		
		<!-- BACKGROUND IMAGE -->
		<!--===================================================-->
		<div id="bg-overlay" class="bg-img img-balloon" style="background-image: url({{asset('template/template/img/bg-img/bg-img-7.jpg')}}) !important;"></div>
		
		
		<!-- HEADER -->
		<!--===================================================-->
		<div class="cls-header cls-header-lg">
			<div class="cls-brand">
				<a class="box-inline" href="index.html">
					<!-- <img alt="Nifty Admin" src="img/logo.png" class="brand-icon"> -->
					<span class="brand-title">Eventos IER <span class="text-thin">Admin</span></span>
				</a>
			</div>
		</div>
		<!--===================================================-->
		
		
		<!-- LOGIN FORM -->
		<!--===================================================-->
		<div class="cls-content">
			<div class="cls-content-sm panel">
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<p class="pad-btm">Identifícate para entrar</p>
					<form action="{{ route('login_post') }}" method="POST">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-user"></i></div>
								<input type="text" class="form-control" placeholder="usuario" name="username" value="{{old('username')}}">
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
								<input type="password" class="form-control" placeholder="Password" name="password">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-8 text-left checkbox">
								<label class="form-checkbox form-icon">
								<input type="checkbox" name="remember" value="true"> Recordarme
								</label>
							</div>
							<!--
							<div class="col-xs-4">
								<div class="form-group text-right">
								<button class="btn btn-success text-uppercase" type="submit">Sign In</button>
								</div>
							</div>
							-->
						</div>
						<!--
						<div class="mar-btm"><em>- or -</em></div>
						-->
						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<button class="btn btn-primary btn-lg btn-block" type="submit">
							<i class="fa fa-sign-in fa-fw"></i> Acceder
						</button>
					</form>
				</div>
			</div>
			<div class="pad-ver">
				<a href="{{ url('/password/email') }}" class="btn-link mar-rgt" >Recordar contraseña</a>
				<a href="#" class="btn-link mar-lft">Crear nueva cuenta</a>
			</div>
		</div>
		<!--===================================================-->
		
		
		
		<!--===================================================-->
		
		
		
	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->


		
	<!--JAVASCRIPT-->
	<!--=================================================-->

	<!--jQuery [ REQUIRED ]-->
	<script src="{{asset('js/jquery-2.1.1.min.js')}}"></script>


	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="{{asset('js/bootstrap.min.js')}}"></script>


	<!--Fast Click [ OPTIONAL ]-->
	<script src="{{asset('plugins/fast-click/fastclick.min.js')}}"></script>

	
	<!--Nifty Admin [ RECOMMENDED ]-->
	<script src="{{asset('js/nifty.min.js')}}"></script>
		

</body>
</html>
