<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-hospital-o"></i> &nbsp;Asistentes por Iglesia</h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Iglesia</th>
					    <th>Total</th>                                                
                    </tr>
                </thead>
                <tbody> 
                	@foreach($users_by_church as $church)               	
                	<tr>
                        <td>
                        	<span class="text-muted">{{$church->city}}</span>
                        </td>
						<td><span><strong>{{$church->total}}</strong></span></td>                 
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>    
</div>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title"> <i class="fa fa-male"></i><i class="fa fa-female"></i> Asistentes por G&eacute;nero</h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center">G&eacute;nero</th>
					    <th>Total</th>                                                
                    </tr>
                </thead>
                <tbody> 
                	@foreach($users_by_gender as $gender)               	
                	<tr>
                        <td>
                        	<span class="text-muted"> &nbsp; {{$gender->genre}}</span>
                        </td>
						<td><span><strong>{{$gender->total}}</strong></span></td>                 
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>    
</div>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-sort-numeric-asc"></i> &nbsp; Asistentes por Edades</h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Edades</th>
					    <th>Total</th>                                                
                    </tr>
                </thead>
                <tbody> 
                	@foreach($users_by_age as $age)               	
                	<tr>
                        <td>
                        	<span class="text-muted">{{$age->age}}</span>
                        </td>
						<td><span><strong>{{$age->total}}</strong></span></td>                 
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>    
</div>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-tags"></i> &nbsp; Asistentes por tipo</h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Desempe&ntilde;o</th>
					    <th>Total</th>                                                
                    </tr>
                </thead>
                <tbody> 
                	@foreach($users_by_role as $role)               	
                	<tr>
                        <td>
                        	<span class="text-muted">{{$role->event_role}}</span>
                        </td>
						<td><span><strong>{{$role->total}}</strong></span></td>                 
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>    
</div>