@push('css')
<style>
.report_box .panel-heading, .report_box .panel-body{
    background-color: #FFFFFF;
    box-shadow: 0 2px 0 rgba(0,0,0,0.05);
}
.report_box .panel-body{
    padding-bottom: 0px !important;
}
</style>
@endpush

<div class="row">
    <div class="report_box col-md-6 col-lg-3 panel">
        <div class="panel-heading">
            <h3 class="panel-title">Guarder&iacute;a</h3>
        </div>
        
        <div class="panel-body">
            <div class="row">

                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_nursery_by_age">
                    <div class="form-group">
                        <label class="control-label" for="demo-hor-inputemail"><strong>Listado por edad (desde/hasta)</strong></label>
                        <br/>
                        Desde los <input type="text" class="form-inline" value="0" name="age_from" size="3"> a los
                        <input type="text" class="form-inline" value="3" name="age_to" size="3"> años
                        <br/>
                        <br>
                        Apuntados a guardería:
                        <select name="nursery" class="form-control">
                            <option value="si" selected="">Si</option>
                            <option value="no">No</option>
                            <option value="todos">Todos</option>
                        </select>
                        <br/>
                        Ordenar por:
                         <select name="order_by" class="form-control">
                            <option value="apellidos" selected>Alfabético (Apellidos)</option>
                            <option value="inscripciones">Inscripciones</option>
                            <option value="edad">Edad</option>
                            <option value="iglesias">Iglesia</option>
                        </select>
                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="nursery_by_age">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>  
                            </div>
                        </div>
                    </div>
                </form>

                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_nursery_by_year">
                    <div class="form-group">
                        <label class="control-label" for="demo-hor-inputemail"><strong>Por año nacimiento (desde/hasta)</strong></label>
                        <br/>
                        Desde <input type="text" class="form-inline" value="2012" name="year_from" size="4"> al 
                        <input type="text" class="form-inline" value="2015" name="year_to" size="4"> 
                         <br>
                         <br>
                        Apuntados a guardería:
                        <select name="nursery" class="form-control">
                            <option value="si" selected="">Si</option>
                            <option value="no">No</option>
                            <option value="todos">Todos</option>
                        </select>
                        <br/>
                        Ordenar por:
                         <select name="order_by" class="form-control">
                            <option value="apellidos" selected>Alfabético (Apellidos)</option>
                            <option value="inscripciones">Inscripciones</option>
                            <option value="año">Año</option>
                            <option value="iglesias">Iglesia</option>
                        </select>
                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="nursery_by_year">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>  
                                <button class="btn btn-default btn-submit-report" type="button" data-output="bracelet"> P </button>  
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>



    <!-- ASISTENTES -->
    <div class="report_box col-md-6 col-lg-3 panel">
        <div class="panel-heading">
            <h3 class="panel-title">Asistentes</h3>
        </div>
        
        <div class="panel-body">
            <div class="row">

                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_inscriptions_by_church">
                    <div class="form-group">
                        
                        <label class="control-label" for="demo-hor-inputemail">
                            <strong>Por iglesia</strong>  
                            <small>Para elegir varias, ctrl + click</small>
                        </label>
                        <br/>
                        <select name="church[]" multiple class="form-control">
                            <option value="ALL" selected>TODAS</option>
                            @foreach($churchs as $church)
                            <option value="{{$church->id}}">{{$church->city}}</option>
                            @endforeach
                        </select>
                        <br>
                        Ordenar por:
                        <select name="order_by" class="form-control">
                            <option value="apellidos" selected="">Alfabético (Apellidos)</option>
                            <option value="nombre" selected="">Alfabético (Nombre)</option>
                            <option value="inscripciones">Inscripciones</option>
                            <option value="iglesias">Iglesia</option>
                            <option value="talleres">Taller</option>
                            <option value="roles">Roles</option>
                        </select>

                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="inscriptions_by_church">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>  
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- COMIDAS -->
    <div class="report_box col-md-6 col-lg-3 panel">
        <div class="panel-heading">
            <h3 class="panel-title">Comidas</h3>
        </div>
        
        <div class="panel-body">
            <div class="row">

                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_meals_by_meal">
                    <div class="form-group">
                        
                        <label class="control-label" for="demo-hor-inputemail">
                            <strong></strong>  
                            <small>Para elegir varias, ctrl + click</small>
                        </label>
                        <br/>

                        <select name="meals[]" class="form-control" data-mandatory="true" data-mandatory-fieldname="Comida">
                            @foreach($meals as $meal)
                            <option value="{{$meal->id}}">{{substr($meal->type,2)}} del {{DateUtils::mysqlDateToStandarDate($meal->date)}}</option>
                            @endforeach
                            <option value="someday">Cualquier dia</option>
                        </select>
                        <br>

                        {{-- Comen:
                        <select name="comen" class="form-control">
                            <option value="SI" selected>SI</option>
                            <option value="NO">NO</option>
                        </select>
                        <br> --}}
                        Ordenar por:
                        <select name="order_by" class="form-control">
                            <option value="apellidos" selected="">Alfabético (Apellidos)</option>
                            <option value="inscripciones">Inscripciones</option>
                            <option value="fecha">fecha (útil para múltiples días)</option>
                            <option value="marcaje">marcaje</option>
                        </select>

                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="meals_by_meal">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="bracelet"> P </button> 
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <!-- PARKING -->
    <div class="report_box col-md-6 col-lg-3 panel">
        <div class="panel-heading">
            <h3 class="panel-title">Parking</h3>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_parking_complete">
                    <div class="form-group">
                        <label class="control-label" for="demo-hor-inputemail">
                            <strong>Evento completo</strong>  
                        </label>
                        <br/>
                        Ordenar por:
                        <select name="order_by" class="form-control">
                            <option value="apellidos" selected="">Alfabético (Apellidos)</option>
                            <option value="iglesias">Iglesias</option>
                        </select>

                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="parking_complete">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>  
                            </div>
                        </div>
                    </div>
                </form>

                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_parking_by_days">
                    <div class="form-group">
                        <label class="control-label" for="demo-hor-inputemail">
                            <strong>Por días</strong>  
                            <small>Elige una fecha</small>
                        </label>
                        <br/>
                        <select name="days" class="form-control" data-mandatory="true" data-mandatory-fieldname="Días de parking">
                            @foreach($parking_days as $day)
                            <option value="{{$day}}">{{DateUtils::mysqlDateToStandarDate($day)}}</option>
                            @endforeach
                        </select>
                        <br>
                        Ordenar por:
                        <select name="order_by" class="form-control">
                            <option value="apellidos" selected="">Alfabético (Apellidos)</option>
                            <option value="iglesia">iglesia</option>
                        </select>

                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="parking_by_days">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>  
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- TALLERES -->
    <div class="report_box col-md-6 col-lg-3 panel">
        <div class="panel-heading">
            <h3 class="panel-title">Talleres</h3>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_studios">
                    <div class="form-group">
                        <label class="control-label" for="demo-hor-inputemail">
                            <strong>Taller:</strong>  
                        </label>
                        <br/>
                        <select name="studio_id" class="form-control">
                            @foreach($studios as $s)
                                <option value="{{ $s->short_name .'|'.$s->id }}">{{ $s->short_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        Ordenar por:
                        <select name="order_by" class="form-control">
                            <option value="apellidos" selected="">Alfabético (Apellidos)</option>
                            <option value="iglesias">Iglesias</option>
                        </select>

                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="studios">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>  
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- ALOJAMIENTO -->
    <div class="report_box col-md-6 col-lg-3 panel">
        <div class="panel-heading">
            <h3 class="panel-title">Alojamiento</h3>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_lodgement_by_room">
                    <div class="form-group">
                        <label class="control-label" for="demo-hor-inputemail">
                            <strong>Por habitación</strong> <small>(en 
                        @foreach($lodgements as $l)
                            @if ($l->external == 0)
                                <input type="hidden" name="lodgement_id" value="{{ $l->id }}"/>
                                 <i>{{ $l->name }} </i>
                            @endif
                        @endforeach
                         )  </small>
                        </label>
                        <br/>

                        Filtrar:
                        <select name="filter_room" class="form-control">
                            <option value="sin">Sin habitación</option>
                            <option value="con">Con habitación</option>
                            <option value="all">Todos</option>
                        </select>
                        <br/>

                        Fecha de inscripción<br/>
                        <small>fechas inclusivas, dejar vacio para no aplicar filtro</small>
                        <br>
                        Desde <input type="text" name="desde" value=""/> <small>dd-mm-aaaa</small>
                        <br/>
                        Hasta <input type="text" name="hasta" value=""/> <small>dd-mm-aaaa</small>

                        <br><br>

                        Ordenar por:
                        <select name="order_by" class="form-control">
                            <option value="apellidos" selected="">Alfabético (Apellidos)</option>
                            <option value="inscripcion" selected="">Inscripcion</option>
                            <option value="iglesia">Iglesia</option>
                            <option value="habitacion">Habitación</option>
                        </select>

                        <br>

                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="lodgement_by_room">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>  
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="row">
                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_lodgement_by_days">
                    <div class="form-group">
                        <label class="control-label" for="demo-hor-inputemail">
                            <strong>Por días</strong>  
                        </label>
                        <br/>
                        <select name="days" class="form-control" data-mandatory="true" data-mandatory-fieldname="Días de parking">
                            @foreach($lodgement_dates as $ld)
                            <option value="{{ $ld }}">{{ DateUtils::mysqlDateToStandarDate($ld) }}</option>
                            @endforeach
                        </select>

                        <br>
                        Alojamiento:  Para elegir varias, ctrl + click  
                        <select name="lodgement_id[]" class="form-control" multiple>
                            @foreach($lodgements as $l)
                            <option value="{{ $l->id }}">{{ $l->name }}</option>
                            @endforeach
                        </select>
                        <br>

                        Ordenar por:
                        <select name="order_by" class="form-control">
                            <option value="apellidos" selected="">Alfabético (Apellidos)</option>
                            <option value="iglesia">Iglesia</option>
                            <option value="alojamiento">Alojamiento</option>
                        </select>

                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="lodgement_by_days">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>  
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- PAGOS -->
    <div class="report_box col-md-6 col-lg-3 panel">
        <div class="panel-heading">
            <h3 class="panel-title">Pagos</h3>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_payment">
                    <div class="form-group">
                        <label class="control-label" for="demo-hor-inputemail">
                            <strong>Balances de pago:</strong>  
                        </label>
                        <br/>
                        <select name="payment_balance" class="form-control">
                            <option value="paid" selected>Pagado</option>
                            <option value="unpaid">Sin pagar</option>
                            <option value="refundable">A devolver</option>
                        </select>
                    </div>

                    <div class="form-group">
                        Ordenar por:
                        <select name="order_by" class="form-control">
                            <option value="apellidos" selected="">Alfabético (Apellidos)</option>
                            <option value="iglesias">Iglesias</option>
                            <option value="importe_asc" selected="">Importe ASC</option>
                            <option value="importe_desc" selected="">Importe DESC</option>
                        </select>

                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="payment">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>  
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

     <!-- ROLES -->
    <div class="report_box col-md-6 col-lg-3 panel">
        <div class="panel-heading">
            <h3 class="panel-title">Roles</h3>
        </div>
        
        <div class="panel-body">
            <div class="row">
                <form method="post" target="_blank" action="{{route('admin.eventos.informes_post',$event_id)}}" id="report_roles">
                    <div class="form-group">
                        <label class="control-label" for="demo-hor-inputemail">
                            <strong>Roles de evento:</strong>  
                        </label>
                        <br/>
                        <select name="roles" class="form-control" multiple>
                            @foreach($roles as $role)
                                @if ($role != 'USUARIO')
                                <option value="{{$role}}">{{$role}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        Ordenar por:
                        <select name="order_by" class="form-control">
                            <option value="apellidos" selected="">Alfabético (Apellidos)</option>
                            <option value="iglesias">Iglesias</option>
                            <option value="roles" selected="">Roles</option>
                        </select>

                    </div>

                    <div class="pad-btm form-inline">
                        <div class="row">
                            <div class="col-sm-12 table-toolbar-right">            
                                <input type="hidden" name="report_type" value="roles">
                                <input type="hidden" name="output_type" value="screen">        
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">          
                                <button class="btn btn-default btn-submit-report" type="button" data-output="screen"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="excel"><i class="fa fa-file-excel-o"></i></button>
                                <button class="btn btn-default btn-submit-report" type="button" data-output="print"><i class="fa fa-print"></i></button>  
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('styles')
<style>
.report_box
{
    margin: 3px;
}
</style>
@endpush
@push('scripts')
<script>
    window.onload = function()
    {
        $('form button.btn-submit-report').on('click',function()
        {
            var output_type = $(this).data('output');
            $(this).parent().find('input[name="output_type"]').val(output_type);
            var form_id =  'report_'+$(this).parent().find('input[name="report_type"]').val();

            var errors = [];
            $('#'+form_id).find('input[data-mandatory="true"], select[data-mandatory="true"]').each(function(index,el){
                if ($(el).val() == '' || $(el).val() == null)
                    errors.push($(el).data('mandatory-fieldname'));
            });

            if (errors.length > 0)
                alert('Los siguientes campos son obligatorios: \n\n- ' + errors.join('\n-'))
            else
                $('#'+form_id).submit();
        });
    }

</script>
@endpush