@push('css')
<style>
.report_box .panel-heading, .report_box .panel-body{
    background-color: #FFFFFF;
    box-shadow: 0 2px 0 rgba(0,0,0,0.05);
}
.report_box .panel-body{
    padding-bottom: 0px !important;
}
</style>
@endpush

<div class="panel">

    <div class="report_box col-md-6 col-lg-3">
        <div class="panel-heading">
            <h3 class="panel-title">Comidas</h3>
        </div>
            
        <div class="panel-body">
            <div class="row">
                <div class="form-group">
                    <label class="control-label" for="demo-hor-inputemail"><strong>Elige comida</strong></label>
                    <br>
                     <select class="form-control" id="meal_id">
                        @foreach ($meals as $m)
                            <option value="{{$m->id}}">{{substr($m->type,2)}} del {{DateUtils::getDayNameByDate($m->date)}} {{DateUtils::mysqlDateToStandarDate($m->date)}} </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <div class="input-group mar-btm">
                        <input id="inscription_code_reg" type="text" placeholder="REGISTRAR" class="form-control" autocomplete="off" name="search">
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="submit" id="btn_register_meal"> &nbsp;<i class="fa fa-barcode"></i></button>
                        </span>
                    </div>

                </div>

                <div class="form-group">
                    <div class="input-group mar-btm">
                        <input id="inscription_code_info" type="text" placeholder="INFO" class="form-control" autocomplete="off" name="search">
                        <span class="input-group-btn">
                            <button class="btn btn-info" type="submit" id="btn_info_meal"> &nbsp;<i class="fa fa-info" style="min-width:12px;"></i></button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="inscription_modal" class="modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <!--
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      -->
      <div class="modal-body" style="background-color:#e7ebee; padding:0px;">
        <iframe id="iframe_modal" style="width:100%; min-height:600px;" frameBorder="0"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="$('#demo-input-search2').focus();">cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- SUCCESS -->
<div style="background:rgba(0,205,102,0.7); position:absolute; width:100%; height:200px; z-index:9999; top:0; left:0; text-align:center; text-transform:uppercase; padding:24px; display: none;" id="ok_screen">
    <br>
    <span style="font-size:74px; background-color:#FFFFFF;">  OK! :)</span>
    <span id="show_picnic">
        <span style="font-size:74px; color:#FF0000;" class="text-bold">(PICNIC)</span>
    </span>
    <br/><br/>
    <div style="font-size: 24px;">
        <span id="meals_pending"></span> pendientes
    </div>
</div>

<!-- ERROR -->
<div style="background:rgba(255,0,0,0.7); position:absolute; width:100%; height:250px; z-index:9999; top:0; left:0; text-align:center; text-transform:uppercase; padding:24px; display: none;" id="error_screen">
    <span style="font-size:74px; background-color:#FFFFFF;" id="txt_error"></span>\
</div>

<script>
var url_info_meals = '{{route('admin.inscripcion.meals_info','-id-')}}';
var url_post_meals = '{{route('admin.eventos.registros_post','a')}}';
var file_audio_ok = new Audio('{{asset('sounds/ok.wav')}}');
var file_audio_ko = new Audio('{{asset('sounds/ko.wav')}}');

//file_audio_ok.play();
//file_audio_ko.play();

function showInfo(id)
{
    $('#iframe_modal').attr('src',url_info_meals.replace('-id-',String(id)));
    $('#inscription_modal').modal('show');
}

$(document).ready(function()
{
    $('#inscription_code_info, #inscription_code_reg').on('keypress',function(e)
    {
        var code = e.keyCode || e.which;

        if (code == 13)
        {
            if ($(this).attr('id') == 'inscription_code_reg')
                registerMeal();
            else
                showInfo($(this).val());
        }
    });

    $('#btn_info_meal').on('click',function(){
        showInfo($('#inscription_code_info').val());
    });

    $('#btn_register_meal').on('click',function(){
        registerMeal();
    });
});

function registerMeal()
{
    $('#inscription_code_reg').prop('disabled', true);

    axios.get(url_post_meals, { params : {
            _token  : '{{csrf_token()}}',
            user_public_id    : $.trim($('#inscription_code_reg').val()),
            meal_id : $('#meal_id').val()
        }})
        .then(function(result){
            var data = result.data;

            if (data.hasOwnProperty('error'))
                onRegisterMealError(data.error);
            else
            {
                file_audio_ok.play();
                $('#ok_screen').show();

                if (data.picnic == true)
                    $('#show_picnic').show();
                else
                    $('#show_picnic').hide();

                $('#meals_pending').text(data.pending);

                setTimeout(function(){
                    $('#ok_screen').hide();
                },1500);
            }
        }.bind(this))
        .catch(function(error){
            onRegisterMealError('error al realizar la consulta');
        }.bind(this))
        .then(function(data){
            $('#inscription_code_reg').prop('disabled', false);
            $('#inscription_code_reg').val('');
            $('#inscription_code_reg').focus();
        }.bind(this));
}

function onRegisterMealError(error)
{
    file_audio_ko.play();

    var msg;

    switch(error)
    {
        case 'INVALID_USER_PUBLIC_ID':
            msg = 'formato inválido de inscripción';
        break;

        case 'INVALID_MEAL_ID':
            msg = 'formato inválido de comida';
        break;

        case 'INSCRIPTION_NOT_EXISTS':
            msg = 'inscripción no encontrada';
        break;

        case 'USER_MEAL_NOT_EXISTS':
            msg = 'el usuario no tiene esta comida';
        break;

        case 'ALREADY_REGISTERED':
            msg = 'comida ya registrada anteriormente';
        break;

        default:
            msg = 'ERROR DESCONOCIDO';
        break;
    }

    $('#txt_error').html(' ' + msg + ' ');
    $('#error_screen').show();

    setTimeout(function(){
        $('#error_screen').hide();
    },3000);
}

</script>