<div class="row">

    <div class="col-sm-6 col-lg-3">         
        <!--Registered User-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="panel media pad-all">
            <div class="media-left">
                <span class="icon-wrap icon-wrap-sm icon-circle">
                <i class="fa fa-user-plus fa-2x"></i>
                </span>
            </div>
    
            <div class="media-body">
                <p class="text-muted mar-no"> 
                    <a href="{{route('web.evento.inscribirse',$event_id)}}?src=admin" target="_blank" class="btn btn-default">
                        - Nueva inscripci&oacute;n de grupo
                    </a>  
                </p>
            </div>
        </div>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    </div>

	<div class="col-sm-6 col-lg-3">			
        <!--Registered User-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="panel media pad-all">
            <div class="media-left">
                <span class="icon-wrap icon-wrap-sm icon-circle bg-success">
                <i class="fa fa-edit fa-2x"></i>
                </span>
            </div>
    
            <div class="media-body">
                <p class="text-2x mar-no text-thin">{{$day_insc_count}}</p>
                <p class="text-muted mar-no">Inscritos hoy</p>
            </div>
        </div>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    </div>
    
    <div class="col-sm-6 col-lg-3">	
        <!--Sales-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="panel media pad-all">
            <div class="media-left">
                <span class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                <i class="fa fa-user fa-2x"></i>
                </span>
            </div>

            <div class="media-body">
                <p class="text-2x mar-no text-thin">{{$total_insc_count}}</p>
                <p class="text-muted mar-no">Apuntados en total</p>
            </div>
        </div>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

    </div>
    
     <div class="col-sm-6 col-lg-3">	
        <!--Sales-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="panel media pad-all">
            <div class="media-left">
                <span class="icon-wrap icon-wrap-sm icon-circle bg-info">
                <i class="fa fa-calendar fa-2x"></i>
                </span>
            </div>

            <div class="media-body">
                <p class="text-2x mar-no text-thin">{{$days_left}}</p>
                <p class="text-muted mar-no">Días para el evento</p>
            </div>
        </div>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

    </div>

    {{-- <div class="col-sm-6 col-lg-3"> 
        <!--Sales-->
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
        <div class="panel media pad-all">
            <div class="media-left">
                <span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
                <i class="fa fa-dollar fa-2x"></i>
                </span>
            </div>

            <div class="media-body">
                <p class="text-2x mar-no text-thin">{{$unpaid}}&euro;</p>
                <p class="text-muted mar-no">Pendientes de pago</p>
            </div>
        </div>
        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

    </div>    --}}
</div>