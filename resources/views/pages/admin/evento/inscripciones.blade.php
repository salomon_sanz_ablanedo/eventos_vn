
<div class="panel">        

    <!--Data Table-->
    <!--===================================================-->
    <div class="panel-body">
        <div class="pad-btm form-inline">
            <div class="row">
                <div class="col-sm-6 table-toolbar-left">
                	<form method="get" action="{{ route('admin.eventos.inscripciones',$event_id) }}">
                        <div class="form-group">
                            <div class="input-group mar-btm">
                                <input id="demo-input-search2" type="text" placeholder="Introduce apellidos, DNI, o identificador de inscripci&oacute;n" class="form-control" autocomplete="off" style="min-width:400px;" name="search">
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="submit"> Buscar &nbsp;<i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                    
                </div>
                <div class="col-sm-6 table-toolbar-right">                   	
                    <button class="btn btn-default"><i class="fa fa-print"></i></button>
                    <button class="btn btn-default"><i class="fa fa-file-pdf-o"></i></button>
                    
                </div>
            </div>
        </div>

        <div class="bootstrap-table">
        	<div class="fixebd-table-toolbar"></div>
            <div class="fixed-table-container">
                <div class="fixed-table-header">
                	<table></table>
                </div>
               
                @if (count($users) == 0)
                    @if (count($filters) > 0)
                        No se ha encontrado ning&uacute;n resultado con <b>&quot;{{ Request::input('search') }}&quot;</b>, inténtalo de nuevo <br><br>
                    @else
                        Usa el buscador para encontrar inscripciones, puedes buscar por apellido, DNI, nº de inscripción o código de inscripción
                    @endif
                @else
                    Se han encontrado <span class="text-danger"><b>{{count($users)}}</b></span> inscripciones con <b>&quot;{{ Request::input('search') }}&quot;</b> <br><br>

                    <div class="fixed-table-body">
                    <div class="fixed-table-loading" style="top: 37px; display: none;">Cargando, por favor espera…</div>
                    <table data-toggle="table" data-url="data/bs-table.json" data-sort-name="id" data-page-list="[5, 10, 20]" data-page-size="5" data-pagination="true" data-show-pagination-switch="true" class="table table-hover">
                        <thead>
                            <tr>
                                <th style=""><div class="th-inner sortable">Nº </div><div class="fht-cell"></div></th>
                                <th style=""><div class="th-inner sortable">Nombre<span class="order dropup"><span class="caret" style="margin: 10px 5px;"></span></span></div><div class="fht-cell"></div></th>
                                <th style=""><div class="th-inner sortable">DNI<span class="order dropup"><span class="caret" style="margin: 10px 5px;"></span></span></div><div class="fht-cell"></div></th>                                    
                                <th style=""><div class="th-inner sortable">Iglesia<span class="order dropup"><span class="caret" style="margin: 10px 5px;"></span></span></div><div class="fht-cell"></div></th>
                                <th style=""><div class="th-inner sortable">Fecha inscripci&oacute;n</div><div class="fht-cell"></div></th>                                    
                                <th style="text-align: center; "><div class="th-inner sortable">Precio insc.</div><div class="fht-cell"></div></th>                                                                    
                            </tr>
                        </thead>
                        <tbody>                             
                            @foreach ($users as $user)
                            <?php
                            ?>
                            <tr data-index="1">
                                <td style="">
                                    <a href="#!" onclick="showInscription({{ $user->inscription_id }});" class="btn-link"> #{{ $user->inscription_id }}</a>
                                </td>
                                <td style="">
                                    <a href="#!" onclick="showInscription({{ $user->inscription_id }});" class="btn-link">
                                        {{$user->name}} {{$user->surname}}
                                    </a>

                                    <small>({{DateUtils::getYearsDiff($user->birthday, date('Y-m-d'))}})</small>

                                    {{-- @if ($insc_type == 'GROUP')
                                        <a href="{{route('admin.eventos.inscripciones',$user->event_id)}}?search={{$user->public_id}}"><li class="fa fa-users" alt="pincha para mostrar las inscripciones del grupo"></li></a>
                                    @endif --}}
                                </td>
                                <td style="">
                                    @if ($user->nif_no)
                                        --
                                    @else
                                    {{ $user->nif }}
                                    @endif
                                </td>
                                <td style="">{{ $user->church->city }}</td>
                                <td style="">
                                    <span class="text-muted">
                                        {{DateUtils::mysqlDateToStandarDate($user->created_at)}}
                                    </span>
                                </td>
                                <td style="text-align: center;">
                                    <span class="label label-default">                                  
                                        {{$user->payment_amount}} &euro;
                                    </span>

                                    &nbsp;
                                    @if ($user->payment_method == 'PAYPAL')
                                        <img src="{{asset('img/paypal.png')}}" align="absmiddle"/>
                                    @elseif ($user->payment_method == 'CREDIT_CARD')
                                        <img src="{{asset('img/card.png')}}" align="absmiddle"/>
                                    @elseif ($user->payment_method == 'BANK_TRANSFER')
                                        <img src="{{asset('img/bank_transfer.png')}}" align="absmiddle"/>
                                    @endif
                                </td>
                            </tr>   
                           
                            @endforeach                             
                            </tbody>
                        </table>
                    </div>  
                @endif
            </div>        
        </div>
    </div>
    <!--===================================================-->
    <!--End Data Table-->

</div>

<div id="inscription_modal" class="modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <!--
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      -->
      <div class="modal-body" style="background-color:#e7ebee; padding:0px;">
        <iframe id="iframe_modal" style="width:100%; min-height:600px;" frameBorder="0"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="$('#demo-input-search2').focus();">cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
var url_insc = '{{route('admin.inscripcion.inicio','-id-')}}';

function showInscription(id)
{
    $('#iframe_modal').attr('src',url_insc.replace('-id-',String(id)));
    $('#inscription_modal').modal('show')
}

window.onload = function()
{
    @if ($autoopen == true)
        showInscription('{{ $users[0]->inscription_id }}');
    @else
        $('#demo-input-search2').focus();
    @endif
}



</script>