		<div class="tab-base">
        
            <!--Nav Tabs-->
            <ul class="nav nav-tabs" id="config_event_options">
                <li class="active" data-opt="general">
                    <a data-toggle="tab" href="#demo-lft-tab-1">General</a>
                </li>
                <li data-opt="comida">
                    <a data-toggle="tab" href="#demo-lft-tab-2">Comida</a>
                </li>
                 <li>
                    <a data-toggle="tab" data-opt="alojamiento" href="#demo-lft-tab-3">Alojamiento</a>
                </li>
                <li>
                    <a data-toggle="tab" data-opt="servicios" href="#demo-lft-tab-4">Servicios</a>
                </li>
                 <li>
                    <a data-toggle="tab" data-opt="transporte" href="#demo-lft-tab-5">Transporte</a>
                </li>
            </ul>

            <!--Tabs Content-->
            <div class="tab-content">
                <div id="demo-lft-tab-1" class="tab-pane fade active in">
                	<form class="form-horizontal" method="post">
                        <h4 class="text-thin">Datos del Evento</h4>
                        <hr>                                                           
    
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="demo-hor-inputemail">Nombre del evento</label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="Nombre del evento" id="demo-hor-inputemail" class="form-control" value="{!!$evento->name!!}"/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Eslogan</label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="Eslogan" id="input_eslogan" class="form-control" value="{!!$evento->slogan!!}"/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Tipo de evento</label>
                            <div class="col-sm-9">
                                <select class="form-control">
                                	<option @if ($evento->type=='CONGRESO')selected @endif>CONGRESO</option>
                                	<option @if ($evento->type=='RETIRO')selected @endif>RETIRO</option>
                                	<option @if ($evento->type=='CAMPAMENTO')selected @endif>CAMPAMENTO</option>
                                    <option @if ($evento->type=='OTRO')selected @endif>OTRO</option>
                                </select>
                            </div>
                        </div>
                        
                       <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Visible</label>
                            <div class="col-sm-9">
                                <span class="switchery switchery-default" style="border-color: rgb(100, 189, 99); box-shadow: rgb(100, 189, 99) 0px 0px 0px 10px inset; transition: border 0.4s, box-shadow 0.4s, background-color 1.2s; -webkit-transition: border 0.4s, box-shadow 0.4s, background-color 1.2s; background-color: rgb(100, 189, 99);"><small style="left: 17px; transition: left 0.2s; -webkit-transition: left 0.2s; background-color: rgb(255, 255, 255);"></small></span>
                            </div>
                      </div>
                      
                       <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Imagen del evento</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control"/>
                            </div>
                        </div>                                          

					    <br>
                        <h4 class="text-thin">Fechas del Evento</h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="date_from">Comienzo</label>
                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <input type="text" class="form-control input-sm" id="date_from">
                                    <span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Finalizaci&oacute;n</label>
                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <input type="text" class="form-control input-sm" id="date_to">
                                    <span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="demo-hor-inputemail">Apertura inscripciones</label>
                          <div class="col-sm-9">
                                <div class="input-group date">
                                    <input type="text" class="form-control input-sm" id="date_from_registration">
                                    <span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Finalización inscripciones</label>
                            <div class="col-sm-9">
                                                            <div class="input-group date">
                                    <input type="text" class="form-control input-sm" id="date_to_registration">
                                    <span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
                              </div>
                            </div>
                        </div>                         			
                      
					   <br>                      
                       <h4 class="text-thin">Localizaci&oacute;n del Evento</h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="demo-hor-inputemail">Direcci&oacute;n del evento</label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="Dirección del evento" id="demo-hor-inputemail" class="form-control" value="{!!$evento->address!!}"/>
                            </div>
                        </div>
                        
    					<div class="form-group">
                            <label class="col-sm-3 control-label" for="demo-hor-inputemail">Direcci&oacute;n del evento</label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="latitud" id="demo-hor-inputemail" class="form-control" value="{!!$evento->latitude!!}"/><input type="text" placeholder="latitud" id="demo-hor-inputemail" class="form-control" value="{!!$evento->longitude!!}"/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan"></label>
                            <div class="col-sm-9">
                                 <button type="submit" class="btn btn-info">Guardar</button>
                            </div>
                      </div>
				</form>
				
                </div>
                
                
                <!-- TAB 2 -->
                <div id="demo-lft-tab-2" class="tab-pane fade">
                    <form class="form-horizontal" method="post">
                        <h4 class="text-thin">Comidas</h4>
                        <hr>    
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="demo-hor-inputemail">Tipo de comida</label>
                            <div class="col-sm-9">
                                <select class="form-control">
			                        <option>Incluido</option>										                                    <option>Opcional</option>                                    <option>No Incluido</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Precio por comida</label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="Eslogan" id="input_eslogan" class="form-control">
                            </div>
                        </div>                       
    
                        <h4 class="text-thin">Listado de comidas</h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="date_from"></label>
                            <div class="col-sm-9">
                                <div class="input-group date">
                                    <input type="text" class="form-control input-sm" id="date_from">
                                    <span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Finalización inscripciones</label>
                            <div class="col-sm-9">
                                                            <div class="input-group date">
                                    <input type="text" class="form-control input-sm" id="date_to_registration">
                                    <span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
                              </div>
                            </div>
                        </div>           
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan"></label>
                            <div class="col-sm-9">
                                 <button type="submit" class="btn btn-info">Guardar</button>
                            </div>
                        </div>                                                           
              	</form>
          	</div>

            <!-- TAB 3 -->
            <div id="demo-lft-tab-3" class="tab-pane fade">
                    <form class="form-horizontal" method="post">
                        <h4 class="text-thin">Alojamiento</h4>
                        <hr>    
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="demo-hor-inputemail">Tipo de alojamiento</label>
                            <div class="col-sm-9">
                                <select class="form-control">
									<option>Incluido</option>
									<option>No Incluido</option>                        
                                	<option>Opcional</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Precio por noche</label>
                            <div class="col-sm-9">
                                <input type="text" placeholder="Eslogan" id="input_eslogan" class="form-control">
                            </div>
                        </div>
                       <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Visible</label>
                            <div class="col-sm-9">
                                <span class="switchery switchery-default" style="border-color: rgb(100, 189, 99); box-shadow: rgb(100, 189, 99) 0px 0px 0px 10px inset; transition: border 0.4s, box-shadow 0.4s, background-color 1.2s; -webkit-transition: border 0.4s, box-shadow 0.4s, background-color 1.2s; background-color: rgb(100, 189, 99);"><small style="left: 17px; transition: left 0.2s; -webkit-transition: left 0.2s; background-color: rgb(255, 255, 255);"></small></span>
                            </div>
                      </div>
    
                        <h4 class="text-thin">Listado de alojamientos</h4>
                        <hr>
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan">Pr&oacute;ximamente</label>
                            <div class="col-sm-9"></div>
                        </div> 
                                  
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="input_eslogan"></label>
                            <div class="col-sm-9">
                                 <button type="submit" class="btn btn-info">Guardar</button>
                            </div>
                        </div>                                                           
              	</form>
            </div>

             <!-- TAB 4 -->
            <div id="demo-lft-tab-4" class="tab-pane fade">
                <form class="form-horizontal" method="post">
                    <h4 class="text-thin">Infantil</h4>
                    <hr>  
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="demo-hor-inputemail">Servicio de Guarder&iacute;a</label>
                        <div class="col-sm-9">
                                <span class="switchery switchery-default" style="border-color: rgb(100, 189, 99); box-shadow: rgb(100, 189, 99) 0px 0px 0px 10px inset; transition: border 0.4s, box-shadow 0.4s, background-color 1.2s; -webkit-transition: border 0.4s, box-shadow 0.4s, background-color 1.2s; background-color: rgb(100, 189, 99);"><small style="left: 17px; transition: left 0.2s; -webkit-transition: left 0.2s; background-color: rgb(255, 255, 255);"></small></span>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="input_eslogan">Edad m&aacute;xima</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Eslogan" id="input_eslogan" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="demo-hor-inputemail">Escuelas dominicales</label>
                        <div class="col-sm-9">
                                <span class="switchery switchery-default" style="border-color: rgb(100, 189, 99); box-shadow: rgb(100, 189, 99) 0px 0px 0px 10px inset; transition: border 0.4s, box-shadow 0.4s, background-color 1.2s; -webkit-transition: border 0.4s, box-shadow 0.4s, background-color 1.2s; background-color: rgb(100, 189, 99);"><small style="left: 17px; transition: left 0.2s; -webkit-transition: left 0.2s; background-color: rgb(255, 255, 255);"></small></span>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="input_eslogan">Edad m&aacute;xima</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="Eslogan" id="input_eslogan" class="form-control">
                        </div>
                    </div>                     

                    <h4 class="text-thin">Otros servicios</h4>
                    <hr>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="date_from">Parking</label>
                        <div class="col-sm-9">
                            <div class="input-group date">
                                <input type="text" class="form-control input-sm" id="date_from">
                                <span class="input-group-addon"><i class="fa fa-calendar fa-lg"></i></span>
                            </div>
                        </div>
                    </div>
                    
                               
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="input_eslogan"></label>
                        <div class="col-sm-9">
                             <button type="submit" class="btn btn-info">Guardar</button>
                        </div>
                    </div>
            </form>
          	</div>
          
            <!-- TAB 5 -->
            <div id="demo-lft-tab-5" class="tab-pane fade">
                <form class="form-horizontal" method="post">
                    <h4 class="text-thin">Transporte</h4>
                    <hr>    
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="demo-hor-inputemail">Próximamente</label>
                        <div class="col-sm-9">
                          
                        </div>
                    </div>                                                                                 
              	</form>
          	</div>
        </div>
    </div>
<script>
window.onload = function()
{
	if (window.location.hash)
	{
		
		$(document).ready(function(){

				$('<a data-toggle="tab" href="#demo-lft-tab-2">Comida</a>').trigger('click');
				$('#config_event_options li[data-opt="'+window.location.hash.replace('#','')+'"]').trigger('click');
			
			//$('#config_event_options li a[data-opt="'+window.location.hash+'"]').trigger('click');
		});
	}
}
</script>