@push('styles')
<style>
    .room-place
    {
        font-size:10px;
        padding: 2px !important;
        cursor: pointer;
    }

    .room-place:hover{
        background-color: #c9e3f1;
    }

    .room-name
    {
        vertical-align: middle !important;
        font-size: 16px;
        text-align: center;
        max-width: 60px;
    }

    #assignment_section
    {
        width    : 380px;
        top      : 135px;
        position : fixed;
        right    : 25px;
        padding: 0px 15px 0px 15px !important;
        background-color: #f3f3f2;
    }

    #result_list:focus {
        outline: 0;
    }

    .bed_selected
    {
        border:2px solid #5fa2dd;
        background-color: #c9e3f1;
    }

    .unasigned_place{
        font-style: italic;
        color: red !important;
    }
</style>
@endpush

<div class="panel">        

    <!--Data Table-->
    <!--===================================================-->
    <div class="panel-body">
        <div class="pad-btm form-inline">
            <div class="row" id="row_lodgements">
                <div class="col-sm-9 table-toolbar-left" id="lodgements">
                    Zona: 
                    <select id="select_zone" v-model="zone" @change="load">
                      @foreach($zones as $zone)
                      <option value="{{ $zone->zone }}">{{ $zone->zone }}</option>
                      @endforeach
                    </select>

                    &nbsp; Planta:
                    <select id="select_floor" v-model="floor" @change="load">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                    &nbsp;&nbsp; 
                    &nbsp;&nbsp;

                    <template v-if="room_selected || insc_selected">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        {{-- <img v-if="insc_selected_public" :src="'{{ asset('uploads/users_photos/') }}'+insc_selected_public"/> --}}
                        <button type="button" v-show="insc_selected && insc_selected != ''" @click="_unassign"><i class="fa fa-trash"></i> desasignar cama</button>
                        <button type="button" @click="_deselectAll"><i class="fa fa-remove"></i> deseleccionar </button>
                    </template>

                    Plazas libres: @{{ total_free_beds }} &nbsp; &nbsp;
                    Personas pendientes: @{{ total_pending_beds }}
                    <button type="button" class="btn btn-default" @click="_print" id="btn_print"><i class="fa fa-print"></i></button>


                    <br/>
                    <br/>
                    <div class="row">
                        <div class="col-sm-5">

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <template v-for="room in rooms2">
                                        <tr :class="{'bg-success': room.beds - room.places.length == 0}">
                                            <td>
                                                <div v-if="room.places.length" v-for="place in room.places" class="room-place" @click="_selectAssignedPlace(place.place_id, place.inscription.line_id, place.inscription.public_id)" :id="_getAssignedBedId(place.place_id)">
                                                    <template v-if="place.inscription">
                                                        <template v-if="'{{ $event->type }}' == 'CAMPAMENTO' && place.inscription.event_role != 1">
                                                             <b>
                                                             <span style="background-color:yellow;" v-if="place.inscription.event_role == 2">
                                                                (gorra)
                                                            </span>
                                                            <span style="background-color:#cbf9e4;" v-else-if="place.inscription.event_role == 6">
                                                                (ayudante)
                                                            </span>
                                                            </b>
                                                        </span>
                                                        </template>
                                                        <span :style="{'font-weight' : place.inscription.event_role == 2 ? 'bold':'normal', 'text-transform': place.inscription.event_role == 2 ? 'uppercase' : 'none' }">
                                                        @{{ place.inscription.name }} @{{ place.inscription.surname }}</span>
                                                        (@{{ getAge(place.inscription.birthday)}}, @{{ place.inscription.church.city }})
                                                    </template>
                                                </div>
                                                <template>
                                                    <div v-if="(room.beds - room.places.length) > 0" v-for="n in (room.beds - room.places.length)" class="room-place unasigned_place text-center" :id="_getBedId(room.room_id,n)" @click="_selectPlace(room.room_id,n)"> 
                                                        --- plaza todavía no asignada ---
                                                    </div>
                                                </template>
                                            </td>
                                            <td class="room-name">
                                                <span style="font-weight: bold;">@{{ room.name }}</span>
                                                <br>
                                                <small class="text-muted" style="font-size: 10px;">@{{ room.description }}</small>
                                                <template v-if="_shouldShowLockBtn(room)">
                                                    <button class="btn btn-sm btn-default" @click="_lockRoom(room)" v-if="!_roomIsLocked(room)">
                                                        <i class="fa fa-lock"></i>
                                                    </button>
                                                    <button class="btn btn-sm btn-default" @click="_unlockRoom(room)" v-if="_roomIsLocked(room)">
                                                        <i class="fa fa-unlock"></i>
                                                    </button>

                                                </template>
                                            </td>
                                        </tr>
                                        </template>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-sm-5 col-md-offset-0">

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tbody>
                                        <template v-for="room in rooms1">
                                        <tr :class="{'bg-success': room.beds - room.places.length == 0}">                                        
                                            <td class="room-name">
                                                <span style="font-weight: bold;">@{{ room.name }}</span>
                                                <br>
                                                <small class="text-muted" style="font-size: 10px;">@{{ room.description }}</small>
                                                <template v-if="_shouldShowLockBtn(room)">
                                                    <button class="btn btn-sm btn-default" @click="_lockRoom(room)" v-if="!_roomIsLocked(room)">
                                                        <i class="fa fa-lock"></i>
                                                    </button>
                                                    <button class="btn btn-sm btn-default" @click="_unlockRoom(room)" v-if="_roomIsLocked(room)">
                                                        <i class="fa fa-unlock"></i>
                                                    </button>

                                                </template>
                                            </td>
                                            <td>
                                                <div v-if="room.places.length" v-for="place in room.places" class="room-place" @click="_selectAssignedPlace(place.place_id, place.inscription.line_id, place.inscription.public_id)" :id="_getAssignedBedId(place.place_id)">
                                                    <template v-if="place.inscription">
                                                        <template v-if="'{{ $event->type }}' == 'CAMPAMENTO' && place.inscription.event_role != 1">
                                                             <b>
                                                             <span style="background-color:yellow;" v-if="place.inscription.event_role == 2">
                                                                (gorra)
                                                            </span>
                                                            <span style="background-color:#cbf9e4;" v-else-if="place.inscription.event_role == 6">
                                                                (ayudante)
                                                            </span>
                                                            </b>
                                                        </span>
                                                        </template>
                                                        <span :style="{'font-weight' : place.inscription.event_role == 2 ? 'bold':'normal', 'text-transform': place.inscription.event_role == 2 ? 'uppercase' : 'none' }">
                                                        @{{ place.inscription.name }} @{{ place.inscription.surname }}</span>
                                                        (@{{ getAge(place.inscription.birthday)}}, @{{ place.inscription.church.city }})
                                                    </template>
                                                </div>
                                                <template>
                                                    <div v-if="(room.beds - room.places.length) > 0" v-for="n in (room.beds - room.places.length)" class="room-place unasigned_place text-center" :id="_getBedId(room.room_id,n)" @click="_selectPlace(room.room_id,n)"> 
                                                        --- plaza todavía no asignada ---
                                                    </div>
                                                </template>
                                            </td>
                                        </tr>
                                        </template>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <nav class="col-sm-4" id="assignment_section" style="border:2px solid #CCC;">
                    <div>
                        <div>
                            <h4 class="text-center"> <i class="fa fa-search"></i> Buscador</h4>
                        </div>
                        <div>
                            <input type="text" name="search" input="search" @keyup="_secureSearch" @keyup.down.up="_onKeyDownUp" v-model="search" placeholder="nombre o apellidos" style="width: 200px;">
                            &nbsp;Iglesias 
                            <select @change="_search" v-model="church">
                                <option value=""> -- todas -- </option>
                                @foreach($churchs as $c)
                                <option value="{{ $c->id }}">{{ $c->city }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div>
                            Pagado <select @change="_search" v-model="payment">
                                <option value=""> -- todos -- </option>
                                <option value="true">Si</option>
                                <option value="false">No</option>
                            </select>
                            &nbsp; Edad
                            <select @change="_search" v-model="age">
                                <optgroup label="prioritarios">
                                    <option value="3">Bebés (3 o menos)</option>
                                    <option value="65">Ancianos (65 o más)</option>
                                    <option value="REDUCED_MOBILITY">Movilidad reducida</option>
                                </optgroup>
                                <option value=""> -- todas -- </option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div>

                    <br/>
                            
                            <b>Total encontrados: @{{ list.length }}</b>

                           {{--  <div style="border: 1px solid #CCC; background-color:#f3f3f2; height: 200px; overflow-y: auto; overflow-x: hidden; "> --}}
                            <select :size="list.length > 1 ? 12 : 2" style="width: 100%; height: 100%;" id="result_list" v-model="selected_inscription">
                                <option :disabled="item.lodgement_without_room==1" :value="item" v-for="(item, index) in list" :style="{ borderTop : index > 0 && list[index-1].inscription_id != item.inscription_id ? '2px solid #CCC' : ''}">
                                <template v-if="index== 0 || (index > 0 && list[index-1].inscription_id != item.inscription_id)">
                                    #@{{ item.inscription_id }} -
                                </template>
                               
                                <template v-if="getAge(item.birthday) <=3">
                                    👶
                                </template>
                                <template v-else-if="getAge(item.birthday) >= 65">
                                    🏑
                                </template>
                                <template v-else-if="item.reduced_mobility==1">
                                    ♿
                                </template>
                                <template v-else-if="item.lodgement_without_room">
                                    🛏️❌
                                </template>
                                 <template v-else>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp; - 
                                </template>
                                  @{{ item.surname }}, @{{ item.name }} (@{{ getAge(item.birthday) }}) | @{{ item.church }} </option>
                            </select>

                            <div>

                            <template v-if="selected_inscription && '{{ $event->type }}' == 'CAMPAMENTO'">
                                <div style="width: 75px; height: 75px; margin-top:5px;" class="pull-left">
                                    <a :href="'{{ asset('uploads/users_photos/') }}/'+selected_inscription.public_id+'.jpeg'" target="_blank">
                                        <img :src="'{{ asset('uploads/users_photos/') }}/'+selected_inscription.public_id+'.jpeg'" height="75" width="75" />
                                    </a>
                                </div>
                            </template>

                                <div style="padding: 7px;" class="pull-left" v-if="selected_inscription">

                                    <div>

                                        <div v-if="selected_inscription && '{{ $event->type }}' == 'CAMPAMENTO' && getAge(selected_inscription.birthday) >= 18">
                                            Rol:
                                                <select name="adult_role" v-model="adult_role">
                                                    <option value="1">CAMPISTA</option>
                                                    <option value="2">GORRA</option>
                                                    <option value="6">AYUDANTE DE GORRA</option>
                                                    <option value="4">PROFESOR</option>
                                                    <option value="7">EQUIPO DE TRABAJO</option>
                                                    <option value="3">RESPONSABLE</option>
                                                    <option value="5">PASTOR</option>
                                                </select>
                                            <br/>
                                            <template v-if="adult_role == 6">
                                                <br> Será ayudante de:<br/>
                                                <select name="monitor" v-model="assigned_monitor">
                                                    <option :value="m.line_id" v-for="m in monitors_without_helpers">
                                    @{{ m.surname }}, @{{ m.name }} (@{{ getAge(m.birthday) }}) | @{{ m.church }} </option>
                                                </select>
                                            </template>
                                        </div>
                                        <template v-else>
                                            {{-- <div style="height: 40px;"></div> --}}
                                        </template>

                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <button class="btn btn-success form form-control" style="width: 100%; margin-top:12px;" :disabled="!selected_inscription || !room || room == ''" @click="_asignBed"> Asignar a cama seleccionada <i class="fa fa-bed"></i></button>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>

<script>
var url_insc = '{{route('admin.inscripcion.inicio','-id-')}}';

function showInscription(id)
{
    $('#iframe_modal').attr('src',url_insc.replace('-id-',String(id)));
    $('#inscription_modal').modal('show')
}

var assignment = new Vue({

    el : '#assignment_section',

    data : {
        free_beds            : 0,
        room                 : '',
        search               : '',
        church               : '',
        payment              : '',
        age                  : '',
        list                 : [],
        timeout              : null,
        selected_inscription : null,
        adult_role           : '2',
        monitors_without_helpers   : [],
        assigned_monitor     : null,
    },

    watch : {

        adult_role(newval)
        {
            if (newval && newval == 6)
            {
                axios.get('{{ route('admin.eventos.alojamiento_gorras_without_helpers', $event->id, false)}}')
                    .then(function(result){
                        this.monitors_without_helpers = result.data;
                    }.bind(this));
            }
        }
    },

    methods : {

        getAge : function(birthday)
        {
             return Utils.getAge(birthday, '{{ $event->date_from }}');
        },

        show : function(reset)
        {
            $(this.$el).modal('show');

            if (reset)
                this._reset();
        },

        hide : function()
        {
            $(this.$el).modal('hide');
        },

        _secureSearch : function()
        {       
            this.timeout = setTimeout(function()
            {
                this._search();
            }.bind(this), 500);
        },

        _search : function()
        {
            clearTimeout(this.timeout);

            var params = {
                search  : this.search
            };

            if (this.church && this.church != '')
                params.church = this.church;

            if (this.age && this.age != '')
                params.age = this.age;

            if (this.payment && this.payment != '')
                params.payment = this.payment;

            axios.get('{{ route('admin.eventos.alojamiento_search', $event->id, false)}}', {params : params})
                .then(function(result){
                    this.list = result.data;
                }.bind(this));
        },

        _onKeyDownUp : function(){
            $('#result_list').focus();
        },

        _asignBed : function()
        {
            var params = {
                zone        : lodgements.zone,
                floor       : lodgements.floor,
                room        : this.room,
                inscription : this.selected_inscription.line_id,
                adult_role  : this.adult_role
            };

            if (this.adult_role == '6')
                params.monitor = this.assigned_monitor;

            axios.post('{{ route('admin.eventos.alojamiento_assign', $event->id, false)}}', params)
                .then(function(result)
                {
                    lodgements.assign(result.data.room_id, result.data.inscription);

                    $.each(this.list, function(index,val)
                    {
                        if (val.line_id == this.selected_inscription.line_id)
                        {
                            this.list.splice(index,1);
                            return false;
                        }
                    }.bind(this));

                    this.selected_inscription = null;
                    lodgements._updateTotalStatistics();

                }.bind(this))
                .catch(function(error){
                    alert('ha ocurrido un error al asignar la habitacion: '+ result.error);
                }.bind(this));
        }
    }
});

var lodgements = new Vue({

    el : '#lodgements',

    data : {
        zone          : 'colegio 1',
        floor         : '1',
        massive       : false,
        rooms1        : [],
        rooms2        : [],
        room_selected : null,
        insc_selected : null,
        insc_selected_public : null,
        total_free_beds    : 0,
        total_pending_beds : 0
    },

    created : function()
    {
        this.load();
        this._updateTotalStatistics();
    },

    methods : {

        getAge : function(birthday)
        {
             return Utils.getAge(birthday, '{{ $event->date_from }}');
        },

        _getAssignedBedId : function(place_id)
        {
            return 'place-'+place_id;
        },

        _getBedId : function(id,index)
        {
            return 'bed-'+id+'-'+index;
        },

        load : function()
        {
            var params = {
                zone  : this.zone,
                floor : this.floor
            };

            axios.get('{{ route('admin.eventos.alojamiento_info', $event->id, false)}}', { params : params })
                .then(function(result)
                {
                    this.rooms1 = result.data.rooms1;
                    this.rooms2 = result.data.rooms2;
                }.bind(this));
        },

        _deselectAll : function()
        {
            $('.room-place.bed_selected').removeClass('bed_selected');
            this.room_selected = null;
            this.insc_selected = null;
            this.insc_selected_public = null;
            assignment.room = null;
        },

        _selectPlace : function(room_id, index)
        {
            this._deselectAll();
            var bedid = this._getBedId(room_id, index);
            $('#'+bedid).addClass('bed_selected');
            this.room_selected = room_id;
            assignment.room = room_id;
        },

        _selectAssignedPlace : function(place_id, insc, public_id)
        {
            this._deselectAll();
            var place_id = this._getAssignedBedId(place_id);
            $('#'+place_id).addClass('bed_selected');
            this.insc_selected = insc;
            this.insc_selected_public = public_id;
            assignment.room = null;
        },

        _onCheckMassive : function(e)
        {
            if (!e.currentTarget.checked)
                this.room_selected = [];
        },

        assign : function(room_id, inscription)
        {
            if (!this._updateInRooms(this.rooms1, room_id, inscription))
                this._updateInRooms(this.rooms2, room_id, inscription)
        },

        _updateInRooms : function(rooms, room_id, inscription)
        {
            var found = false;

            $.each(rooms, function(index, room)
            {
                if (room.room_id == room_id)
                {
                    found = true;

                    var new_place = {
                        room_id       : room_id,
                        inscription_id: inscription.line_id,
                        inscription   : inscription
                    }

                    if (!room.hasOwnProperty('places'))
                        room.places = [new_place];
                    else
                        room.places.push(new_place);

                    return false;
                }
            });

            return found;
        },

        _removePlaceRoom : function(rooms, room_id, inscription)
        {
            var found = false;

            $.each(rooms, function(index, room)
            {
                if (room.room_id == room_id)
                {
                    $.each(room.places, function(index2, place)
                    {
                        if (place.inscription.line_id == inscription)
                        {
                            found = true;
                            room.places.splice(index2,1);
                            return false;
                        }
                    }.bind(this));

                    if (found)
                        return false;
                }
            }.bind(this));

            return found;
        },


        _unassign : function()
        {
            var params = {
                inscription : this.insc_selected
            };

            axios.post('{{ route('admin.eventos.alojamiento_unassign', $event->id, false)}}', params)
                .then(function(result)
            {
                if (!this._removePlaceRoom(this.rooms1, result.data.room_id, result.data.inscription_id))
                    this._removePlaceRoom(this.rooms2, result.data.room_id, result.data.inscription_id)

                assignment._search();
                this._deselectAll();
                this._updateTotalStatistics();
            }.bind(this))
            .catch(function(error){
                alert('ha ocurrido un error al desaasignar la plaza: '+ result.error);
            }.bind(this));
        },

        _print : function()
        {
            $('#mainnav-container').hide();
            $('#navbar').hide();
            $('.navbar-content').hide();
            $('#assignment_section').hide();
            $('#btn_print').hide();
            $('hr').hide();
            $('body').css({
                padding : '0px',
                backgroundColor : '#FFFFFF'
            });
            $('body').html($('#lodgements')[0].innerHTML);
            window.print();
        },

        _shouldShowLockBtn : function(room)
        {
            //  if (room.name == '11')
            //       debugger;

            return room.places.length && ((room.beds - room.places.length) > 0 || (room.places.length && room.places.some(function(bed){
                return bed.inscription == null;
            }) && room.places.some(function(bed){
                return bed.inscription != null;
            })));
        },

        _lockRoom : function(room)
        {
            axios.post('{{ route('admin.eventos.alojamiento_lock', $event->id, false)}}', { room_id : room.room_id })
                .then(function(result)
                {
                    var tofill = room.beds - room.places.length;

                    for (var a=0; a<tofill; a++)
                    {
                        room.places.push({
		                    room_id        : room.room_id,
		                    inscription_id : null
                        });
                    }
                    this._updateTotalStatistics();
                }.bind(this))
                .catch(function(result){
                    alert('ha ocurrido un error al bloquear la habitación');
                });
        },

        _unlockRoom : function(room)
        {
            axios.post('{{ route('admin.eventos.alojamiento_unlock', $event->id, false)}}', { room_id : room.room_id })
                .then(function(result)
                {
                    room.places = room.places.filter(function(p){
                        return p.inscription_id != null
                    });
                    this._updateTotalStatistics();
                }.bind(this))
                .catch(function(result){
                    alert('ha ocurrido un error al bloquear la habitación');
                });
        },

        _roomIsLocked : function(room){
            return (room.beds - room.places.length) == 0 && room.places.some(function(bed){
                return bed.inscription == null;
            });
        },

        _updateTotalStatistics : function()
        {
            axios.get('{{ route('admin.eventos.alojamiento_totals', $event->id, false)}}')
                .then(function(result)
                {
                    this.total_free_beds = result.data.total_free_beds;
                    this.total_pending_beds = result.data.total_pending_beds;
                }.bind(this))
                .catch(function(result){
                    console.error('error getting total statistics: '+result.data.description);  
                });
        }
    }
});

var Utils = {

    getAge : function(date1,date2)
    {
        if (typeof date2 == 'undefined')
            var today = new Date();
        else
            var today = new Date(date2);

        var birthDate = new Date(date1);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
            age--;

        return age;
    }
}

$(document).ready(function(){
    assignment._search();
    $('#search').focus();
    setTimeout(function(){
        $('.mainnav-toggle').trigger('click');
    },1);
});

</script>