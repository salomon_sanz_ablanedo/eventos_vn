<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">+ Crear nuevo usuario</h3>
    </div>
    <div class="panel-body">

        <!-- Inline Form  -->
        <!--===================================================-->
        <form class="form-inline" method="post" action="{{route('admin.eventos_post')}}">
            <div class="form-group">
                <input type="text" placeholder="Nombre" id="demo-inline-inputmail" name="name" class="form-control">
                <input type="text" placeholder="Apellidos" id="demo-inline-inputmail" name="surname" class="form-control">
                <input type="text" placeholder="DNI" id="demo-inline-inputmail" name="legal_id" class="form-control">
            </div>
            <div class="form-group">
                <label for="demo-inline-inputpass">Iglesia</label>
                <select class="form-control" name="type" name="church_id">
                	@foreach ($churchs as $church)
                	<option value="{{$church->id}}">{!!$church->name_short!!}</option>
                	@endforeach
                </select>
            </div>
            <button class="btn btn-danger" type="submit">Dar de alta</button>
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        </form>
        <!--===================================================-->
        <!-- End Inline Form  -->

    </div>
</div>

<div class="panel">        

    <!--Data Table-->
    <!--===================================================-->
    <div class="panel-body">
        <div class="pad-btm form-inline">
            <div class="row">
                <div class="col-sm-6 table-toolbar-left">
                	<form method="get" action="{{route('admin.usuarios')}}">
                        <div class="form-group">
                            <div class="input-group mar-btm">
                                <input id="demo-input-search2" type="text" placeholder="Introduce nombre, apellidos, email, DNI o id" class="form-control" autocomplete="off" style="min-width:410px;" name="search">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"> Buscar usuario &nbsp;<i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                    
                    
                </div>
                <div class="col-sm-6 table-toolbar-right">                   	
                    <button class="btn btn-default"><i class="fa fa-print"></i></button>
                    <button class="btn btn-default"><i class="fa fa-file-pdf-o"></i></button>
                    
                </div>
            </div>
        </div>

            <div class="bootstrap-table">
            	<div class="fixed-table-toolbar"></div>
                <div class="fixed-table-container">
                <div class="fixed-table-header">
                	<table></table>
                </div>
				 
                @if (isset($filters) && count($filters) > 0)                
                	@if (count($users) > 0)                        	
                    	Total encontrados: {{count($users)}}
                    @else
                        No se ha encontrado ning&uacute;n resultado con los datos introducidos: <b>&quot;{!!Input::get('search')!!}&quot; </b>                         
                    @endif                 					
                @endif
				
                @if (count($users) > 0)
                <div class="fixed-table-body">
               	  <div class="fixed-table-loading" style="top: 37px; display: none;">Loading, please wait…</div>
                    <table data-toggle="table" data-url="data/bs-table.json" data-sort-name="id" data-page-list="[5, 10, 20]" data-page-size="5" data-pagination="true" data-show-pagination-switch="true" class="table table-hover">
                    <thead>
                        <tr>
                            <th style=""><div class="th-inner sortable">Id </div><div class="fht-cell"></div></th>
                            <th style=""><div class="th-inner sortable">Nombre<span class="order dropup"><span class="caret" style="margin: 10px 5px;"></span></span></div><div class="fht-cell"></div></th>
                            <th style=""><div class="th-inner sortable">Edad<span class="order dropup"><span class="caret" style="margin: 10px 5px;"></span></span></div><div class="fht-cell"></div></th>
                            <th style=""><div class="th-inner sortable">DNI<span class="order dropup"><span class="caret" style="margin: 10px 5px;"></span></span></div><div class="fht-cell"></div></th>                                    
                            <th style=""><div class="th-inner sortable">Iglesia<span class="order dropup"><span class="caret" style="margin: 10px 5px;"></span></span></div><div class="fht-cell"></div></th>
                            <th style=""><div class="th-inner sortable">F. alta</div><div class="fht-cell"></div></th>                                <th style=""><div class="th-inner sortable">F. modif.</div><div class="fht-cell"></div></th>
                        </tr>
                    </thead>
                    <tbody>  
                        @foreach ($users as $user)                       
                        <tr data-index="1">
                            <td style="">{{$user->user_id}}</a><br></td>
                            <td style=""><a href="#!" onclick="showUser('{{$user->user_id}}');" class="btn-link">{{$user->surname}}, {{$user->name}}</a></td>
                            <td style="">{{$user->birthdate}}</td>                                          
                            <td style="">{{$user->legal_id}}</td>
                            <td style="">{{$user->church->name}}</td>
                            <td style=""><span class="text-muted"><i class="fa fa-clock-o"></i> {{$user->created_at}}</span></td>
                            <td style=""><span class="text-muted"><i class="fa fa-clock-o"></i> {{$user->modified_at}}</span></td>
                        </tr>   
                        @endforeach 
                        </tbody>
                   </table>
                 </div>  
            </div>   
            @endif     
        </div>
    </div>
    <!--===================================================-->
    <!--End Data Table-->
</div>

<div id="user_modal" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!--
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          -->
          <div class="modal-body" style="background-color:#e7ebee; padding:0px;">
            <iframe id="iframe_modal" style="width:100%; min-height:600px;" frameBorder="0"></iframe>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="$('#demo-input-search2').focus();">cerrar</button>
          </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

var url_user = '{{route('admin.usuario.inicio','-id-')}}';

function showUser(id)
{
    $('#iframe_modal').attr('src',url_user.replace('-id-',String(id)));
    $('#user_modal').modal('show')
}
</script>