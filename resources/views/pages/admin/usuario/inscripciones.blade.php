<div class="panel">        

        <!--Data Table-->
        <!--===================================================-->
        <div class="panel-body">

                <div class="bootstrap-table">
                    <div class="fixed-table-toolbar"></div>
                    <div class="fixed-table-container">
                    <div class="fixed-table-header">
                        <table></table>
                    </div>
                                        
                    <div class="fixed-table-body">
                      <div class="fixed-table-loading" style="top: 37px; display: none;">Loading, please wait…</div>
                        <table data-toggle="table" data-url="data/bs-table.json" class="table table-hover">
                            <thead>
                                <tr>
                                    <th style=""><div class="th-inner sortable">Evento</div><div class="fht-cell"></div></th>
                                    <th style=""><div class="th-inner sortable">&nbsp;</div><div class="fht-cell"></div></th>
                                    <th style=""><div class="th-inner sortable">Fecha inscripción</div><div class="fht-cell"></div></th>                                                                  
                                </tr>
                            </thead>
                            <tbody>                             
                                @foreach ($inscriptions as $reg)                           
                                <tr data-index="1">
                                    <td style="">
                                        <img src="{{asset('img/eventos/'.$reg->event_id.'/screen.jpg')}}" style="width:64px;" class="img-thumbnail"/>
                                    </td>
                                    <td>
                                        <strong>{{$reg->event->name}}</strong>
                                        <br/>
                                        <a href="{{route('admin.inscripcion.inicio',$reg->registration_id)}}" class="btn-link">
                                            Inscripci&oacute;n #{{$reg->registration_id}}
                                        </a>
                                    </td>
                                    <td style=""><span class="text-muted"><i class="fa fa-clock-o"></i> {{DateUtils::mysqlDateToStandarDate($reg->created_at)}}</span></td>
                                </tr>   
                                @endforeach                  
                                </tbody>
                           </table>
                          
                     </div>  
                </div>        
            </div>
        </div>
        <!--===================================================-->
        <!--End Data Table-->
    
    </div>