<div class="panel">        

    <!--Data Table-->
    <!--===================================================-->
    <div class="panel-body">
        <div id="demo-lft-tab-1" class="tab-pane fade active in">
            <form class="form-horizontal" method="post" action="{{route('admin.usuarios.inicio_post',$user_id)}}" id="form_user">
                <h4 class="text-thin">Datos Generales</h4>
                <hr>                                                           

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="user_name" >Nombre:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="user_name" name="name" value="{{$user->name}}">
                        <label class="control-label"></label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="user_surname" >Apellidos:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="user_surname" name="surname" value="{{$user->surname}}">
                        <label class="control-label"></label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="date_from">Género:</label>
                    <div class="col-sm-9">
                        <input type="radio" value="M" name="gender" {{$user->gender == 'M'?'checked':''}}> Masculino &nbsp;
                        <input type="radio" value="F" name="gender" {{$user->gender == 'F'?'checked':''}}> Femenino
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="date_from">Fecha de Nacimiento:</label>
                    <div class="col-sm-9">
                        <div class="form-inline">
                            <div class="input-group date">
                                <input type="text" name="birthdate" class="form-control input-sm" id="date_birthdate" value="{{DateUtils::mysqlDateToStandarDate($user->birthdate,'d/m/Y')}}" onkeydown="return false;" readonly style="-webkit-touch-callout: none;
                        -webkit-user-select: none;
                        -khtml-user-select: none;
                        -moz-user-select: none;
                        -ms-user-select: none;
                        user-select: none;" data-required data-required-fieldname="Fecha de nacimiento">
                                <span class="input-group-addon"><i class="fa fa-calendar fa-1"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="select_church">Iglesia:</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="select_church" name="church_id">
                            @foreach ($churchs as $church)
                                <option value="{{$church->id}}" {{$church->id == $user->church_id? 'selected':''}}>{{$church->city}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="input_DNI">DNI:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="input_DNI" name="legal_id" value="{{$user->legal_id}}" data-original-value="{{$user->legal_id}}" {{$user->fake_legal_id == '1'?'disabled':''}}>
                    </div>
                    <label class="col-sm-3 control-label" for="input_DNI">&nbsp;</label>
                    <div class="col-sm-9">
                        <div class="form-inline">
                            <label class="checkbox-inline">
                                <input type="checkbox" class="form-control" name="no_dni" id="checkbox_fake_dni" value="1" {{$user->fake_legal_id=='1'?'checked':''}}>
                                Marca esta casilla si el usuario no dispone de NIF/NIE (menores, indocumentados, etc)
                            </label>
                        </div>
                    </div>
                </div>                        

                <br>
                <h4 class="text-thin">Datos de contacto</h4>
                <hr>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="input_email">Email:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" id="input_email" value="{{$user->email}}" name="email">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="input_phone1">Teléfono 1:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" id="input_phone1" value="{{$user->phone1}}" name="phone1">
                    </div>
                </div>

                 <div class="form-group">
                    <label class="col-sm-3 control-label" for="input_phone2">Teléfono 2:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" id="input_phone2" value="{{$user->phone2}}" name="phone2">
                    </div>
                </div>

                <br/>
                <h4 class="text-thin">Acceso al admin</h4>
                <hr>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="select_user_type">Tipo de usuario:</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="select_user_type" name="role">
                            <option value="USER" {{$user->role == 'USER'?'selected':''}}>Normal (no acceso al admin)</option>
                            <option value="COORDINATOR" {{$user->role == 'COORDINATOR'?'selected':''}}>Coordinador (acceso a algunas partes del admin)</option>
                            <option value="ADMIN" {{$user->role == 'ADMIN'?'selected':''}}>Administrador (acceso a todo el admin)</option>
                        </select>
                    </div>
                </div>   

                <div id="form_only_admin" {{$user->role=='USER'?'style="display:none;"':''}}>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="input_phone2">Usuario:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="input_user_login" name="login_user" value="{{$user->username}}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="input_phone2">Password:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="input_password_login" name="login_password" value="{{$user->password}}">
                        </div>
                    </div>
                </div>

                <br>
                <h4 class="text-thin">Otros</h4>
                <hr/>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="input_phone2">Fotografía:</label>
                    <div class="col-sm-9">
                        <input type="file" class="form-control input-sm" id="input_foto" name="user_image">
                    </div>
                </div>

                <br/>
                <div class="form-group">
                    <label class="col-sm-3 control-label">&nbsp;</label>
                    <div class="col-sm-9">
                        <button type="button" id="btn_submit" class="btn btn-primary"> <li class="fa fa-save"></li> Guardar</button>
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            </form>
        </div>  
    </div>
    <!--===================================================-->
    <!--End Data Table-->
</div>

<script>

window.onload = function()
{
    $('#date_birthdate').datepicker({
        format: "dd/mm/yyyy",
        startDate: "01/01/1900",
        endDate: "-3 day",
        startView: 2,
        language: "es-ES",
        todayHighlight: true,
        toggleActive: true,
        autoclose: true
    }).on('changeDate', function(){});

    $('#select_user_type').on('change',function(){
        if (this.value == 'USER')
            $('#form_only_admin').hide();
        else
            $('#form_only_admin').show();
    });

    $('#checkbox_fake_dni').on('change',function()
    {
        var orig_value = $('#input_DNI').data('original-value');

        if (this.checked)
        {
            $('#input_DNI').val('--')
                           .attr('disabled',true);
        }
        else
        {
            $('#input_DNI').val(orig_value)
                           .attr('disabled',false);
        }
    });

    $('#select_user_type').trigger('change');
    $('#checkbox_fake_dni').trigger('change');

    $('#btn_submit').on('click',function(){

        var errors = [];

        if ($.trim($('#user_name').val()) == '')
            errors.push('Nombre');

        if ($.trim($('#user_surname').val()) == '')
            errors.push('Apellidos');

        if ($.trim($('#date_birthdate').val()) == '')
            errors.push('Fecha de nacimiento');

        if ($('#checkbox_fake_dni')[0].checked == false)
        {
            // validate both: NIF and NIE
            if (/(([X-Z]{1})([-]?)(\d{7})([-]?)([A-Z]{1}))|((\d{8})([-]?)([A-Z]{1}))/.test($('#input_DNI').val()) === false)
                errors.push('NIF/NIE (formato inválido)');
        }

        if ($.trim($('#input_email').val()) != '' && 
            /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test($.trim($('#input_email').val())) === false
            )
            errors.push('Email (formato inválido)');

        if (errors.length > 0)
            alert('Los siguientes campos están vacíos o no son válidos: \n\n' + ' - ' + errors.join('\n-'));
        else
            $('#form_user').submit();

    });
};
</script>