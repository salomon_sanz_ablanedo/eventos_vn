
<br>

<div class="panel">        
    <div class="panel-body">
        <ul>
        	<li><a href="{{ $url_print_insc }}" target="_blank">Imprimir justificante de inscripción</a></li>
        	<hr/>
        	<li>
                <a href="#" onclick="togglePulseras();">Imprimir Pulseras</a>
                @if (!empty($kids))
                    <form target="_blank" method="POST" action="{{ $route_form_post }}" style="display:block;" class="form_pulseras">
                        <select name="inscription_id[]" multiple>
                            <optgroup label="tamaño infantil"></optgroup>
            			    @foreach($kids as $kid)
            			        <option value="{{ $kid->line_id }}">{{ $kid->name }} {{ $kid->surname }}</option>
            			    @endforeach
    	        		</select>
    	        		<input type="hidden" name="FUNCTION" value="print_bracelet">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
    	        		<input type="hidden" name="size" value="kids">
    	        		<button type="submit"> Imprimir</button>
    	        	</form>
                @endif

                @if (!empty($adults))
                    <form target="_blank" method="POST" action="{{ $route_form_post }}" style="display:block;" class="form_pulseras">
                        <select name="inscription_id[]" multiple>
                            <optgroup label="tamaño adulto"></optgroup>
                            @foreach($adults as $adult)
                                <option value="{{ $adult->line_id }}">{{ $adult->name }} {{ $adult->surname }}</option>
                            @endforeach
                        </select>
                        <input type="hidden" name="FUNCTION" value="print_bracelet">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="size" value="adults">
                        <button type="submit"> Imprimir</button>
                    </form>
                @endif
        	</li>
        	<li>
                <a href="#" onclick="toggleIdentificadores();">Imprimir Etiquetas Identificación</a>
                <form target="_blank" method="POST" action="{{ $route_form_post }}" style="display:none;" id="form_identificadores">

                    <select name="inscription_id[]" multiple>
                        @foreach($inscription->lines as $insc)
                        <option value="{{ $insc->line_id }}">{{ $insc->name }} {{ $insc->surname }}</option>
                        @endforeach
                    </select>

                    <input type="hidden" name="FUNCTION" value="print_identificator">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit"> Imprimir</button>
                </form>
            </li>
            <hr/>
            <li>
                <a href="javascript:void(0);" id="a_delete_inscription" class="text-danger"><i class="fa fa-remove"></i> Eliminar completamente toda la inscripción</a>
            </li>
        </ul>
    </div>
</div>

@push('scripts')

<script>

$('#a_delete_inscription').on('click', function(){

    if (confirm('Atención, ¿Seguro que quieres borrar la inscripción?\n\n Se borrarán absolútamente TODOS los datos: las personas, comidas, datos de pagos, etc'))
    {
        var url = '{{ route('api.inscription.delete') }}?inscription_id={{ $inscription->inscription_id }}';

        axios.delete(url)
            .then(function(){
                alert('Se ha borrado correctamente toda la inscripción');
                parent.location.reload();
            }.bind(this))
            .catch(function(){
                alert('ha ocurrido un error al intentar borrar la inscripción');
            }.bind(this));
    }
});

    function togglePulseras(){
        $('.form_pulseras').toggle();
    }

    function toggleIdentificadores(){
        $('#form_identificadores').toggle();
    }
</script>

@endpush