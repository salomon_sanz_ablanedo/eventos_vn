@php
$total = 1;

if ($payment_balance > 0)
{
    $class = 'btn-danger';
    $text_balance = $payment_balance.'&euro;';
}
else if ($payment_balance <0)
{
    $class = 'btn-warning';
    $text_balance = abs($payment_balance).'&euro; a favor';
}
else
{
    $class = 'btn-info';
    $text_balance = 'OK!';
}
@endphp

<br> 
<b>Contacto: </b>{{ $inscription->contact_name }} | {{ $inscription->contact_phone }} | {{ $inscription->contact_email }}
<br> 
<a class="text-info text-bold pull-right" href="{{ route('admin.inscripcion.modify', $inscription->inscription_id) }}"> <i class="fa fa-user-plus"></i> Añadir una persona a la inscripción</a>
<h4>Resumen de Servicios</h4>

<div class="panel">        
    <div class="panel-body">   

        @if (!empty($welcome_kit_user))
            <div class="pull-right"> Kit recogido por <strong>{{ $welcome_kit_user->name .' '.$welcome_kit_user->surname}}</strong> &nbsp; <button type="button" class="btn btn-default btn-sm btn_kit_welcome" data-user-id="-1">deshacer</button>
            </div>
        @endif

            <div class="pull-left">

            <button class="btn btn-lg {!! $class !!} text-bold btn-inverse"> <i class="fa fa-lg fa-dollar"></i> {!! $text_balance !!}</button>
            
            <button class="btn btn-lg btn-info text-bold btn-inverse"> <i class="fa fa-lg fa-users"></i> x{!! count($inscription->lines) !!}</button>

            @if ($total_parkings > 0)
                <button class="btn btn-lg btn-info text-bold"> <i class="fa fa-lg fa-car"></i> x{!! $total_parkings !!}</button>
            @endif

            @if ($total_nursery > 0)
                <button class="btn btn-lg btn-info text-bold"> <i class="fa fa-lg fa-child"></i> x{!! $total_nursery !!}</button>

                @if ($total_petos > 0)
                    <button class="btn btn-lg btn-info text-bold"></i> x{!! $total_petos !!} petos comprados</button>
                @endif

                @if ($total_nursery - $total_petos > 0)
                    <button class="btn btn-lg btn-warning text-bold"></i> x{!! $total_petos !!} petos sin comprar</button> 
                @endif
            @endif

            @if ($total_meals > 0)
                <button class="btn btn-lg btn-info text-bold"> <i class="fa fa-lg fa-cutlery"></i> x{!! $total_meals !!}</button>
            @endif

            @if ($total_nights > 0)
                <button class="btn btn-lg btn-info text-bold"> <i class="fa fa-lg fa-moon-o"></i> x{!! $total_nights !!}</button>
            @endif

            </div>
            <div style="clear:both;"></div>
    </div>
</div>

@foreach ($inscription->lines as $insc)
    <h4>
        {{ $insc->name }} {{ $insc->surname }}, {{ DateUtils::getAge($insc->birthday) }} años 
        <small class="text-muted">({{ DateUtils::getYearFromDate($insc->birthday) }}, {{ $insc->public_id }}) </small>

        {{-- <a href="{{ route('admin.usuario.inicio',$insc->user_id) }}" class="btn-link"><small>+ Info del usuario</small></a> --}}

        @if ($insc->event_role != 'USUARIO')
            <span class="label label-info"><strong>{{ $insc->event_role }}</strong></span>
        @endif

        @if (empty($welcome_kit_user) && DateUtils::getYearsDiff($insc->birthday,$inscription->event->date_from) >= 18)
            <button type="button" class="btn btn-primary btn-sm btn_kit_welcome" data-user-id="{{ $insc->line_id }}">
                <strong>¿Recoge Kit Bienvenida?</strong>
            </button>
        @endif

        <div class="pull-right">

            <a href="{{ route('admin.inscripcion.modify', $insc->inscription_id) }}?line_id={{ $insc->line_id }}" class="btn btn-danger btn-sm" data-user-id="{{ $insc->line_id }}">
                <i class="fa fa-lg fa-edit"></i> <strong>Modificar</strong>
            </a>

            <button type="button" class="btn btn-danger btn-sm btn_move_inscription" data-user-id="{{ $insc->line_id }}">
                <i class="fa fa-lg fa-share-square-o"></i> <strong>Mover</strong>
            </button>

            <button type="button" class="btn btn-danger btn-sm btn_delete_inscription" data-user-id="{{ $insc->line_id }}">
                <i class="fa fa-lg fa-trash-o"></i> <strong>Borrar</strong>
            </button>

        </div>
    </h4>
    
    
<div class="panel">   
    @if ($inscription->event->require_photo)
    <div style="float: left; position: absolute; margin:12px;">
        <img src="{{ asset('uploads/users_photos/'.$insc->public_id.'.jpeg') }}?{{ rand() }}" class="img-thumbnail" />
    </div>    
    @endif

    <div class="panel-body" style="border:2px dashed #CCC; @if ($inscription->event->require_photo) padding-left: 142px; min-height: 200px; @endif">        
        <div class="form-group">
            <label class="col-sm-3 control-label text-right" for="input_eslogan"><strong>Iglesia</strong></label>
            <label class="col-sm-9 control-label text-left">
                {{ $insc->church->city }}
            </label>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label text-right" for="input_eslogan"><strong>Fecha Insc.</strong></label>
            <label class="col-sm-9 control-label text-left">
                {{DateUtils::mysqlDateToStandarDate($insc->created_at)}} 
                @if (empty($insc->created_at) || $insc->created_at != $insc->updated_at)
                (Última modificación el {{DateUtils::mysqlDateToStandarDate($insc->updated_at)}})
                @endif
            </label>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label text-right" for="input_eslogan"><strong>Importe</strong></label>
            <label class="col-sm-9 control-label text-left">
                {{$insc->payment_amount}}&euro;
            </label>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label text-right"><strong>Alojamiento</strong></label>
            <label class="col-sm-9 control-label text-left">
                @if (!empty($insc->lodgement))
                    @php
                        $insc->lodgement_dates = $insc->lodgement_dates;   
                    @endphp

                    @if (!in_array($inscription->event->type,['CAMPAMENTO','PRECAMPAMENTO']))
                    x{{ count($insc->lodgement_dates) }} noche/s en {{ $insc->lodgement->name }}
                    &nbsp; 
                    @endif

                    @if ($insc->lodgement->external == 0)
                        @if (empty($insc->lodgement_room))
                            (habit. no asignada)
                        @else
                            (habitación <b>{{ $insc->lodgement_room }}</b>)
                        @endif
                    @endif

                    @if (!in_array($inscription->event->type,['CAMPAMENTO','PRECAMPAMENTO']))
                        <a href="javascript:void(0);" class="btn-link" onclick="$('#lodgement_detail_{{$insc->inscription_id}}').toggle();"><small>+ ver noches</small></a>

                        <small class="text-mutted" style="display:none;" id="lodgement_detail_{{ $insc->inscription_id }}">
                        <br>

                        @if (!empty($insc->lodgement_dates))
                        
                        @foreach($insc->lodgement_dates as $ln)
                            @php
                            $day = $ln->date; 
                            @endphp
                            {{ DateUtils::getDayNameByDate($day) }} {{DateUtils::getMonthDayFromDate($day)}}, 
                        @endforeach
                        @endif
                        </small>
                    @endif
                    &nbsp; 
                @else
                    No
                @endif
            </label>
        </div>

        @if (!in_array($inscription->event->type,['CAMPAMENTO','PRECAMPAMENTO']))
        <div class="form-group">
            <label class="col-sm-3 control-label text-right"><strong>Comidas</strong></label>
            <label class="col-sm-9 control-label text-left">
                @if (count($insc->meals_list) > 0)
                    x{{count($insc->meals_list)}}
                    &nbsp; 
                    <a href="javascript:void(0);" class="btn-link" onclick="$('#meals_detail_{{$insc->line_id}}').toggle();"><small>+ mostrar</small></a>
                    &nbsp;&nbsp;&nbsp;<a href="{{ route('admin.inscripcion.meals_info', [$insc->public_id]) }}" class="btn-link"><small>+ ver marcajes</small></a>

                    <small class="text-mutted" style="display:none;" id="meals_detail_{{$insc->line_id}}">
                    <br>
                    @foreach($insc->meals_list as $ms)
                        <?php
                        $day = $ms->meal->date; 
                        ?>
                        {{substr($ms->meal->type,2)}} del {{DateUtils::getDayNameByDate($day)}} {{DateUtils::getMonthDayFromDate($day)}}, 
                    @endforeach
                    </small>
                    &nbsp; 
                @else
                    No
                @endif
            </label>
        </div>
        @endif

        <div class="form-group">
            <label class="col-sm-3 control-label text-right"><strong>Comidas especiales</strong></label>
            <label class="col-sm-9 control-label text-left">
                @if ($insc->specials_meals)
                    SI &nbsp; <a href="javascript:void(0);" class="btn-link" onclick="$('#meal_obs_{{$insc->inscription_id}}').toggle();"><small>+ mostrar observaciones</small></a><br>
                    <small class="text-mutted" style="display:none;" id="meal_obs_{{$insc->inscription_id}}">{{$insc->specials_meals_obs}}</small>
                @else
                    NO
                @endif
            </label>
        </div>

        @if (in_array($inscription->event->type,['CAMPAMENTO','PRECAMPAMENTO']) && DateUtils::getYearsDiff($insc->birthday, $inscription->event->date_from) < 18)
            <div class="form-group">
                <label class="col-sm-3 control-label text-right"><strong>Observ. esp.</strong></label>
                <label class="col-sm-9 control-label text-left">
                    @if ($insc->specials_details)
                        SI &nbsp; <a href="javascript:void(0);" class="btn-link" onclick="$('#special_obs{{$insc->inscription_id}}').toggle();"><small>+ mostrar observaciones</small></a><br>
                        <small class="text-mutted" style="display:none;" id="special_obs{{$insc->inscription_id}}">{{$insc->specials_details_obs}}</small>
                    @else
                        NO
                    @endif
                </label>
            </div>

            @if (!empty($insc->monitor))
             <div class="form-group">
                <label class="col-sm-3 control-label text-right"><strong>Monitor</strong></label>
                <label class="col-sm-9 control-label text-left">
                   {{ $insc->_monitor->name . ' ' . $insc->_monitor->surname }}
                </label>
            </div>
            @endif
        @endif

        @if ($inscription->event->cost_parking && DateUtils::getYearsDiff($insc->birthday, $inscription->event->date_from) >= 18)
        <div class="form-group">
            <label class="col-sm-3 control-label text-right"><strong>Parking</strong></label>
            <label class="col-sm-9 control-label text-left">
                @if (empty($insc->parking) && empty($insc->parking_days))
                    NO
                @elseif (!empty($insc->parking))
                    SI (evento completo)
                @else (!empty($insc->parking_days))
                    SI, por días (x{{$insc->parking_days}})
                @endif
            </label>
        </div>
        @endif

        @if (count($inscription->event->studios) > 0 && DateUtils::getYearsDiff($insc->birthday,$inscription->event->date_from) >= 13)
        <div class="form-group">
            <label class="col-sm-3 control-label text-right"><strong>Talleres</strong></label>
            <label class="col-sm-9 control-label text-left">
                @if (empty($insc->studio1) || empty($insc->studio2))
                    ¿?
                @else
                    {{ $insc->studio_1->name }} / {{ $insc->studio_2->name }}
                @endif
            </label>
        </div>
        @endif

        @if (DateUtils::getYearsDiff($insc->birthday,$inscription->event->date_from) < 13 && $inscription->event->nursery)
        <div class="form-group">
            <label class="col-sm-3 control-label text-right"><strong>Guardería</strong></label>
            <label class="col-sm-9 control-label text-left">
                @if (empty($insc->nursery))
                    NO
                @else
                    SI
                @endif
            </label>
        </div>
        @endif
        
        <form class="form-horizontal" method="post" id="formulario" action="{{route('admin.inscripcion.inicio_post',$inscription->inscription_id)}}">
            <input type="hidden" name="user_welcome_kit" id="hidden_user_welcome_kit">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        </form>
    </div>
</div>
@endforeach

@push('scripts')

<script>
window.onload = function()
{
    $('.btn_kit_welcome').on('click',function(){
        var user_id = $(this).data('user-id');
        $('#hidden_user_welcome_kit').val(user_id);
        $('#formulario').submit();
    });
}

$('.btn_delete_inscription').on('click', function()
{
    if (confirm('¿Estas seguro que quieres borrar esta persona de la inscripción? \n\n Una vez borrado no se podrán recuperar los datos'))
    {
        var line_id = $(this).data('userId');
        var url = '{{ route('api.inscription.line','-lineid-')}}'.replace('-lineid-', line_id);

        axios.delete(url)
            .then(function(result)
            {
                alert('OK! :) Se ha borrado correctamente');
                document.location.reload();
            })
            .catch(function(result){
                alert('Ha ocurrido un problema al borrar la inscripción');
            });
        ;
    }
});

$('.btn_move_inscription').on('click', function()
{
    var new_inscription_id = prompt('Por favor, escribe el nº de inscripción al que quieres mover esta persona');

    if (new_inscription_id)
    {
        var line_id = $(this).data('userId');
        var url = '{{ route('api.inscription.move','-newlineid-') }}'.replace('-newlineid-', line_id);

        var params = {
            new_inscription_id : new_inscription_id
        }

        axios.post(url, params)
                .then(function(result)
                {
                    alert('OK! :) Se ha movido correctamente');
                    document.location.reload();
                })
                .catch(function(){
                    alert('Ha ocurrido un problema al mover la inscripción');
                }.bind(this));
    }
});
</script>
@endpush