@extends('layouts.app')

@section('content')
<inscription-modify 
    event_id="{{ $event_id }}" 
    url_inscription_line="{{ route('api.inscription.line', ['']) }}" 
    inscription_id="{{ $inscription->inscription_id }}" 
    line_id="{{ $line_id ?? '' }}" 
    url_back="{{ route('admin.inscripcion.inicio',$inscription->inscription_id) }}"
    logged_user="{{ Auth::user() }}"
></inscription-modify>
@endsection

@push('scripts')

@endpush