@extends('layouts.admin.naked')

@section('naked_content')

    <h2> <li class="fa fa-cutlery"></li> {{ $insc->name . ' ' .$insc->surname}} 
        <a href="{{ route('admin.inscripcion.inicio', $insc->inscription_id) }}" class="btn-link">
            <small class="text-mutted">Ver inscripción</small>
        </a>
    </h2>

    <br>

    <div style="clear:both;"></div>

    @if (Session::has('alert'))
        <div id="page_msg_alert" class="alert alert-{{ Session::get('alert-type','info') }} alert-dismissible" role="alert" style="margin-top:12px; margin-bottom:-8px;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong></strong> {{Session::get('alert')}}
        </div>
        <script>
            setTimeout(function(){
                $("#page_msg_alert").alert('close');
            },3000);
        </script>
    @endif

    <div class="panel">        
        <div class="panel-body">   
            @if (count($insc->meals_list) == 0)
                Ninguna comida contratada en el evento
            @else
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th style=""><div class="th-inner">FECHA </div><div class="fht-cell"></div></th>                            
                            <th style=""><div class="th-inner">FECHA MARCAJE</div><div class="fht-cell"></div></th>                                    
                            <th style=""><div class="th-inner">MARCADO POR</div><div class="fht-cell"></div></th>                                    
                        </tr>
                    </thead>
                    <tbody>        
                        @foreach ($insc->meals_list as $user_meal)                     
                        <tr data-index="1">
                            <td style="">
                                <small class="text-muted">#{{ $user_meal->id }}</small> - 
                                 {{ substr($user_meal->meal->type,2) .' del '. DateUtils::getDayNameByDate($user_meal->meal->date)}} 
                                    @if (!empty($insc->meals_picnic) && !empty($user_meal->meal->picnic_opt))
                                        
                                        <span class="text-danger text-bold">(PICNIC)</span>
                                    @endif
                            </td>
                            <td style="">
                                @if (empty($user_meal->registration_date))
                                    --
                                @else
                                    <span class="text-success text-bold">{{ DateUtils::mysqlDateToStandarDate($user_meal->registration_date) }}</span>
                                @endif
                            </td>
                            <td style="">
                                @if (empty($user_meal->registered_by_user))
                                    --
                                @else
                                    <i>{{ $user_meal->user_registrator->name . ' '. $user_meal->user_registrator->surname}}</i>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

@stop