
<br>

<div class="container-fluid">
    <div class="row">

    	<form method="POST" action="{{route('admin.inscripcion.anotaciones_post',$inscription->inscription_id)}}">
    		<div class="timeline col-xs-11" style="padding-left:0px;">
				<div class="timeline-entry" style="margin-bottom:0px;">
		    		<div class="timeline-stat">
						<div class="timeline-icon bg-info">
							<!--<i class="fa fa-envelope fa-lg"></i>-->
							<img class="img-circle img-user media-object" src="http://eventosvidanueva.com/img/av{{Auth::user()->gender == 'M'?'1':'4'}}.png" alt="Profile Picture">
						</div>
					</div>
					<div class="timeline-label">
						<p style="margin:0px;">
			        		<textarea id="text_message" name="message" placeholder="Escribe aqu&iacute; el texto de la nota" class="form-control" rows="3"></textarea>
						</p>
					</div>
	            	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				</div>
			</div>

    		<div class="col-xs-1" style="padding-top:7px;">
				<button class="btn btn-primary pull-right" type="submit" style="height:84px;">Enviar</button>
	    	</div>
	    </form>
	</div>
</div>

@push('styles')
<style>

.timeline{
	padding-bottom: 0px;
}

.timeline-entry{
	margin-bottom:12px;
}

.timeline-stat{
	margin-bottom:0px;
	padding-bottom:0px;
}

.timeline-icon{
	margin-top:12px;
}

.timeline:before, .timeline:after{
	width:0px !important;
}

.timeline-entry
{
	margin-bottom: 0px !important;
}
</style>
@endpush

<hr/>
<div class="timeline">
					
	<!-- Timeline header -->
	@foreach ($inscription->notes as $note)
	<div class="timeline-entry">
		<div class="timeline-stat">
			<div class="timeline-icon bg-info">
				<!--<i class="fa fa-envelope fa-lg"></i>-->
				<img class="img-circle img-user media-object" src="{{asset('img/av'.($note->user->gender == 'M'?'1':'4').'.png')}}" alt="Profile Picture">
			</div>
			<div class="timeline-time"></div>
		</div>
		<div class="timeline-label">
			por <span class="btn-link text-semibold">{{$note->user->name}}</span> <small class="muted"> el {{DateUtils::mysqlDateToStandarDate($note->created_at,'d/m/y h:m:i')}}</small>
			<p style="margin:10px 0 0 0;">{{$note->message}}</p>
		</div>
	</div>
	@endforeach
</div>