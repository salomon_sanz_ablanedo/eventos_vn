<br>

<?php

if ($total_balance < 0)
{
	$class = 'text-danger';
	$text1 = 'Total: ';
	$text_balance = $total_balance.'&euro;';
	$text2 = ' pendiente de pago';
}
else if ($total_balance >0)
{
	$class = 'text-warning';
	$text1 = 'Total: ';
	$text_balance = $total_balance.'&euro;';
	$text2 = ' a favor del usuario';
}
else
{
	$class = 'text-success';
	$text1 = '<i class="fa fa-lg fa-check"></i> Inscripción pagada';
	$text_balance = '';
	$text2 = '';
}
?>

<div class="panel">        
    <div class="panel-body">

    	<div class="pad-btm form-inline">
            <div class="row">
                <div class="col-sm-5 table-toolbar-left">
                	<h3 class="text-left {{$class}}">{!!$text1!!} <span>{!! $text_balance  !!} <small>{{$text2}}</small></span>
                </div>
                <div class="col-sm-7 table-toolbar-right">                   	
                    <button class="btn btn-default form-control" id="btn_new"><small> <i class="fa fa-lg fa-plus-square-o"></i>&nbsp; Nuevo registro</small></button>
                    <!--<button class="btn btn-default form-control" disabled id="btn_modify"><small> <i class="fa fa-lg fa-pencil-square-o"></i>&nbsp; Modificar registro</small></button>-->
                    <button class="btn btn-default" disabled id="btn_delete"><small> <i class="fa fa-lg fa-trash-o"></i>&nbsp; Borrar registro</small></button>
                    @if ($total_balance < 0)
                		<button class="btn btn-success" id="btn_mark_as_paid"><small> <i class="fa fa-lg fa-check"></i>&nbsp;  Marcar inscripción como pagada</small></button>
                	@endif
                </div>
            </div>
        </div>

        <form method="POST" action="{{route('admin.inscripcion.cargos_post',$inscription->inscription_id)}}" id="form_modify">
	    	<table class="table table-condensed table-hover demo-add-niftyradio">
				<thead>
					<tr>
						<th></th>
						<th>Concepto</th>
						<th class="text-center">Cantidad</th>
						<th class="text-center">Importe</th>
						<th class="text-center"> TOTAL </th>
						<th>Usuario</th>
						<th>Fecha</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr id="row_add_line">
						<td valign="baseline"><small id="new_line_role">Nuevo</small></td>
						<td>
							<div>
								<input type="text" class="form-control" id="charge_concept" name="concept" placeholder="ej: &quot;Pago transferencia&quot;"/></td>
							</div>
						</td>
						<td class="text-center">
							<input type="text" id="charge_quantity" name="quantity" size="1" class="form-control text-center only_number" value="1" data-allow-negatives="true"/>
						</td>
						<td class="text-center">
							<input type="text" id="charge_price" name="price" size="3" class="form-control only_number"/>
						</td>
						<td class="text-center" valign="middle">
							<span id="total_new_reg" class="text-semibold"></span>
						</td>
						<td>
							<select class="selectpicker" name="inscription" id="charge_inscription">
								@if (count($inscription->lines) > 1)
									<option value="">--- Elige inscripción ---</option>
								@endif

								@foreach($inscription->lines as $insc)
									<option 
									@if (count($inscription->lines) == 1)
									 selected 
									@endif
									value="{{ $insc->line_id }}">{{ $insc->name }} {{ $insc->surname }}</option>
								@endforeach
							</select>
						</td>
						<td>
							<input type="hidden" id="charge_id" name="charge_id" size="3"/>
							<input type="hidden" id="hidden_delete" name="delete" value="false" size="3"/>
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<button type="submit" id="btn_submit_form"> Enviar</button>
						</td>
					</tr>

				{{--*/ $index = 0; /*--}}

				@foreach($inscription->lines as $insc)
	        		@foreach($insc->charges as $charge)
					<tr data-index="{{ $loop->index }}">
						<td class="bs-checkbox text-center">
							@if ($charge->manual_charge == '1')
							<label class="form-radio form-icon">
								<input data-index="{{ $loop->index }}" name="btSelectItem" type="radio" class="radio-table" 
								data-charge-id="{{ $charge->id }}" 
								data-charge-concept="{{ $charge->description }}" 
								data-charge-quantity="{{ $charge->quantity }}" 
								data-charge-price="{{ $charge->price }}" 
								data-charge-obs="{{ $charge->obs }}" 
								data-charge-inscription="{{ $inscription->inscription_id }}"
							/>
							</label>
							@else
								<li class="fa fa-lock" alt="no modificable, cargo generado por inscripción" title="no modificable, cargo generado por inscripción"></li>
							@endif
						</td>
						<td>{{ $charge->description }}</td>
						<td class="text-center">{{ $charge->quantity }}</td>
						<td class="text-center">{{ $charge->price }}&euro;</td>
						<td class="text-center">
							<?php
							$total = $charge->price * $charge->quantity;
							?>
							@if ($total > 0)
								<span class="text-danger text-semibold">+{{ $total }}&euro;</span>
							@else
								<span class="text-success text-semibold">{{ $total }}&euro;</span>
							@endif
						</td>
						<td><small>{{ $insc->name }} {{ $insc->surname }}</small></td>
						<td><small>{{ DateUtils::mysqlDateToStandarDate($charge->created_at,'d-m-y') }}</small></td>
					</tr>
					{{--*/ $index++; /*--}}
	        		@endforeach
	        	@endforeach
				</tbody>
			</table>
		</form>
    </div>
</div>

<script type="text/javascript">

window.addEventListener('load', function(){

	$('.radio-table').on('click',function(){
		if (!$(this).parent().hasClass('active'))
			return;
		//alert('llega');
		//$(this).parent().parent().parent().parent().find('tr').removeClass('selected');
		$(this).parent().parent().parent().parent().find('tr').css('background-color','#FFF');
		$(this).parent().parent().parent().css('background-color','#f2f5ca');
		$('#new_line_role').html('modif.');
		$('#btn_delete').prop('disabled',false);
		modifySelected();
	});

	$('#btn_new').on('click',clearSelected);

	$('#btn_delete').on('click',function(){
		if (confirm('Atención! \n\n Vas a proceder a borrar un registro, ¿estás seguro? El borrado de un registro no implica la modificación de una inscripción, por lo que si el registro está relacionado con un servicio del evento asegúrate de modificar también la inscripción'))
		{
			modifySelected();
			$('#hidden_delete').val('true');
			$('#form_modify').submit();
		}
	});

	$('#charge_quantity, #charge_price').on('keyup',updateTotal);

	$("input.only_number").keydown(function (event) {

        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) || 
            (event.keyCode >= 96 && event.keyCode <= 105) || 
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190
            || ($(this).data('allow-negatives') == true && event.keyCode == 189) /* guion del negativo */) {

        } else {
            event.preventDefault();
        }

        if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
            event.preventDefault(); 
        //if a decimal has been added, disable the "."-button

    });

    $('#btn_mark_as_paid').on('click',function(){
    	$(this).prop('disabled',true);
    	$('#charge_concept').val('Pago de inscripción');
    	$('#charge_quantity').val('-1');
    	$('#charge_price').val('{{ abs($total_balance) }}');
    	$('#charge_inscription').val('{{ $inscription->lines[0]->line_id }}');
    	$('#charge_id').val('');
    	$('#hidden_delete').val('');
    	$('#form_modify').submit();
    });
});

function modifySelected()
{
	var $selected = $('.bs-checkbox label.active input');
	$('#new_line_role').html('modif. <br> #'+$selected.data('charge-id'));
	$('#charge_id').val($selected.data('charge-id'));
	$('#charge_concept').val($selected.data('charge-concept'));
	$('#charge_quantity').val($selected.data('charge-quantity'));
	$('#charge_price').val($selected.data('charge-price'));
	$('#charge_inscription').val($selected.data('charge-inscription'));
	$('#charge_inscription').selectpicker('render');
	updateTotal();
	$('#btn_submit_form').text('Enviar');
	//$('#charge_obs').val($selected.data('charge-obs'));
}

function clearSelected()
{
	$('#charge_id').val('');
	$('#charge_concept').val('');
	$('#charge_quantity').val('');
	$('#charge_price').val('');
	$('#charge_inscription').val('');
	//$('#charge_obs').val('');

	$('.bs-checkbox label.active').parent().parent().css('background-color','#FFF');
	$('.bs-checkbox label.active').removeClass('active');
	$('#btn_delete').prop('disabled',true);
	$('#new_line_role').html('Nuevo');
	$('#btn_submit_form').text(' Enviar');
	updateTotal();
}

function updateTotal(){
	var total = $('#charge_quantity').val() * $('#charge_price').val();

	if (isNaN(total)){
		$('#total_new_reg')
			.removeClass('text-success text-danger')
			.html('--');
	}
	else if (total > 0)
	{
		$('#total_new_reg')
			.removeClass('text-success')
			.addClass('text-danger')
			.html('+'+total+'€');
	}
	else
	{
		$('#total_new_reg')
			.removeClass('text-danger')
			.addClass('text-success')
			.html(total+'€');
	}
}

</script>