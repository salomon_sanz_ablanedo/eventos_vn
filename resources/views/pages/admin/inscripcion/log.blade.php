
<br>

<div class="panel">        
    <div class="panel-body">
        @if (count($logs) == 0)
                Ningun registro
            @else
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th style=""><div class="th-inner"> Fecha </div><div class="fht-cell"></div></th>                                    
                            <th style=""><div class="th-inner"> Acción</div><div class="fht-cell"></div></th>                            
                            <th style=""><div class="th-inner"> Usuario</div><div class="fht-cell"></div></th>                                                                        
                        </tr>
                    </thead>
                    <tbody>        
                        @foreach ($logs as $log)                     
                        <tr data-index="1">
                        	 <td style="">
                             	<i>{{ DateUtils::mysqlDateToStandarDate($log->created_at) }}</i>
                            </td>

                            <td style="">
                               {{ $log->text }}
                            </td>
          
                            <td style="">
                               	@if (!empty($log->by_user_id))
                            		<small> por {{ $log->user->name .' '.$log->user->surname }}</small>
                            	@endif
                            </td>
                           
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
    </div>
</div>