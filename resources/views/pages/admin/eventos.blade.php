@extends('layouts.admin.base')
@section('content')		
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title" style="display:inline;">+ Crear nuevo evento</h3>  <!-- Inline Form  -->
        <!--===================================================-->
        <form class="form-inline" style="display:inline;" method="post" action="{{route('admin.eventos_post')}}">
            <div class="form-group">
                <input type="text" placeholder="Nombre del evento" id="demo-inline-inputmail" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="demo-inline-inputpass">Tipo</label>
                <select class="form-control" name="type">
                    <option value="CAMPAMENTO">CAMPAMENTO</option>
                    <option value="CONGRESO">CONGRESO</option>                    
                    <option value="RETIRO">RETIRO</option>  
                    <option value="OTRO">OTRO</option>                            
                </select>
            </div>           
            <button class="btn btn-primary" type="submit">Crear</button>
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        </form>
        <!--===================================================-->
        <!-- End Inline Form  -->
    </div>
    {{--<div class="panel-body">

    </div>--}}
</div>
<div class="row">	
@foreach ($events as $event)
	<div class="col-sm-6 col-lg-4" style="min-height:400px;">
        <a href="{{route('admin.eventos.inicio',$event->id)}}"><img src="{{asset('img/eventos/'.$event->id.'/screen.jpg')}}" style="width:100%"/></a>
        <!--Sparkline Area Chart-->
        <div class="panel panel-success panel-colorful text-center">
            <!--
            <div class="panel-body">

                <div id="demo-sparkline-area">
                	
                </div>

            </div>
            -->
            <div class="bg-light pad-ver">
                <h4 class="text-thin"><a href="{{route('admin.eventos.inicio',$event->id)}}" class="btn-link">{{$event->name}}</a></h4> 
                <blockquote style="font-style:italic; font-size:12px;">"{!!nl2br($event->slogan)!!} "</blockquote>
                <hr>
                del <b>{{DateUtils::mysqlDateToStandarDate($event->date_from)}}</b> al <b>{{DateUtils::mysqlDateToStandarDate($event->date_to)}}</b> <br>
                <br>
                 @if ($event->visible == '1')
                	<span class="label label-success pull-right">VISIBLE</span>
                @else
                	<span class="label label-danger pull-right">NO VISIBLE</span>
                @endif
            </div>
        </div>
    </div>
@endforeach
</div>
@stop