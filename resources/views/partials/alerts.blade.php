<div id="v-alerts" v-if="alerts.length">
    <div v-for="alert in alerts" class="alert alert-dismissible fade show col" :class="'alert-' +alert.type" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        @{{ alert.content }}                     
    </div>
</div>

@push('scripts')

<script>
var V_Alerts = new Vue({

    el: '#v-alerts',

    data: {

        types: ['danger', 'warning', 'success', 'info'],

        alerts: [
            // for testing                  
            /*
            { type : 'danger', content : 'asdf asdf asdf asdf'},
            { type : 'warning', content : 'asdf asdf asdf asdf'},
            { type : 'success', content : 'asdf asdf asdf asdf'},
            { type : 'info', content : 'asdf asdf asdf asdf'},                  
            */
        ]
    },

    methods: {

        add: function (type, content, opts, autoclose) {
            this.alerts.push({
                type: type,
                content: content,
                opts: opts
            });

            if (autoclose) {
                setTimeout(function () {
                    $.each(this.alerts, function (index, alert) {
                        if (alert.type == type && alert.content == content) {
                            this.alerts.splice(index, 1);
                            return false;
                        }
                    }.bind(this));
                }.bind(this), autoclose);
            }
        }
    }
});
</script>
@endpush