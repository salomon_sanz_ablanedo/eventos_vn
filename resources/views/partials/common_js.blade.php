<script>

window.onload = function()
{
    setTimeout(function(){
        @if (session('error'))
        V_Alerts && V_Alerts.add('danger','{{ session('error') }}', {}, 5000);
        @endif
    }.bind(this), 1000); 

    setTimeout(function(){
        var input = document.querySelector('#refreshbrowser');
        if (!input)
            return;
        if (input.value == 'yes') {
            window.onbeforeunload = null;
            window.location.href = '/';
        }
        else
            input.value = 'yes';
    }, 100);
}
    
</script>