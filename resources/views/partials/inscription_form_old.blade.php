<style>

#main_form h2
{
    padding-bottom: 0px !important;
}
</style>

<form class="form-horizontal" id="inscription_form" autocomplete="off">

        <h2 class="page-header"><li class="fa fa-food"></li>Datos personales</h2>
        <hr/>

        <!-- Nombre -->
        <div class="form-group">
            <label for="input_nombre" class="col-sm-2 control-label">Nombre</label>
            <div class="col-sm-10">
                <div class="form-inline" id="div_DNI">
                    <input type="text" class="form-control" id="input_nombre" placeholder="Nombre" name="nombre" data-required data-required-fieldname="Nombre" v-model="name">
                     <input type="text" class="form-control" id="input_apellidos" placeholder="Apellidos" name="apellidos" data-required data-required-fieldname="Apellidos" v-model="surname">
                </div>
            </div>

        </div>

        <!-- Género -->
        <div class="form-group">
            <label for="input_genero" class="col-sm-2 control-label">Género</label>
            <div class="col-sm-10">
                <label class="radio-inline">
                    <input type="radio" id="input_genero" name="genero" value="M" v-model="genre" default_checked> Masculino &nbsp;&nbsp;
                </label>
                <label class="radio-inline">
                    <input type="radio" id="input_genero" name="genero" value="F" v-model="genre"> Femenino
                </label>
            </div>
        </div>

      <!-- fecha de nacimiento -->
        <div class="form-group">
            <label for="input_fecha_nac" class="col-sm-2 control-label">Fecha de nacimiento</label>
            <div class="col-sm-10">
                <div class="form-inline">
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar fa-1"></i></span>
                        <input type="text" name="fec_nacimiento" class="form-control input-sm" id="date_birthday" onkeydown="return false;" readonly style="-webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;" data-required data-required-fieldname="Fecha de nacimiento" v-model="birthday">
                    </div>
                </div>
            </div>
        </div>

      <!-- DNI -->
        <div class="form-group">
            <label for="input_DNI" class="col-sm-2 control-label">NIF/NIE</label>
            <div class="col-sm-10">
                <div class="form-inline" id="div_DNI">
                    <input type="text" class="form-control" id="input_DNI" name="DNI" placeholder="aqui tu NIF/NIE" data-placeholder="aqui tu NIF/NIE" v-model="nif1" data-disable-copypaste>
                    <input type="text" class="form-control" placeholder="repite aquí tu NIF/NIE" name="DNI_rep" data-placeholder="repite aqui tu NIF/NIE" v-model="nif2" data-disable-copypaste>
              </div>
            </div>
            <div class="col-sm-offset-2 col-sm-10">
                <label class="checkbox-inline"><input type="checkbox" name="no_dni" id="input_no_DNI" value="true" v-model="noNif"> Si no tienes NIF/NIE, marque esta casilla (menores sin DNI, extranjeros no regularizados, etc)</label>
            </div>
        </div>

      <!-- TELÉFONO 1-->
      {{-- 
        <div class="form-group" id="row_phone1">
            <label for="input_telefono1" class="col-sm-2 control-label">Teléfono 1</label>
            <div class="col-sm-10">
                <div class="form-inline">
                    <input type="text" class="form-control" id="input_telefono1" placeholder="tel&eacute;fono 1" name="telefono1" data-disable-copypaste>
                    <input type="text" class="form-control" id="input_telefono1_rep" placeholder="repite el tel&eacute;fono 1" name="telefono1_rep" data-disable-copypaste>
                </div> 
            </div>
        </div>

        <div class="form-group" id="row_phone2">
            <label for="input_telefono1" class="col-sm-2 control-label">Teléfono 2 <br><small class="text-muted">opcional</small></label>
            <div class="col-sm-10">
                <div class="form-inline">
                    <input type="text" class="form-control" id="input_telefono2" placeholder="tel&eacute;fono 2" name="telefono2" data-disable-copypaste>
                    <input type="text" class="form-control" id="input_telefono2_rep" placeholder="repite el tel&eacute;fono 2" name="telefono2_rep" data-disable-copypaste>
                </div>  
            </div>
        </div>
        --}}

        <!-- EMAIL -->
        {{--
        <div class="form-group" id="row_email">
            <label for="input_email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <div class="form-inline">
                    <input type="text" class="form-control" id="input_email" placeholder="email" name="email" data-disable-copypaste>
                    <input type="text" class="form-control" id="input_email_rep" placeholder="repite tu email" name="email_rep" data-disable-copypaste>
                </div>  
            </div>
        </div> 
        --}}

      <!-- IGLESIA -->
        <div class="form-group">
            <label for="input_fecha_nac" class="col-sm-2 control-label">Iglesia</label>
            <div class="col-sm-10">
                <select name="church" class="selectpicker" data-required data-required-fieldname="Iglesia" v-model="church">
                    <option value=""> ---- Elige tu iglesia ---- </option>
                    @foreach ($churchs as $church)
                    <option value="{{$church->id}}">{{$church->city}}</option>
                    @endforeach
                </select>
            </div>
        </div>

      <!-- ROL EVENTO -->
        
        <div class="form-group" id="row_event_role">

        @if (Input::get('src') == 'admin' && Input::has('rcode')) 

            <label for="input_event_role" class="col-sm-2 control-label">Puesto/Rol</label>
            <div class="col-sm-10">
                <div class="form-inline">
                    <select id="input_event_role" name="event_role" class="selectpicker" data-required data-required-fieldname="Rol" v-model="eventRole">
                    @foreach($roles as $r)
                        <option value="{{$r}}">{{$r}}</option>
                    @endforeach
                    </select>
                </div>
            </div>

        @else

            <label for="input_event_role" class="col-sm-2 control-label">Pastor/Pastora</label>
            <div class="col-sm-10">
                <div class="form-inline">
                    <label class="checkbox-inline">
                        <input type="checkbox" id="input_event_role" name="event_role" value="PASTOR" v-model="isPastor"> 
                    Marca esta casilla si eres Pastor o Pastora
                    </label>
                </div>
            </div>

        @endif
        </div>

      <!-- ACOPLARSE A GRUPO -->
        {{-- @if ($inscription_type == 'individual')
        <div class="form-group">
            <label for="check_acoplate_to_group" class="col-sm-2 control-label">Acoplarme a grupo ya existente</label>
            <div class="col-sm-10">
                <div class="form-inline">
                    <label class="checkbox-inline">
                        <input type="checkbox" id="check_acoplate_to_group" name="acoplate_to_group" value="true">
                   Si deseas unirte un grupo ya inscrito, marca esta casilla
                    </label>
                </div>
                </div>
            <div class="col-sm-offset-2 col-sm-10" style="display:none;" id="row_acoplate_to_group">
                <div class="form-inline">
                Introduce el código de inscripción del grupo al que te quieres unir (es un código alfanumérico de 5 digitos).<br>
                <input type="text" class="form-control" id="input_acoplate_group_id" placeholder="#####" name="acoplate_to_group_id" data-required-fieldname="Código de identificación del grupo al que deseas unirte" style="text-transform: uppercase;">
              </div>
            </div>
        </div>
        @endif --}}

      <!-- RESPONSABLE DE GRUPO -->
{{--         @if ($inscription_type == 'group') --}}
        <div class="form-group" id="row_responsable">
            <label for="input_main_user" class="col-sm-2 control-label">Responsable de Grupo</label>
            <div class="col-sm-10">
                <div class="form-inline">
                    <label class="checkbox-inline">
                      <input type="checkbox" id="input_main_user" name="main_user" value="true" v-model="isGroupLeader"> 
                       Marca esta casilla si deseas que este usuario sea el responsable del grupo. <br>
                       <span id="helpBlock" class="help-block">
                            El responsable del grupo es quien recibirá las notificaciones, y a quien irán vinculado los pagos del grupo.<br/>
                            <small>Sólo puede haber uno por grupo, si ya lo has marcado antes en otra inscripción se quitará de esa y se pondrá en este</small>
                       </span>
                    </label>
                </div>
            </div>
        </div>
        {{-- @endif --}}

      <!-- PARENTESCO CON EL RESPONSABLE DE GRUPO -->
        <div class="form-group" id="div_relation" style="display:'block';">
            <label for="select_relation" class="col-sm-2 control-label">Relación con responsable del grupo</label>
            <div class="col-sm-10">
                <select name="relation" id="select_relation" class="selectpicker" data-required-fieldname="Relación con el responsable del grupo" v-model="liderRelationshipType">
                    <option value=""> ---- Elige uno ----</option>
                    @foreach ($relations as $rel)
                        <option value="{{$rel}}">{{$rel}}</option>
                    @endforeach
                </select>
                Elige qué relación tiene respecto al responsable del grupo (hijo del responsable, padre del responsable, etc)
            </div>
        </div>

        <h2 class="page-header"><li class="fa fa-food"></li>Alojamiento</h2>
        <hr/>


        <!-- ALOJAMIENTO -->
        <div class="form-group">
            <div class="col-sm-12">
                Por favor, especifica donde se va a alojar esta persona (aunque sea una sola noche). <br/>
                Si no te vas a alojar, o te alojas en otro lugar que no aparece en la lista, o esta persona no ocupa plaza de cama (p. ej. un niño que duerme con su madre) elige la primera opción. <br/><br/>
            </div>
            <div class="col-sm-10 col-sm-offset-2">
                <label class="radio-inline">
                    <input type="radio" name="lodgement_place" value="" onchange="if (this.checked){$('#row_alojamiento').hide();}" v-model="lodgement_place"> No me alojo / otros <small class="text-muted">(si no se aloja, o se aloja en otro lugar, o si no va a gastar una plaza de cama (p.e. un niño que duerma con su madre) </small>
                    <small class="text-muted"></small>
                </label>
                @foreach($event->lodgements as $l)
                <div class="form-inline">
                    <label class="radio-inline">
                        <input type="radio" v-model="lodgement_place" name="lodgement_place" value="{{ $l->id }}" onchange="if (this.checked){ if (this.dataset.mainlodgement == 'true'){ $('#external_lodgement_info').hide(); $('#internal_lodgement_info').show(); $('.internal_lodgement_price').show(); }else{ $('#external_lodgement_info').show(); $('#internal_lodgement_info').hide(); $('.internal_lodgement_price').hide();} $('#row_alojamiento').show(); $('#lodgementName').text(this.dataset.lodgementname);}" data-mainlodgement="{{ $l->id == $lodgement_main->id? 'true' : 'false' }}" data-lodgementname="{{ $l->name }}"> {{ $l->name }}
                        @if ($l->id == $lodgement_main->id)
                            <small class="text-muted alert-success">gestionado por el evento</small>
                        @else
                            <small class="text-muted alert-warning">alojamiento externo</small>
                        @endif
                    </label>
                </div>
                @endforeach
            </div>
        </div>

        <div class="form-group" id="row_alojamiento" style="display:none;">
            <label for="" class="col-sm-2 control-label">Noches</label>
            <div class="col-sm-10" id="row_alojamiento">
                <div class="form-inline">
                    ¿Qué noches dormirá esta persona en <span id="lodgementName" style="font-style:italic; font-weight:bold;"></span> ?
                </div>
            </div>
            
            <div class="col-sm-10 col-sm-offset-2">
                <div class="form-inline">

                @foreach ($lodgement_dates as $lday)
                <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox3" name="lodgement_date[]" v-model="lodgement_date" value="{{ $lday['date'] }}">
                    {{ $lday['date_name'] }}
                    <span class="badge internal_lodgement_price">{{ $lodgement_main->nigth_price }}&euro;</span>
                    <br/>
                </label>
              @endforeach
              </div>
            </div>
            
        </div>

        <div class="col-sm-12 alert alert-danger" id="external_lodgement_info" role="alert">
            <span class="fa fa-warning" aria-hidden="true"></span>
            <span class="sr-only">Atención:</span>
Al elegir un alojamiento externo, tanto la reserva como el pago del alojamiento deberá ser gestionado por el propio usuario, la organización del evento no gestiona ningún tipo de reserva ni pagos de alojamientos externos al evento. La información se guarda exclusivamente para datos estadísticos, no tiene ningún tipo de vinculación de reserva ni nada parecido.
        </div>
        <div class="col-sm-11 alert alert-danger" id="internal_lodgement_info" role="alert">
            <span class="fa fa-warning" aria-hidden="true"></span>
            <span class="sr-only">Atención:</span>
Debido a la alta demanda de plazas, a partir del día 12 de marzo (inclusive) las inscripciones que elijan alojamiento en la Pirámide, no se les podrá garantizar la plaza. Se les pondrá en una cola de plazas, y se les irá asignando según orden de inscripción. En el caso de que finalmente no se les pudiera asignar la plaza, se le comunicaría con la suficiente antelación.
        </div>

        <h2 class="page-header"><li class="fa fa-food"></li>Comidas</h2>
        <hr/>


        <!-- COMIDAS -->
        <div class="form-group">
            <label for="" class="col-sm-2 control-label">Comidas</label>
            <div class="col-sm-10">
                <label class="radio-inline">
                    <input type="radio" v-model="meals" name="meals_enabled" value="true" onchange="if (this.checked){$('#row_comidas').show();}"> Si 
                </label>
                <label class="radio-inline">
                    <input type="radio" v-model="meals" name="meals_enabled" value="false" onchange="if (this.checked){$('#row_comidas').hide();}"> No
                </label>
            </div>
        </div>

        <div class="form-group" id="row_comidas" style="display:none;">
        @foreach ($event_meals_by_day as $day=>$daymeals)
            <div class="form-group">
                <label for="input_comidas" class="col-sm-2 control-label">
                    {{ DateUtils::getDayNameByDate($day) }} {{ DateUtils::getMonthDayFromDate($day) }} de
                    {{ DateUtils::getMonthNameByDate($day) }}
                </label>
              
                <div class="col-sm-10">
                @foreach ($daymeals as $meal)
                    @if ($meal->picnic_opt)
                        <label class="radio-inline">
                            {{ substr($meal->type,2) }} 
                            <span class="badge">{{$meal->price}}&euro;</span>
                            <small class="text-muted">2 opciones: </small>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" v-model="meals_list" name="meal_type[]" value="{{$meal->id}}"> 
                            Normal
                        </label>
                        <label class="radio-inline">
                            <input type="radio" v-model="meals_list" name="meal_type[]" value="pic-{{$meal->id}}"> 
                            Picnic <small class="text-muted">para llevar</small>
                        </label>
                    @else
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox3" v-model="meals_list" name="meal_type[]" value="{{$meal->id}}"> 
                            {{ substr($meal->type,2) }} 
                            <span class="badge">{{ $meal->price }}&euro;</span>

                            @if (!empty($meal->meal_obs))
                                <small class="text-muted">({{ $meal->meal_obs }})</small>
                            @endif
                        </label>
                    @endif  
                @endforeach
                </div>
            </div>
        @endforeach
            
            <br>

           <!-- DIETAS ESPECIALES -->
            <div class="form-group">
                <label for="input_comidas_especiales" class="col-sm-2 control-label">Dietas especiales</label>
                <div class="col-sm-10">
                    <label class="checkbox-inline">
                        <input type="checkbox" v-model="meals_specials" id="check_special_meals" name="special_meals" value="true"> Marca esta casilla si esta persona requiere una dieta especial (alergias a alimentos, diabetes, celiacos, vegetarianos, etc).
                    </label>  
                </div>
                <div class="col-sm-offset-2 col-sm-10" style="display:none;" id="div_special_meals_obs">
                    <br>
                    Por favor, especifica un poco más en el siguiente campo de observaciones (máx. 128 caracteres):
                    <br>
                    <textarea v-model="meals_specials_obs" name="special_meals_obs" cols="50" rows="3" maxlength="128" class="form-control" placeholder="Ej: es celiaco, o intolerante a la lactosa, vegetariano, etc..."></textarea>
                </div>
            </div>
        </div>

        <!-- FIN DE COMIDAS -->

        <h2 class="page-header" id="header_other_services">Otras opciones</h2>
        <hr/>


        <!-- TALLERES -->
        @if (count($event->studios) > 1)
        <div class="form-group" id="row_studios">
        <?php
        $counter = 0;
        ?>
            <label for="" class="col-sm-2 control-label">Talleres</label>
            @foreach($event->studios as $studio)
            <div class="{{$counter > 0? 'col-sm-offset-2':''}} col-sm-10">
                <div class="form-inline">
                    <label class="radio-inline">
                        <input type="radio" v-model="studios" name="event_studio" value="{{$studio->id}}"> 
                        {{$studio->name}}
                    </label>
                </div>
            </div>
            <?php
            $counter++;
            ?>
            @endforeach
        </div>
        @endif

        <!-- DEPORTES -->
        @if (count($event->sports) > 1)
        <div class="form-group" id="row_sports">
        <?php
        $counter = 0;
        ?>
            <label for="" class="col-sm-2 control-label">Deportes</label>
            <div class="col-sm-10" id="sports_list">
                    <label class="radio-inline">
                        <input type="radio" v-model="sports" name="event_sport" default_checked value=""> 
                        Ninguno                    
                    </label>
                @foreach($event->sports as $sport)
                    <label class="radio-inline">
                        <input type="radio" v-model="sports" name="event_sport" value="{{$sport->sport_id}}"> 
                        {{$sport->name}}
                    </label>
                @endforeach
            </div>
            <div class="col-sm-10" id="sports_list_advice" style="display:none;">
                <div class="form-inline">
                    <span class="text-muted">Opción no disponible para este usuario (edad mínima 16)</span>
                </div>
            </div>
        </div>
        @endif

      <!-- Guardería -->
      @if (!empty($event->nursery))
      <div class="form-group" id="row_guarderia">
        <label for="input_guarderia" class="col-sm-2 control-label">Guarder&iacute;a</label>
        <div class="col-sm-10">
            <label class="checkbox-inline">
                <input type="checkbox" v-model="nursery" id="input_guarderia" name="guarderia" value="true"/> 
            &nbsp;Marca esta casilla si deseas apuntar a este niño a la guarder&iacute;a
            </label>
        </div>
      </div>
      @endif

      <!-- si hay opción de parking -->
      {{-- 
      @if (is_numeric($event->cost_parking) || is_numeric($event->cost_parking_day))
        <div class="form-group" id="row_parking">
          <label for="input_parking" class="col-sm-2 control-label">Parking</label>

        <?php 
        // Opción sólo de parking completo 
        ?>
        @if (is_numeric($event->cost_parking) && is_numeric($event->cost_parking_day))
          <div class="col-sm-10">
            <div class="form-inline">
              <label class="radio-inline">
                <input type="radio" id="input_parking_complete" name="parking_type" value="none" default_checked onchange="if (this.checked){$('#checks_parking_days_radio').hide();}"> No
              </label>
            </div>
          </div>

          <div class="col-sm-offset-2 col-sm-10" default_checked>
            <div class="form-inline">
              <label class="radio-inline">
                <input type="radio" id="input_parking_complete" name="parking_type" value="complete" onchange="if (this.checked){$('#checks_parking_days_radio').hide();}"> Completo <span class="badge">{{$event->cost_parking}}&euro;</span>
                @if (!empty($event->cost_parking_obs))
                  <small class="text-muted">
                    {{$event->cost_parking_obs}}
                  </small>
                @endif
              </label>
            </div>
          </div>
          <div class="col-sm-offset-2 col-sm-10">
            <div class="form-inline">
              <label class="radio-inline">
                <input type="radio" id="input_parking_by_day" name="parking_type" value="by_day" onchange="if (this.checked){$('#checks_parking_days_radio').show();}"> Por días
                @if (!empty($event->cost_parking_day_obs))
                  <small class="text-muted">
                    {{$event->cost_parking_day_obs}}
                  </small><br>
                @endif
                <?php
                $days = with(new DateTime($event->date_to))->diff(new DateTime($event->date_from))->format("%a");
                ?>
                <div id="checks_parking_days_radio" style="display:none;">
                @for($a=0; $a<=$days; $a++)
                  <label class="checkbox-inline">
                    <?php
                    $date = new DateTime($event->date_from);
                    $date->add(new DateInterval('P'.(string) $a.'D')); // P1D means a period of 1 day
                    ?>
                    <input type="checkbox" name="parking_day[]" value="{{$date->format('Y-m-d')}}">
                    {{DateUtils::getDayNameByDate($date->format('Y-m-d'))}}
                    <?php
                    //$date->add(new DateInterval('P1D')); // P1D means a period of 1 day
                    ?>
                    <span class="badge">{{$event->cost_parking_day}}&euro;</span>
                  </label>
                @endfor
                </div>
              </label>
            </div>
          </div>
        @else
          <div class="col-sm-10">
            <div class="form-inline">
              <label class="checkbox-inline">
                <input type="checkbox" id="input_parking" name="parking" value="true"> 
                &nbsp;(+{{$event->cost_parking}}&euro;) Marca esta casilla si deseas parking
              </label>
            </div>
          </div>
        @endif

        </div>
      @endif --}}

</form>

<script>

var inscription_form = new Vue({

    el : '#inscription_form',

    data : {
        event            : JSON.parse('{!! addslashes(json_encode($event)) !!}'),
        event_meals      : JSON.parse('{!! addslashes(json_encode($event->meals)) !!}'),
        event_lodgements : JSON.parse('{!! addslashes(json_encode($event->lodgements)) !!}'),
        /*****/
        name                  : null,
        surname               : null,
        genre                 : null,
        birthday              : null,
        nif1                  : null,
        nif2                  : null,
        noNif                 : null,
        church                : null,
        eventRole             : null,
        isPastor              : null,
        isGroupLeader         : null,
        liderRelationshipType : null,
        lodgement_place       : null,
        lodgement_date        : [],
        meals                 : null,
        meals_list            : null,
        meals_specials        : null,
        meals_specials_obs    : null,
        studios               : null,
        sports                : null,
        nursery               : null,
    },

    computed : {},

    methods : {

        load : function(id)
        {

        },

        validate : function()
        {

        }
    }
});

</script>


{{-- <script>


var inscription_type = 'group';
var inscriptions_pull = [];
var event_info = JSON.parse('{!! addslashes(json_encode($event)) !!}');
var event_meals = JSON.parse('{!! addslashes(json_encode($event->meals)) !!}');
var event_lodgements = JSON.parse('{!! addslashes(json_encode($event->lodgements)) !!}');
var original_inscription_code; // solo para inscripciones que se modifiquen
var debug = false;

if (inscriptions_data.length == 1)
    inscription_type = 'individual';

var inscriptions = new Vue({

    el : '#inscriptions',

    data : {

    },

    computed : {

    },

    methods : {

    }

});

$(document).ready(function()
{
    $('#date_birthday').datepicker({
        format  : "dd/mm/yyyy",
        startDate: "01/01/1900",
        endDate: "-3 day",
        startView: 2,
        language: "es-ES",
        todayHighlight: true,
        toggleActive: true,
        autoclose: true
    }).on('changeDate', hideFieldsByAge);

    $('.selectpicker').selectpicker();

    $('#main_form').submit(function( event )
    {
        event.preventDefault();
        var inscription = getSerializedFormInscription();

        if (validateInscription(inscription))
        {
          if (inscription_type == 'group')
          {
            addInscriptionToGroup(inscription);
            resetInscriptionForm();
            renderInscriptionsBoxes();
            $('#main_form').hide();
          }
          else
          {
              inscriptions_pull = [];
              inscriptions_pull.push(inscription);
              showResume();
          }
        }
    });

    $('#input_no_DNI').on('change',function()
    {
        if ($(this).prop('checked'))
        {
            $.each($('#div_DNI input'),function(index,input){
              $(this).attr('disabled',true);
              $(this).val('');
              $(this).attr('placeholder','');
            });  
        }
        else
        {
          $.each($('#div_DNI input'),function(index,input){
              $(this).attr('disabled',false);
              $(this).attr('placeholder',$(this).data('placeholder'));
            }); 
        }
    });

    $('#check_acoplate_to_group').on('change',function()
    {
        if ($(this).prop('checked')){
            $('#row_acoplate_to_group').show();
            $('#div_relation').show();
        }
        else
        {
          $('#row_acoplate_to_group').hide();
          $('#div_relation').hide();
        }
    });

  $('#check_special_meals').on('change',function(){
    if ($(this).prop('checked')){
      $('#div_special_meals_obs').show();
    }
    else
      $('#div_special_meals_obs').hide();
  });

  $('textarea[maxlength]').keyup(function(){  
      //get the limit from maxlength attribute  
      var limit = parseInt($(this).attr('maxlength'));  
      //get the current text inside the textarea  
      var text = $(this).val();  
      //count the number of characters in the text  
      var chars = text.length;  

      //check if there are more characters then allowed  
      if(chars > limit){  
          //and if there are use substr to get the text before the limit  
          var new_text = text.substr(0, limit);  

          //and change the current text with the new text  
          $(this).val(new_text);  
      }  
  });

  $('#btn_submit_group').on('click',function(){
    if (inscriptions_pull.length < 2 && inscription_type=='group')
      alert('La incripción en grupo debe tener al menos 2 personas');
    else
    {
      // comprobamos que exista al menos un responsable
      // y que estén definidas todas las relaciones

      var errors = [];
      var main_user = null;
      var undefined_relations = [];

      $.each(inscriptions_pull,function(index,insc)
      {
          if (insc.main_user == 'true')
            main_user = insc;
          else if (insc.relation == '')
            undefined_relations.push(insc.nombre);
      });

      if (main_user == null)
        errors.push('- Debes definir a alguna de las personas inscritas como responsable del grupo. El responsable del grupo debe ser mayor de edad.');
      
      if (undefined_relations.length > 0)
        errors.push('- Los siguientes inscripciones no tienen definidas la relación con el responsable de grupo: \n\n    - '+undefined_relations.join('\n    - '));
      
      if (errors.length > 0)
      {
         alert(errors.join('\n'));
      }
      else
      {
        showResume();
      }
    }
  });

  $('#input_main_user').on('change',function()
  {
      if ($(this).prop('checked'))
      {
          $('#div_relation').hide();
          $('#div_relation select').val('')
                                   .selectpicker('render');
      }
      else
      {
        $('#div_relation').show();
      }
  });

  $('input[type="text"][data-disable-copypaste]').bind('copy', function (e){
      alert('Por una mayor seguridad, se ha desabilitado el copiado en este campo.');
      e.preventDefault();
  });

  $('#btn_register_inscription').on('click',function(){
     registerInscription(); 
  });


  if (inscription_type=='individual')
  {
    $('#main_form').show();
  }
  else
    renderInscriptionsBoxes();
});


function parseInscriptions()
{

  var new_insc;

  /*$.each(inscriptions_data,function(index,insc)
  {
    new_insc = 
    {
       public_id : insc.public_id,
       created_at : insc.created_at,
       insc_local_ref : Utils.getRandomString(), // PENDIENTE generar al vuelo
       nombre : insc.users.name,
       apellidos : insc.users.surname,
       genero : insc.users.gender,
       fec_nacimiento : Utils.mysqlDate2standar(insc.users.birthdate),
       DNI : insc.users.legal_id,
       DNI_rep : insc.users.legal_id,
       no_dni : insc.users.fake_legal_id==1?'true':'false',
       telefono1 : insc.users.phone1,
       telefono1_rep : insc.users.phone1,
       telefono2 : insc.users.phone2,
       telefono2_rep : insc.users.phone2,
       email : insc.users.email,
       email_rep : insc.users.email,
       church : insc.users.church_id, // id
       main_user : insc.parent_inscription == insc.registration_id?'true':'false',
       relation : insc.relation,
       meals_enabled : insc.meals.length > 0 ?'true' : 'false', //2 'true' or 'false',
       special_meals : insc.specials_meals=='1'?'true':'false',
       special_meals_obs : insc.specials_meals_obs, // 'true' or 'false'
       guarderia : insc.guarderia=='1'?'true':'false',
       accept_conditions : 'true',
    };

    @if (Input::get('src') == 'admin')
      new_insc['event_role'] = insc.event_role;
    @else
      new_insc['event_role'] = insc.event_role == 'PASTOR'?'true':'false';
    @endif

    new_insc['meal_type[]'] = [];
    new_insc['parking_day[]'] = [];

    if (insc.meals.length > 0)
    {
      $.each(insc.meals, function(index,m){
        new_insc['meal_type[]'].push(m.event_meal_id);
      });
    }

    if (insc.hasOwnProperty('studio_id') && insc.studio_id != '')
      new_insc.event_studio = insc.studio_id;

    if (insc.hasOwnProperty('lodgement_id') && insc.lodgement_id != '' && insc.lodgement_id != null)
    {
        new_insc.lodgement_place = insc.lodgement_id;
    }

    new_insc['lodgement_date[]'] = [];

    $.each(insc.lodgement_nights, function(index,value){
        new_insc['lodgement_date[]'].push(value.date);
    });




    if (insc.parking == '0' && insc.parking_days == '0')
      new_insc.parking_type = 'none';  // none, complete, by_day
    else if (insc.parking == '1')
      new_insc.parking_type = 'complete';
    else if (insc.parking_days > 0)
    {
      new_insc.parking_type = 'by_day';
      new_insc['parking_day[]'] = insc.parking_days_data.split(',');
    }

    inscriptions_pull.push(new_insc);

    original_inscription_code = '{{Input::get('rcode')}}';
  });
  */
}

function showResume()
{
    $('#group_resume_modal').modal('show');
    updateResume();
}

function updateResume()
{
    var concepts = [];
    var description_html;
    var total = 0;
    var insc_prices;

    $('#table_resume_price > tbody').html('');

    $.each(inscriptions_pull, function(index,insc)
    {
        concepts = [];
        description_html = '';
        subtotal = 0;
        insc_prices = getInscriptionPrice(insc);    

        if (insc_prices.inscription != null)
        {
            concepts.push({
                service_name : 'Inscripc.',
                cant : 1,
                total : insc_prices.inscription
            });
        }

        if (insc_prices.lodgements != null)
        {
            var lodgement_total = 0;

            $.each(insc_prices.lodgements, function(i,val){
                lodgement_total += val;
            });

            concepts.push({
                service_name : 'Alojamiento',
                total : lodgement_total
            });
        }

        if (insc_prices.meals != null)
        {
            var meals_total = 0;

            $.each(insc_prices.meals,function(i,val){
                meals_total += val;
            });

            concepts.push({
                service_name : 'Comidas',
                total : meals_total
            });
        }

        if (insc_prices.parking != null)
        {
            concepts.push({
                service_name : 'Parking',
                total : insc_prices.parking
            });
        }

        $.each(concepts,function(index,c)
        {
            if (description_html != '')
                description_html += ' + ';

            description_html += c.service_name;

            if (c.cant > 1)
                description_html += " (x"+c.cant+") ";

            subtotal += c.total;
        });

        $('#table_resume_price tbody').append('\
          <tr>\
            <td class="text-left">\
              <b>'+insc.nombre +' ' + insc.apellidos+'</b>\
            </td>\
            <td class="text-left">\
              <small class="text-muted">'+description_html+'</small>\
            </td>\
            <td class="text-center">\
              '+String(subtotal)+'&euro;\
            </td>\
          </tr>\
        ');

        total += subtotal;
    });

    $('#total_inscriptions_payment').html(String(total)+'&euro;')
}

function hideFieldsByAge()
{
  if ($('#date_birthday').val() != '')
  {
    var old_age = $('#date_birthday').val().split('/')
    var age = Utils.getAge(old_age[2]+'-'+old_age[1]+'-'+old_age[0],event_info.date_from);

    if (age < 18)
    {
      $('#row_responsable, #row_event_role, #row_parking').hide();
      $('#row_event_role input').prop('checked',false);
      $('#row_event_role select').val('USUARIO');

      $('#input_main_user').prop('checked',false);
      $('#input_parking').prop('checked',false);
    }
    else
      $('#row_responsable, #row_event_role, #row_parking').show();

    if (age < 13)
    {
      $('#row_guarderia').show();
      $('#row_email, #row_phone1, #row_phone2, #row_studios').hide();
    }
    else
    {
      $('#row_guarderia').hide();
      $('#row_email, #row_phone1, #row_phone2, #row_studios').show();
    }

    if (age<16)
    {
        $('#sports_list').hide();
        $('#sports_list_advice').show();
    }
    else
    {
        $('#sports_list').show();
        $('#sports_list_advice').hide();
    }

  }
  else
  {
    $('#row_email, #row_phone1, #row_phone2, #row_responsable, #row_event_role, #row_parking, #row_guarderia').show();
  }
}

function validateInscription(insc)
{
  var errors = [];
  var dni_is_required = insc.no_dni == 'true'? false : true;
  var phone_is_required = false;
  var email_is_required = false;
  var age = 0;

  if (inscription_type == 'individual' || 
      inscription_type == 'group' && insc.main_user == 'true'
  )
  {
    phone_is_required = true;
    email_is_required = true;
  }

  if ($.trim(insc.nombre) == '')
    errors.push('Nombre');

  if ($.trim(insc.apellidos)=='')
    errors.push('Apellidos');

  if ($.trim(insc.fec_nacimiento)=='')
    errors.push('Fecha de nacimiento');
  else
  {
    var birthday = insc.fec_nacimiento.split('/');
    age = Utils.getAge(birthday[2]+'-'+birthday[1]+'-'+birthday[0], event_info.date_from);

    if (inscription_type=='individual' && insc.acoplate_to_group != 'true' && age < 18)
      errors.push('Edad (los menores de edad no se pueden apuntar individualmente)');
  }

  // DNI
  if (dni_is_required)
  {
    if ($.trim(insc.DNI)=='')
      errors.push('NIF/NIE');
    else
    {
      // validate both: NIF and NIE
      if (/(([X-Z]{1})([-]?)(\d{7})([-]?)([A-Z]{1}))|((\d{8})([-]?)([A-Z]{1}))/.test(insc.DNI) === false)
        errors.push('NIF/NIE (formato inválido)');
      else if (insc.DNI != insc.DNI_rep)
        errors.push('NIF/NIE (no coinciden)');
    }
  }

  if (phone_is_required && $.trim(insc.telefono1) == '')
      errors.push('Teléfono 1');

  if ($.trim(insc.telefono1) != '')
  {
    if (String(insc.telefono1).length < 9 || String(insc.telefono1).length > 13)
      errors.push('Teléfono 1 (formato incorrecto)');
    else if (insc.telefono1 != insc.telefono1_rep)
      errors.push('Teléfono 1 (no coinciden)');
  }

  if ($.trim(insc.telefono2) != '')
  {
    if (String(insc.telefono2).length < 9 || String(insc.telefono2).length > 13)
      errors.push('Teléfono 2 (formato incorrecto)');
    else if (insc.telefono2 != insc.telefono2_rep)
      errors.push('Teléfono 2 (no coinciden)');
  }

  if (email_is_required && $.trim(insc.email) == '')
      errors.push('Email');

  if ($.trim(insc.email) != '')
  {
    if (/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(insc.email) === false)
      errors.push('Email (formato incorrecto)');
    else if (insc.email != insc.email_rep)
      errors.push('Email (no coinciden)');
  }

  // IGLESIA
  if (insc.church == '')
    errors.push('Iglesia');

  // ACOPLE GRUPO
  if (insc.acoplate_to_group == 'true' && $.trim(insc.acoplate_to_group_id).length != 5)
    errors.push('Código de identificación del grupo al que te quieres unir');

  // RELACION
  //inscription_type == 'group' || 

  if (insc.relation == '' && (
      (inscription_type == 'group' && insc.main_user != 'true') ||
      (inscription_type == 'individual' && insc.acoplate_to_group == 'true')
    ))
    errors.push('Relación con responsable del grupo');

    // alojamiento
    if (typeof event_lodgements != 'undefined' && event_lodgements.length > 0)
    {
        if (typeof insc.lodgement_place == 'undefined')
            errors.push('Especifica si se aloja o no');
        else if (insc.lodgement_place != '' && typeof insc['lodgement_date[]'] == 'undefined')
            errors.push('Especifica las noches que te alojarás');
    }

    // COMIDAS
    if (typeof event_meals != 'undefined' && event_meals.length > 0)
    {
        if (typeof insc.meals_enabled == 'undefined')
        {
            errors.push('Especificar si come o no');
        }
        else if (insc.meals_enabled == 'true')
        {
            if (typeof(insc['meal_type[]']) == 'undefined')
                errors.push('Comida (marca los días que deseas)');

        // DIETA ESPECIAL
        if (insc.special_meals == 'true' && $.trim(insc.special_meals_obs) == '')
            errors.push('Observaciones de dieta especial');
        }
        else
        {
            delete insc['meal_type[]'];
            delete insc.special_meals;
            delete insc.special_meals_obs;
        }
    }

  // TALLERES
  if (age >= 13)
  {
    if (!insc.hasOwnProperty('event_studio') || insc.event_studio == '')
      errors.push('Taller');
  }
  
  if (age < 13)
  {
    delete insc.event_studio;
    delete insc.parking_type;
    delete insc['parking_day[]'];
  }

  if (insc.parking_type == 'by_day' && typeof(insc['parking_day[]']) == 'undefined')
    errors.push('Parking (marca los dias que deseas)');

  if (insc.accept_conditions != 'true')
    errors.push('Aceptar las condiciones');

  if (errors.length > 0){
      alert('Los siguientes campos o datos están vacíos o no son correctos: \n\n' + '- '+errors.join('\n- '));
      return false;
  }
  else
    return true;
}

function getSerializedFormInscription()
{
  var inscription = {};

  $.each($('#main_form').find(':input, select, textarea').serializeArray(),function(index,prop){
    // is is array
    if (prop.name.match(/\[\]$/))
    {
      var key = prop.name;
      if (typeof inscription[key] == 'undefined')
        inscription[key] = [];
      inscription[key].push(prop.value);
    }
    else
      inscription[prop.name] = prop.value;
  });

  if ('DNI' in inscription)
  {
    inscription.DNI = inscription.DNI.replace(/\W/g, '').toUpperCase();
    inscription.DNI_rep = inscription.DNI_rep.replace(/\W/g, '').toUpperCase();
  }

  if ('telefono1' in inscription)
  {
    inscription.telefono1 = inscription.telefono1.replace(/[^0-9\.]+/g, '');
    inscription.telefono1_rep = inscription.telefono1_rep.replace(/[^0-9\.]+/g, '');
  }

  if ('telefono2' in inscription)
  {
    inscription.telefono2 = inscription.telefono2.replace(/[^0-9\.]+/g, '');
    inscription.telefono2_rep = inscription.telefono2_rep.replace(/[^0-9\.]+/g, '');
  }

  if ('email' in inscription)
  {
    inscription.email = $.trim(inscription.email);
    inscription.email_rep = $.trim(inscription.email_rep);
  }

  return inscription;
}

function addInscriptionToGroup(inscription)
{
  var new_inscription = true;

  $.each(inscriptions_pull,function(index,insc)
  {
    if (inscription.insc_local_ref == insc.insc_local_ref)
    {
      new_inscription = false;
      inscriptions_pull[index] = inscription;
      return false;
    }
  });

    if (inscription.main_user == 'true' && inscriptions_pull.length > 0)
    {
      // revisamos si ha cambiado el usuario principal por si hay que avisar de los parentescos
      $.each(inscriptions_pull,function(index,insc){
          if (insc.insc_local_ref != inscription.insc_local_ref && insc.main_user == 'true')
          {
            insc.main_user = null;
            delete insc.main_user;
            alert('Has cambiado de responsable de grupo, por favor, revisa la relación en cada inscripción de usuario de este grupo respecto al nuevo responsable de grupo');
            return false;
          }
      });
    }

  if (new_inscription)
    inscriptions_pull.push(inscription);
}

function renderInscriptionsBoxes()
{
  $('#inscriptions_box_wrapper').html('');

  $.each(inscriptions_pull,function(index,insc){
      $('#inscriptions_box_wrapper').append('\
        <div data-ref-id="'+insc.insc_local_ref+'" class="inscription_item">\
            <div class="box_badge"><span class="badge">'+String(index+1)+'</span></div>\
            <div class="username"><small>'+insc.nombre+' '+insc.apellidos +'</small></div>\
            <small> Edad: '+Utils.getAge(Utils.standar2MysqlDate(insc.fec_nacimiento))+'</small><br>\
            '+(insc.main_user == 'true'?'\
            <span class="label label-danger"><li class="fa fa-star fa-fw"></li>responsable</span>':'\
            <span class="label label-info">'+(insc.relation==''?' relación no definida ':insc.relation.toLowerCase())+'</span>')+'\
            <button type="button" class="btn btn-xs btn-default btn_remove_user"><i class="fa fa-trash"></i> <small>Borrar</small></button>\
            <br>\
        </div>\
      ');
  });

  $('#inscriptions_box_wrapper').append('\
    <div id="new_box" class="inscription_item bg-warning text-center">\
      <i class="fa fa-user-plus fa-2x"></i><br>\
        Añadir persona\
    </div>\
    ');

  $('#inscriptions_box_wrapper div').on('click',function()
  {
    if ($(this).hasClass('selected') == true)
      return false;
    else
      resetInscriptionForm();
    
    $(this).parent().find('div').removeClass('selected');
    $(this).addClass('selected');
    
    if (typeof $(this).data('ref-id') != 'undefined')
    {
      fillForm(getLocalInscriptionById($(this).data('ref-id')));
      $('#main_form button[type="submit"]').html('<li class="fa fa-save fa-lg"></li> Guardar cambios');
    }
    else
      $('#main_form button[type="submit"]').html('<li class="fa fa-user-plus fa-lg"></li> Añadir inscripción al grupo');

    hideFieldsByAge();

    $('#main_form').show();
  });

  $('#inscriptions_box_wrapper div .btn_remove_user').on('click',function(e)
  {
    e.stopPropagation();
    //e.preventDefault();

    if (confirm('¿Seguro que quieres quitar esta persona de la inscripción en grupo?'))
    {
      //if ($(this).parent().hasClass('selected') == true){
        resetInscriptionForm();
        $('#main_form').hide();
      //}

      var local_ref = $(this).parent().data('ref-id');

      $.each(inscriptions_pull,function(index,inscription){
          if (inscription.insc_local_ref == local_ref){
            inscriptions_pull.splice(index, 1);
            return false;
          }
      });

      renderInscriptionsBoxes();
    }

  });
  /*
  if (inscriptions_pull.length > 1)
      $('#btn_submit_group').prop('disabled',false);
  else
     $('#btn_submit_group').prop('disabled',true);
  */
}

function getLocalInscriptionById(str)
{
  var result = null;
  $.each(inscriptions_pull,function(index,inscription){
      if (inscription.insc_local_ref == str){
        result = inscription;
        return false;
      }
  });
  return result;
}

function fillForm(data)
{  
  $.each(data, function(name, val){

    var $el = $('[name="'+name+'"]');

    if ($el.length > 0)
    {
      switch($el[0].tagName)
      {
          case 'INPUT':

            switch($el[0].getAttribute('type'))
            {
                case 'checkbox':

                    if ($el.length > 1 && typeof val == 'object')
                    {
                        $.each(val,function(index,new_val){
                          $el.filter('[value="'+new_val+'"]').prop('checked', true).trigger('change');
                        });
                    }
                    else
                      $el.filter('[value="'+val+'"]').prop('checked', true).trigger('change');
                break;

                case 'radio':
                    $el.filter('[value="'+val+'"]').prop('checked', true).trigger('change');
                    break;
                default:
                    $el.val(val);
            }
          break;

          case 'SELECT':
            $el.val(val);
            $el.trigger('change');
            $el.selectpicker('render');
          break;

          case 'TEXTAREA':
            $el.val(val);
          break;
        }
      }
  });
}

function resetInscriptionForm()
{
  // generamos el id de inscripción (para javascript)
  $('#insc_local_ref').val(Utils.getRandomString());

  // type text
  $('#main_form input[type="text"]').val('');

  // type radio
  $('#main_form :input[type="radio"]').prop('checked',false);
  $('#main_form :input[type="radio"][default_checked=""]').prop('checked',true);
  $('#main_form :input[type="radio"]:checked').trigger('change');

  // type checkbox
  $('#main_form :input[type="checkbox"]').prop('checked',false);
  $('#main_form :input[type="checkbox"][default_checked=""]').prop('checked',true);
  $('#main_form :input[type="checkbox"]').trigger('change');

   // select
  $('#main_form select').val('');
  $('#main_form select').selectpicker('render');

  // textarea
  $('#main_form textarea').val('');
}

function getInscriptionPrice(insc)
{
    var concept = {
        inscription : null,
        meals       : null,
        lodgements  : null,
        parking     : null
    };

    var insc_birthdate = insc.fec_nacimiento.split('/');
    var insc_age = Utils.getAge(insc_birthdate[2]+'-'+insc_birthdate[1]+'-'+insc_birthdate[0],event_info.date_from);

    // tiene que pagar inscripción
    if (event_info.cost_inscription > 0)
    {
        // inscripcion
        if (insc_age < 4)
            concept.inscription = 0;
        else if (insc_age < 14)
            concept.inscription = event_info.cost_inscription_childs;
        else
        {
            // si se hace antes de una fecha
            var base_time = 0;

            if (insc.hasOwnProperty('created_at') && insc.created_at != '')
                base_time = new Date(insc.created_at).getTime();
            else
                base_time = new Date().getTime();

            if (base_time > new Date(event_info.date_reduced_inscription_end).getTime())
                concept.inscription = event_info.cost_inscription;
            else
                concept.inscription = event_info.cost_inscription_reduced;
        }
    }

    // comidas
    if ('lodgement_place' in insc && insc.lodgement_place != '' &&
        'lodgement_date[]' in insc && insc['lodgement_date[]'].length > 0)
    {
        $.each(event_lodgements, function(index,l)
        {
            if (l.id == insc.lodgement_place)
            {
                if (l.external == 0 && l.nigth_price > 0)
                {
                    concept.lodgements = [];

                    $.each(insc['lodgement_date[]'], function (i,ld)
                    {
                        concept.lodgements.push(l.nigth_price);
                    });
                }
                return false;
            }
        });
    }

    if ('meal_type[]' in insc && insc['meal_type[]'].length > 0)
    {
        concept.meals = [];

        $.each(insc['meal_type[]'], function(index,meal)
        {
            $.each(event_meals,function(ind2,event_meal)
            {
                //debugger;
                if (parseInt(String(meal).replace('pic-','')) == event_meal.id)
                    concept.meals.push(event_meal.price);
            });
        });
    }

    // parking
    if (insc.parking == 'true')
        concept.parking = event_info.cost_parking;

    if ('parking_type' in insc && insc.parking_type != 'none')
    {
        if (insc.parking_type == 'complete')
            concept.parking = event_info.cost_parking;
        else
            concept.parking = event_info.cost_parking_day*insc['parking_day[]'].length;
    }

    return concept;
}

var Utils = {

  getAge : function(date1,date2)
  {
    if (typeof date2 == 'undefined')
      var today = new Date();
    else
      var today = new Date(date2);

    var birthDate = new Date(date1);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
  },

  getDayDiff : function(date1,date2)
  {
    var date1 = new Date(date1);
    var date2 = new Date(date2);

    return (date2-date1)/(1000*60*60*24);
  },

  standar2MysqlDate : function(date){
    var date = date.split('/');
    return date[2]+'-'+date[1]+'-'+date[0];
  },

  mysqlDate2standar : function(date){
    var date = date.split('-');
    return date[2]+'/'+date[1]+'/'+date[0];
  },

  getRandomString : function(){
    return Math.random().toString(36).substr(2, 5)
  }
}

function registerInscription()
{
  var json = JSON.stringify(inscriptions_pull)
                .replace(/lodgement_date\[]/g,'lodgement_date')
                .replace(/meal_type\[]/g,'meals')
                .replace(/parking_day\[]/g,'parking_day');

  if (debug == true)
  {
    $('#hidden_inscriptions').val(json);
    $('#debug_form').submit();
    return;
  }

  var orig_text = $('#btn_register_inscription').html();
  $('#btn_register_inscription').prop('disabled',true);
  $('#btn_register_inscription').html('<i class="fa fa-cog fa-spin"></i> Enviando...');
  
  var data = {
    _token : '{{csrf_token()}}',
    inscriptions : json,
    original_inscription_code : original_inscription_code,
    event_id : '{{$event->id}}'
  }

  $.ajax({
      method : "POST",
      url    : "{{route('web.evento.inscribirse_post',$event->id)}}",
      data   : data, //inscriptions_pull,
      dataType :'json',
      cache : false
  })
  .success(function(data)
  {
    if ('error' in data)
    {
      $('#btn_register_inscription').html(orig_text);
      $('#btn_register_inscription').prop('disabled',false);

      $('#alert_error_registering').show();
      $('#alert_error_registering #error_msg').html(data.error.description);

      setTimeout(function(){
        $('#alert_error_registering').hide();
      },5000);
    }
    else
    {
      window.onbeforeunload = null;

      //setTimeout(function(){
        window.location.replace("{{route('web.inscripcion_completada')}}?insc="+data.inscription_id+'&sm='+data.store_mode);
      //},200);
    }
  })
  .error(function(data){
    alert('Ha ocurrido un error al registrar la inscripción. Por favor, inténtalo de nuevo más tarde');
    $('#btn_register_inscription').html(orig_text);
    $('#btn_register_inscription').prop('disabled',false);
  })
  .complete(function(){
    
  });
}
/*
window.onbeforeunload = function (e) {
    return 'Si te vas perderás los datos metidos y tendrás que volverlos a meter más tarde';
}
*/
</script> --}}