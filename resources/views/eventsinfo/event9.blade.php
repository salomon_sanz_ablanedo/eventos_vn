{{-- <div class="row">
  	<div class="col-md-12 no-padding">                        
        <div class="panel">        
            <div class="panel-body">
            	<h3 class="header">Información</h3>
			</div>
		</div>
	</div>
</div> --}}

@push('styles')
<style>
    .panel-body
    {
        font-size: 14px;
    }
</style>
@endpush

<div class="row">
    <div class="col-md-12">
    	<div class="panel">        
            <div class="panel-body">
            	<h3 class="header">Presentación</h3>
                <br/>
            	<p>
            		Los jóvenes mayores de 18 años tenemos la oportunidad de ir al pre-campamento, donde recibiremos visión y cogeremos fuerzas para la escuela-campamento.

                    <br/>
                    <br/>
                    El precio es de {{ $event->cost_inscription }}&euro;, no obstante el pre-campamento está subvencionado para los j&oacute;venes que asistan a la escuela-campamento, pasando a costar {{ $event->cost_inscription_reduced }}&euro;. 
                    <br/>
                    <br/>

                    El precio incluye alojamiento y pensión completa.
		    	</p>
			</div>
		</div>
    </div>
</div>