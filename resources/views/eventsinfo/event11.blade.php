{{-- <div class="row">
  	<div class="col-md-12 no-padding">                        
        <div class="card">        
            <div class="card-body">
            	<h3 class="header">Información</h3>
			</div>
		</div>
	</div>
</div> --}}

@push('styles')
<style>
    .card-body
    {
        /* font-size: 13px; */
    }
</style>
@endpush

<div class="row pb-4">
    <div class="col-md-12">
    	<div class="card">        
            <div class="card-body p-4">
            	<p>
					Un año más somos convocados como iglesias en restauración a separar unos días para buscar juntos a Dios.</p>
				<p> ¡Qué gran privilegio! ¡Qué honor! Empecemos a orar y prepararnos para estos días tan excepcionales, donde podremos escuchar a la voz de nuestro amado Dios y disfrutar de su Presencia.
		    	</p>
			</div>
		</div>
    </div>
</div>

<div class="h2 text-center"><i class="fa fa-map-marker"></i> Ubicación</div>

<div class="row pb-4">
	<div class="col-md-12">
		<div class="card">       

			<div class="card-body row">
				<div class="col-md-4">
					<img src="{{ asset('img/places/piramide.jpg') }}" style="max-width:100%;"/>
				</div>
				<div class="col-md-8">

					<p>
					A la altura del Km. 66 de la carretera nacional de Zaragoza a Huesca, a 4,1 Km. de
					esta última ciudad, nace el camino a la localidad de Cuarte, y a 150 m.
					aproximadamente de su nacimiento se encuentra la entrada al IES La Pirámide, antiguamente la Universidad Laboral de
					Huesca.
					<br/>
					Sus terrenos, están enclavados en el llamado Saso de la Alberca y en una parcela de aproximadamente 40 hectáreas de secano. 
					<br/>Cuenta el centro con todos los servicios suficientes para una población escolar de 1.000 alumnos internos y 200
					externos: dormitorios, comedores, salón de actos, capilla, aulas, talleres, laboratorios,
					biblioteca, cafetería e instalaciones deportivas.
					<br/>
					Destaca el salón de actos, cubierto por una estructura piramidal de gran altura, al cual se
					accede por las puertas exteriores y por un amplio vestíbulo, en cuyo interior, con
					perfectas condiciones acústicas y de visibilidad, se acomodan ampliamente más de
					1.000 personas.

					<div class="alert alert-secondary" style="width: 250px; padding: 12px;">
						Carretera de Cuarte S/N . <br/>
						22071 - Huesca 
						<a href="https://www.google.es/maps/place/IES+Pir%C3%A1mide/@42.116672,-0.447875,15z/data=!4m5!3m4!1s0x0:0x5e1794a9ff04023a!8m2!3d42.116672!4d-0.447875" rel="nofollow" target="_blank">
							 - <u>Como llegar</u>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="h2 text-center"><i class="fa fa-clock"></i> Horario</div>

<div class="row pb-4">
    <div class="col-md-12">
    	<div class="card">
            <div class="card-body row pt-0">
            	<div class="col-md-12">
            		<table class="table col-12">
						<thead>
            			<tr>
            				<td>
            					<b>Jueves 18</b>
            				</td>
            				<td>
            					<b>Viernes 19</b>
            				</td>
            				<td><b>Sábado 20</b></td>
            				<td><b>Domingo 21</b></td>
						</tr>
						</thead>
						<tbody class="small">
            			<tr>
            				<td>&nbsp;</td>
            				<td>8 a 9 - Desayuno</td>
            				<td>8 a 9 - Desayuno</td>
            				<td>8 a 9 - Desayuno</td>
            			</tr>
            			<tr>
							<td>&nbsp;</td>
            				<td>10:00 - Devocional y plenaria</td>
            				<td>10:00 - Devocional y plenaria</td>
            				<td>10:00 - Plenaria y cena del Señor</td>
            			</tr>
            			<tr>
							<td>&nbsp;</td>
            				<td>13:30 - Descanso</td>
            				<td>13:30 - Descanso</td>
            				<td>&nbsp;</td>
            			</tr>
            			<tr>
            				<td>&nbsp;</td>
            				<td>14:00 - Comida y tiempo libre</td>
            				<td>14:00 - Comida y tiempo libre</td>
            				<td>14:00 - Comida o picnic</td>
            			</tr>
            			<tr>
            				<td>16:00 - Recepción</td>
            				<td>16:00 - Talleres</td>
            				<td>16:00 - Talleres</td>
            				<td>16:00 - Salida</td>
            			</tr>
            			<tr>
            				<td>18:00 - Plenaria</td>
            				<td>18:00 - Descanso</td>
            				<td>18:00 - Descanso</td>
            				<td>&nbsp;</td>
            			</tr>
            			<tr>
            				<td>&nbsp;</td>
            				<td>18:30 - Plenaria</td>
            				<td>18:30 - Plenaria</td>
            				<td>&nbsp;</td>
            			</tr>
            			<tr>
            				<td>21:00 - Cena</td>
            				<td>21:00 - Cena</td>
            				<td>21:00 - Cena</td>
            				<td>&nbsp;</td>
						</tr>
						<tr>
            				<td>&nbsp;</td>
            				<td>22:00 - Veladas</td>
            				<td>22:00 - Veladas</td>
            				<td>&nbsp;</td>
						</tr>
						</tbody>
            	</table>
            	</div>
			</div>
		</div>
    </div>
</div>

<div class="row pb-4">
    <div class="col-md-12">
    	<div class="card">        
            <div class="card-body row">
            	<div class="col-md-6" style="padding-right: 30px; border-right: 1px solid #CCC;">
            		<h3 class="header text-center"> <i class="fa fa-utensils"></i> Comida y Alojamiento</h3>
					<br/>
					<ul>
						<li>Coste retiro completo: 105&euro; (35&euro;/dia IVA incluido)
							<ul>
								<li>Desayuno: 3&euro;</li>
								<li>Comidas: 8,5&euro;</li>
								<li>Cenas: 8,5&euro;</li>
								<li>Alojamiento: 15&euro;</li>
							</ul>
						</li>
						<li>Habitaciones compartidas (camas y/o literas para 4 personas</li>
						<li>Baños comunitarios</li>
						<li>No hay servicio de toallas</li>
					</ul>

					<p>
						Las comidas, cenas y desayunos se pueden coger los días que se quieran, es totalmente opcional. El último día habrá opción de elegir comida para llevar (picnic). Todas estas opciones se pueden elegir a la hora de realizar la inscripción online.
					</p>
					<p>
						Se podrá pedir el menú para el dia en recepción el mismo día antes de las 11:00 de la mañana.
					</p>
					<p> El alojamiento en Pirámide está sujeto al pago y a la disponibilidad de plazas.
					</p>

            		{{-- El menú consistirá en: 
						<div style="padding: 0 12px;">
            			<table class="table">
            				<tr class="text-bold">
            					<td>Dia</td>
            					<td>Comida</td>
            					<td>Cena</td>
            				</tr>
            				<tr>
            					<td>Viernes 30</td>
            					<td> 
                                    Menestra de verduras<br>
                                    Guisado de ternera
                                 </td>
            					<td> 
                                    Cardo con jamón <br/>
                                    Tortilla de patatas
                                </td>
            				</tr>
            				<tr>
            					<td>Sábado 31</td>
            					<td> 
                                    Espaguetis boloñesa <br/>
                                    Pavo en salsa
                                </td>
            					<td> 
                                    Ensalada de Chaca <br/>
                                    Merluza rebozada con pimientos verdes
                                 </td>
            				</tr>
            				<tr>
            					<td>Domingo 1</td>
            					<td> 
                                    2 bocadillos <br/>
                                    Fruta. Pastelito. <br/>
                                    Botellín agua. <br/>
                                 </td>
            					<td> -- </td>
            				</tr>
            			</table>
                        <br/>
                        <br/>
                        Postres: Fruta del tiempo, yogures variados, pastel de manzana, flan, natillas, tarta de bizcocho, hojaldre y crema<br>
                        Bebidas: agua (Se podrán comprar refrescos). 
            		</div>--}}
		    	</div>
            	<div class="col-md-6" style="padding-left: 30px;">
            		<h3 class="header text-center"><i class="fa fa-child"></i> Guarderías</h3>

            		<p>
					Durante el tiempo de la palabra se organizará un servicio de guardería. Se habilitarán 5 espacios para atender a los niños y se dispondrá de sala de lactancia. Los cuidadores recogerán a los niños en la entrada del auditorio y permanecerán con ellos hasta que los recojan sus padres.
					</p>
					<p>Se ruega que las mochilas, botellas, bolsa de pañales, etc, estén identificado con el nombre del niño y el lugar de procedencia. Las comidas y cenas durante las guarderías los darán los padres.
					</p>
					
					<p>
            		Los niños usarán un peto de un color específico según la edad, el cual puede ser adquirido al realizar la inscripción, por el precio de 1&euro;. No obstante podrán llevar sus propios petos mientras sean de los siguientes colores:
            		</p>
            		<div style="padding: 0 12px;">
            			<table class="table">
            				<tr>
            					<td>de 0 a 2 (2017/2018)</td>
            					<td>Azul oscuro</td>
								<td><button class="img-circle" style="background-color:blue; width:22px; height:20;">&nbsp;</span></td>
            				</tr>
            				<tr>
								<td>de 3 a 4 (2015/2016)</td>
            					<td>Amarillo</td>
								<td><button class="img-circle" style="background-color:yellow; width:22px; height:20;">&nbsp;</span></td>
							</tr>
							<tr>
								<td>de 5 a 6 (2013/2014)</td>
								<td>Naranja</td>
								<td><button class="img-circle" style="background-color:orange; width:22px; height:20;">&nbsp;</span></td>
								</tr>
								<tr>
									<td>de 7 a 8 (2011/2012)</td>
									<td>Azul brillante</td>
									<td><button class="img-circle" style="background-color:#00c0ffd6; width:22px; height:20;">&nbsp;</span></td>
								</tr>
								<tr>
									<td>de 9 a 12 (2007/2010)</td>
									<td>Rojo</td>
									<td><button class="img-circle" style="background-color:red; width:22px; height:20;">&nbsp;</span>
								</td>
							</tr>
            			</table>
            		</div>
		    	</div>
			</div>
		</div>
    </div>
</div>
<div class="h2 text-center"><i class="fa fa-bed"></i> Alojamientos externos</div>

<div class="row pb-2">
    <div class="col-md-12">
        <div class="card">        
            <div class="card-body">
                <p class="text-info">
                    Cada interesado debe hacer directamente la reserva en estos alojamientos.
                    <br> A la hora de hacer la reserva es importante decir que se va por el evento, para obtener acceso a los precios especiales abajo descritos.
				</p>
			</div>
		</div>
	</div>
</div>

<div class="row">

@foreach($event->lodgements as $l)
	@continue($l->id == 1)
<div class="col-md-4 py-2">
        <div class="card">        
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
						{{-- <div class="col-md-3">
							<img src="{{ asset('img/lodgements/'.$l->id.'.jpg') }}" style="max-width:100%;" />
						</div> --}}
						<h3>{{ $l->name }}</h3>
						<br>
						{!! nl2br(e($l->address)) !!}
						<br>
						<br>

						{!! nl2br(e($l->info)) !!}

						<br>
						<br>
						@if (!empty($l->lat) && !empty($l->long))
						<a href="{{ $l->maps }}" rel="nofollow" target="_blank">
							<i class="fa fa-map-marker"></i> <u>Ubicación</u>
						</a> &nbsp; | &nbsp;
						@endif

						@if (!empty($l->web))
							
						<a href="{{ $l->web }}" rel="nofollow" target="_blank">
							<i class="fa fa-link"></i> <u>Web</u>
						</a>
						@endif
						<div style="padding-top:20px;">
							<p class="small">{!! nl2br(e($l->html_info)) !!}</p>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
</div>