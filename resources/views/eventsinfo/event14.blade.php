{{-- <div class="row">
  	<div class="col-md-12 no-padding">                        
        <div class="card">        
            <div class="card-body">
            	<h3 class="header">Información</h3>
			</div>
		</div>
	</div>
</div> --}}

@push('styles')
<style>
    .card-body
    {
        /* font-size: 13px; */
    }
</style>
@endpush

<div class="row pb-4">
    <div class="col-md-12">
    	<div class="card">        
            <div class="card-body p-4">
            	<p>
					Un año más somos convocados como iglesias en restauración a separar unos días para buscar juntos a Dios.</p>
				<p> ¡Qué gran privilegio! ¡Qué honor! Empecemos a orar y prepararnos para estos días tan excepcionales, donde podremos escuchar a la voz de nuestro amado Dios y disfrutar de su Presencia.
				</p>
				<p>Amados, no tenemos otra cosa que el Evangelio, nuestro mensaje es el Evangelio, el motivo de nuestra vida es el Evangelio. <br>El Evangelio de la cruz de nuestro Señor Jesucristo.<br/>
<i>1ª Corintios 1:23</i>
</p>
			</div>
		</div>
    </div>
</div>

<div class="h2 text-center"><i class="fa fa-map-marker"></i> Ubicación</div>

<div class="row pb-4">
	<div class="col-md-12">
		<div class="card">       

			<div class="card-body row">
				<div class="col-md-4">
					<img src="{{ asset('img/places/navarra_arena.jpg') }}" style="max-width:100%;"/>
				</div>
				<div class="col-md-8">

					<p>
						<br/>
					El Navarra Arena es un pabellón multiusos construido en un solar anexo al Estadio El Sadar, en Pamplona. 
					El pabellón, cuenta con una pista central multiusos con una capacidad de 10 000 espectadores en configuración deportiva y hasta 11 800 espectadores en configuración de concierto y frontón para 3000 espectadores. 
					La infraestructura está preparada para poder acoger eventos de todo tipo desde campeonatos deportivos hasta reuniones empresariales pasando por conciertos o espectáculos.
					</p>
					<div class="alert alert-secondary" style="width: 250px; padding: 12px;">
						Plaza Aizagerria, 1 <br/>
						31006 - Pamplona 
						<br/>
						<a href="https://www.google.es/maps/place/Navarra+Arena/@42.7960052,-1.6374167,17z/data=!3m1!4b1!4m5!3m4!1s0xd5093b4fdc25075:0x9f93e23988f3cfdb!8m2!3d42.7960013!4d-1.635228?hl=es" rel="nofollow" target="_blank">
							 - <u>Como llegar</u>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="h2 text-center"><i class="fa fa-clock"></i> Horario</div>

<div class="row pb-4">
    <div class="col-md-12">
    	<div class="card">
            <div class="card-body row pt-0">
            	<div class="col-md-12">
            		<table class="table col-12">
						<thead>
            			<tr>
            				<td>
            					<b>Jueves 9</b>
            				</td>
            				<td>
            					<b>Viernes 10</b>
            				</td>
            				<td><b>Sábado 11</b></td>
            				<td><b>Domingo 12</b></td>
						</tr>
						</thead>
						<tbody class="small">
            			<tr>
							<td>&nbsp;</td>
            				<td>10:00 - Devocional y plenaria</td>
            				<td>10:00 - Devocional y plenaria</td>
            				<td>10:00 - Plenaria y cena del Señor</td>
            			</tr>
            			<tr>
							<td>&nbsp;</td>
            				<td>13:30 - Descanso</td>
            				<td>13:30 - Descanso</td>
            				<td>&nbsp;</td>
            			</tr>
            			<tr>
            				<td>&nbsp;</td>
            				<td>14:00 - Comida y tiempo libre</td>
            				<td>14:00 - Comida y tiempo libre</td>
            				<td>14:00 - Comida o picnic</td>
            			</tr>
            			<tr>
            				<td>16:00 - Recepción</td>
            				<td>16:00 - Talleres</td>
            				<td>16:00 - Talleres</td>
            				<td>16:00 - Salida</td>
            			</tr>
            			<tr>
            				<td>18:00 - Plenaria</td>
            				<td>17:30 - Descanso</td>
            				<td>17:30 - Descanso</td>
            				<td>&nbsp;</td>
            			</tr>
            			<tr>
            				<td>&nbsp;</td>
            				<td>18:30 - Cruzada Evangel&iacute;stica</td>
            				<td>18:30 - Cruzada Evangel&iacute;stica</td>
            				<td>&nbsp;</td>
            			</tr>
            			<tr>
            				<td>21:00 - Cena</td>
            				<td>21:00 - Cena</td>
            				<td>21:00 - Cena</td>
            				<td>&nbsp;</td>
						</tr>
						</tbody>
            	</table>
            	</div>
			</div>
		</div>
    </div>
</div>

<div class="row pb-4">
    <div class="col-md-4">
    	<div class="card">
            <div class="card-body">
				Talleres
				<ul>
					<li>Mujeres</li>
					<li>Hombres</li>
					<li>Adolescentes y jóvenes hasta los 35 años</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4">
    	<div class="card">
            <div class="card-body">
				Precio
				<ul>
					<li>10&euro; para asistentes de fuera de Pamplona</li>
					<li>25&euro; para asistentes de Pamplona</li>
					<li>Menores de 13 años, gratis</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-md-4">
    	<div class="card">
            <div class="card-body">
				
				<p>Guardería<br>
					Petos de Guardería se podrán adquirir por 1,5&euro;, <br>
					Los padres/cuidadores recogerán a los niños en la entrada del auditorio y permanecerán con
ellos hasta que los recojan sus padres. Se ruega que las mochilas, botellas, bolsas de
pañales, etc. estén identificados con el nombre del niño y el lugar de procedencia. Las
comidas y cenas durante las guarderías las darán los padres.
				</p>
			</div>
		</div>
    </div>
</div>

<div class="h2 text-center"><i class="fa fa-utensils"></i> Menú</div>

<div class="row pb-4">
    <div class="col-md-12">
    	<div class="card">
            <div class="card-body row pt-0">
            	<div class="col-md-12">
            		<table class="table col-12">
						<thead>
            			<tr>
            				<td>
            					<b>Jueves 9</b>
            				</td>
            				<td>
            					<b>Viernes 10</b>
            				</td>
            				<td><b>Sábado 11</b></td>
            				<td><b>Domingo 12</b></td>
						</tr>
						</thead>
						<tbody class="small">
            			<tr>
							<td>&nbsp;</td>
							<td>
								<b>Comida</b>
								<ul>
									<li>Ensalada mixta – lechuga, cebolla, zanahoria, atún, huevo duro, maíz y aceitunas. Paté</li>
									<li>Paella mixta - + arróz</li>
									<li>Tarta selva fruta</li>
								</ul>
							</td>
							<td><b>Comida</b>
								<ul>
									<li>Ensalada rusa</li>
									<li>Carne guisada de Ternera con pimientos, patatas y tomate.</li>
									<li>Fruta</li>
								</ul>
							</td>
							<td><b>Comida</b>
								<ul>
									<li>Menestra de verduras</li>
									<li>Pollo guisado en salsa</li>
									<li>Tarta de hojaldre</li>
								</ul>
							</td>
            			</tr>
            			<tr>
							<td>
								<b>Cena</b>
								<ul>
									<li>Crema de Verduras</li>
									<li>Lomo con salsa y patatas</li>
									<li>Yogurth</li>
								</ul>
							</td>
            				<td>
								<b>Cena</b>
								<ul>
									<li>Ensalada – patata, huevo duro, puerro aceitunas y atún.</li>
									<li>Pechugas de pollo empanadas con guarnición.</li>
									<li>Flán</li>
								</ul>
							</td>
            				<td>
								<b>Cena</b>
								<ul>
									<li>Ensalada de pasta</li>
									<li>Tortilla de patata y croquetas 3</li>
									<li>Yogurth</li>
								</ul>
							</td>
							<td>
								<b>Picnic</b>
								<ul>
									<li>Bocadillo s 1⁄2 barra de pán Jamón con tomate</li>
									<li>Lomo con pimientos Fruta y pastelito</li>
									<li>Botellín de agua</li>
								</ul>
							</td>
            			</tr>
						</tbody>
            	</table>
            	</div>
			</div>
		</div>
    </div>
</div>

<div class="h2 text-center"><i class="fa fa-bed"></i> Alojamientos ({{ count($event->lodgements) }})</div>

<div class="row pb-2">
    <div class="col-md-12">
        <div class="card">        
            <div class="card-body">
                <p class="text-info">
                    Cada interesado debe hacer directamente la reserva en estos alojamientos.
                    <br> A la hora de hacer la reserva es importante decir que se va por el evento, para obtener acceso a los precios especiales abajo descritos.
				</p>
			</div>
		</div>
	</div>
</div>

<div class="row">

@foreach($event->lodgements as $l)
	@continue($l->id == 1)
<div class="col-md-4 py-2">
        <div class="card">        
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
						{{-- <div class="col-md-3">
							<img src="{{ asset('img/lodgements/'.$l->id.'.jpg') }}" style="max-width:100%;" />
						</div> --}}
						<h3>{{ $l->name }}</h3>
						<br>
						{!! nl2br(e($l->address)) !!}
						<br>
						<br>

						{!! nl2br(e($l->info)) !!}

						<br>
						<br>
						@if (!empty($l->lat) && !empty($l->long))
						<a href="{{ $l->maps }}" rel="nofollow" target="_blank">
							<i class="fa fa-map-marker"></i> <u>Ubicación</u>
						</a> &nbsp; | &nbsp;
						@endif

						@if (!empty($l->web))
							
						<a href="{{ $l->web }}" rel="nofollow" target="_blank">
							<i class="fa fa-link"></i> <u>Web</u>
						</a>
						@endif
						<div style="padding-top:20px;">
							<p class="small">{!! nl2br(e($l->html_info)) !!}</p>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
</div>