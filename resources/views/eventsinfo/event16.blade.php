{{-- <div class="row">
  	<div class="col-md-12 no-padding">                        
        <div class="card">        
            <div class="card-body">
            	<h3 class="header">Información</h3>
			</div>
		</div>
	</div>
</div> --}}

@push('styles')
<style>
    .card-body
    {
        /* font-size: 13px; */
    }
</style>
@endpush

{{-- <div class="row pb-4">
    <div class="col-md-12">
    	<div class="card">        
            <div class="card-body p-4">
            	<p>
					Un año más somos convocados como iglesias en restauración a separar unos días para buscar juntos a Dios.</p>
				<p> ¡Qué gran privilegio! ¡Qué honor! Empecemos a orar y prepararnos para estos días tan excepcionales, donde podremos escuchar a la voz de nuestro amado Dios y disfrutar de su Presencia.
				</p>
				<p>Amados, no tenemos otra cosa que el Evangelio, nuestro mensaje es el Evangelio, el motivo de nuestra vida es el Evangelio. <br>El Evangelio de la cruz de nuestro Señor Jesucristo.<br/>
<i>1ª Corintios 1:23</i>
</p>
			</div>
		</div>
    </div>
</div> --}}

{{-- <div class="h2 text-center"><i class="fa fa-map-marker"></i> Ubicación</div> --}}

<div class="row pb-4 mt-4">
	<div class="col-md-12">
		<div class="card">       

			<div class="card-body row">

				<div class="col-md-8">

					<p>
						<br/>
					El Navarra Arena es un pabellón multiusos construido en un solar anexo al Estadio El Sadar, en Pamplona. 
					El pabellón, cuenta con una pista central multiusos con una capacidad de 10 000 espectadores en configuración deportiva y hasta 11 800 espectadores en configuración de concierto y frontón para 3000 espectadores. 
					La infraestructura está preparada para poder acoger eventos de todo tipo desde campeonatos deportivos hasta reuniones empresariales pasando por conciertos o espectáculos.
					</p>
					<div class="alert alert-secondary" style="width: 250px; padding: 12px;">
						Plaza Aizagerria, 1 <br/>
						31006 - Pamplona 
						<br/>
						<a href="https://www.google.es/maps/place/Navarra+Arena/@42.7960052,-1.6374167,17z/data=!3m1!4b1!4m5!3m4!1s0xd5093b4fdc25075:0x9f93e23988f3cfdb!8m2!3d42.7960013!4d-1.635228?hl=es" rel="nofollow" target="_blank">
							 - <u>Como llegar</u>
						</a>
					</div>
					<p>
						Para más información y contacto: <a href="mailto:eventosiglesiasrestauracion@gmail.com">eventosiglesiasrestauracion@gmail.com</a>
					</p>
				</div>
				<div class="col-md-4" style="text-align: right;">
					<img src="{{ asset('img/places/navarra_arena.jpg') }}" style="max-width:100%; max-height:300px;"/>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- <div class="h2 text-center row">
	
</div>

 --}}

<div class="row pb-4">
	<div class="col-md-6 mb-2">
    	<div class="card">
			<h5 class="card-title p-2 pt-3 text-center">Precio</h5>						
            <div class="card-body">
				<p>Los precios de inscripción serán los siguientes:</p>
				<ul>
					<li>10&euro; para asistentes de fuera de Pamplona</li>
					<li>15&euro; para asistentes de Pamplona</li>
					<li>Menores de 13 años, gratis (nacidos desde 2010 inclusive)</li>
				</ul>
				<p>Forma de pago: la forma de pago se hará en efectivo a cada responsable de iglesia</p>
			</div>
		</div>
	</div>
	<div class="col-md-6 mb-2">
    	<div class="card">			
				<h5 class="card-title p-2 pt-3 text-center"><i class="fa fa-clock"></i> Horario</h5>						
            <div class="card-body row pt-0">            	
            		<table class="table col-12">
						<thead>
            			<tr>
            				<td>
            					<b>Sábado 13</b>
            				</td>
            				<td>
            					<b>Domingo 14</b>
            				</td>
						</tr>
						</thead>
						<tbody class="small">
            			<tr>							
            				<td>15:00 - Llegada</td>
							<td>10:00 - Gran Celebración al Señor</td>            				            				
            			</tr>
            			<tr>
							<td>16:00 - Culto al Señor y visión del Congreso</td>							
            				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Despedida</td>            				
            			</tr>
            			<tr>
							<td>19:30 - Evangelio / Concierto</td>
            				<td>&nbsp;</td>            				
            			</tr>            			
						</tbody>
            	</table>            	
			</div>
		</div>
    </div>	
</div>

<div class="row pb-4">
	<div class="col-md-6">
    	<div class="card">			
			<h5 class="card-title p-2 pt-3 text-center"><i class="fa fa-child"></i> Guarderia</h5>							
            <div class="card-body">
				<p>
					Los petos de guardería se adquieren al realizar la inscripción online, con un coste de 2&euro;, y serán entregados por cada responsable en cada una de las iglesias donde deberán identificarse con el nombre y teléfono de contacto de uno de los padres.
<br/><br/>El tiempo de guardería tanto el sábado como el domingo será después del tiempo de alabanza hasta el final del culto. Durante el concierto del sábado los niños estarán con los padres.

				</p>
			</div>
		</div>
    </div>
	<div class="col-md-6">
    	<div class="card">											
            <div class="card-body">						
				<table class="table" style="margin-bottom: 0px !important;">
					<tr>
						<td>Bebés</td>
						<td>Nacidos entre 2018-2021</td>								
					</tr>
					<tr>
						<td>Medianos 1 </td>
						<td>Nacidos entre 2016-2017</td>								
					</tr>
					<tr>
						<td>Medianos 2</td>
						<td>Nacidos entre 2014-2015</td>								
					</tr>
					<tr>
						<td>Mayores 1</td>
						<td>Nacidos entre 2012-2013</td>							
					</tr>
					<tr>
						<td>Mayores 2</td>
						<td>Nacidos entre 2010-2011</td>								
					</td>
					</tr>
				</table>            		
			</div>
		</div>
    </div>
</div>

<div class="row pb-4">
    <div class="col-md-6">
    	<div class="card">
			<h5 class="card-title p-2 pt-3 text-center"><i class="fa fa-utensils"></i> Comidas</h5>							
            <div class="card-body">            	
            		<p>
					La cena del sábado deberá ser traida por cada asistente. <br/>
					No obstante, hay posibilidad de contratar el servicio de catering (picnic) para la comida del último día, será el único servicio de comida.<br/><br/>

					El precio del picnic es de <b>8&euro;</b> y se compone de:<br/>
					<span class="small text-muted">Bocadillo de Tortilla de patatas casera,  pan tumaca con aceite de oliva. Fruta. Pastelito dulce. Botellin agua 500ml.</span>
					</p>
			</div>
		</div>
    </div>
	 <div class="col-md-6">
    	<div class="card">
			<h5 class="card-title p-2 pt-3 text-center"><i class="fa fa-bed"></i> Alojamiento</h5>							
            <div class="card-body">            	
            		<p>
					Cada interesado debe hacer directamente la reserva en estos alojamientos. <br/>
					A la hora de hacer la reserva es importante decir que se va por el evento, para obtener acceso a los precios especiales abajo descritos.
					<br/><br/>
					A continuación se detallan alojamientos y lugares cercanos económicos para comer (para aquellos que decidan decidan comer o cenar por su cuenta).
					</p>
			</div>
		</div>
    </div>
</div>

<div class="h2 text-center"> Listado de Alojamientos ({{ count($event->lodgements) }})</div>

	<div class="row pb-4">

	@foreach($event->lodgements as $l)
		@continue($l->id == 1)
	<div class="col-md-4 py-2">
			<div class="card">        
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							{{-- <div class="col-md-3">
								<img src="{{ asset('img/lodgements/'.$l->id.'.jpg') }}" style="max-width:100%;" />
							</div> --}}
							<h3>{{ $l->name }}</h3>
							<br>
							{!! nl2br(e($l->address)) !!}
							<br>
							<br>

							{!! nl2br(e($l->info)) !!}

							<br>
							<br>
							@if (!empty($l->lat) && !empty($l->long))
							<a href="{{ $l->maps }}" rel="nofollow" target="_blank">
								<i class="fa fa-map-marker"></i> <u>Ubicación</u>
							</a> &nbsp; | &nbsp;
							@endif

							@if (!empty($l->web))
								
							<a href="{{ $l->web }}" rel="nofollow" target="_blank">
								<i class="fa fa-link"></i> <u>Web</u>
							</a>
							@endif
							<div style="padding-top:20px;">
								<p class="small">{!! nl2br(e($l->html_info)) !!}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach
	</div>

	@php
$restaurants = array(
	'La Tasca de Don José La Morea
Picoteo y Restaurante
Barrio La Morea
10:00–23:00
&quot;Restaurante dentro de un centro comercial.&quot;
Comer allí·Para llevar·No hay entrega a domicilio',

'Wok to Walk La Morea
Cocina asiática
Barrio La Morea, s/n
12:00–0:00
Restaurante de comida rápida que sirve woks preparados al momento y platos
vegetarianos y veganos.
Comer allí·Para llevar',

'Sidrería Txinparta Sagardotegia
Sidrería
C.C La Morea, Calle la Morea, s/n
12:00–0:00
&quot;El restaurante genial&quot;
Comer allí·Para llevar',

'La Tagliatella
CC La Morea, Barrio La Morea, s/n
13:00–16:30, 19:30–23:00
Iluminación cálida y adornos hogareños antiguos en cadena de restaurantes de
gastronomía tradicional italiana.
Comer allí·Para llevar·A domicilio',

'Pasta &amp; Pizza
Parque Comercial Galaria, 2
10:00–0:00
Comer allí·Para llevar·A domicilio',

'VIPS
Parque Comercial Galaria, 3
13:00–23:00
Platos para comidas informales y menú en cadena de cafeterías con horario amplio, tienda
de comida y revistas.
Comer allí·Para llevar·A domicilio',

'Foster&#39;s Hollywood
Hamburguesa
C.C. La Morea. Barrio La Morea, s/n
13:00–23:00
Hamburguesas y comida típica americana y tex-mex en cadena de diners con decoración
industrial y de películas.
Comer allí·Para llevar·A domicilio',

'TGB- The Good Burger
Hamburguesa
Centro Comercial La Morea Barrio La, Carr. de la Morea, s/n
12:00–21:00
&quot;Me recuerda al típico restaurante Americano.&quot;
Comer allí·Recogida sin entrar·Entrega sin contacto',

'Subway
Bocadillo
Pamplona
13:00–23:00
Cadena de restaurantes de comida rápida, con bocadillos y ensaladas preparados al gusto
con ingredientes saludables.
Comer allí·Para llevar·A domicilio',

'Crunchy Fried Chicken
· Comida rápida
Pamplona
13:00–23:00
Comer allí·Para llevar·A domicilio
McDonald&#39;s
Comida rápida
C.C. La Morea, Rotonda Cordovilla, s/n
10:00–22:30'
);
@endphp
<div class="h2 text-center"> Lugares cercanos para comer (C.C. GALARIA EN LA MOREA)</div>

	<div class="row pb-4">

		@foreach($restaurants as $r)		
		<div class="col-md-4 py-2">
			<div class="card">        
				<div class="card-body">
					<div class="row">
						<div class="col-md-12">
							{!! nl2br($r) !!}
						</div>
					</div>
				</div>
			</div>			
		</div>
		@endforeach
	</div>
</div>