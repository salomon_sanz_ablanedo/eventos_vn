{{-- <div class="row">
  	<div class="col-md-12 no-padding">                        
        <div class="card">        
            <div class="card-body">
            	<h3 class="header">Información</h3>
			</div>
		</div>
	</div>
</div> --}}

@push('styles')
<style>
    .card-body
    {
        /* font-size: 13px; */
    }
</style>
@endpush

<div class="row pb-4">
    <div class="col-md-12">
    	<div class="card">        
            <div class="card-body p-4">
            	<p>
            		Dios est&aacute; buscando una generación digna de confianza y nos convoca, un año más, a su escuela-campamento que se celebrará del 7 al 12 de julio en IES Pirámide de Huesca. 
                    <br/>Juntos podremos buscar a Dios y hacer fiesta en Su honor, mientras aprendemos más de Él.
		    		<br/>
                    Además disfrutaremos de un ambiente fraternal, momentos de juego, deporte, piscina y veladas muy divertidas.
                    <br/>
                    <br/>
                    El campamento está dirigido a niños de entre 4 y 17 años (nacidos entre 2001 y 2014) y jóvenes.
		    	</p>
			</div>
		</div>
    </div>
</div>

<div class="h2 text-center"><i class="fa fa-map-marker"></i> Ubicación</div>

<div class="row pb-4">
	<div class="col-md-12">
		<div class="card">       

			<div class="card-body row">
				<div class="col-md-4">
					<img src="{{ asset('img/places/piramide.jpg') }}" style="max-width:100%;"/>
				</div>
				<div class="col-md-8">

					<p>
					A la altura del Km. 66 de la carretera nacional de Zaragoza a Huesca, a 4,1 Km. de
					esta última ciudad, nace el camino a la localidad de Cuarte, y a 150 m.
					aproximadamente de su nacimiento se encuentra la entrada al IES La Pirámide, antiguamente la Universidad Laboral de
					Huesca.
					<br/>
					Sus terrenos, están enclavados en el llamado Saso de la Alberca y en una parcela de aproximadamente 40 hectáreas de secano. 
					<br/>Cuenta el centro con todos los servicios suficientes para una población escolar de 1.000 alumnos internos y 200
					externos: dormitorios, comedores, salón de actos, capilla, aulas, talleres, laboratorios,
					biblioteca, cafetería e instalaciones deportivas.
					<br/>
					Destaca el salón de actos, cubierto por una estructura piramidal de gran altura, al cual se
					accede por las puertas exteriores y por un amplio vestíbulo, en cuyo interior, con
					perfectas condiciones acústicas y de visibilidad, se acomodan ampliamente más de
					1.000 personas.

					<div class="alert alert-secondary" style="width: 250px; padding: 12px;">
						Carretera de Cuarte S/N . <br/>
						22071 - Huesca 
						<a href="https://www.google.es/maps/place/IES+Pir%C3%A1mide/@42.116672,-0.447875,15z/data=!4m5!3m4!1s0x0:0x5e1794a9ff04023a!8m2!3d42.116672!4d-0.447875" rel="nofollow" target="_blank">
							 - <u>Como llegar</u>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row pb-4">
    <div class="col-md-12">
    	<div class="card">        
            <div class="card-body row">
            	<div class="col-md-6" style="padding-right: 30px; border-right: 1px solid #CCC;">
            		<h3 class="header"> <i class="far fa-tasks"></i>  Material necesario</h3>

            		<br/>
                    <ul>
                        <li>Biblia</li>
                        <li>Cuaderno y bolígrafo</li>
                        <li>Calzado deportivo</li>
                        <li>Estuche con pinturas, tijeras, pegamento</li>
                        <li>Bañador, chancletas y toalla</li>
                        <li>Crema y gorra para el sol</li>
                        <li>Fotocopia de la tarjeta de la Seguridad Social.</li>
                        <li>Los niños nacidos de 2001 a 2014 deben llevar un regalo unisex para su amigo invisible. Valor aprox. 3&euro;</li>
                        <li>Dinero para comprarse helados, etc, en la piscina (recomendamos no más de 6&euro;)</li>
                        <li>Ropa cómoda y discreta para todas las actividades del campamento</li>
                        <li><u>NO</u> llevarse sábanas propias</li>
                    </ul>

		    	</div>
            	<div class="col-md-6" style="padding-left: 30px;">
            		<h3 class="header"><i class="fa fa-exclamation-circle"></i> Otras consideraciónes</h3>

                    <ul>
						<li>Destinado a niños y adolescentes nacidos entre 2000 y 2013</li>
						<li>Es necesario inscribir a todos los bebés aunque usen cuna</li>
						<li>El precio es de {{ $event->cost_inscription }}&euro; e incluye alojamiento y pensión completa</li>
						<li>En caso de inscripción y pago, no se devolverá el importe si finalmente cancela a menos que sea una causa justificada de importancia mayor (médica, laboral, etc)</li>
                        <li>
                            En la inscripción online estará la opción de apuntarse en autobús (Pamplona - Huesca ida y vuelta) por {{ $event->bus_cost }}&euro; más. <br/>
                            {!! nl2br(e($event->bus_info)) !!}
                        </li>
                        <li>No está permitido el uso de móvil, ni de cualquier otro tipo de aparato electrónico a menores de 18 años.</li>
                        <li>Se ruega a los padres que revisen el pelo de sus hijos y si es necesario pongan tratamiento para evitar posibles contagios.</li>
                    </ul>
		    	</div>
			</div>
		</div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
    	<div class="card">        
            <div class="card-body row">
				<div class="col-12">
					<p>
							<u>Datos de contacto durante el campamento:</u> <br/>
							Vicente Mateo: <i>667 347 377</i><br/>
							Santiago Mancebo: <i>636 433 511</i><br/>
							Luis Nasarre: <i>649 297 228</i>
					</p>
					<p>
						<u>Para cualquier consulta:</u> <br/>
						<i>montes.garzon@gmail.com (Felipe Montes)</i>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>