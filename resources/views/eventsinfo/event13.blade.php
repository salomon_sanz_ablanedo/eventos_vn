{{-- <div class="row">
  	<div class="col-md-12 no-padding">                        
        <div class="card">        
            <div class="card-body">
            	<h3 class="header">Información</h3>
			</div>
		</div>
	</div>
</div> --}}

@push('styles')
<style>
    .card-body
    {
        /* font-size: 13px; */
    }
</style>
@endpush

<div class="row pb-4">
    <div class="col-md-12">
    	<div class="card">        
            <div class="card-body p-4">
            	<p>
            		Los jóvenes mayores de 18 años tenemos la oportunidad de ir al pre-campamento, del 5 al 7 de Julio en la Pirámide de Huesca, donde recibiremos visión y cogeremos fuerzas para la escuela-campamento.
                </p>
                <p>Tiene un precio de 70€, pero si te apuntas también al campamento (antes), tiene un precio subvencionado de 40€.</p>
                <p>El precio incluye: noches (13€ p/noche), comida y cenas (9€ p/comida o cena), desayunos (3€ p/desayuno) y piscina + instalaciones (2&euro;)</p>
			</div>
		</div>
    </div>
</div>

<div class="h2 text-center"><i class="fa fa-map-marker"></i> Ubicación</div>

<div class="row pb-4">
	<div class="col-md-12">
		<div class="card">       

			<div class="card-body row">
				<div class="col-md-4">
					<img src="{{ asset('img/places/piramide.jpg') }}" style="max-width:100%;"/>
				</div>
				<div class="col-md-8">

					<p>
					A la altura del Km. 66 de la carretera nacional de Zaragoza a Huesca, a 4,1 Km. de
					esta última ciudad, nace el camino a la localidad de Cuarte, y a 150 m.
					aproximadamente de su nacimiento se encuentra la entrada al IES La Pirámide, antiguamente la Universidad Laboral de
					Huesca.
					<br/>
					Sus terrenos, están enclavados en el llamado Saso de la Alberca y en una parcela de aproximadamente 40 hectáreas de secano. 
					<br/>Cuenta el centro con todos los servicios suficientes para una población escolar de 1.000 alumnos internos y 200
					externos: dormitorios, comedores, salón de actos, capilla, aulas, talleres, laboratorios,
					biblioteca, cafetería e instalaciones deportivas.
					<br/>
					Destaca el salón de actos, cubierto por una estructura piramidal de gran altura, al cual se
					accede por las puertas exteriores y por un amplio vestíbulo, en cuyo interior, con
					perfectas condiciones acústicas y de visibilidad, se acomodan ampliamente más de
					1.000 personas.

					<div class="alert alert-secondary" style="width: 250px; padding: 12px;">
						Carretera de Cuarte S/N . <br/>
						22071 - Huesca 
						<a href="https://www.google.es/maps/place/IES+Pir%C3%A1mide/@42.116672,-0.447875,15z/data=!4m5!3m4!1s0x0:0x5e1794a9ff04023a!8m2!3d42.116672!4d-0.447875" rel="nofollow" target="_blank">
							 - <u>Como llegar</u>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>