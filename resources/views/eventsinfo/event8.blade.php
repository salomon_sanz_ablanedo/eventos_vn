{{-- <div class="row">
  	<div class="col-md-12 no-padding">                        
        <div class="panel">        
            <div class="panel-body">
            	<h3 class="header">Información</h3>
			</div>
		</div>
	</div>
</div> --}}

<div class="row">
    <div class="col-md-12">
    	<div class="panel">        
            <div class="panel-body">
            	<h3 class="header">Presentación</h3>
            	<p>
            		Te invitamos a que apartes estos días para buscar juntos a Dios, serán días excepcionales.
		    		Por tanto, empieza ya a orar y a prepararte para este tiempo.
		    		<br/>
		    		<br/>
		    		Todos somos hijos, pero cada uno de nosotros tenemos un concepto y entendimiento muy diferente de lo que significa la extraordinaria condición de ser HIJO.
		    		<br/>
		    		<br/>
		    		 Acudamos a este encuentro dispuestos a disfrutar de ser hijos amados y obedientes del Dios Eterno, nuestro Padre celestial.
		    	</p>
			</div>
		</div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
    	<div class="panel">        
            <div class="panel-body">
            	<div class="col-md-4">
            		<img src="http://www.navarrainformacion.es/wp-content/uploads/2015/04/espacio.jpg" style="max-width:100%;"/>
            	</div>
            	<div class="col-md-8">
            		<h2 class="header">Ubicación</h2>

            		<p>
            		Auditorio de Barañáin es un espacio de reciente construcción (2003). Unas instalaciones modernas, llenas de oportunidades capaces de solventar cualquier necesidad comunicativa o representativa. 
            		<br/>
En 2005 recibió el premio a la Programación Más Innovadora en España y al año siguiente el Premio a las Buenas Prácticas Medioambientales otorgado por el Gobierno de Navarra.

            		<div style="width: 200px; padding: 12px;">
            			<b>Dirección:</b><br/>
            		Calle Comunidad de Navarra, 2 <br/>
            		31010 Barañain – Navarra
            		</div>
	            	<p>
                        <a href="http://www.auditoriobaranain.com/" rel="nofollow" target="_blank">
                            <i class="fa fa-link"></i> <u>Web oficial</u>
                        </a>
                        &nbsp; | &nbsp;
	            		<a href="https://www.google.es/maps/place/Auditorio+Bara%C3%B1a%C3%ADn/@42.8053533,-1.6906225,16.2z/data=!4m5!3m4!1s0x0:0x89c19f6d63f23bf2!8m2!3d42.8049337!4d-1.6883326" rel="nofollow" target="_blank">
	            			<i class="fa fa-map-marker"></i> <u>Como llegar</u>
	            		</a>
	            		
			    	</p>
		    	</div>
			</div>
		</div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
    	<div class="panel">        
            <div class="panel-body">

            	<div class="col-md-12">
            		<h2 class="header"><i class="fa fa-clock-o"></i> Horario</h2>
            		<br/>
            		<table class="table">
            			<tr>
            				<td>
            					<b>Viernes 30</b>
            				</td>
            				<td><b>Sábado 31</b></td>
            				<td><b>Domingo 1</b></td>
            			</tr>
            			<tr>
            				<td>8 a 11 - Recepción</td>
            				<td>&nbsp;</td>
            				<td></td>
            			</tr>
            			<tr>
            				<td>11:00 - Devocional y plenaria</td>
            				<td>10:00 - Devocional y plenaria</td>
            				<td>10:00 - Culto de adoración</td>
            			</tr>
            			<tr>
            				<td>13:30 - Descanso</td>
            				<td>13:30 - Descanso</td>
            				<td> </td>
            			</tr>
            			<tr>
            				<td>14:00 - Comida y tiempo libre</td>
            				<td>14:00 - Comida y tiempo libre</td>
            				<td>14:00 - Comida o picnic</td>
            			</tr>
            			<tr>
            				<td>16:00 - Talleres</td>
            				<td>16:00 - Talleres</td>
            				<td>16:00 - Salida</td>
            			</tr>
            			<tr>
            				<td>18:00 - Descanso</td>
            				<td>18:00 - Descanso</td>
            				<td></td>
            			</tr>
            			<tr>
            				<td>18:30 - Plenaria</td>
            				<td>18:30 - Plenaria</td>
            				<td></td>
            			</tr>
            			<tr>
            				<td>21:00 - Cena</td>
            				<td>21:00 - Cena</td>
            				<td></td>
            			</tr>
            	</table>
            	</div>
			</div>
		</div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
    	<div class="panel">        
            <div class="panel-body">
            	<div class="col-md-6" style="padding-right: 30px; border-right: 1px solid #CCC;">
            		<h3 class="header"> <i class="fa fa-cutlery"></i>  Comidas y cenas</h3>

            		<p>
            		Las comidas y cenas se realizarán en el frontón de la Asociación Deportiva Lagunak de Barañáin (a 300 metros del auditorio).
            		<br/>
            		<br/>
            		El coste de cada comida o cena, es de 8&euro;, y se puede coger los días que se quieran, es totalmente opcional. El último día habrá opción de elegir comida para llevar (picnik). Todas estas opciones se pueden elegir a la hora de realizar la inscripción online.
            		<br/>
            		<br/>

            		El menú consistirá en: 
						<div style="width: 400px; padding: 0 12px;">
            			<table class="table">
            				<tr class="text-bold">
            					<td>Dia</td>
            					<td>Comida</td>
            					<td>Cena</td>
            				</tr>
            				<tr>
            					<td>Viernes 30</td>
            					<td> 
                                    Menestra de verduras<br>
                                    Guisado de ternera
                                 </td>
            					<td> 
                                    Cardo con jamón <br/>
                                    Tortilla de patatas
                                </td>
            				</tr>
            				<tr>
            					<td>Sábado 31</td>
            					<td> 
                                    Espaguetis boloñesa <br/>
                                    Pavo en salsa
                                </td>
            					<td> 
                                    Ensalada de Chaca <br/>
                                    Merluza rebozada con pimientos verdes
                                 </td>
            				</tr>
            				<tr>
            					<td>Domingo 1</td>
            					<td> 
                                    2 bocadillos <br/>
                                    Fruta. Pastelito. <br/>
                                    Botellín agua. <br/>
                                 </td>
            					<td> -- </td>
            				</tr>
            			</table>
                        <br/>
                        <br/>
                        Postres: Fruta del tiempo, yogures variados, pastel de manzana, flan, natillas, tarta de bizcocho, hojaldre y crema<br>
                        Bebidas: agua (Se podrán comprar refrescos).
            		</div>
		    	</div>
            	<div class="col-md-6" style="padding-left: 30px;"">
            		<h3 class="header"><i class="fa fa-child"></i> Guarderías</h3>

            		<p>
            		Habrá disponible guarderías para los niños desde 1 a 12 años inclusive.
            		<br/>
            		Los niños usarán un peto de un color específico según la edad, el cual puede ser adquirido al realizar la inscripción, por el precio de 1&euro;. No obstante podrán llevar sus propios petos mientras sean de los siguientes colores:
            		<br/>
            		<br/>
            		<div style="width: 400px; padding: 0 12px;">
            			<table class="table">
            				<tr>
            					<td>de 0 a 2</td>
            					<td>Verde oscuro (talla pequeña)</td>
            					<td><button class="img-circle" style="background-color:yellow; width:22px; height:20;">&nbsp;</span></td>
            				</tr>
            				<tr>
            					<td>de 3 a 4</td>
            					<td>Azul marino (talla pequeña)</td>
            					<td><button class="img-circle" style="background-color:orange; width:22px; height:20;">&nbsp;</span></td>
            				</tr>
            				<tr>
            					<td>de 5 a 6</td>
            					<td>Amarillo (talla grande)</td>
            					<td><button class="img-circle" style="background-color:red; width:22px; height:20;">&nbsp;</span></td>
            				</tr>
            				<tr>
            					<td>de 7 a 8</td>
            					<td>Naranja (talla grande)</td>
            					<td><button class="img-circle" style="background-color:#00c0ffd6; width:22px; height:20;">&nbsp;</span></td>
            				</tr>
            				<tr>
            					<td>de 9 a 12</td>
            					<td>Verde chillón, no botella (talla grande)</td>
            					<td><button class="img-circle" style="background-color:#34d234; width:22px; height:20;">&nbsp;</span></td>
            				</tr>
            			</table>
            		</div>
		    	</div>
			</div>
		</div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel">        
            <div class="panel-body">
                <h2 class="header"> <i class="fa fa-cutlery"></i>  Alojamiento</h2>
                <br/>
                <p class="text-info">
                    Cada interesado debe hacer directamente la reserva en estos alojamientos.
                    <br> A la hora de hacer la reserva es importante decir que se va por el evento, para obtener acceso a los precios especiales abajo descritos.
                </p>
                <hr/>

                @foreach($event->lodgements as $l)
                @php
                    $l = $l->lodgement;
                @endphp
                <div class="row">
                    <div class="col-md-12">
                            <div class="col-md-3">
                                <img src="{{ asset('img/lodgements/'.$l->id.'.jpg') }}" style="max-width:100%;" />
                            </div>
                            <div class="col-md-4">
                                <h3>{{ $l->name }}</h3>
                                <br>
                                {!! nl2br(e($l->address)) !!}
                                <br>
                                <br>

                                {!! nl2br(e($l->info)) !!}

                                <br>
                                <br>
                                <a href="{{ $l->maps }}" rel="nofollow" target="_blank">
                                    <i class="fa fa-map-marker"></i> <u>Ubicación</u>
                                </a> &nbsp;

                                @if (!empty($l->web))
                                 | &nbsp;
                                <a href="{{ $l->web }}" rel="nofollow" target="_blank">
                                    <i class="fa fa-link"></i> <u>Web</u>
                                </a>
                                @endif
                            </div>
                            <div class="col-md-5" style="padding-top:40px;">
                                <p class="small">{!! nl2br(e($l->html_info)) !!}</p>
                            </div>
                      </div>
                </div>
                <hr/>
                @endforeach
            </div>
        </div>
    </div>
</div>
