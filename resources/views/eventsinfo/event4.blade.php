<div class="row">
  
  	<div class="col-md-12">                        
        <div class="panel">        
            <div class="panel-body">

				<div style="line-height: normal;">
					<h2 style="text-align: center;"><span style="color: #000000;">INFORMACIÓN DEL</span></h2>
					<h3 style="text-align: center;"><span style="color: #000000;">RETIRO EN HUESCA</span></h3>
					<h3 style="text-align: center;"><span style="color: #000000;">IGLESIAS EN RESTAURACIÓN</span></h3>
					<h4 style="text-align: center;"><span style="color: #000000;">del 13 al 16 de Abril de 2017</span></h4>
				</div>
				<div class="gkblock-4" style="border: dotted #cd2018; font-size: 16px; background-color: transparent;">
					<span style="color: #000000;"><span style="font-size: 18pt;">Imprescindible:</span>&nbsp;Para asistir al Retiro es necesario inscribirse antes del <span style="font-size: 18pt;"><strong>3 de Abril</strong></span>,&nbsp;aunque sea sólo un día. Incluido niños y bebés, bien a través de esta web <span style="text-decoration: underline;"><a href="http://eventosiglesiasenrestauracion.com/evento/2/informacion"><span style="color: #000000; text-decoration: underline;">Pinchando aquí</span></a></span> o del formulario entregándolo debidamente cumplimentado al responsable designado en tu iglesia para que ingrese los datos. Una vez inscrito NO se podrán hacer modificaciones.</span>
				</div>
				<br /> <br />
				<div align="center">
					<a href="#donde" style="margin-right: 50px;" title="Dónde es el Retiro"><img src="/images/CLICK.png" alt="CLICK" /></a><a href="#programa" style="margin-right: 50px;" title="Programa para el Retiro en Huesca"><img src="/images/CLOCK.png" alt="CLOCK" /></a><a href="#catering" style="margin-right: 50px;" title="Catering para el Retiro en Huesca"><img src="/images/FOOD.png" alt="FOOD" /></a><a href="#alojamientos" style="margin-right: 50px;" title="Alojamientos"><img src="/images/HOTEL.png" alt="HOTEL" /></a>
				</div>	
			</div>
		</div>
	</div>
</div>

<div class="row">
  	<div class="col-md-12">                        
        <div class="panel">        
            <div class="panel-body">	
            	<div class="box  nomargin"><h3 class="header">Lugar del Retiro</h3>

					<div class="vertical-column-30" style="width: 100%;"><a name="donde"></a>
						<h4 class="gkSmallHeader" style="text-align: center;">&nbsp;</h4>
					</div>
					<div>
						<div class="vertical-column-70" style="padding: 0px 40px !important; float: left; width: 40%; line-height: normal;">
							<p style="text-align: justify; padding: 0px 30px 0px 0px;">Este año el retiro de iglesias en restauración se celebrará en Huesca, en el IES Pirámide. Se encuentra a 3km de la ciudad de Huesca, en la Carretera de Cuarte , s/nº.<br /> El acceso es sencillo:<br /> <br /> - Desde Zaragoza por la autovía A-23 (salida 567 sentido Huesca y 568 sentido Zaragoza)<br /><br /> - Desde Lerida por la autovía A-22 en construcción y N-240<br /><br /> - Desde Pamplona por la autovía A-22 en construcción y la carretera A-132<br /><br /> - Desde Francia por la carretera N-330</p>
						</div>
						<div style="float: left; width: 50%;">

							<div id='mapbody77_rvyor_0' style="display: none; text-align:center">
								<div id="googlemap77_rvyor_0" class="map" style="margin-right: auto; margin-left: auto; width:400px; height:350px;">
								</div>
							</div>
							<a href="#TB_inline?inlineId=renfe&amp;height=500&amp;width=700&amp;modal=1" rel="sexylightbox" title=""> </a>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>					
						
<div class="row">
  	<div class="col-md-12">                        
        <div class="panel">        
            <div class="panel-body">
            	<h3 class="header">HORARIO</h3>
				<div class="vertical-column-30" style="float: left; width: 21%;">
					<div class="gkblock-7" style="width: 100%;">
						<a name="programa"></a><span style="font-size: 16pt;">DIA 13. Jueves</span>
						<table style="width: 100%; height: 124px;" border="0">
						<tbody style="padding: 2px">
						<tr>
						<td><strong>16:00</strong></td>
						<td>Recepción</td>
						</tr>
						<tr>
						<td valign="top"><strong>18:00</strong></td>
						<td valign="top">Plenaria</td>
						</tr>
						<tr>
						<td><strong>21:00</strong></td>
						<td>Cena</td>
						</tr>
						</tbody>
						</table>
					</div>
				</div>
				<div class="vertical-column-30" style="float: left; width: 25%;">
						<div class="gkblock-7"><a name="programa"></a><span style="font-size: 16pt;">DIA 14. Viernes</span>
						<table style="width: 100%; height: 209px;" border="0">
						<tbody>
						<tr>
						<td><strong>8h a 9h</strong></td>
						<td>Desayuno</td>
						</tr>
						<tr>
						<td valign="top"><strong>10:00</strong></td>
						<td valign="top">Devocional<br /> Plenaria</td>
						</tr>
						<tr>
						<td><strong>13:30</strong></td>
						<td>Descanso</td>
						</tr>
						<tr>
						<td><strong>14:00</strong></td>
						<td>Comida</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>Tiempo Libre</td>
						</tr>
						<tr>
						<td><strong>16:00</strong></td>
						<td>Talleres</td>
						</tr>
						<tr>
						<td><strong>18:00</strong></td>
						<td>Descanso</td>
						</tr>
						<tr>
						<td><strong>18:30</strong></td>
						<td>Plenaria</td>
						</tr>
						<tr>
						<td><strong>21:00</strong></td>
						<td>Cena</td>
						</tr>
						<tr>
						<td><strong>22:00</strong></td>
						<td>Veladas</td>
						</tr>
						</tbody>
						</table>
			</div>
			</div>
			<div class="vertical-column-30" style="float: left; width: 25%;">
			<div class="gkblock-7"><a name="programa"></a><span style="font-size: 16pt;">DIA 15. Sábado</span>
			<table style="width: 100%; height: 209px;" border="0">
			<tbody>
			<tr>
			<td><strong>8h a 9h</strong></td>
			<td>Desayuno</td>
			</tr>
			<tr>
			<td valign="top"><strong>10:00</strong></td>
			<td valign="top">Devocional<br /> Plenaria</td>
			</tr>
			<tr>
			<td><strong>13:30</strong></td>
			<td>Descanso</td>
			</tr>
			<tr>
			<td><strong>14:00</strong></td>
			<td>Comida</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			<td>Tiempo Libre</td>
			</tr>
			<tr>
			<td><strong>16:00</strong></td>
			<td>Talleres</td>
			</tr>
			<tr>
			<td><strong>18:00</strong></td>
			<td>Descanso</td>
			</tr>
			<tr>
			<td><strong>18:30</strong></td>
			<td>Plenaria</td>
			</tr>
			<tr>
			<td><strong>21:00</strong></td>
			<td>Cena</td>
			</tr>
			<tr>
			<td><strong>22:00</strong></td>
			<td>Veladas</td>
			</tr>
			</tbody>
			</table>
			</div>
			</div>
			<div class="vertical-column-30" style="float: left; width: 29%;">
			<div class="gkblock-7" style="width: 185px;"><a name="programa"></a><span style="font-size: 16pt;">DIA 16. Domingo</span><br />
			<table style="width: 175px; height: 124px;" border="0">
			<tbody>
			<tr>
			<td style="padding: 0px 0px !important;"><strong>8h a 9h</strong></td>
			<td>Desayuno</td>
			</tr>
			<tr>
			<td valign="top"><strong>10:00</strong></td>
			<td valign="top">Plenaria y<br /> Cena del Señor</td>
			</tr>
			<tr>
			<td><strong>14:00</strong></td>
			<td>Comida o Picnic</td>
			</tr>
			<tr>
			<td><strong>16:00</strong></td>
			<td>Salida</td>
			</tr>
			</tbody>
			</table>
			</div>
			</div>
			</div>
		</div>
	</div>
</div>	
<div style="clear: left;">
<div class="gkblock-7" style="width: 100%; font-size: 24px; background-image: none;">
<h3 class="header">TALLERES</h3>
<div class="vertical-column-30" style="float: left; width: 50%;">
<div class="gkblock-7" style="width: 100%;"><a name="programa"></a><span style="font-size: 18pt;">Taller, Día 14</span>
<table style="width: 100%; height: 124px;" border="0">
<tbody>
<tr>
<td>1. Taller para Matrimonios<br /> (Enfocado a matrimonios jóvenes o futuros matrimonios)</td>
</tr>
<tr>
<td>2. Taller para Padres y Madres<br /> (Enfocado a padres y madres de niños y adolescentes)</td>
</tr>
<tr>
<td>3. Taller para Jóvenes y Adolescentes<br /> (Enfocado a padres y madres de niños y adolescentes)</td>
</tr>
<tr>
<td>4. Taller para Hombres y Mujeres<br /> (Enfocado hacia el verdadero servicio)</td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="vertical-column-30" style="float: left; width: 50%;">
<div class="gkblock-7" style="width: 100%;"><a name="programa"></a><span style="font-size: 18pt;">Taller, Día 15</span>
<table style="width: 100%; height: 124px;" border="0">
<tbody>
<tr>
<td>1. Taller de Alabanza</td>
</tr>
<tr>
<td>2. Taller de intercesión</td>
</tr>
<tr>
<td>3. Taller de predicadores</td>
</tr>
<tr>
<td>4. Taller acerca de los dones del Espíritu Santo</td>
</tr>
<tr>
<td>5. Taller de comunidad de vida<br /> (Dirigido a personas que desean involucrarse en la restauración de vidas)</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>	
</div>
</div></div><div class="box "><h3 class="header">CATERING COMEDOR</h3>

<div class="content">

<div class="custom"  >

	<div>
<p><a name="catering"></a></p>
<p style="line-height: normal; padding: 0px 20px 0px 20px;">Durante el Retiro habrá servicio de catering para desayunar, comer y cenar. Los desayunos, las comidas y cenas serán dentro del IES Pirámide y es una buena ocasión para disfrutar de la compañia unos de otros. Si deseas solicitarlo lo deberás indicar en la inscripción.</p>
<p style="text-align: center;"><strong style="font-size: 20px;">Desayunos a 3€ | Comidas a 8.5€ | Cenas a 8.5€ | Pic-Nic a 8.5€</strong></p>
</div>	
</div>
</div></div>
			</section>
				
			
						
			<section id="gkMainbody">
									<section class="blog">
	
		
		
	
	
	</section>
							</section>
			
			    	</section>
    	
    		</div>
	    
		<section id="gkBottom1" class="gkCols6">
		<div class="gkPage">
			<div>
				<div class="box  gkmod-1 nomargin"><h3 class="header">ALOJAMIENTO</h3>

				<div class="content">

<div class="custom"  >

	<div class="alojamientopiramide">
<a name="alojamientos"></a>
<strong style="font-size: 25px; padding: 20px">I.E.S. Pirámide</strong><br />
<div class="vertical-column-30" style="width: 40%;">
<div class="gkblock-7" style="line-height: normal; color: #CD2018 !important">
<strong style="font-size: 25px">ALOJAMIENTO</strong><br />
<table>
<tbody>
<tr>
<td width="260px">- Habitaciones compartidas<br /> (Camas y/o literas para 3 o 4 personas)<br /> - Baños comunitarios<br /> - <span style="font-size: 14pt;">No hay servicios de toallas</span></td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="vertical-column-30" style="width: 30%">
<div class="gkblock-7" style="line-height: normal; color: #cd2018 !important">
<a name="alojamientos"></a>
<strong style="font-size: 25px">CATERING</strong><br />
<table>
<tbody>
<tr>
<td width="250px">- En el comedor de I.E.S. Pirámide - Huesca se puede pedir el mismo día antes de las 11:00 h.</td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="vertical-column-30" style="width: 28%; line-height: normal"">
<table>
<tbody>
<tr>
<td width="150px" style="text-align: center">
<div style="width: 160px; height:65px; border: 2px solid #BE1621; vertical-align: center">
RETIRO COMPLETO
<strong style="font-size: 34px">100€</strong>
</div>
</td>
</tr>
<tr>
<td width="150px" style="text-align: center">
<div style="width: 160px; height:65px; border: 2px solid #BE1621; vertical-align: center">
POR DÍA<br>
<strong style="font-size: 34px">34€</strong>
</tr>
</tbody>
</table>
</div>
</div>
<div><strong style="font-size: 20px">IMPORTANTE: El alojamiento en la Pirámide está sujeto al pago y a la disponibilidad de plazas</strong></div>	
</div>
</div></div>
			</div>
		</div>
	</section>
	    
        <section id="gkBottom2" class="gkCols6">
    	<div class="gkPage">
    		<div>
    			<div class="box  gkmod-1 nomargin"><h3 class="header">OTROS ALOJAMIENTOS</h3>

    			<div class="content">

<div class="custom"  >

	<div>
<p>Cada interesado debe realizar DIRECTAMENTE LA RESERVA de estos alojamientos. Hay precios especiales para los asistentes al retiro. Para que nos apliquen las tarifas negociadas en los hoteles, cuando hagamos las reservas, tenemos que decir que somos del Retiro Evangélico.</p>
</div>
<div style="width: 100%;">
<div id="hotelabba" style="height: 400px;">
<div style="float: left; line-height: normal; width: 50%;">HOTEL ABBA ****<br /> Telf.: 974 292 900<br /> <span id="cloak9053">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak9053').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy9053 = 'h&#117;&#101;sc&#97;' + '&#64;';
 addy9053 = addy9053 + '&#97;bb&#97;h&#111;t&#101;l&#101;s' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak9053').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy9053 + '\'>' + addy9053+'<\/a>';
 //-->
 </script><br /> www.abbahuescahotel.com<br />
<table>
<tbody>
<tr>
<td><span style="color: #000000;">Habitación individual</span></td>
<td><span style="font-size: 24px; color: #cc0033;">75,00 €</span></td>
</tr>
<tr>
<td><span style="color: #000000;">Habitación doble</span></td>
<td><span style="font-size: 24px; color: #cc0033;">85,00 €</span></td>
</tr>
<tr>
<td><span style="color: #000000;">Habitación triple</span></td>
<td><span style="font-size: 24px; color: #cc0033;">105,00 €</span></td>
</tr>
<tr>
<td><span style="color: #000000;">Habitación familiar</span></td>
<td><span style="font-size: 24px; color: #cc0033;">125,00 €</span></td>
</tr>
</tbody>
</table>
*Desayuno buffet, parking e IVA incluido en los precios.</div>
<div style="float: right; width: 50%;"><img src="/images/HOTEL%20ABBA.jpg" alt="HOTEL ABBA" width="400" height="300" /></div>
</div>
<div id="apartahotel" style="height: 500px;">
<div style="float: left; width: 50%;"><img src="/images/APARTAHOTEL.jpg" alt="APARTAHOTEL" width="400" height="300" /></div>
<div style="float: right; line-height: normal; width: 50%;">APARTAHOTEL ***<br /> Telf.: 974 239 945<br /> <span id="cloak68436">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak68436').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy68436 = '&#105;nf&#111;' + '&#64;';
 addy68436 = addy68436 + '&#97;p&#97;rt&#97;h&#111;t&#101;lh&#117;&#101;sc&#97;' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak68436').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy68436 + '\'>' + addy68436+'<\/a>';
 //-->
 </script>&gt;<br /> www.apartahotelhuesca.com<br />
<table>
<tbody>
<tr>
<td><span style="color: #000000;">&nbsp; Apartamento 1 pax</span></td>
<td><span style="font-size: 24px; color: #cc0033;">50,00 €</span></td>
</tr>
<tr>
<td><span style="color: #000000;">&nbsp; Apartamento 2 pax</span></td>
<td><span style="color: #000000;"><span style="font-size: 24px; color: #cc0033;">58,00 €</span></span></td>
</tr>
<tr>
<td><span style="color: #000000;">&nbsp; Apartamento 3 pax</span></td>
<td><span style="color: #000000;"><span style="font-size: 24px; color: #cc0033;">81,00 €</span></span></td>
</tr>
<tr>
<td><span style="color: #000000;">&nbsp; Apartamento 4 pax</span></td>
<td><span style="color: #000000;"><span style="font-size: 24px; color: #cc0033;">96,00 €</span></span></td>
</tr>
<tr>
<td><span style="color: #000000;">&nbsp; Apartamento 5 pax</span></td>
<td><span style="color: #000000;"><span style="font-size: 24px; color: #cc0033;">111,00 €</span></span></td>
</tr>
<tr>
<td><span style="color: #000000;">&nbsp; Apartamento 6 pax</span></td>
<td><span style="color: #000000;"><span style="font-size: 24px; color: #cc0033;">126,00 €</span></span></td>
</tr>
</tbody>
</table>
*Precios por día IVA incluido. Régimen sólo alojamiento. Uso de cocina: bajo petición, 15,00 € toda la estancia. Existe la opción de desayuno buffet 6,50€ persona/día.</div>
</div>
<div id="posadaluna" style="height: 450px;">
<div style="float: left; width: 50%;">POSADA DE LA LUNA ***<br /> <span id="cloak67118">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak67118').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy67118 = 'r&#101;c&#101;pc&#105;&#111;n' + '&#64;';
 addy67118 = addy67118 + 'p&#111;s&#97;d&#97;d&#101;l&#97;l&#117;n&#97;' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak67118').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy67118 + '\'>' + addy67118+'<\/a>';
 //-->
 </script><br /> www.posadadelaluna.com<br />
<div><span style="color: #000000;">Habitación individual <span style="font-size: 24px; color: #cc0033;">51,00 €</span></span></div>
<div><span style="color: #000000;">Habitación doble <span style="font-size: 24px; color: #cc0033;">65,00 €</span></span></div>
<div><span style="color: #000000;">Habitación triple <span style="font-size: 24px; color: #cc0033;">87,00 €</span></span></div>
<div><span style="color: #000000;">*Desayuno e IVA incluido en los precios.</span></div>
</div>
<div style="float: right; width: 50%;"><img src="/images/hotelposadaluna.jpg" alt="hotelposadaluna" width="300" height="200" /></div>
</div>
<div id="joaquincosta" style="heigth: 400px;">
<div style="float: left; width: 50%;"><img src="/images/joaquincosta.jpg" alt="joaquincosta" width="278" height="191" /></div>
<div style="float: right; width: 50%;">HOSTAL JOAQUIN COSTA **<br /> Telf.: 974 241 774<br />
<div><span style="font-size: 24px; color: #cc0033;"><span style="font-size: 16px; color: #000000;">Habitación individual</span> 42,00 €</span></div>
<div><span style="font-size: 24px; color: #cc0033;"><span style="font-size: 16px; color: #000000;">Habitación doble</span> 52,00 €</span></div>
<div><span style="font-size: 16px; color: #000000;"><span style="color: #000000;">Habitación trip</span></span><span style="font-size: 16px; color: #000000;"><span style="color: #000000;">le</span>&nbsp; <span style="font-size: 24px; color: #cc0033;"></span><span style="font-size: 24px; color: #cc0033;">70,00 €</span><br /></span></div>
<div>&nbsp;</div>
<div style="text-align: justify;"><span style="color: #000000;">*Desayuno e IVA incluido en los precios.</span></div>
</div>
</div>
</div>	
</div>
</div></div>
    		</div>
    	</div>
    </section>
        
        <section id="gkBottom3" class="gkCols6">
    	<div class="gkPage">
    		<div>
    			<div class="box  gkmod-1 nomargin">

    			<div class="content">

<div class="custom"  >

	<div class="gkblock-4" style="background-color: transparent !important; border: 2px solid #CD2018">
<p style="font-size: 28px; text-align: center;">Para inscribirte <a href="http://eventosiglesiasenrestauracion.com/evento/2/informacion">pincha aquí</a></p>
</div>	
</div>
</div></div>
    		</div>
    	</div>
    </section>
        
        <section id="gkBottom4" class="gkCols6">
    	<div class="gkPage">
    		<div>
    			<div class="box  gkmod-1 nomargin">

    			<div class="content">

<div class="custom"  >

	<p style="text-align: center;"><span style="font-size: 14pt; color: #000000;">CONTACTO:</span> <span id="cloak57634">Esta dirección de correo electrónico está siendo protegida contra los robots de spam. Necesita tener JavaScript habilitado para poder verlo.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak57634').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy57634 = 'r&#101;t&#105;r&#111;&#105;gl&#101;s&#105;&#97;s' + '&#64;';
 addy57634 = addy57634 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 var addy_text57634 = 'r&#101;t&#105;r&#111;&#105;gl&#101;s&#105;&#97;s' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak57634').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy57634 + '\'>'+addy_text57634+'<\/a>';
 //-->
 </script>&nbsp;<span style="color: #000000;">/&nbsp;667 347 377 (Vicente Mateo) / 699 45 63 82 (Pili Montes)</span></p>	
</div>
</div></div>
    		</div>
    	</div>
    </section>
        
        <section id="gkSocial">
    	<div class="gkPage">
    		

<div class="custom"  >

	<div>
<h3>ORGANIZA</h3>
<div style="width: 50%; float: left"><img src="/images/logojovenes.jpg" alt="logojovenes" width="100" height="80" /><br>Asociación Jóvenes para Cristo</div>
<div style="width: 50%; float: right"><img src="/images/LOGO_IG.jpg" alt="LOGO IG" width="100" height="97" /><br>I.E.B. de Pamplona</div>
</div>	
</div>

    	</div>
    </section>
        
    
        
    
<footer id="gkFooter" class="gkPage">
		<p class="gkCopyrights">Copyright-Asociación Jóvenes para Cristo / Iglesia Evangélica Bautista de Pamplona</p>
		
		
	</footer>     --}}	
   	
