<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title></title>
  @@css
  
  @stack('styles')

  <!-- JS HEADER--> 
  @stack('js')
  @@js_header
</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
      @yield('naked_content')

  @@js_footer
  @@js_blade
  @stack('scripts')

</body>
</html>
