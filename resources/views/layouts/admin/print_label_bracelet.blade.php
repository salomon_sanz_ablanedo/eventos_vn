@php
 
$width = $size == 'kids'? 17.78: 27.8;
$margin_left = $size == 'kids'? 3: 5;
$right_width = $size == 'kids' ? 9.5 : 11;
$label_height = 2.461;
@endphp

<!DOCTYPE html>
<html lang="es" style="margin:0px;padding:0px;">
	<meta charset="utf-8">
  	<head>
    <style>
      html {
        box-sizing: border-box;
      }
      body,html{
        width:{{ $width }}cm;
        margin:0px;
        padding:0px;
      }
      *, *:before, *:after {
        box-sizing: inherit;
      }

      body{
        overflow-x:hidden; 
      }

      h1,h2,h3,h4{
        margin: 0px;
        padding: 0px;
      }
      .label-wrapper{
        height: {{ $label_height }}cm;
        margin-left: {{ $margin_left }}cm;
        padding: 2mm;
        clear: both;
        overflow: hidden;
      }

      .label{
        height: 100%;
      }

      .label .left, .label .right{
        border: 1px solid #000;
        border-radius: 7px;
        height: 100%;;
      }

      .label .left{
        float:left;
        width:2cm;
        text-align: left;
      }

      .label .left .group_number{
        font-weight: bold;
        font-size: 20px;
        text-align: center;
        letter-spacing: 3px;
      }

      .right{
        float: left;
        display:flex;
        width:{{ $right_width }}cm;
        text-align: left;
        margin-left: 10px;
      }

      .label .right .childname{
        font-size: 4mm;
        width: 100%;
        font-weight: bold;
        text-overflow: ellipsis; 
        white-space: nowrap;
        overflow: hidden;
        text-transform: uppercase;
      }

      .barcode_wrapper {
        /* width: 3.5cm;       */
        padding:0 .7cm;
        text-align: center;
      }

      .user_data{
        font-size: 3mm;
        padding-top: 3px; 
        overflow: hidden;
        text-overflow: ellipsis;
        flex-grow: 1;
      }

      .bracelet_type{
        font-size: 12px;
        text-align: center;
      }

      /* @font-face {
          font-family: 'idautomationhc39mregular';
          src: url('{{ asset('fonts/codebar/idautomationhc39m.woff2') }}') format('woff2'),
               url('{{ asset('fonts/codebar/idautomationhc39m.woff') }}') format('woff');
          font-weight: normal;
          font-style: normal;
      }       */
    </style>
  </head>

  <body>
    @foreach($inscriptions as $insc)
    <div class="label-wrapper">
  	<div class="label">

      @php
      $user_age = \DateUtils::getYearsDiff($insc->birthday, $event_date_from);
      @endphp
      <div class="left">
        <div class="group_number">            
          {{ $insc->inscription_id }}
        </div>
        <div>
          <hr style="margin:2px;"/>
          <div class="bracelet_type">
          @if ($user_age < 13)
            INFANTIL
          @else
            ADULTO
          @endif
          </div>
        </div>
      </div>
      <div class="right">
        <div class="barcode_wrapper codebar">
         <svg class="img_barcode" style="width:150px;" 
          jsbarcode-value="{{ $insc->public_id }}"
          jsbarcode-textmargin="0"
          jsbarcode-width="2"
          jsbarcode-height="40"
          jsbarcode-displayValue="true"
          ></svg>

        </div>
        <div class="user_data">
            <div class="childname">
              {{ $insc->name }}
              @if ($user_age > 12)
                {{ $insc->surname }}
              @endif
            </div>
            {{ $insc->church->city }}, {{ $user_age }} años
            <br/>
            @if ($user_age < 13)              
              (@if ($insc->inscription->lider_relationship_type != 'OTROS')
              <i>{{$insc->lider_relationship_type}}</i> de 
              @endif
              <i>{{$insc->inscription->contact_name}} </i>  - {{ $insc->inscription->contact_phone }})
            @endif
        </div>
      </div>
	</div>
</div>
    @endforeach
    <script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/barcodes/JsBarcode.code128.min.js"></script>
    <script defer>
      JsBarcode(".img_barcode").init();
      // JsBarcode(".img_barcode", {
      //   format: "pharmacode",
      //   //lineColor: "#0aa",
      //   width: 4,
      //   height: 40,
      //   displayValue: true
      // });
       (function(){

        @if ($print == 'true')
            window.print();
        @endif

        @if ($auto_close == 'true')
            //setTimeout(function () { window.close(); }, 100);
        @endif
        })();
    </script>
    {{-- <script src="{{ asset('/js/labels.js') }}" defer></script> --}}
  </body>
</html>