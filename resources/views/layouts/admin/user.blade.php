@extends('layouts.admin.naked')

@section('naked_content')

    <h2>{!! $h1 or '' !!}</h2>

    <br>

    <ul class="nav navbar-top-links pull-left clear-both" style="background-color:#FFF; clear:both; width:100%;">                                             
        <li class="{{Route::getCurrentRoute()->getName()=='admin.usuario.inicio'?'open':''}} btn-active-dark">
            <a class="mainnav" href="{{route('admin.usuario.inicio',$user_id)}}">
                <i class="fa fa-info-circle fa-lg"></i> &nbsp;Datos &nbsp; 
            </a>
        </li>

        <li class="{{Route::getCurrentRoute()->getName()=='admin.usuario.inscripciones'?'open':''}}">
            <a class="mainnav" href="{{route('admin.usuario.inscripciones',$user_id)}}">
                <i class="fa fa-pencil-square-o fa-lg"></i> &nbsp;Inscripciones 
                <!--<span class="badge" style="background-color:red; font-weight:bold;"><small>4</small></span>-->
            </a>       
        </li>

        <li class="{{Route::getCurrentRoute()->getName()=='admin.usuario.acciones'?'open':''}}">
            <a class="mainnav" href="{{route('admin.usuario.acciones',$user_id)}}">
                <i class="fa fa-flash fa-lg"></i> &nbsp;Acciones &nbsp; 
            </a>       
        </li>                                  
    </ul>

    <div style="clear:both;"></div>

     @if (Session::has('alert'))
        <div id="page_msg_alert" class="alert alert-{{Session::get('alert-type','info')}} alert-dismissible" role="alert" style="margin-top:12px; margin-bottom:-8px;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong></strong> {{Session::get('alert')}}
        </div>
        <script>
            setTimeout(function(){
                $("#page_msg_alert").alert('close');
            },3000);
        </script>
    @endif

    <br/>
    {!! $user_content or '' !!}

@stop