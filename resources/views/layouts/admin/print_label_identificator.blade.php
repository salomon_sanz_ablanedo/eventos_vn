<!DOCTYPE html>
<html lang="es" style="margin:0px;padding:0px;">
    <head>
        <meta charset="utf-8">
        <title>Etiquetas</title>
        <style>

          html {
            box-sizing: border-box;
          }

          *, *:before, *:after {
            box-sizing: inherit;
          }

          body{
            margin:0px;
            padding:0px;
            width:170px;
            overflow-x:hidden; 
            margin-top:0px;
          }

          h1,h2,h3,h4{
            margin: 0px;
            padding: 0px;
          }

          .label{
            height: 119px;
            width:160px;
            padding-top:9px;
            overflow: hidden;
            clear: both;
          }

          .barcode_wrapper {
            width: 100%;
            font-family: 'idautomationhc39mregular';
            font-size: 20px;        
            padding-top:2px;
            text-align: center;
          }

          .user_data{
            font-size: 12px;
            text-align: center;
            min-height: 30px;
          }

          .bracelet_type{
            font-size: 14px;
            text-align: center;
          }


          @font-face {
                font-family: 'idautomationhc39mregular';
                src: url('http://eventosiglesiasenrestauracion.com/fonts/codebar/idautomationhc39m.woff2') format('woff2'),
                     url('http://eventosiglesiasenrestauracion.com/fonts/codebar/idautomationhc39m.woff') format('woff');
                font-weight: normal;
                font-style: normal;
            }   
        </style>
    </head>
    <body>
        @foreach($inscriptions as $insc)
            <div class="label">
                <div class="user_data">{{ $insc->inscription_id }} - {{ $insc->name }} {{ $insc->surname }}</div>
                <div class="barcode_wrapper">*{{ $insc->public_id }}*</div>
            </div>  	
        @endforeach

    </body>
</html>
    
    <script>
    (function(){
    
        @if ($print == 'true')
            window.print();
        @endif

        @if ($auto_close == 'true')
            setTimeout(function () { window.close(); }, 100);
        @endif
    })();
    </script>
</script>