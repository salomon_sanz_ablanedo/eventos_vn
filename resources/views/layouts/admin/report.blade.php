<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Informe</title>
	<link href="{{asset('css/reports.css')}}" rel="stylesheet">
</head>
<body>
	<h2>{{$report->event->name}}</h2>
	<strong>{{$report->title}}</strong> a {{date('d/m/Y h:m:i')}}
	<br/>
	<br/>
	<fieldset>
		<legend>Par&aacute;metros</legend>
		<ul>
			@foreach ($report->params_mapped as $key=>$value)
			@if ($key !='output_type')
			<li>
				<b>{{$key}}:</b> <i>{{$value}}</i>
			</li>	
			@endif
			@endforeach
		</ul>
		@if (!empty($report->note))
		<small>* {{$report->note}}</small>
		@endif
	</fieldset>
	<br/>
	@section('content')
	@show
</body>
<script>
	@if ($print == true)
	window.onload = function(){
		window.print();
	}
	@endif
</script>