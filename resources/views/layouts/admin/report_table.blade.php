@extends('layouts.admin.report')
@section('content')
	<h3>Total: {{count($report->data_rows)}}</h3>
	<table>
		<thead>
			<tr>
				<th></th>
				@foreach($report->data_headers as $field)
				<th>{{$field}}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			<?php $a = 1; ?>
			@foreach ($report->data_rows as $row)
			<tr>
				<td><small>#{{$a++}}</small></td>
				@foreach($row as $r)
				<td class="uppercase">{{$r}}</td>
				@endforeach
			</tr>
			@endforeach
		</tbody>
	</table>
@stop