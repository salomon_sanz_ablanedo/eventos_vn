@extends('layouts.admin.naked')

@section('naked_content')

    <div id="container" class="container-fluid" style="padding:15px;">

    <h2>{{ $h1_inscription }}</h2>

    <br>

    <ul class="nav navbar-top-links pull-left clear-both" style="background-color:#FFF; clear:both; width:100%;">                                             
        <li class="{{ Route::getCurrentRoute()->getName()=='admin.inscripcion.inicio'?'open':''}} btn-active-dark">
            <a class="mainnav" href="{{ route('admin.inscripcion.inicio',$inscription->inscription_id) }}">
                <i class="fa fa-info-circle fa-lg"></i> &nbsp;Inscripción/es &nbsp; 
            </a>
        </li>

        <li class="{{ Route::getCurrentRoute()->getName()=='admin.inscripcion.anotaciones'?'open':''}}">
            <a class="mainnav" href="{{ route('admin.inscripcion.anotaciones',$inscription->inscription_id) }}">
                <i class="fa fa-comments fa-lg"></i> &nbsp;Anotaciones
                @if ($notes_count > 0)
                <span class="badge badge-danger" style="font-weight:bold;"><small>{{$notes_count}}</small></span>
                @endif
            </a>       
        </li>

        <li class="{{ Route::getCurrentRoute()->getName()=='admin.inscripcion.cargos'?'open':''}}">
            <a class="mainnav" href="{{ route('admin.inscripcion.cargos',$inscription->inscription_id) }}">
                <i class="fa fa-usd fa-lg"></i> &nbsp;Cargos / Pagos &nbsp; 
            </a>
        </li>


        <li class="{{ Route::getCurrentRoute()->getName()=='admin.inscripcion.acciones'?'open':''}}">
            <a class="mainnav" href="{{ route('admin.inscripcion.acciones',$inscription->inscription_id) }}">
                <i class="fa fa-flash fa-lg"></i> &nbsp;Acciones &nbsp; 
            </a>       
        </li>

        <li class="{{ Route::getCurrentRoute()->getName()=='admin.inscripcion.log'?'open':''}}">
            <a class="mainnav" href="{{ route('admin.inscripcion.log',$inscription->inscription_id) }}">
                <i class="fa fa-book fa-lg"></i> &nbsp;Registro &nbsp; 
            </a>  
        </li>
                                         
    </ul>

    <div style="clear:both;"></div>

     @if (Session::has('alert'))
        <div id="page_msg_alert" class="alert alert-{{Session::get('alert-type','info')}} alert-dismissible" role="alert" style="margin-top:12px; margin-bottom:-8px;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong></strong> {{Session::get('alert')}}
        </div>
        <script>
            setTimeout(function(){
                $("#page_msg_alert").alert('close');
            },3000);
        </script>
    @endif


    {!! $inscription_content !!}

    </div>

@endsection