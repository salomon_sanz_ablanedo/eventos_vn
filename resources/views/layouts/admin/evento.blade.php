@extends('layouts.admin.base')
@section('menu_sup')
<li class="{{Route::getCurrentRoute()->getName()=='admin.eventos.inicio'?'open':''}}">
	<a class="mainnav" href="{{route('admin.eventos.inicio',$event_id)}}">
        <i class="fa fa-dashboard fa-lg"></i> Portada
    </a>
</li>

<li class="{{Route::getCurrentRoute()->getName()=='admin.eventos.inscripciones'?'open':''}}">
    <a class="mainnav" href="{{route('admin.eventos.inscripciones',$event_id)}}">
        <i class="fa fa-users fa-lg"></i> Inscripciones
    </a>	   
</li>

<li class="{{Route::getCurrentRoute()->getName()=='admin.eventos.registros'?'open':''}}">
    <a class="mainnav" href="{{route('admin.eventos.registros',$event_id)}}">
        <i class="fa fa-barcode fa-lg"></i> Registros
    </a>       
</li>

<li class="{{Route::getCurrentRoute()->getName()=='admin.eventos.alojamiento'?'open':''}}">    
    <a class="mainnav" href="{{route('admin.eventos.alojamiento',$event_id)}}">
        <i class="fa fa-bed fa-lg"></i> Alojamiento
    </a>
</li> 

<li class="{{Route::getCurrentRoute()->getName()=='admin.eventos.informes'?'open':''}}">    
    <a class="mainnav" href="{{route('admin.eventos.informes',$event_id)}}">
        <i class="fa fa-newspaper-o fa-lg"></i> Informes
    </a>
</li>   

<li class="{{Route::getCurrentRoute()->getName()=='admin.eventos.estadisticas'?'open':''}}">    
    <a class="mainnav" href="{{route('admin.eventos.estadisticas',$event_id)}}">
        <i class="fa fa-line-chart fa-lg"></i> Estad&iacute;sticas
    </a>
</li>   
 
<li class="{{Route::getCurrentRoute()->getName()=='admin.eventos.configuracion'?'open':''}}">  
    <a class="mainnav" href="{{route('admin.eventos.configuracion',$event_id)}}">
        <i class="fa fa-cog fa-lg"></i> Configuraci&oacute;n
    </a>  
</li>
@stop

@section('content_header')
	@parent
    
     <div style="margin-top:66px;"></div>
@stop

@section('content')		
		{!!$content!!}   	
@stop