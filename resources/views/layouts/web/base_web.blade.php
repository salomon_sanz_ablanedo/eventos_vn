@extends('layouts.web.base')

@section('root_content')
    <nav class="navbar navbar-default navbar-static-top hidden-on-print" id="header_nav_bar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="#">
            <img src="{{asset('img/logo.png')}}" height="49"  style="margin:1px 12px 0 0;"/>
          </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li id="logo">
              <a href="#">
                <img src="{{asset('img/logo.png')}}" height="49"/>
              </a>
            </li>
            <li class="{{ Request::segment(0) == 'evento' ? 'active' : '' }}"><a href="{{ route('web.index',['4']) }}">Eventos</a></li>
            <li class="{{ Request::segment(1) == 'contacto' ? 'active' : '' }}">
              <a href="{{ route('web.contact') }}">Contacto</a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{route('login')}}">Admin</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
    	@yield('content')
    </div> 

    <footer class="footer hidden-on-print">
      <div class="container">
        <p class="text-muted">
          Eventos Iglesias en Restauración. {{ date('Y') }} 
          | <span>Sesión: <small class="text-muted"><i>{{ Session::getId() }}</i></small></span>

          </p>
      </div>
    </footer>
@stop