<!DOCTYPE html>
<html lang="en">
  <head>
    <!--
    <meta charset="utf-8">  
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    -->
    <link rel="icon" href="../../favicon.ico">

    @@css
  
    @stack('styles')

    <!-- JS HEADER--> 
    @@js_header

    <title>@yield('meta_title', 'PEINR')</title>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
      
  </head>

  <body>
    	@section('root_content')
          	@yield('all_content')
      @show

      <!-- JS FOOTER -->  
      @@js_footer

      <!-- JS BLADE-->  
      @@js_blade

      @stack('scripts')
  </body>
</html>