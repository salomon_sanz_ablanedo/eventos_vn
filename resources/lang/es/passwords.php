<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Tu contraseña ha sido reestablecida',
    'sent' => 'Te hemos enviado un email con un enlace para que reestablezcas la contraseña',
    'token' => 'El token para reestablecer la contraseña no es válido.',
    'user' => "No se ha encontrado ningún usuario con ese email.",

];
