
// var $uploadCrop;

// var inscription_form = new Vue({

//     el : '#inscription_form',

//     data : {
//         context          : 'cart',
//         error            : false,
//         loading          : false,

//         url_cart_line        : null,
//         url_cart_line_add    : null,
//         url_inscription_line : null,

//         /*****/
//         inscription_id               : null,
//         line_id                      : null,
//         public_id                    : null,
//         name                         : null,
//         surname                      : null,
//         genre                        : null,
//         user_photo                   : false,
//         birthday                     : null,
//         nif                          : null,
//         nif_rep                      : null,
//         nif_no                       : null,
//         church_id                    : null,
//         event_role                   : null,
//         is_pastor                    : null,
//         lider_relationship_type      : null,
//         lodgement_id                 : null,
//         lodgement_dates              : [],
//         meals                        : null,
//         meals_list                   : [],
//         meals_specials               : null,
//         meals_specials_obs           : null,
//         meals_picnic                 : null,
//         specials_details             : null,
//         specials_details_obs         : null,
//         arrival_departure            : null,
//         arrival_departure_obs        : null,
//         bus                          : null,
//         studio1                      : null,
//         studio2                      : null,
//         sports                       : null,
//         nursery                      : null,
//         nursery_peto                 : 'true',

//     },

//     watch : {

//         nif_no : function(val)
//         {
//             if (val == 'true'){
//                 this.nif     = '';
//                 this.nif_rep = '';
//             }

//         },

//         birthday : function(newval, oldval)
//         {
//             if (this.event.type == 'CAMPAMENTO' && this.age > 3)
//             {
//                 this.lodgement_id = this.lodgement_main.id;
//             }
//         }
//     },

//     created : function()
//     {
//         this.event  = event;
//         this.event_meals = event_meals;
//         this.event_lodgements = event_lodgements;
//         this.lodgement_main  = lodgement_main;
//         this.url_cart_line = url_cart_line;
//         this.url_cart_line_add = url_cart_line_add;
//         this.url_inscription_line = url_inscription_line;

//         this.storable        = ['line_id','name','surname','genre','birthday','nif','nif_no','church_id','event_role','is_pastor','lider_relationship_type','lodgement_id','lodgement_dates','meals','meals_list','meals_specials','meals_specials_obs','meals_picnic','bus','specials_details','specials_details_obs','arrival_departure','arrival_departure_obs','studio1','studio2','sports','nursery','nursery_peto'];
//         this.mandatory_fields = {
//             name         : 'Nombre',
//             surname      : 'Apellidos',
//             genre        : 'Género',
//             birthday     : 'Fecha de nacimiento',
//             church_id    : 'Iglesia'
//         };

//         this.croppieOpts = {
//             enableOrientation : true,
//             viewport: {
//                 width: 150,
//                 height: 166,
//             },
//             enableExif: true,
//             boundary : {
//                 width : 156,
//                 height : 172
//             }
//         };
//     },

//     mounted : function()
//     {
//         setTimeout(this.initCrop.bind(this),1);
//     },

//     computed : {

//         isAdult : function()
//         {
//             if (!this.birthday)
//                 return null;
//             return Utils.getAge(this.birthday.split('/').reverse().join('-'),this.event.date_from) >= 18 ;
//         },

//         isChild : function()
//         {
//             if (!this.birthday)
//                 return null;
//             return Utils.getAge(this.birthday.split('/').reverse().join('-'),this.event.date_from) < 13 ;
//         },

//         isBaby : function()
//         {
//             return Utils.getAge(this.birthday.split('/').reverse().join('-'),this.event.date_from) < 5 ;
//         },

//         age : function()
//         {
//             if (!this.birthday)
//                 return null;
//             return Utils.getAge(this.birthday.split('/').reverse().join('-'),this.event.date_from);
//         },

//         yearOfBirth : function()
//         {
//             return this.birthday.split('/')[2];
//         },

//         showPicnic : function()
//         {
//             if (!this.event_meals.length)
//                 return false;
//             var meals = this.event_meals.filter(function(m){
//                 return m.picnic_opt == 1;
//             });

//             meals = String(meals[0].id);
//             return this.meals_list.length && (this.meals_list.indexOf(meals) >= 0 || this.meals_list.indexOf(parseInt(meals)) >= 0);
//         }
//     },

//     methods : {

//         completeEvent : function()
//         {
//             this.lodgement_id    = '6';
//             this.meals           = 'true';
//             this.lodgement_dates = [];
//             this.meals_list      = [];


//             setTimeout(function(){

//                 $('#div_lodgement_dates input').each(function(index,val){
//                     this.lodgement_dates.push(val.value);
//                 }.bind(this));

//                 $('#div_meals_dates input').each(function(index,val){
//                     this.meals_list.push(val.value);
//                 }.bind(this));

//             }.bind(this),100);

//             $('#msg_event_complete').show();

//             setTimeout(function(){
//                 $('#msg_event_complete').hide();
//             },3000);
//         },

//         load : function(id)
//         {
//             this.line_id = id;
//             this.error = false;
//             this.loading = true;

//             var params = {
//                 line_id : id
//             };

//             var url;

//             if (this.context == 'cart')
//                 url = this.url_cart_line;
//             else
//                 url = this.url_inscription_line;

//             AjaxServices.get(url, params, function(error, result)
//             {
//                 this.loading = false;

//                 if (error)
//                 {
//                     this.error = true;
//                     alert('error al cargar');
//                 }
//                 else
//                 {
//                     for (var key in result)
//                     {
//                         if (this.hasOwnProperty(key))
//                         {
//                             if (typeof result[key] == 'boolean')
//                                 this[key] = result[key] ? 'true' : 'false';
//                             else if (result[key] == null)
//                             {
//                                 if (Object.prototype.toString.call( this[key] ) === '[object Array]')
//                                     this[key] = [];
//                                 else
//                                     this[key] = '';
//                             }
//                             else
//                                 this[key] = result[key];
//                         }
//                     }

//                     this.nif_rep = this.nif;
//                     this.birthday = result.birthday.split('-').reverse().join('/');

//                     if (this.event.require_photo)
//                     {
//                         setTimeout(function(){
//                             this.loadCropImgFromUrl('http://eventosiglesiasenrestauracion.com/uploads/users_photos/'+this.public_id+'.jpeg?r='+Math.random());
//                         }.bind(this),1000);
//                     }
//                 }
//             }.bind(this));

//         },

//         _isEmpty : function(d)
//         {
//             return d == null || (typeof d == 'string' && d.trim() == '');
//         },

//         _validate : function()
//         {
//             var errors = [];

//             for (key in this.mandatory_fields)
//             {
//                 if (this._isEmpty(this[key]))
//                     errors.push(this.mandatory_fields[key]);
//             }

//             // if (this.event.require_photo && (this._isEmpty(this.user_photo) || this.user_photo == false))
//             //    errors.push('Fotografía');

//             if (this._isEmpty(this.nif_no) || this.nif_no == 'false')
//             {
//                 if (this._isEmpty(this.nif) || this._isEmpty(this.nif_rep))
//                 {
//                     if (this._isEmpty(this.nif))
//                         errors.push('NIF/NIE ');

//                     if (this._isEmpty(this.nif_rep))
//                         errors.push('NIF/NIE repetido');
//                 }
//                 else if (this.nif.toUpperCase() != this.nif_rep.toUpperCase())
//                     errors.push('No coincide el NIF/NIE con el repetido')
//                 else if (Utils.validateDNI(this.nif) === false)
//                     errors.push('NIF/NIE (formato inválido)');
//             }

//             if (this.event.external_lodgements && this.event_lodgements.length || (this.event.type == 'CAMPAMENTO' || this.event.type == 'PRECAMPAMENTO'))
//             {
//                 if (this.lodgement_id == null)
//                 {
//                     if ((this.event.type != 'CAMPAMENTO') || (this.event.type == 'CAMPAMENTO' && this.age < 4))
//                         errors.push('No has especificado el alojamiento');
//                 }
//                 else if (this.lodgement_main && this.lodgement_id == this.lodgement_main.id && !this.lodgement_dates.length && (this.event.type != 'CAMPAMENTO' && this.event.type != 'PRECAMPAMENTO'))
//                 {
//                     errors.push('No has marcado ningún día de alojamiento');
//                 }
//             }

//             if (this.event_meals.length && this._isEmpty(this.meals))
//             {
//                 errors.push('No has especificado si comes o no')
//             }
//             else if (this.meals == 'true' && !this.meals_list.length)
//             {
//                 errors.push('No has seleccionado ninguna comida');
//             }

//             if (this._isEmpty(this.meals_specials_obs) && this.meals_specials == 'true' && (this.meals == 'true'
//                 || ((this.event.type == 'CAMPAMENTO' || this.event.type == 'PRECAMPAMENTO') && !this._isEmpty(this.lodgement_id))))
//                 errors.push('Falta indicar las observaciones de comidas especiales');

//             if (this.specials_details == 'true' && this._isEmpty(this.specials_details_obs))
//                 errors.push('Falta indicar las observaciones especiales');

//             if (this.arrival_departure == 'true' && this._isEmpty(this.arrival_departure_obs))
//                 errors.push('Falta indicar las observaciones de salida, llegada o ausencia');

//             if (!this._isEmpty(this.birthday))
//             {
//                 if (this.isAdult && this.event.studios.length)
//                 {
//                     if (this._isEmpty(this.studio1))
//                         errors.push('No has elegido un taller' + (this.event.studios[0].date ? ' para el dia 1' : ''));

//                     if (this.event.studios[0].date && this._isEmpty(this.studio2))
//                         errors.push('No has elegido un taller para el dia 2');
//                 }

//                 if (this.isChild && this.event.nursery)
//                 {
//                     if (this._isEmpty(this.nursery))
//                         errors.push('No has especificado si guardería o no');

//                     if (this.nursery == 'true' && this._isEmpty(this.nursery_peto))
//                         errors.push('No has elegido ninguna opción del peto');
//                 }
//             }

//             return errors;
//         },

//         saveInscription : function(callback)
//         {
//             var invalids = this._validate();

//             if (invalids.length > 0)
//             {
//                 alert('Los siguientes datos no son válidos o están incompletos: \n\n -' + invalids.join('\n- '))
//                 return;
//             }

//             var params = {
//                 session : '{{ Session::getId() }}'
//             };

//             $.each(this.storable, function(index,prop)
//             {
//                 if (this.$data[prop] != null)
//                 {
//                     params[prop] = this.$data[prop];

//                     if (params[prop] == '')
//                         params[prop] = null;
//                     else if (params[prop] == 'true' || params[prop] == 'false')
//                         params[prop] = params[prop] == 'true' ? '1':'0';
//                 }
//             }.bind(this));

//             // update
//             if (this.line_id)
//                 params.line_id = this.line_id;

//             params.birthday = params.birthday.split('/').reverse().join('-');

//             var url;

//             if (this.context !== 'cart')
//                 params.inscription_id = this.inscription_id;

//             if (this.event.require_photo && (!this.line_id || (this.line_id && this.user_photo == true && $('#user_photo').val() !== '')))
//             {
//                 $uploadCrop.croppie('result', {
//                     type : 'base64', 
//                     size : 'viewport', 
//                     format : 'jpeg'
//                 })
//                     .then(function(image){
//                         params.photo = image;
//                         this._ajaxInscription(callback, params);
//                     }.bind(this));
//             }
//             else
//                 this._ajaxInscription(callback, params);
//         },

//         _ajaxInscription : function(callback, params)
//         {
//             if (this.context == 'cart')
//                 url = this.url_cart_line_add;
//             else
//                 url = this.url_inscription_line;

//             AjaxServices.post(url, params, function(error, result)
//             {
//                 if (error)
//                 {
//                     callback(error);
//                 }
//                 else
//                 {
//                     callback(null, result);
//                 }
//             }.bind(this));
//         },

//         _reset : function()
//         {
//             if (this.event.require_photo)
//             {
//                 this._quitPhoto();
//             }

//             $.each(this.storable, function(index,key)
//             {
//                 if (Object.prototype.toString.call( this[key] ) === '[object Array]')
//                     this[key] = [];
//                 else
//                     this[key] = null;
//             }.bind(this));

//             this.user_photo = false;

//             this.nif_rep = null;
//         },

//         _preventNonAlphaNumeric : function(e)
//         {
//             var regex = new RegExp("^[a-zA-Z0-9]+$");
//             var code = !e.charCode ? e.which : e.charCode;
//             var str = String.fromCharCode(code);
//             // permitidos: tab, shift, delete, supr, izq, dcha, inicio, fin
//             var keys_valid = [16,9,8 ,46, 37, 39, 48, 96, 35, 36]; // los ceros tb pasa
//             if (regex.test(str) || keys_valid.indexOf(code) >= 0 )
//                 return true;

//             e.preventDefault();
//             return false;
//         },

//         _removeNonAlphaNumeric : function(e)
//         {
//             if (this.nif)
//                 this.nif = this.nif.replace(/[^0-9a-z]/gi, '');

//             if (this.nif_rep)
//                 this.nif_rep = this.nif_rep.replace(/[^0-9a-z]/gi, '');
//         },

//         _quitPhoto : function()
//         {
//             $('#user_photo').val('');

//             this.user_photo = false;

//             $uploadCrop.croppie('bind', {
//                 url : ''
//             });

//             //$('#user_photo').click();
//         },

//         _changePhoto : function()
//         {
//             $('#user_photo').trigger('click');
//         },

//         _rotatePhotoLeft : function(){
//             $uploadCrop.croppie('rotate',90);
//         },

//         _rotatePhotoRight : function(){
//             $uploadCrop.croppie('rotate',-90);
//         },

//         initCrop : function()
//         {
//             $('#upload-wrapper > div').remove();

//             $uploadCrop = $('#upload-wrapper').croppie(this.croppieOpts);

//             $('#user_photo').on('change', function (e){ 
//                 this.readCropFile(e.target); 
//             }.bind(this));

//             $('#drag_photo_area, #upload-wrapper').on('drop', function(e)
//             {
//                 this.readCropFile(e.originalEvent.dataTransfer);
//                 e.preventDefault();
//                 e.stopPropagation();
//             });

//             $('#drag_photo_area, #upload-wrapper').on('dragover', function(e)
//             {
//                 e.preventDefault();
//             });
//         },

//         readCropFile : function(input)
//         {
//             if (input.files && input.files[0]) 
//             {
//                 if (!/^image/.test( input.files[0].type))
//                 {
//                     alert('Por favor, elige un fichero de tipo imagen');
//                     return;
//                 }

//                 var reader = new FileReader();
                
//                 reader.onload = function(e){
//                     this.loadCropImgFromUrl(e.target.result);
//                 }.bind(this);
                
//                 reader.readAsDataURL(input.files[0]);
//             }
//         },

//         loadCropImgFromUrl : function(url)
//         {
//             $uploadCrop.croppie('bind', {
//                 url : url
//             }).then(function(){
//                 this.user_photo = true;
//             }.bind(this));
//         },

//         updateCrop : function()
//         {
//             $uploadCrop.croppie('bind', { url : ''});
//         },

//         quitCrop : function()
//         {
//             $uploadCrop.destroy();
//         }
//     }
// });

// $(document).ready(function()
// {
//     var today = new Date();
//     today = today.getDate() +'/'+ (today.getMonth()+1) + '/'+ today.getFullYear();

//     $('#date_birthday').datepicker({
//         format         : "dd/mm/yyyy",
//         //startDate    : "11/11/1910",
//         endDate        : today,
//         startView      : 'decades',
//         maxViewMode    : '3',
//         language       : "es-ES",
//         todayHighlight : true,
//         toggleActive   : true,
//         autoclose      : true,
//         defaultViewDate: {
//             year : 1999
//         }
//     }).on('changeDate', function(e){
//         inscription_form.$data.birthday = this.value;
//     });

//     $('.selectpicker').selectpicker();

//     $('[data-toggle="popover"]').popover(); 
// });