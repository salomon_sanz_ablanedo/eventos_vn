new Vue({

    el   : '#modal',

    data : {
        modalShow : false
    },
    
    created : function()
    {
        if (!window.localStorage.getItem('why_register_shown'))
        {
            this.modalShow = true;
            window.localStorage.setItem('why_register_shown', true);
        }
    },

    methods : {
        
        showModal : function()
        {
            this.modalShow = true;
        }
    }
});

//app.$data.modalShow = true;