<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Models\EventInscription;

class InscriptionTotalPriceModified
{
    use SerializesModels;

    public $inscription;

    /**
     * Create a new event instance.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function __construct(EventInscription $inscription)
    {
        $this->inscription = $inscription;
    }
}