<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InscriptionAmountChanged extends Mailable
{
    use Queueable, SerializesModels;

    public $inscription;
    public $to;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inscription)
    {
        $this->inscription = $inscription;
        //$this->to = $inscription->contact_email;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $inscription = $this->inscription;
        $balance = $this->inscription->total_price - $this->inscription->total_paid;
        
        $text_email = 'Hola '.$inscription->contact_name.'<br/><br/>';
        $text_email .= 'Nos ponemos en contacto contigo para comunicarte que tu inscripción ha sido modificada.<br/><br/>';

        if ($balance > 0)
            $text_email .= 'El importe pendiente de abonar actualizado es de <b>'.$balance.'&euro;</b><br/><br/>';

        $text_email .= 'Un saludo';

        if (app()->environment('development'))
            $to = 'djmon84@gmail.com';
        else
            $to = $this->inscription->contact_email;

        return $this->to($to)
                    ->subject('Inscripción modificada e importe actualizado')
                    ->view('mail::html.message')
                    ->with(array(
                        'slot'=>$text_email
                    ));
    }
}
