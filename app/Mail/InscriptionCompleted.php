<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InscriptionCompleted extends Mailable
{
    use Queueable, SerializesModels;

    public $inscription;
    public $to;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inscription)
    {
        $this->inscription = $inscription;
        //$this->to = $inscription->contact_email;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $inscription = $this->inscription;
        $link = route('web.inscripcion_completada', [$inscription->public_id]);
        $id = $inscription->inscription_id;

        $payment_balance = $inscription->total_price - $inscription->total_paid;

        $text_email = 'Hola <b>'.$inscription->contact_name.'</b><br><br>';
        $text_email .= 'Gracias por registrarte en el <i>'.$inscription->event->name.'</i>. <br><br>';
        $text_email .= 'Tu número de inscripción es el: <b>'.$inscription->inscription_id.'</b><br>';
        $text_email .= 'Tu código de inscripción es el: <b>'.$inscription->public_id.'</b><br><br>';

        if ($payment_balance > 0)
        {
            $text_email .= 'Importe pendiente de pago: '. $payment_balance.'&euro; <br/>';
            $text_email .= 'Te recordamos que para completar la inscripción es necesario que realices el pago. <br>';
        }

        if ($payment_balance == 0 && $inscription->total_price > 0 && $inscription->payment_method == 'CARD')
            $text_email .= 'Hemos recibido correctamente el pago de la inscripción, <br/>';

        $text_email .= 'Puedes consultar los datos de tu inscripción ';

        if ($payment_balance > 0)
            $text_email .= 'así como las instrucciones de pago ';

        $text_email .= ' accediendo al siguiente enlace: <br>';

        $text_email .= '<a href="'.$link.'">'.$link.'</a> <br><br>';
        $text_email .= 'Por favor, si no has imprimido el resumen de inscripción, accede al enlace que te facilitamos arriba e imprímelo, es necesario para poder presentarlo a tu llegada al evento.<br><br>';

        if ($payment_balance > 0)
            $text_email .= 'Te volveremos a enviar otro email de confirmación cuando hayamos recibido el pago. <br><br>';

        $text_email .= 'Un saludo';

        if (app()->environment('development'))
            $to = 'djmon84@gmail.com';
        else
            $to = $this->inscription->contact_email;

        return $this->to($to)
                    ->subject('Inscripción realizada')
                    ->view('mail::html.message')
                    ->with(array(
                        'slot'=>$text_email
                    ));
    }
}
