<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PaymentReceived extends Mailable
{
    use Queueable, SerializesModels;

    public $inscription;
    public $to;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inscription)
    {
        $this->inscription = $inscription;
        //$this->to = $inscription->contact_email;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $inscription = $this->inscription;
        $link = route('web.inscripcion_completada', [$inscription->public_id]);
        $id = $inscription->inscription_id;
        $balance = $inscription->total_paid - $inscription->total_price;
        
        $text_email = 'Hola '.$inscription->contact_name.'<br/><br/>';
        $text_email .= 'Nos ponemos en contacto contigo para comunicarte que hemos recibido el pago correspondiente al evento &quot;'.$inscription->event->name.'&quot; ';

        if ($balance == 0)
            $text_email .= ' y por tanto tu inscripción se ha finalizado correctamente.<br/><br/>';
        else
            $text_email .=', por lo sólo te queda pendiente de pago '.abs($balance).'&euro;.<br/><br/>';

        $text_email .= 'Un saludo';

        if (app()->environment('development'))
            $to = 'djmon84@gmail.com';
        else
            $to = $this->inscription->contact_email;

        return $this->to($to)
                    ->subject('Pago recibido')
                    ->view('mail::html.message')
                    ->with(array(
                        'slot'=>$text_email
                    ));
    }
}
