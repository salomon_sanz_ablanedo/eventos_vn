<?php

namespace App\Utils;

class DateUtils{

   public static $DAYS = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];

   public static $MONTHS = ['','Enero','Febrero','Marzo','Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
   //public static $MONTHS_SHORT = [''[]'']

   final public static function getMonthNameByIndex($index){
        return self::$MONTHS[$index]; 
   }

    final public static function getMonthNameByDate($date){
        return self::getMonthNameByIndex((int) date_format(date_create($date),'m'));
    }

    final public static function getDayNameByIndex($index){
       return self::$DAYS[$index];
    }

   final public static function getDayNameByDate($date){
       return self::getDayNameByIndex((int) date("w",date_timestamp_get(date_create($date))));
   }

    final public static function mysqlDateToStandarDate($_date,$_output_format = null){
      $output_format = empty($_output_format)?'d-m-Y':$_output_format;
      $_date = trim($_date);
      $date = date($output_format,date_timestamp_get(date_create($_date)));
      if (strpos($_date,' ') !== false && empty($_output_format))
        $date .= ' '.explode(' ',$_date)[1];
      return $date;
    }

    final public static function standarDate2mysqlDate($date){
      return date('Y-m-d',date_timestamp_get(date_create_from_format('d-m-Y',$date)));
    }

    final public static function getMonthDayFromDate($str){
      return date("d", strtotime($str));
    }

    final public static function getYearFromDate($str){
      return date("Y", strtotime($str));
    }

    final public static function getDaysBetweenDates($date_start, $date_end){
        $dias   = (strtotime($date_start)-strtotime($date_end))/86400;
        $dias   = abs($dias); $dias = floor($dias);     
        return $dias;
    }

  final public static function getYearsDiff($date1,$date2 = null){
    $d1 = new \DateTime($date1);
    $d2 = new \DateTime($date2);
    $diff = $d2->diff($d1);
    return $diff->y;
  }

  final public static function getAge($date){
    return self::getYearsDiff($date,date('Y-m-d'));
  }
}

?>