<?php

namespace App\Utils;

class StrUtils{

	final public static function clean($str){
		return trim(preg_replace('!\s+!', ' ', $str));
	}

	final public static function random($size){
		$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $str = '';
	    $len = strlen($chars)-1;
	    for ($i = 0; $i < $size; $i++){
	        $str .= $chars[rand(0, $len)];
	    }
	    return $str;
	}

	final public static function readableRandom($size){
		$chars = '123456789123456789ABCDEFGHJKLMNPQRSTUVWXYZ';
	    $str = '';
	    $len = strlen($chars)-1;
	    for ($i = 0; $i < $size; $i++){
	        $str .= $chars[rand(0, $len)];
	    }
	    return $str;
	}
}

?>