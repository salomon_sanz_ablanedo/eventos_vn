<?php 

namespace App\Http\Controllers\API;

use View, Input, DB, Exception, DateUtils, Session, Redsys, Log;
use Illuminate\Http\Request;
use \App\Http\Resources\EventCart as EventCartResource;
use \App\Models\EventCart as EventCart;
use Illuminate\Support\Arr;

class EventCartController extends \App\Http\Controllers\Controller {
	
	protected $layout = 'layouts.web.base_web';

	public function __construct()
	{
		//$this->middleware('auth');
		//$this->middleware('guest');
    }

    public function getCart(Request $request){
		return new EventCartResource(
			EventCart::with('lines')
				->session(Session::getId())
				->eventId($request->get('event_id'))
				->first()
		);
	}

	public function getCartLine(
        \App\Models\EventCartLine $EventCartLine,
        Request $request,
		$line_id = null
		)
	{
		try
		{			
			$line = $EventCartLine::with('cart')->find($line_id);

			if ($line->cart->session != Session::getId())
				throw new Exception('No eres el dueño de esta sesión');

			return response()->json($line);
		}
		catch(Exception $e)
		{
			report($e);
			return $this->jsonError($e);
		}
	}

	public function postCartLine(
		\App\Models\EventCart $EventCart,
		\App\Models\EventInscriptionLine $EventInscriptionLine,
		\App\Models\EventCartLine $EventCartLine,
		\App\Services\RegistrationsService $RegistrationsService,
		Request $request,
		$line_id = null
		)
	{
		try
		{
			$RegistrationsService::getInstance();
			$cart = $EventCart::session(Session::getId())->eventId($request->input('event_id'))->first();
			$params = $request->except('session','_token','line_id','public_id');

			foreach($params as &$p)
			{
				if ($p=='')
					$p = null;
			}

			$params['cart_id'] = $cart->cart_id;

			if (isset($params['nif_no']) && $params['nif_no'] == true)
				$params['nif'] = \App\Services\UsersService::getInstance()->generateFakeDNI($params['birthday'], $params['name'], $params['surname']);


			// check if dni exists
			if ($RegistrationsService->legalIdExistsInInscription($params['nif'], $request->input('event_id')))
				throw new Exception('El usuario ya se encuentra registrado en otra inscripción #1');

			// only if not update
			if (empty($line_id) && $RegistrationsService->legalIdExistsInCart($params['nif'], $cart->cart_id))
                throw new Exception('El usuario '.$params['nif'].' ya había sido añadido previamente a esta inscripción');
            
            // security checks
            $age = \App\Utils\DateUtils::getAge($request->input('birthday'));

			if (array_key_exists('lodgement_id', $params) && empty($params['lodgement_id']))
				$params['lodgement_id'] = null;


            if ($age < 18)
            {
                if ($request->has('is_pastor'))
                    $params['is_pastor'] = '0';

                if ($age < 14)
                {
                    if ($request->has('studio1'))
                       $params['studio1'] = null;

                    if ($request->has('studio2'))
                       $params['studio2'] = null;
                }

            }
            else
            {
                if ($request->has('nursery'))
                    $params['nursery'] = '0';
                if ($request->has('nursery_peto'))
                    $params['nursery_peto'] = '0';
                if ($request->has('lodgement_without_room'))
                    $params['lodgement_without_room'] = '0';
                if ($request->has('nursery') && $request->input('nursery') == '0')
                    $params['nursery_peto'] = '0';
            }

			//$duplicated = $EventInscriptionLine::where('nif',$params['nif'])
			//     ->inscription()->event($event_id)->count();
			// if ($duplicated > 0)
			// throw new Exception('DNI no válido, ya existe un usuario registrado con ese dni');

            //$comments = App\Post::find(1)->comments()->where('title', 'foo')->first();
            
			if (empty($line_id))
				$params['public_id'] = \App\Utils\StrUtils::readableRandom(5);

			$line = $EventCartLine::updateOrCreate(
				['line_id' =>$line_id],
				Arr::except($params, ['photo','event_id'])
			);

			$charges_info = $RegistrationsService->calculateUserInscriptionCharges($line, $request->input('event_id'));

			$line->payment_amount  = $charges_info['total'];
			$line->payment_charges = $charges_info['charges'];
			$line->save();

			if ($request->has('photo'))
				$RegistrationsService->storeUserPhoto($request->get('photo'), $line);
			else
				$RegistrationsService->removeUserPhoto($line);

			return response()->json($line);
		}
		catch(Exception $e)
		{
			report($e);
			return $this->jsonError($e);
		}
	}
	
	public function deleteCartLine(
		\App\Models\EventCartLine $EventCartLine,
		$line_id
		)
	{
		try
		{
			$line = $EventCartLine::with('cart')->find($line_id);

			if ($line->cart->session != Session::getId())
				throw new Exception('No eres el dueño de esta sesión');

			$user_photo = public_path().'/uploads/users_photos/'.$line->public_id.'.jpeg';
			$line->delete();
			
			@unlink($user_photo);
			return response()->json(['ok']);
		}
		catch(Exception $e)
		{
			report($e);
			return $this->jsonError($e);
		}
	}

	public function postCart(
		\App\Models\EventCart $EventCart,
		\App\Services\RegistrationsService $RegistrationsService,
        \App\Services\EmailService $EmailService,
        Request $request
	)
	{
		try
		{
			$cart = $EventCart::with('lines')
				->where('session', Session::getId())
				//->where('event_id',$event_id)
                ->first();

			$event_id = $cart->event_id;
			
			$cart_params = [
				'contact_name'   => $request->input('contact_name'),
				'contact_phone'  => $request->input('contact_phone'),
				'contact_email'  => $request->input('contact_email'),
				'payment_method' => $request->input('payment_method')
			];

			if (\Auth::check())
				$cart_params['created_by_user'] = \Auth::user()->id;

			$cart->fill($cart_params)->save();

			$RegistrationsService::getInstance();

			$cart = $EventCart::with('lines')
						->where('session', Session::getId())
						->where('event_id',$event_id)
						->first();

			$legal_id = Arr::pluck($cart->lines, 'nif');

			$dni_already = $RegistrationsService->legalIdExistsInInscription($legal_id, $event_id);

			if ($dni_already)
			{	
				$dni_already = array_unique(Arr::pluck($dni_already, 'nif'));

				$users_already = Arr::where($cart->lines->toArray(), function ($value, $key) use($dni_already) {
				    return in_array($value['nif'], $dni_already);
				});

				$names_already = [];

				foreach ($users_already as $user)
					$names_already[] = $user['name'] .' '.$user['surname']; 

				Log::info('Dnis duplicated: ' . implode(',',$dni_already));
				throw new Exception('Los DNIs de los siguientes usuarios aparecen registrado en otra inscripción: ' . implode(', ', $names_already));
			}
			
			if (in_array($cart->payment_method, ['BANK','CHURCH']))
			{
				try
				{
                    DB::beginTransaction();
					$new_insc = $RegistrationsService->finishCartInscription($cart);
					DB::commit();
				}
				catch(Exception $e)
				{
					DB::rollback();
					report($e);
					throw $e;
				}
				$redirect = route('web.inscripcion_completada', ['inscription_id'=>$new_insc->public_id]);
			}
            else
            {
                $redirect = route('web.tpv_redirect').'?insc='.$cart->public_id;
            }

			return response()->json(array(
				'redirect' => $redirect
			));
		}
		catch(Exception $e)
		{
			report($e);
			return $this->jsonError($e);
		}
	}

	private function jsonError(Exception $e)
	{
		$error = [
		    'error' => array(
				'description' => $e->getMessage(),
				'code'        => $e->getCode(),
				'line'        => $e->getLine(),
				'file'        => $e->getFile()
			)
		];

		return response()->json($error, 500);
	}
}
