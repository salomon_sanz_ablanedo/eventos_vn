<?php 

namespace App\Http\Controllers\API;

use View, DB, Exception, DateUtils, Session, Redsys, Log;
use Illuminate\Http\Request;
//use \App\Http\Resources\Inscription as InscriptionResource;

class EventInscriptionController extends \App\Http\Controllers\Controller {
	
	protected $layout = 'layouts.web.base_web';

    public function getInscription(){
		// return new InscriptionResource(
		// 	EventInscription::with('lines')
		// 		//->eventId($event_id)
		// 		->first()
		// );
	}

	public function getInscriptionLine(
        Request $request,
        \App\Models\EventInscriptionLine $EventInscriptionLine,
		$line_id
		)
	{
		$EventInscriptionLine =  new \App\Models\EventInscriptionLine();

		$line = $EventInscriptionLine::with('meals_list','lodgement_dates')->find($line_id);
		$is_for_form = true;

		if ($is_for_form)
		{
			$line       = $line->toArray();
			$meals_list = [];

			foreach($line['meals_list'] as $m)
				$meals_list[] = $m['event_meal_id'];

			$line['meals_list'] = $meals_list;

			$lodgement_dates = [];

			foreach($line['lodgement_dates'] as $ld)
				$lodgement_dates[] = $ld['date'];

			$line['lodgement_dates'] = $lodgement_dates;
		}

		try
		{
			return response()->json($line);
		}
		catch(Exception $e)
		{
			return $this->jsonError($e);
		}
	}

	public function postInscriptionLine(
		\App\Models\EventCart $EventCart,
		\App\Models\EventInscription $EventInscription,
		\App\Models\EventInscriptionLine $EventInscriptionLine,
		\App\Models\EventCartLine $EventCartLine,
		\App\Services\RegistrationsService $RegistrationsService,
		\App\Events\InscriptionTotalPriceModified $InscriptionTotalPriceModified,
		Request $request,
		$line_id = null
		)
	{
		try
		{
			$inscription                   = $EventInscription->find($request->input('inscription_id'));
			$RegistrationsService          = $RegistrationsService::getInstance();
			$EventInscriptionLine          = new \App\Models\EventInscriptionLine();
			$EventInscriptionLineMeal      = new \App\Models\EventInscriptionLineMeal();
			$EventInscriptionLineLodgement = new \App\Models\EventInscriptionLineLodgement();
			$EventInscriptionLineCharge    = new \App\Models\EventInscriptionLineCharge();

			$operation_type = !empty($line_id) ? 'UPDATE' : 'CREATE';

			$params = $request->except('session','_token','line_id','meals_list','lodgement_dates','photo','event_id');

			foreach($params as &$p)
			{
				if ($p=='')
					$p = null;
			}

			//if (isset($params['nif_no']) && $params['nif_no'] == true)
				$params['nif'] = \App\Services\UsersService::getInstance()->generateFakeDNI($params['birthday'], $params['name'], $params['surname']);
				$params['nif_no'] = false;

			// check if dni exists
			if ($result = $RegistrationsService->legalIdExistsInInscription($params['nif'], $inscription->event_id))
			{
				if ($operation_type == 'CREATE' || ($operation_type == 'UPDATE' && $result->line_id != $line_id))
					throw new Exception('El dni del usuario ya se encuentra registrado en otra inscripción');
			}

			\DB::beginTransaction();

			if ($operation_type == 'UPDATE')
			{
				// borramos los registros de meals, lodgements y charges para meterlos otra vez desde cero
	    		$EventInscriptionLineMeal::inscriptionId($line_id)->delete();
	    		$EventInscriptionLineLodgement::inscriptionId($line_id)->delete();
				$EventInscriptionLineCharge::inscriptionLines($line_id)->where('manual_charge',0)->delete();
			}
			else
			{
				$params['public_id'] = \App\Utils\StrUtils::readableRandom(5);
			}

			$line = $EventInscriptionLine::updateOrCreate(
				['line_id' => $line_id],
				$params
			);

			if ($request->has('photo'))
				$RegistrationsService->storeUserPhoto($request->input('photo'), $line);

			/************
			* 
			*    meals
			*
			*************/

			$params_meals = [];

			if ($request->has('meals_list') && !empty($line->meals))
			{
				foreach($request->input('meals_list') as $meal_id)
				{
					$params_meals[] = array(
						'event_meal_id'  => $meal_id,
						'inscription_id' => $line->line_id,
					); 
				}
			}

	    	if (!empty($params_meals))
	    		$EventInscriptionLineMeal::insert($params_meals);

	    	/************
			* 
			* lodgements
			*
			************/

			$params_lodgements = [];

	    	if (!empty($line->lodgement_id) && $request->has('lodgement_dates'))
    		{
    			foreach($request->input('lodgement_dates') as $lodg_date)
    			{
    				$params_lodgements[] = array(
						'inscription_id' => $line->line_id,
    					'lodgement_id'   => $line->lodgement_id,
    					'date'           => $lodg_date
					);
				}
    		}

	    	if (!empty($params_lodgements))
	    		$EventInscriptionLineLodgement::insert($params_lodgements);

			// charges
			$charges_info = $RegistrationsService->calculateUserInscriptionCharges($line, $inscription->event_id);
			//die(json_encode($charges_info));
			$params_charges = [];

			foreach($charges_info['charges'] as $ci)
				$params_charges[] = $ci->toArray();

	    	if (!empty($params_charges))
	    		$EventInscriptionLineCharge::insert($params_charges);

			$line->payment_amount = $charges_info['total'];
			$line->save();

			$RegistrationsService->updateInscriptionPrice($inscription);

			$log_text = 'datos de inscripcion '.$line->line_id . ' ('.$line->name .' '.$line->surname.') ';

			if ($operation_type == 'UPDATE')
				$log_text .= 'modificados';
			else
				$log_text .= 'creados';

			$RegistrationsService->registerInscriptionLog($inscription->inscription_id, $log_text);

			\DB::commit();

			if ($operation_type == 'UPDATE')
				event(new $InscriptionTotalPriceModified($inscription));

			return response()->json($line);
		}
		catch(Exception $e)
		{
			\DB::rollback();
			return $this->jsonError($e);
		}
			
	}
	
	public function deleteInscriptionLine(
		\App\Models\EventCartLine $EventCartLine,
		$line_id
		)
	{
		try
		{
			$EventInscriptionLine = new \App\Models\EventInscriptionLine();
			$RegistrationsService = \App\Services\RegistrationsService::getInstance();
			$line                 = $EventInscriptionLine::find($line_id);
			$inscription          = $line->inscription;

			\DB::beginTransaction();

			$line->delete();

			$RegistrationsService->updateInscriptionPrice($inscription);

			$log_text = 'Se ha borrado a ' .$line->name . ' ' .$line->surname .' de la inscripción';
			$RegistrationsService->registerInscriptionLog($inscription->inscription_id, $log_text);

			\DB::commit();

			return response()->json(['ok']);
		}
		catch(Exception $e)
		{
			\DB::rollback();
			return $this->jsonError($e);
		}
	}

	private function jsonError(Exception $e)
	{
		$error = [
		    'error' => array(
				'description' => $e->getMessage(),
				'code'        => $e->getCode(),
				'line'        => $e->getLine(),
				'file'        => $e->getFile()
			)
		];

		return response()->json($error, 500);
    }
    
    public function postInscriptionLineMove(
			Request $request,
			$line
		)
		{
			try
			{
				$EventInscription     = new \App\Models\EventInscription();
				$EventInscriptionLine = new \App\Models\EventInscriptionLine();
				$RegistrationsService = \App\Services\RegistrationsService::getInstance();
				$line                 = $EventInscriptionLine::find($line);
				$old_inscription      = $EventInscription::find($line->inscription_id);
				$new_inscription      = $EventInscription::find($request->input('new_inscription_id'));

				\DB::beginTransaction();

				$line->inscription_id = $new_inscription->inscription_id;
				$line->save();
				$RegistrationsService->updateInscriptionPrice($old_inscription);
				$RegistrationsService->updateInscriptionPrice($new_inscription);

				$log_old = 'Se ha movido a '.$line->name .' '.$line->surname .' a la inscripción '.$new_inscription->inscription_id;
				$log_new = 'Se ha incorporado '.$line->name .' '.$line->surname .' desde la inscripción '.$old_inscription->inscription_id;

				$RegistrationsService->registerInscriptionLog($old_inscription->inscription_id, $log_old);
				$RegistrationsService->registerInscriptionLog($new_inscription->inscription_id, $log_new);

				\DB::commit();

				return response()->json(['ok']);
			}
			catch(Exception $e)
			{
				\DB::rollback();
				return $this->jsonError($e);
			}
		}

	public function deleteInscription(
			Request $request
	)
	{
		try
		{
			$EventInscription     = new \App\Models\EventInscription();
			$EventInscription::find($request->input('inscription_id'))->delete();
			return response()->json(['ok']);
		}
		catch(Exception $e)
		{
			return $this->jsonError($e);
		}
	}
}
