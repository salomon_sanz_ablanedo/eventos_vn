<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use View, Input, DB, DateTime, DateInterval, Exception, DateUtils;
use \Illuminate\Support\Arr;
use \Illuminate\Support\Str;

class EventsController extends \App\Http\Controllers\Controller{
	
	protected $layout = 'layouts.admin.evento';
	
	public function __construct()
	{
		$this->middleware('auth');
		//$this->middleware('guest');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getInicio(\App\Models\Evento $Event, 
							  \App\Models\EventInscription $EventInscription, 
							  $id)
	{
		$event = $Event::find($id);
		$total = DB::select('SELECT 
			COUNT(*) as total
			FROM eventsportal.event_inscriptions_lines
			INNER JOIN eventsportal.event_inscriptions
			ON event_inscriptions_lines.inscription_id = event_inscriptions.inscription_id
			WHERE event_inscriptions.event_id = '.$id)[0]->total;

		$day = DB::select('SELECT 
			COUNT(*) as total
			FROM eventsportal.event_inscriptions_lines
			INNER JOIN eventsportal.event_inscriptions
			ON event_inscriptions_lines.inscription_id = event_inscriptions.inscription_id
			WHERE (event_inscriptions.created_at BETWEEN "'.date('Y-m-d').' 00:00:00'.'" AND "'.date('Y-m-d').' 23:59:59") AND  event_inscriptions.event_id = '.$id)[0]->total;

		$days = with(new DateTime($event->date_from))->diff(new \DateTime(date('Y-m-d')))->format("%a");
		
		return view($this->layout, array(
			'h1_sup' => $event->name,
			'h2' => 'Portada',
			'event_id' => $event->id,
			//'h1' => 'Inicio',
			'content'=>view('pages.admin.evento.inicio',array(
				'day_insc_count' => $day,
				'total_insc_count' => $total,
				'days_left' => $days,
				'event_id' => $event->id
			))
		));
	}

	public function getInscribir(\App\Models\Evento $evento, $id, $id_user = null)
	{
		$evento = $evento::find($id);

		$users_found = [];
		$user = NULL;

		// if (!empty($id_user) && ctype_digit(trim($id_user)))
		// {
		// 	$user = new \App\Models\User;
		// 	$user = $user::find((integer) $id_user);

		// 	//TODO: si ya está registrado, error!!
		// }
		// else if ($request->has('search'))
		// {
		// 	$userService = \App\Services\UsersService::getInstance();
		// 	$users_found = $userService->findUsersByString($request->input('search'));
		// }

		return view($this->layout, array(
			'h1_sup' => $evento->name,
			'h2' => 'Inscribir usuario',
			'event_id' => $evento->id,
			//'h1' => 'Inicio',
			'content'=>view('pages.admin.evento.inscribir',array(
				'event_id' => $evento->id,
				'users' => $users_found,
				'user' => $user
			))
		));
	}
	
	public function getInscripciones(

		\App\Models\Evento $Evento, 
		\App\Models\EventInscription $EventInscription, 
		\App\Models\EventInscriptionLine $EventInscriptionLine, 
		Request $request,
		$id)
	{
		$Evento  = $Evento::find($id);	
		$filters = [];
		$users   = [];
		
		if ($request->has('search'))
		{
			$users_event = $EventInscription::eventId($id);

			$int = (int) trim($request->input('search'));
			
			// si es numérico
			$is_numeric  = ctype_digit(trim($request->input('search'))) && !empty(trim($request->input('search')));
			$has_numbers = preg_match_all( "/[0-9]/", $request->input('search')) > 0;

			// nº de inscripcion
			if ($is_numeric)
			{
				$filters[] = 'registration';
				$users     = $users_event->with('lines')->find($request->input('search'));

				if ($users)
					$users = $users->lines;

				if (!empty($users))
					$users = $users_event->first()->lines;
			}
			// si es un DNI (si tiene números)
			elseif($has_numbers)
			{
				$filters[] = 'dni';
				$users = $users_event->with('lines')
						->join('event_inscriptions_lines','event_inscriptions_lines.inscription_id','=','event_inscriptions.inscription_id')
						->where('event_inscriptions_lines.nif','like','%'.$request->input('search').'%')
						->get();

				if (count($users) > 0)
					$users = Arr::pluck($users,'lines')[0];
			}
			
			// código de inscripción
			if (count($users) == 0 && strlen($request->input('search')) == 5)
			{
				$filters[] = 'code';
				$users     = $EventInscription::with('lines')->eventId($id)->publicId($request->input('search'))->get();

				if (count($users) > 0)
					$users = $users->first()->lines;
			}

			// apellidos
			if (count($users) == 0 && !$is_numeric)
			{
				$filters[] = 'surname';
				$users = $EventInscription::with('lines')->eventId($id)
					->join('event_inscriptions_lines','event_inscriptions_lines.inscription_id','=','event_inscriptions.inscription_id')
					->where('event_inscriptions_lines.surname','like','%'.$request->input('search').'%')
					->get();

				if (count($users) > 0)
					$users = Arr::collapse(Arr::pluck($users,'lines'));
			}

			if (isset($users) && count($users)>0)
			{
				$all_same = true;

				if (count($users) > 1)
				{
					$last_parent = NULL;

					foreach($users as $insc)
					{
						if (!empty($last_parent) && $last_parent != $insc->inscription_id)
						{
							$all_same = false;
							break;
						}
						else
							$last_parent = $insc->inscription_id;
					}
				}
			}
		}

		//dd($users);

		return view($this->layout, array(
			'h1_sup' => $Evento->name,
			//'h2' => 'Inscripciones',
			'event_id' => $Evento->id,
			//'h1' => 'Inicio',
			'content'=>view('pages.admin.evento.inscripciones',array(
				'users' => $users,
				'event_id' => $Evento->id,
				'filters' => $filters,
				'autoopen' => isset($all_same) && $all_same
			)),
		));
	}

	public function getRegistros(
		Request $request,
		\App\Models\Evento $Event, 
		\App\Models\EventMeal $EventMeal, 
		$id
	)
	{
		$event = $Event::find($id);
		$meals = $EventMeal::eventId($id)->orderBy('date','ASC')->orderBy('type','ASC')->get();

		$hours_limit = array(
			'DESAYUNO' => '00:00',
			'COMIDA'   => ''
		);

		return view($this->layout, array(
			'h1_sup'   => $event->name,
			'h2'       => 'Registros',
			'event_id' => $event->id,
			//'h1' => 'Inicio',
			'content'=>view('pages.admin.evento.registros',array(
				'meals'    => $meals,
				'event_id' => $event->id
			))
		));
	}
	/******************

		ALOJAMIENTO

	****************/

	public function getAlojamiento(
		\App\Models\Evento $Event,
		\App\Models\EventLodgementRoom $EventLodgementRoom,
		\App\Models\Church $Church,
		$id)
	{
		$event = $Event::find($id);

		$zones = $EventLodgementRoom::distinct()->get(['zone']);
		$churchs = $Church::all(['id','city']);

		return view($this->layout, array(
			'h1_sup' => $event->name,
			//'h2' => 'Informes',
			'event_id' => $event->id,
			//'h1' => 'Inicio
			'content'=>view('pages.admin.evento.alojamiento',array(
				'event' => $event,
				'zones' => $zones,
				'churchs' => $churchs
			))
		));
	}

	public function getAlojamientoSearch(
		Request $request,
		\App\Models\EventInscriptionLine $EventInscriptionLine,
		\App\Models\Evento $Event,
		$id
	)
	{
		$event = $Event::find($id);

		$result = DB::table('event_inscriptions_lines')
	    	->join('churchs', 'event_inscriptions_lines.church_id', '=', 'churchs.id')
	    	->join('event_inscriptions', 'event_inscriptions_lines.inscription_id', '=', 'event_inscriptions.inscription_id')
	    	->select('event_inscriptions_lines.inscription_id','event_inscriptions_lines.public_id','event_inscriptions_lines.name','event_inscriptions_lines.surname','event_inscriptions_lines.line_id','event_inscriptions_lines.birthday','event_inscriptions_lines.reduced_mobility','event_inscriptions_lines.lodgement_without_room','churchs.city as church')
			->whereNull('event_inscriptions_lines.lodgement_room')
			->where('event_inscriptions.event_id', $id)
			->where('event_inscriptions_lines.lodgement_id', 1);

		$is_numeric = false;

		if ($request->has('search'))
		{
			$is_numeric = ctype_digit(trim($request->input('search'))) && !empty(trim($request->input('search')));

			if ($is_numeric)
			{
				$result->where('event_inscriptions_lines.inscription_id', $request->input('search'));
			}
			else
			{
				$result->where(function($query) use ($request){
					$query->where('event_inscriptions_lines.name', 'like', '%'.$request->input('search').'%')
				  		->orWhere('event_inscriptions_lines.surname', 'like', '%'.$request->input('search').'%');
				});
			}
		}

		if ($request->has('church') && !$is_numeric)
			$result->where('event_inscriptions_lines.church_id',$request->input('church'));

		// TODO: age
		if ($request->has('age'))
		{
			if (is_numeric($request->input('age')))
			{
				$operator = '=';
	
				if ($request->input('age') == '18' || $request->input('age')==65)
					$operator = '>=';
				elseif ($request->input('age') == '3')
					$operator = '<=';
	
				$result->whereRaw('TIMESTAMPDIFF( YEAR, event_inscriptions_lines.birthday, "'.$event->date_from.'" ) '.$operator.' '.$request->input('age'));
			}
			else if ($request->input('age') == 'REDUCED_MOBILITY')
			{
				$result->whereRaw('event_inscriptions_lines.reduced_mobility=1');				
			}
		}

		if ($request->has('payment'))
		{
			$result->whereRaw('event_inscriptions.total_price - event_inscriptions.total_paid '. ($request->input('payment') == 'true' ? '<=':'>').' 0');
		}

		//die($result->toSql());

		return response()->json($result->orderBy('event_inscriptions_lines.inscription_id','asc')->get());
	}

	public function getAlojamientoRoomFloorInfo(
		Request $request,
		\App\Models\EventLodgementRoom $EventLodgementRoom,
		$id)
	{
		$rooms1 = $EventLodgementRoom::with('places', 'places.inscription', 'places.inscription.church')->zone($request->input('zone'))->floor($request->input('floor'))->floorZone('interior')->orderBy('floor_zone_order', 'asc')->get();
		$rooms2 = $EventLodgementRoom::with('places', 'places.inscription', 'places.inscription.church')->zone($request->input('zone'))->floor($request->input('floor'))->floorZone('exterior')->orderBy('floor_zone_order', 'asc')->get();

		return response()->json(array(
			'rooms1'   => $rooms1,
			'rooms2'   => $rooms2,
			'event_id' => $id
		));
	}

	public function getAlojamientoGorrasWithoutHelpers(
		Request $request,
		\App\Models\EventLodgementRoom $EventLodgementRoom,
		$id)
	{
		$result = DB::table('event_inscriptions_lines')
	    	->join('churchs', 'event_inscriptions_lines.church_id', '=', 'churchs.id')
	    	->join('event_inscriptions', 'event_inscriptions_lines.inscription_id', '=', 'event_inscriptions.inscription_id')
			->select('event_inscriptions_lines.inscription_id',
					 'event_inscriptions_lines.public_id',
					 'event_inscriptions_lines.name',
					 'event_inscriptions_lines.surname',
					 'event_inscriptions_lines.line_id',
					 'event_inscriptions_lines.birthday',
					 'event_inscriptions_lines.reduced_mobility',
					 'event_inscriptions_lines.lodgement_without_room',
					 'churchs.city as church')
			->whereNotNull('event_inscriptions_lines.event_role')
			->whereNotIn('event_inscriptions_lines.event_role', ['1','6'])
			->where('event_inscriptions.event_id', $id)
			->where('event_inscriptions_lines.lodgement_id', 1)
			->whereNotIn('line_id', function($query) use ($id) {
				$query->select('event_inscriptions_lines.monitor')
					  ->from('event_inscriptions_lines')
					  ->join('event_inscriptions', 'event_inscriptions_lines.inscription_id', '=', 'event_inscriptions.inscription_id')
					  ->where('event_inscriptions.event_id', $id)
					  ->where('event_inscriptions_lines.event_role', '=', 6)
					  ->whereNotNull('monitor');
			})
			->get();
		// $rooms1 = $EventLodgementRoom::with('places', 'places.inscription', 'places.inscription.church')->zone($request->input('zone'))->floor($request->input('floor'))->floorZone('interior')->orderBy('floor_zone_order', 'asc')->get();
		// $rooms2 = $EventLodgementRoom::with('places', 'places.inscription', 'places.inscription.church')->zone($request->input('zone'))->floor($request->input('floor'))->floorZone('exterior')->orderBy('floor_zone_order', 'asc')->get();

		return response()->json($result);
	}

	public function postAlojamientoAssign(
		Request $request,
		\App\Models\EventLodgementRoom $EventLodgementRoom,
		\App\Models\EventLodgementRoomPlace $EventLodgementRoomPlace,
		\App\Models\EventInscriptionLine $EventInscriptionLine,
		\App\Models\Evento $Event,
		\App\Models\EventRole $EventRole,
		$id
	)
	{
		try
		{
			$event = $Event::find($id);
			$event          = $Event::find($id);
			$zone           = $request->input('zone');
			$floor          = $request->input('floor');
			$room_id        = $request->input('room');
			$inscription_id = $request->input('inscription');

			$room = $EventLodgementRoom::find($room_id);
			$total_places_assigned = $EventLodgementRoomPlace::roomId($room_id)->count();

			if ($total_places_assigned >= $room->beds)
				throw new Exception('Habitación completa, no se pueden asignar más camas');

			DB::beginTransaction();

			$EventLodgementRoomPlace::insert(array(
				'room_id'        => $room_id,
				'inscription_id' => $inscription_id
    		));

    		$inscription = $EventInscriptionLine::with('church')
    						->find($inscription_id);

    		$params_update = array(
    			'lodgement_room' => $zone .' planta ' . $floor . ' hab: '.$room->name
    		);

    		if ($event->type == 'CAMPAMENTO')
    		{
				$age = \DateUtils::getYearsDiff($inscription->birthday, $event->date_from);
				$roomers = $EventLodgementRoom::with('places', 'places.inscription')->find($room_id)->places;

				if ($request->has('monitor'))
					$params_update['monitor'] = $request->get('monitor');

				// si es un gorra
				if ($age >= 18)
				{
					$params_update['event_role'] = $request->input('adult_role');

					foreach($roomers as $roomer)
					{
						$roomer = $roomer->inscription;

						if (in_array($roomer->event_role, [$EventRole::ROLE_CAMPISTA, $EventRole::ROLE_AYUDANTE_GORRA]) && $roomer->line_id != $inscription->line_id)
						{
							$roomer->monitor = $inscription->line_id;
							$roomer->save();
						}
					}
				}
				// buscamos si tiene gorra ya la habitación
				else
				{
					$params_update['event_role'] = $EventRole::ROLE_CAMPISTA;

					foreach($roomers as $roomer)
					{
						$roomer = $roomer->inscription;

						if ($roomer->line_id != $inscription->line_id)
						{
							$age = \DateUtils::getYearsDiff($roomer->birthday, $event->date_from);

							if ($age >= 18)
							{
								$params_update['monitor'] = $roomer->line_id;
								break;
							}
						}
					}
				}
    		}

    		$inscription->update($params_update);

			DB::commit();

			return response()->json(array(
				'room_id'     => $room_id,
				'inscription' => $inscription
			));
		}
		catch(Exception $e)
		{
			return response()->json(array(
				'error' => $e->getMessage()
			), 500);
		}
	}

	public function postAlojamientoUnassign(
		Request $request,
		\App\Models\EventLodgementRoom $EventLodgementRoom,
		\App\Models\EventLodgementRoomPlace $EventLodgementRoomPlace,
		\App\Models\EventInscriptionLine $EventInscriptionLine,
		\App\Models\Evento $Event,
		$id
	)
	{
		try
		{
			$event = $Event::find($id);

			DB::beginTransaction();

			$place = $EventLodgementRoomPlace::where('inscription_id', $request->input('inscription'))->first();
			$room_id = $place->room_id;
			$place->delete();

			$inscription = $EventInscriptionLine::find($request->input('inscription'));
			$inscription->update(['lodgement_room' => null,
          						  'monitor' => null,
          						   'event_role' => null]);

          	if ($event->type == 'CAMPAMENTO')
          	{

          		$age = \DateUtils::getYearsDiff($inscription->birthday, $event->date_from);

          		if ($age >= 18)
          		{
          			$roomers = $EventLodgementRoom::with('places', 'places.inscription')->find($room_id)->places;

          			foreach($roomers as $roomer)
					{
						$roomer = $roomer->inscription;

						if ($roomer->line_id != $inscription->line_id)
						{
							$roomer->monitor = null;
							$roomer->save();
						}
					}
          		}
          	}

			DB::commit();

          	return response()->json(array(
				'room_id'        => $room_id,
				'inscription_id' => $request->input('inscription')
			));
		}
		catch(Exception $e)
		{
			return response()->json(array(
				'error' => $e->getMessage()
			), 500);
		}
	}

	public function postAlojamientoLock(
		Request $request,
		\App\Models\EventLodgementRoom $EventLodgementRoom,
		\App\Models\EventLodgementRoomPlace $EventLodgementRoomPlace,
		\App\Models\Evento $Event,
		$id
	)
	{
		try 
		{
			$total_beds = $EventLodgementRoom::find($request->input('room_id'))->beds;
			$total_assigned = $EventLodgementRoomPlace::where('room_id', $request->input('room_id'))
									->count();
			$unasigned = $total_beds - $total_assigned;

			if ($unasigned > 0)
			{
				$empty_inserts = [];

				for ($a = 0; $a <$unasigned; $a++)
				{
					$empty_inserts[] = array(
						'room_id' 		 => $request->input('room_id'),
						'inscription_id' => null
					);
				}

				$EventLodgementRoomPlace::insert($empty_inserts);
			}
			return response()->json(array(
				'result' => 'lock ok'
			));
		} catch (\Throwable $th) {
			return response()->json(array(
				'message' => $th->getMessage()
			),500);
		}
	}

	public function postAlojamientoUnlock(
		Request $request,
		\App\Models\EventLodgementRoom $EventLodgementRoom,
		\App\Models\EventLodgementRoomPlace $EventLodgementRoomPlace,
		\App\Models\Evento $Event,
		$id
	)
	{
		try 
		{
			$EventLodgementRoomPlace::where('room_id', $request->input('room_id'))
									->whereNull('inscription_id')
									->delete();
			
			return response()->json(array(
				'result' => 'unlock ok'
			));
			
		} catch (\Throwable $th) {
			return response()->json(array(
				'message' => $th->getMessage()
			),500);
		}
	}

	public function getAlojamientoTotals(
		\App\Models\EventLodgementRoom $EventLodgementRoom,
		\App\Models\EventLodgementRoomPlace $EventLodgementRoomPlace,
		\App\Models\EventInscriptionLine $EventInscriptionLine,
		$event_id
	)
	{
		try {
			$total_beds 		= $EventLodgementRoom::sum('beds');
			$total_ocuped 		= $EventLodgementRoomPlace->count();
			$total_assigned 	= $EventLodgementRoomPlace::whereNotNull('inscription_id')->count();
			$total_inscriptions = $EventInscriptionLine::where('lodgement_id',1)
														->where('lodgement_without_room', '0')->count();

			return response()->json(array(
				'total_free_beds' 	 => $total_beds - $total_ocuped,
				'total_pending_beds' => $total_inscriptions - $total_assigned
			));
		} catch (\Throwable $th) 
		{
			return response()->json(array(
				'description' => $th->getMessage()
			), 500);
		}
	}

	public function getRegistrosPost(
		Request $request,
		\App\Models\EventMeal $EventMeal,
		\App\Models\EventInscriptionLineMeal $EventInscriptionLineMeal,
		\App\Models\EventInscriptionLine $EventInscriptionLine,
		\App\Services\RegistrationsService $RegistrationsService,
		$id)
	{
		$user_public_id = trim($request->input('user_public_id',''));
		$meal_id = $request->input('meal_id','');

		try
		{
			//if (strlen($user_public_id) < 5)
			//	throw new Exception('INVALID_USER_PUBLIC_ID');

			if ((int) $meal_id == 0)
				throw new Exception('INVALID_MEAL_ID');

			// buscamos que exista ese meal
			try{
				$EventMeal::findOrFail((int) $meal_id);
			}catch(Exception $e){
				throw new Exception('MEAL_NOT_EXISTS');
			}

			// que el usuario lo tenga
			try{
				//$insc_info = $EventInscriptionLine::publicId($user_public_id)->firstOrFail();
				$insc_info = $EventInscriptionLine::publicId($user_public_id)->firstOrFail();
			}catch(Exception $e){
				throw new Exception('INSCRIPTION_NOT_EXISTS');
			}

			try{
				$meal_info = $EventInscriptionLineMeal::with('meal')->eventMealId($meal_id)->inscriptionId($insc_info->line_id)->firstOrFail();
			}catch(Exception $e){
				throw new Exception('USER_MEAL_NOT_EXISTS');
			}

			// que no esté ya usado
			if (!empty($meal_info->registration_date))
				throw new Exception('ALREADY_REGISTERED');

			try
			{
				//die((string) $meal_info->meal->picnic_opt . ' | '. (string) $insc_info->meals_picnic);
				$meal_info->registration_date = date('Y-m-d H:i:s');
				$meal_info->registered_by_user = \Auth::user()->user_id;
				$meal_info->save();
				$pending   = $EventInscriptionLineMeal::eventMealId($meal_id)->unregistered()->count();

				$RegistrationsService::getInstance()->registerInscriptionLog($insc_info->inscription_id, 'paso de comida registrada');

				$result = array(
					'success' => 'true',
					'picnic'  => !empty($meal_info->meal->picnic_opt) && !empty($insc_info->meals_picnic)? true : false,
					'pending'  => $pending
				);
			}
			catch(Exception $e)
			{
				throw new Exception($e->getMessage());
			}
			
		}
		catch(Exception $e)
		{
			$result = array(
				'error' => $e->getMessage()
			);
		}

		return response()->json($result);
	}
	
	public function getInformes(
		\App\Models\Evento $Event,
		\App\Models\Church $Church,
		\App\Models\EventMeal $EventMeal,
		Request $request,
		$id
	)
	{
		$event = $Event::with(['lodgements', 'studios'])->find($id);
        $event_days = with(new DateTime($event->date_to))->diff(new DateTime($event->date_from))->format("%a");
        $parking_days = [];
        $lodgement_dates = [];

        if (count($event->lodgements) > 0)
		{
			$lodgements_total_nights = DateUtils::getDaysBetweenDates($event->date_from, $event->date_to);

			for ($a = 0; $a < $lodgements_total_nights; $a++)
			{
				$day = date_format(date_add(date_create($event->date_from), date_interval_create_from_date_string((string) $a .' days')), 'Y-m-d');

				$lodgement_dates[] = $day;

				/*array(
					'date' 		=> $day,
					'date_name' => DateUtils::getDayNameByDate($day) . ' '. DateUtils::getMonthDayFromDate($day)
				);
				*/
			}
		}

        $date = new \DateTime($event->date_from);
        $parking_days[] = $date->format('d-m-Y');

        for ($a = 0; $a < $event_days; $a++)
        {
        	$date->add(new DateInterval('P1D')); // P1D means a period of 1 day
        	$parking_days[] = $date->format('d-m-Y');
        }

        # Roles

		return view($this->layout, array(
			'h1_sup' => $event->name,
			//'h2' => 'Informes',
			'event_id' => $event->id,
			//'h1' => 'Inicio
			'content'=>view('pages.admin.evento.informes',array(
				'event_id' => $event->id,
				'churchs' => $Church::orderBy('city','ASC')->get(),
				'meals'   => $EventMeal::eventId($id)->orderBy('date','ASC')->orderBy('type','ASC')->get(),
				'parking_days' => $parking_days,
				'roles' => [],
				'lodgements' => $event->lodgements,
				'lodgement_dates' => $lodgement_dates,
				'studios' => $event->studios
			))
		));
	}

	public function postInformes(
		Request $request,
		\App\Models\Evento $Event,
		$id)
	{
		$class = 'App\Models\Reports\Report'.Str::studly($request->input('report_type'));
		$report = new $class($Event::find($id), $request->except(['_token','report_type']));

		switch($request->input('output_type'))
		{
			case 'screen': case 'print':
				return $report->htmlOutput($request->input('output_type') == 'print');
			break;

			case 'excel':
				$report->excelOutput();
			break;

			case 'bracelet':
				return $report->braceletOutput();
			break;
		}
	}
	
	public function getEstadisticas(\App\Models\Evento $evento, $id)
	{		
		$q_iglesias = 'SELECT COUNT(*) as total, churchs.city
			  FROM event_inscriptions_lines 
			  INNER JOIN event_inscriptions ON event_inscriptions.inscription_id = event_inscriptions_lines.inscription_id
			  INNER JOIN churchs ON event_inscriptions_lines.church_id = churchs.id
			  WHERE event_inscriptions.event_id = '.$id.'
			  GROUP BY churchs.id
			  ORDER BY total DESC';
			//die($q);

		$res_iglesias = DB::select($q_iglesias);
		
		$q_genero = 'SELECT COUNT(*) as total, event_inscriptions_lines.genre
			  FROM event_inscriptions_lines 
			  INNER JOIN event_inscriptions ON event_inscriptions.inscription_id = event_inscriptions_lines.inscription_id	
			  WHERE event_inscriptions.event_id = '.$id.'
			  GROUP BY event_inscriptions_lines.genre';

		$res_genero = DB::select($q_genero);
		
		$q_edades = 'SELECT TIMESTAMPDIFF( YEAR, event_inscriptions_lines.birthday, CURDATE( ) ) AS  age, COUNT( * ) AS total
			  FROM event_inscriptions_lines 
			  INNER JOIN event_inscriptions ON event_inscriptions.inscription_id = event_inscriptions_lines.inscription_id	
			  WHERE event_inscriptions.event_id = '.$id.'
			  GROUP BY age
			  ORDER BY age ASC';

		$res_edades = DB::select($q_edades);
		
		$q_roles = 'SELECT COUNT(*) as total, event_role
			  FROM event_inscriptions_lines 
			  INNER JOIN event_inscriptions ON event_inscriptions.inscription_id = event_inscriptions_lines.inscription_id	
			  WHERE event_inscriptions.event_id = '.$id.'
			  GROUP BY event_role ASC';
			  
		$res_roles = DB::select($q_roles);
			
		$evento = $evento::find($id);		
		
		return view($this->layout, array(
			'h1_sup' => $evento->name,
			'h2' => 'Estadísticas',
			'event_id' => $evento->id,
			//'h1' => 'Inicio',
			'content'=>view('pages.admin.evento.estadisticas',array(
				'users_by_church' => $res_iglesias,
				'users_by_gender' => $res_genero,
				'users_by_age'    => $res_edades,
				'users_by_role'   => $res_roles,
			))
		));
	}

	public function getConfiguracion(\App\Models\Evento $evento, $id)
	{
		$evento = $evento::find($id);		
		
		return view($this->layout, array(
			'h1_sup' => $evento->name,			
			'event_id' => $evento->id,
			//'h1' => 'Inicio',
			'content'=>view('pages.admin.evento.configuracion',array(
				'evento' => $evento
			))
		));
	}
}
