<?php 

namespace App\Http\Controllers\Admin;

use View, URL, Redirect, DB, Exception;
use Illuminate\Http\Request;

class InscriptionController extends \App\Http\Controllers\Controller{
	
	protected $layout = 'layouts.admin.inscripcion';

	private $EventInscriptionLine;

	private $h1;
	private $inscription;
	private $default_params = [];

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(\App\Models\EventInscription $EventInscription)
	{
		$this->middleware('auth');
		$this->EventInscription = $EventInscription;
	}

	private function loadInscriptionInfo($id, $withs = [])
	{
		$default_with = array('notes','event');
		$default_with = array_merge($default_with,$withs);
		$this->inscription = $this->EventInscription->with($default_with)->find($id);
		//dd($this->inscription);
		$this->h1 = 'Inscripción '.$id;
	}

	private function renderView($id, $view_name, $withs = [], $params = [])
	{
		//dd($view_name);
		if (empty($this->inscription))
			$this->loadInscriptionInfo($id, $withs);

		$default_params = array(
			'inscription' => $this->inscription,
			'notes_count'  => 0
		);

		$params = array_merge($default_params,$params);

		return view($this->layout, array(
			'h1_inscription'            => $this->h1,
			'inscription'               => $this->inscription,
			'notes_count'               => $default_params['notes_count'],
			'inscription_content'       => view($view_name, $params),
			'show_lodgement_management' => true
		));
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getInicio($id)
	{
		$this->loadInscriptionInfo($id,['lines.studio_1','lines.studio_2','lines.lodgement','lines.lodgement_dates','lines.church','lines.meals_list','lines.meals_list.meal']);

		$params = [
			'welcome_kit_user' => null,
			'payment_balance'  => 0,
			'total_parkings'   => 0,
			'total_nursery'    => 0,
			'total_petos'      => 0,
			'total_meals'      => 0,
			'total_nights'     => 0,
		];

		$params['payment_balance'] = $this->inscription->total_price - $this->inscription->total_paid;

		foreach ($this->inscription->lines as $insc)
		{
			if (!empty($insc->welcome_kit) && empty($params['welcome_kit_user']))
				$params['welcome_kit_user'] = $insc;

			if (!empty($insc->nursery))
			{
				$params['total_nursery']++;

				if (!empty($insc->nursery_peto))
					$params['total_petos']++;
			}

			if (!empty($insc->parking) || !empty($insc->parking_days))
				$params['total_parkings']++;

			if (!empty($insc->meals) > 0)
				$params['total_meals'] += count($insc->meals_list);

			if (!empty($insc->lodgement_dates))
				$params['total_nights'] += count($insc->lodgement_dates);
		}

		return $this->renderView($id, 'pages.admin.inscripcion.inicio', [], $params);
	}

	public function postInicio(
		\App\Models\EventInscriptionLine $EventInscriptionLine,
		\App\Services\RegistrationsService $RegistrationsService,
		$id)
	{
		try
		{
			if ($request->input('user_welcome_kit') > 0)
			{
				$insc_wk = $EventInscriptionLine::find($request->input('user_welcome_kit'));
				$insc_wk -> welcome_kit = 1;
				$insc_wk -> save();
				$RegistrationsService::getInstance()->registerInscriptionLog($insc_wk->inscription_id, 'Kit de bienvenida dado');
			}
			else
				$RegistrationsService::getInstance()->registerInscriptionLog($insc_wk->inscription_id, 'Kit de bienvenida anulado');

			// DB::commit();

			$returned = array(
				'alert'		 => 'Operación realizada correctamente',
				'alert-type' => 'success'
			);
		}
		catch(Exception $e)
		{
			$returned = array(
				'alert' => $e->getMessage(),
				'alert-type' =>'danger'
			);
		}

		return redirect()->back()->with($returned);
	}

	public function getModify(
		Request $request,
		$inscription_id)
	{
		$this->loadInscriptionInfo($inscription_id);

		return view('pages.admin.inscripcion.modify',array(
			'inscription' => $this->inscription,
			'event_id'    => $this->inscription->event_id,
			'line_id' => $request->input('line_id', null)
		));
	}

	// AJAX
	public function getInscriptionLineInfo()
	{
		
	}

	public function getAcciones($id)
	{
		$params = [];

		$this->loadInscriptionInfo($id,['lines.lodgement','lines.lodgement_dates','lines.meals_list']);
		
		$params['url_print_insc']  = route('web.inscripcion_completada', $this->inscription->public_id).'?print=true';
		$params['route_form_post'] = route('admin.inscripcion.acciones_post',$id);
		$params['kids']            = [];
		$params['adults']          = [];
		$params['lodgement_insc']  = [];

		foreach ($this->inscription->lines as $line)
		{
			$age = \DateUtils::getYearsDiff($line->birthday, $this->inscription->event->date_from);
			
			if ($age < 13)
				$params['kids'][] = $line;
			else if (count($line->meals_list) > 0)
			{
				$params['adults'][] = $line;
			}

			if (count($line->lodgement_dates) > 0 && $line->lodgement->external == 0)
				$params['lodgement_insc'][] = $line;
		}

		return $this->renderView($id, 'pages.admin.inscripcion.acciones', [], $params);
	}

	public function postAcciones(
		\App\Services\RegistrationsService $RegistrationsService,
		\App\Models\EventInscription $EventInscription,
		Request $request,
		$id)
	{
		$ids = [];

		if (!is_array($request->input('inscription_id')))
			$ids = [$request->input('inscription_id')];
		else
			$ids = $request->input('inscription_id');

		switch($request->input('FUNCTION'))
		{
			case 'print_bracelet':
				// lo comento por que el id que paso no es el bueno
				//foreach ($ids as $id)
				//	$RegistrationsService::getInstance()->registerInscriptionLog($id, 'impreso etiqueta brazalete');
				return with(\App\Services\EventsService::getInstance())->getBraceletHtml($request->get('inscription_id'), true, true, $request->input('size'));

			break;


			case 'print_identificator':

				// lo comento por que el id que paso no es el bueno
				//foreach ($ids as $id)
				//	$RegistrationsService::getInstance()->registerInscriptionLog($id, 'impreso etiqueta identificador');

				return with(\App\Services\EventsService::getInstance())->getIdentificatorHtml($request->input('inscription_id'), true, true);
			break;

			case 'set_rooms':

				try
				{

					if (!is_array($request->input('rooms')))
						$rooms = [$request->input('rooms')];
					else
						$rooms = $request->input('rooms');

					for ($a = 0; $a < count($ids); $a++) 
					{
						$insc = $EventInscription::find($ids[$a]);

						if (empty($rooms[$a]))
							$insc->lodgement_room = null;
						else
							$insc->lodgement_room = $rooms[$a];

						$insc->save();
					}

					return redirect()->back()->with(array(
						'alert'		 => 'Operación realizada correctamente',
						'alert-type' => 'success'
					));
				}
				catch(Exception $e)
				{
					return redirect()->back()->with(array(
						'alert' => $e->getMessage(),
						'alert-type' =>'danger'
					));
				}

			break;
		}
	}

	public function getAnotaciones($id)
	{
		return $this->renderView($id,'pages.admin.inscripcion.anotaciones',['notes','notes.user']);
	}

	public function postAnotaciones(
		Request $request,
		\App\Models\EventInscriptionAnotation $EventInscriptionAnotation,
		$id)
	{
		try
		{
			if (!is_numeric($id))
				throw new Exception('Id no valido');

			// por seguridad desahemos todo
			DB::beginTransaction();

			$note = new $EventInscriptionAnotation;
			$note->message = $request->input('message');
			$note->inscription_id = $id;
			$note->user_id = \Auth::user()->user_id;
			$note->save();
			
			DB::commit();

			$returned = array(
				'alert'		 => 'Operación realizada correctamente',
				'alert-type' => 'success'
			);
		}
		catch(Exception $e)
		{
			DB::rollback();

			$returned = array(
				'alert' => $e->getMessage(),
				'alert-type' =>'danger'
			);
		}
		return redirect()->back()->with($returned);
	}

	public function getCargos($id)
	{
		$this->loadInscriptionInfo($id,['lines.charges']);

		$total_balance = $this->inscription->total_paid - $this->inscription->total_price;

		return $this->renderView($id,'pages.admin.inscripcion.cargos',['charges'],array(
			'total_balance' => $total_balance
		));
	}

	public function postCargos(
		Request $request,
		\App\Models\EventInscriptionLineCharge $EventInscriptionLineCharge,
		\App\Services\RegistrationsService $RegistrationsService,
		\App\Services\EmailService $EmailService,
		$id
	)
	{
		$EmailService::getInstance();
		$RegistrationsService::getInstance();

		try
		{
			$this->loadInscriptionInfo($id);

			$params = array(
				'description'    => $request->input('concept'),
				'quantity'       => $request->input('quantity'),
				'price'          => $request->input('price'),
				'inscription_id' => $request->input('inscription')
			);
			
			if (!isset($params['price']))
				throw new Exception('Es obligatorio especificar el importe');

			if (isset($params['price']) && is_numeric($params['price']) && $params['price'] < 0)
				throw new Exception('El importe no puede ser negativo nunca, si deseas hacer un abono pon como negativo la cantidad');

			if ($request->has('charge_id') && !empty($request->input('charge_id')))
				$charge = $EventInscriptionLineCharge::find($request->input('charge_id'));
			else
			{
				$charge = new $EventInscriptionLineCharge;
				$params['manual_charge'] = 1;
			}

			$charge->fill($params);

			$total_charge = ($charge->quantity + $charge->price).'&euro; ';

			if ($request->input('delete') == 'true'){
				$RegistrationsService::getInstance()->registerInscriptionLog($id, 'cargo de '.$total_charge.' eliminado');
				$charge->delete();
			}
			else
			{
				$charge_type = 'cargo';

				if ($charge->quantity < 0)
					$charge_type = 'abono';

				$charge_type .= ' de '.$total_charge;

				if ($request->has('charge_id') && !empty($request->input('charge_id')))
					$RegistrationsService::getInstance()->registerInscriptionLog($id, $charge_type.' modificado');
				else
					$RegistrationsService::getInstance()->registerInscriptionLog($id, $charge_type.' creado');

				$charge->save();
			}

			$inscription = $RegistrationsService->updateInscriptionPrice($this->inscription);

			if ($params['quantity'] < 0)
			{
				$RegistrationsService->registerInscriptionLog($inscription->inscription_id, 'Pago comprobado');
				$EmailService->sendPaymentReceived($inscription);
			}

			$returned = array(
				'alert'		 => 'Operación realizada correctamente',
				'alert-type' => 'success'
			);
		}
		catch(Exception $e)
		{
			$returned = array(
				'alert' => $e->getMessage(),
				'alert-type' =>'danger'
			);
		}

		return redirect()->back()->with($returned);
	}

	public function getMealsInfo(
		\App\Models\EventInscriptionLine $EventInscriptionLine,
		$public_id
		)
	{	
		return view('pages.admin.inscripcion.meals_info',array(
			'insc' => $EventInscriptionLine::with('meals_list')->publicId($public_id)->firstOrFail()
		));
	}

	public function getLog(
		\App\Models\EventInscriptionLog $EventInscriptionLog,
		$id)
	{
		// al crear inscripción   - x
		// al modificar           - v
		// al borrar           	  - v
		// al mover           	  - v
		// al marcar como pagado  - x
		// al añadir cargo manual - x
		// al imprimir etiqueta pulsera y normal - x
		// al inscribir comentario - x
		// al recoger kit de bienvenida - x
		// al desacer recoger kit bienvenida -x
		// registrar comidas - x

		$this->loadInscriptionInfo($id);

		$logs = $EventInscriptionLog::with('user')->inscriptionId($id)->get();

		return $this->renderView($id,'pages.admin.inscripcion.log',['charges'],array(
			'logs' => $logs
		));
	}
}
