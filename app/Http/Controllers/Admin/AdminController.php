<?php 

namespace App\Http\Controllers\Admin;

use View, Input, URL, Redirect;

class AdminController extends \App\Http\Controllers\Controller{
	
	protected $layout = 'layouts.admin.base';
	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function inicio()
	{
		//die('llega');
		//return Redirect::route('admin.eventos');
		return view($this->layout, array(
			'h1' => 'Inicio',
			'content'=>view('pages.admin.inicio')
		));
	}
	
	public function eventos(\App\Models\Evento $eventos)
	{
		$events = $eventos::orderBy('date_from','desc')->get();

		return view('pages.admin.eventos', array(
				'events' => $events,
			//'h1_sup' => 'Eventos',
			'h1' => 'Eventos',
		));
	}	
	
	public function eventos_post(\App\Models\Evento $evento)
	{
		$evento->name = Input::get('name');
		$evento->type = Input::get('type');
		$evento->save();	
		return redirect()->route('admin.eventos.configuracion', array('id'=>$evento->id, '#general'));
	}
	
	public function usuarios(\App\Models\User $users, \App\Models\Church $church)
	{
		$filters = array();
		
		if (Input::has('search'))
		{				
			//$users::find(1);
					
			if (ctype_digit(trim(Input::get('search'))) && !empty(trim(Input::get('search'))) && Input::get('search') < 99999)
			{
				// si es numérico
				$filters[] = 'user_id';
				$users = $users::find(Input::get('search'));
			}			
			else if (preg_match_all( "/[0-9]/", Input::get('search')) > 0)
			{
				// si es un DNI (si tiene números)
				$filters[] = 'dni';
				$users = $users->dni(Input::get('search'))->get();
			}	
			else if (strpos(Input::get('search'),'@') !== false)
			{
				// si es un DNI (si tiene números)
				$filters[] = 'dni';
				$users = $users->email(Input::get('search'))->get();
			}
			else 
			{
				// si es un nombre
				$filters[] = 'surname';
				$users = $users->where('surname', 'LIKE', '%'.Input::get('search').'%')
					  ->orWhere('name','LIKE','%'.Input::get('search').'%')
					  ->get();
			}
		}
		else
			$users = $users::all();	
		
		if (count($users) == 1 && basename(get_class($users)) != 'Collection')                            	
            $users = array($users);                    		

		return view($this->layout, array(
			//'h1_sup' => 'Eventos',
			'h1' => 'Usuarios',
			'content'=>view('pages.admin.usuarios',array(
				'users' => $users,
				'churchs' => $church::all(),
				'filters' => $filters
			))
		));
	}
}
