<?php 

namespace App\Http\Controllers\Admin;

use View, Input, DB, Route, DateUtils, Exception;

class UsersController extends \App\Http\Controllers\Controller{
	
	protected $layout = 'layouts.admin.user';
	private $h1;
	private $user_data;
	private $User;
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(\App\Models\User $User)
	{
		$this->User = $User;
		$this->user_data = $User::with([])->find(Route::current()->getParameter('id'));

		//$this->middleware('auth');
		//$this->
		//$this->middleware('guest');
	}

	private function renderView($view_name, $params_ = [])
	{
		$params = array(
			'user' => $this->user_data,
			'user_id' => $this->user_data->user_id
		);

		$params = array_merge($params,$params_);

		return view($this->layout, array(
			'h1' => $this->user_data->name .' '.$this->user_data->surname,
			'user_id' => $this->user_data->user_id,
			'user_content'=>view($view_name,$params)
		));
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getInicio(\App\Models\Church $Church, $id)
	{	
		$params = array(
			'churchs' => $Church::orderBy('city','ASC')->get()
		);
		return $this->renderView('pages.admin.usuario.inicio',$params);
	}

	public function postInicio(\App\Models\User $User, $id)
	{
		$user_service = \App\Services\UsersService::getInstance();

		$user = $User::find($id);

		$user->name = Input::get('name');
		$user->surname = Input::get('surname');
		$user->gender = Input::get('gender');
		$user->church_id = Input::get('church_id');
		$user->email = Input::get('email');
		$user->birthdate = DateUtils::standarDate2mysqlDate(str_replace('/','-',Input::get('birthdate')));
		$user->phone1 = Input::get('phone1');
		$user->phone2 = Input::get('phone2');

		try
		{
			if (Input::has('no_dni'))
			{
				$user->fake_legal_id = 1;
				$user->legal_id = $user_service->generateFakeDNI($user->birthdate,$user->name,$user->surname);
			}
			else
			{
				$user->fake_legal_id = 0;
				$user->legal_id = $user_service->cleanLegalId(Input::get('legal_id'));
			}

			$user->role = Input::get('role');

			if ($user->role != 'USER')
			{
				$user->username = Input::get('login_user');
				$user->password = Input::get('login_password');
			}

			$user->save();

			$returned = array(
				'alert'		 => 'Operación realizada correctamente',
				'alert-type' => 'success'
			);
		}
		catch(Exception $e)
		{
			$returned = array(
				'alert' => $e->getMessage(),
				'alert-type' =>'danger'
			);
		}

		return redirect()->back()->with($returned);
	}

	public function getInscripciones(\App\Models\EventUserInscription $EventUserInscription, $id)
	{
		$params = array(
			'inscriptions' => $EventUserInscription::with(['event' => function($query){
									$query->orderBy('date_from','DESC');
								}])->userId($id)->get()
		);

		return $this->renderView('pages.admin.usuario.inscripciones',$params);
	}

	public function getAcciones($id)
	{
		return $this->renderView('pages.admin.usuario.acciones');
	}
}
