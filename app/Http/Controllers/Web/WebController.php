<?php 

namespace App\Http\Controllers\Web;

use View;

class WebController extends \App\Http\Controllers\Controller {
	
	protected $layout = 'layouts.web.base';
	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
		$this->middleware('guest');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getInicio()
	{	
		//return view('layouts.web.base');
		//return view('pages.web.index');
		return view($this->layout, ['content' => view('pages.web.inicio')]);
		//$this->layout->content = view('pages.web.index');
	}
}
