<?php 

namespace App\Http\Controllers\Web;

use View, Input, DB, Exception, DateUtils, Session, Redsys, Log;
use Illuminate\Http\Request;


class EventController extends \App\Http\Controllers\Controller {
	
	protected $layout = 'layouts.web.base_web';

	public function __construct()
	{
		//$this->middleware('auth');
		//$this->middleware('guest');
	}

	public function getIndex(\App\Models\Evento $Event)
	{
		$events = $Event::whereDate('date_from', '>=',  date('Y-m-d'))
					->where('visible','1')->get();

		if (count($events) == 1)
		{
    		return redirect()->route('web.evento.informacion', $events[0]->id);
		}
		else
		{
			$params = array(
				'events' => $events
			);

			return view('pages.web.index', $params);
		}
	}

	public function updatePublicId()
	{
		// Route::get('/updatebalance', function()
	   $inscriptions = \App\Models\EventInscription::with('lines')->eventId(4)->get();   
	   $reg          = \App\Services\RegistrationsService::getInstance();

	   foreach($inscriptions as $i)
	   {
	        foreach($i->lines as $l)
	        {
    			$l->public_id = \App\Utils\StrUtils::readableRandom(5);
    			$l->save();
	        }
	   }
	}

	public function notification(
		\App\Models\EventCart $EventCart,
		\App\Services\RegistrationsService $RegistrationsService,
		Request $request
	)
	{
		try
        {
        	Log::info('Notification: ');
            Log::info($request->all());
			$parameters_json = base64_decode($request->get('Ds_MerchantParameters'));
			$parameters      = json_decode($parameters_json, true);

			$response = (int) $parameters['Ds_Response'];

			if ($response > 99 && !in_array($response, [900,400]))
			{
				Log::info('failed payment '.$public_id.' notification response: '.$response);
				return response()->json([
					'error' => 'No se ha podido procesar el pago, código: '.$response
				], 500);
			}

			$public_id       = $parameters['Ds_MerchantData'];
			$cart 			 = $EventCart::publicId($public_id)->first();  
			// // TODO: registrar pago
			// $cart->save();total_paid
			$cart->total_paid = $parameters['Ds_Amount'] / 100;
			$cart->payment_method = 'CARD';
			$cart->save();

            // Confirmar transacción a Foti            
			$RegistrationsService::getInstance()->finishCartInscription($cart);
			
			return response()->json([
				'result' => 'ok'
			]);
        } 
        catch (Exception $e) 
        {
			Log::info('¡ERROR! pedigo pagado y no procesado: '.$e->getMessage());
			return response()->json([
				'error' => $e->getMessage()
			], 500);
        }
	}

	public function ko(\App\Models\EventCart $EventCart)
	{
		try
		{
			$cart = $EventCart::where('session',Session::getId())->first();

			if (empty($cart))
				die('Ha ocurrido un error en el pago');

			return redirect()->route('web.evento.inscribirse',array('id'=> $cart->event_id))->with('error','Ha ocurrido un error al realizar el pago, inténtalo de nuevo');
		}
		catch(Exception $e)
		{
			die('ha ocurrido un error en el pago');
		}
	}

	public function tpvRedirect(
		\App\Models\EventCart $EventCart, 
		Request $request)
	{
		$query = $EventCart::with('lines')->where('public_id', $request->get('insc'));
		$cart = $query->first();

		$total_amount = 0;

		foreach($cart->lines as $line)
			$total_amount += $line->payment_amount;

		if (app()->environment('development') || $cart->contact_email == 'djmon84@gmail.com')
		{
        	Redsys::setEnviroment('test');
			$key = config('redsys.key_test');
		}
        else
        {
        	Redsys::setEnviroment('live');
			$key = config('redsys.key');
        }

        Redsys::setAmount($total_amount);

        //TODO: el order??
        Redsys::setOrder(rand(1000,9999) . str_pad($cart->public_id , 8, '0', STR_PAD_LEFT));
        Redsys::setMerchantcode(config('redsys.merchant_code')); //Reemplazar por el código que proporciona el banco
        Redsys::setCurrency('978');
        Redsys::setTransactiontype('0');
        Redsys::setTerminal('1');
        Redsys::setMethod('T'); //Solo pago con tarjeta, no mostramos iupay
        Redsys::setNotification(route('tpv_notification')); //Url de notificacion
        Redsys::setUrlOk(route('web.inscripcion_completada', [$cart->public_id])); //Url OK
        Redsys::setUrlKo(route('web.inscripcion_ko')); //Url KO   
        Redsys::setVersion('HMAC_SHA256_V1');
        Redsys::setTradeName('eventosiglesiasenrestauracion.com');
        Redsys::setTitular('Asociación Jóvenes para Cristo');
        Redsys::setProductDescription('Pago inscripción '. $cart->public_id);
        Redsys::setMerchantData($cart->public_id);
        Redsys::setIdForm('form_tpv');
        $signature = Redsys::generateMerchantSignature($key);
        Redsys::setMerchantSignature($signature);
        Log::info(Redsys::getParameters());

        $form = Redsys::createForm();

        $params = array(
        	'form' => $form
        );

        return view('pages.web.tpv_redirect', $params);
	}

	public function getEventInscriptionStatus($evento)
	{
		$now = time();

		$date_inscription_start = strtotime($evento->date_inscription_start);
		$date_inscription_end = strtotime($evento->date_inscription_end);

		if ($now < $date_inscription_start)
			$inscription_status = 'PENDING';
		else if ($now < $date_inscription_end)
			$inscription_status = 'OPENED';
		else
			$inscription_status = 'CLOSED';

		return $inscription_status;
	}

	public function getEventStatus($evento)
	{
		$now = time();

		$date_event_from = strtotime($evento->date_from);
		$date_event_to   = strtotime($evento->date_to);

		if ($now < $date_event_from)
			$event_status = 'PENDING';
		else if ($now < $date_event_to)
			$event_status = 'PROGRESS';
		else
			$event_status = 'ENDED';

		return $event_status;
	}

	public function getInformacion(\App\Models\Evento $event, $event_id)
	{  
		$evento = $event::with('lodgements.lodgement')->find($event_id);
		$status = $this->getEventStatus($evento);

		if ($status == 'ENDED')
			return redirect()->route('web.index');

		$params = [
			'event' => $evento
		];

		$params['inscription_status'] = $this->getEventInscriptionStatus($evento);
		$params['event_status'] = $this->getEventStatus($evento);

		$params['event_info'] = view('eventsinfo.event'.$evento->id, array(
			'event' => $evento
		));

		return view('pages.web.evento.informacion', $params);
	}

	public function getContact()
	{
		$params = [
		];

		return view('pages.web.contacto', $params);
	}

	public function getInscribirse(
		\App\Models\Evento $Event, 
		\App\Models\EventCart $EventCart,
		Request $request,
		$event_id
	)
	{
		$event = $Event::find($event_id);
		
		$event_status = $this->getEventInscriptionStatus($event);
		
		if ($event_status == 'CLOSED' && !$request->has('src'))
		return redirect()->route('web.index');
		
		$cart = $EventCart::firstOrCreate([
			'event_id' => $event_id,
			'session'  => Session::getId(),
		]);
		
		
		if ($cart->status == 'CLOSED')
		{
			Session::regenerate(true);
			
			$cart = $EventCart::firstOrCreate([
				'event_id' => $event_id,
				'session'  => Session::getId(),
			]);
		}
		
		if (empty($cart->public_id))
		{
			$cart->public_id = \App\Utils\StrUtils::readableRandom(5);
    		$cart->save();
		}
		
		$params['event_id'] = $event_id;

		return view('pages.web.evento.inscribirse', $params);
	}

	public function getInscripcionCompletada(
		\App\Models\Evento $Event, 
		\App\Models\EventInscription $EventInscription, 
		Request $request,
		$publicId)
	{
		$inscription = $EventInscription::with(['lines','event'])->publicId($publicId)->first();
		//dd($inscription);
		$params = $request->all();
		$params['event']     	  = $inscription->event;
		$params['inscription']    = $inscription;
		$params['paid']           = ($inscription->total_paid - $inscription->total_price) >= 0;
		$params['payment_method'] = $EventInscription::$PAYMENT_DESCRIPTIONS[$inscription->payment_method]['label'];

		$generator = new \Picqer\Barcode\BarcodeGeneratorPNG();

		$params['barcode_src'] = 'data:image/png;base64,' . base64_encode($generator->getBarcode($inscription->public_id, $generator::TYPE_CODE_128));		

		return view('pages.web.inscripcion-completada', $params);
	}

	public function getImprimir(){
		return view('pages.web.imprimir');
	}
}
