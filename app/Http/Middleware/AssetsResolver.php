<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AssetsResolver
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if ($request->isMethod('get'))
        {
            $response = $next($request);

            $assets_types = array(
                '@css', '@js_header', '@js_blade','@js_footer'
            );

            $views = config('assets.used_views');

            $assets_groups = array(
                 'css'       => '',
                 'js_header' => '',
                 'js_blade'  => '',                 
                 'js_footer' => '',
            );

            $assets = config('assets.fixed',[]);

            foreach($views as $view)
            {
                $view_name = 'assets.views.'.str_replace('.','|',$view);
                $assets    = Arr::collapse([$assets, config($view_name,[])]);
            }  

            foreach($assets as $asset)
            {
                $html = '';                
                $url = Str::startsWith($asset[1], 'http')? $asset[1] : asset($asset[1]) . (str_contains($asset[1], '?') ? '&' : '?' ) .'dts={DPL_TS}';

        

                switch ($asset[0])
                {
                    case 'css':
                        $html = '<link type="text/css" href="'. $url .'" rel="stylesheet">';
                    break;
                    
                    case 'js_header': case 'js_footer':
                        $html = '<script type="text/javascript" src="'.$url.'"></script>';                        
                    break;                                    

                    case 'js_blade':
                        $html = view($asset[1]);
                    break;                           
                    
                   default:
                        $html = '';
                }

                $assets_groups[$asset[0]] .= $html.PHP_EOL;  
            }

            $response->setContent(str_replace($assets_types, $assets_groups, $response->getContent()));        
            return $response;
        }
        else
        {
            return $next($request);            
        }
    }
}