<?php

namespace App\Http\Resources;

//use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class Event extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        $event = $this->getAttributes();
        $event['meals'] = [];
        $event['studios'] = [];
        $event['lodgements'] = [];
        
        foreach($event as $key=>&$value)
            $value = $this->{$key};

        $meals_by_day = [];        

        if (isset($event['meals']))
        {
            foreach ($event['meals'] as $meal)
            {                
                if (!isset($meals_by_day[$meal['date']]))
                    $meals_by_day[$meal['date']] = [];
                $meals_by_day[$meal['date']][] = $meal;
            }
        }
        else
        {
            $event['meals'] = [];
        }
        
        $event['meals_by_day'] = $meals_by_day;

        $lodgement_dates = [];

        if (!empty($event['lodgements']))
		{
			$lodgements_total_nights = \DateUtils::getDaysBetweenDates($event['date_from'], $event['date_to']);

			for ($a = 0; $a < $lodgements_total_nights; $a++)
			{
				$day = date_format(date_add(date_create($event['date_from']), date_interval_create_from_date_string((string) $a .' days')), 'Y-m-d');

                $lodgement_dates[] = $day;
            }

            $event['lodgement_dates'] = $lodgement_dates;
        }

        return $event;
    }
}
