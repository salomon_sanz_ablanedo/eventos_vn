<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventLodgementRoom extends Model 
{
	protected $table = 'event_lodgement_rooms';

	public $timestamps = false;

	protected $guarded = array('room_id');

	protected $primaryKey = 'room_id';

	protected 
		$room_id,
		$name,
		$beds,
		$zone,
		$floor,
		$floor_zone,
		$floor_zone_order,
		$description;

	public function scopeZone($query, $zone){
		return $query->where('zone', $zone);
	}

	public function scopeFloor($query, $floor){
		return $query->where('floor', $floor);
	}

	public function scopeFloorZone($query, $fzone){
		return $query->where('floor_zone', $fzone);
	}

	public function places(){
		return $this->hasMany('App\Models\EventLodgementRoomPlace', 'room_id', 'room_id');
	}
}
