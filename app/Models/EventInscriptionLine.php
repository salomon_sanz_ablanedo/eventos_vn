<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventInscriptionLine extends Model {	

	protected $table = 'event_inscriptions_lines';
	
	public $timestamps = true;
	
	protected $guarded = array('line_id');

	protected $primaryKey = 'line_id';

	protected $casts = [
		'is_pastor'         	 => 'boolean',
		'nif_no'          		 => 'boolean',
		'meals' 				 => 'boolean',
		'meals_specials' 		 => 'boolean',
		'meals_picnic' 			 => 'boolean',
		'lodgement_without_room' => 'boolean',
		'reduced_mobility'		 => 'boolean',
		'payment_charges'   => 'array',
		'specials_details' 	=> 'boolean',
		'arrival_departure' => 'boolean',
		'nursery'   		=> 'boolean',
		'nursery_peto'   	=> 'boolean',
		'bus'   			=> 'boolean'
    ];
		
	protected 
		$line_id,
		$inscription_id,
		$public_id,
		$name,
		$surname,
		$genre,
		$birthday,
		$nif,
		$nif_no,
		$church_id,
		$event_role,
		$is_pastor,
		$lider_relationship_type,
		$lodgement_id,
		$lodgement_room,
		$lodgement_without_room,
		$reduced_mobility,
		$meals,
		$meals_specials = false,
		$meals_specials_obs,
		$meals_picnic,
		$specials_details,
		$specials_details_obs,
		$studio1,
		$studio2,
		$sports,
		$monitor,
		$nursery,
		$nursery_peto,
		$bus,
		$arrival_departure,
		$arrival_departure_obs,
		$payment_amount,
		$payment_charges,
		$welcome_kit,
		$photo;
	
	public function scopeEventId($query, $event_id){
		return $query->where($this->table.'.event_id', '=', $event_id);
	}

	public function scopeInscription($query, $id){
		return $query->where('inscription_id',$id);
	}

	public function scopePublicId($query, $id){
		return $query->where('public_id',$id);
	}

	public function inscription(){
		return $this->belongsTo('App\Models\EventInscription','inscription_id','inscription_id');
	}

	public function church(){
		return $this->belongsTo('App\Models\Church','church_id','id');
	}

	public function meals_list(){
		return $this->hasMany('App\Models\EventInscriptionLineMeal','inscription_id','line_id')->orderBy('event_meal_id', 'asc');
	}

	public function lodgement(){
		return $this->hasOne('App\Models\EventLodgement','id','lodgement_id');
	}

	public function lodgement_dates(){
		return $this->hasMany('App\Models\EventInscriptionLineLodgement', 'inscription_id', 'line_id');
	}

	public function studio_1(){
		return $this->hasOne('App\Models\EventStudio', 'id', 'studio1');
	}

	public function studio_2(){
		return $this->hasOne('App\Models\EventStudio', 'id', 'studio2');
	}

	public function charges(){
		return $this->hasMany('App\Models\EventInscriptionLineCharge', 'inscription_id', 'line_id');
	}

	public function setMealsSpecialsAttribute($value)
	{
	    $this->attributes['meals_specials'] = $value == true || $value == '1' ? true : false;
	}

	public function setSpecialsDetailsAttribute($value)
	{
	    $this->attributes['specials_details'] = $value == true || $value == '1' ? true : false;
	}

	public function _monitor(){
		return $this->hasOne('App\Models\EventInscriptionLine', 'line_id', 'monitor');
	}		
}
