<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventLodgement extends Model 
{

	protected $table = 'events_external_lodgements';

	public $timestamps = false;

	protected $guarded = array('id','event_id');

	protected $primaryKey = 'id';

	protected 
		$id,
		$name,
		$address,
		$info,
		$web,
		$maps,
		$lat,
		$long,
		$created_at,
		$updated_at,
		$event_id,
		$nigth_price,
		$html_info,
		$must_define_days,
		$external;


	public function event(){
		return $this->belongsTo('App\Models\Evento', 'id', 'event_id');
	}

	public function lodgement(){
		return $this->belongsTo('App\Models\EventLodgementLine', 'id', 'lodgement_id');
	}
}
