<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventInscriptionAnotation extends Model 
{
	protected $table = 'event_inscriptions_anotations';
	
	public $timestamps = true;
	
	protected $primaryKey = 'id';
	protected $guarded = array('id');
	protected $fillable = array('message','inscription_id','user_id','status');

		
	protected 
		$id,
		$message,
		$inscription_id,
		$user_id,
		$status;
	
	public function scopeInscriptionId($query,$id){
		return $query->where('inscription_id', '=', $id);
	}
	
	public function user(){
		return $this->hasOne('App\Models\User','user_id','user_id');	
	}
}
