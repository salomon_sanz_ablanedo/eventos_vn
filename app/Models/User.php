<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    
    protected $fillable = ['name','surname','email','password'];
    
    public $timestamps = true;     
    
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
  /*   protected
        $event_id,
        $legal_id,
        $fake_legal_id,
        $name,
        $surname,
        $birthdate,
        $gender,
        $church_id,
        $email,
        $phone1,
        $phone2,
        //$username,
        $password,
        $role;
 */
    

    public function church()
    {
        return $this->hasOne('App\Models\Church','id','church_id');
    }

    public function scopeDni($query,$dni){
        return $query->where('legal_id', 'LIKE', '%'.$dni.'%');
    }

    public function scopeEventId($query,$event_id){
        return $query->where('event_id', $event_id);
    }
    
    public function scopeEmail($query,$email){
        return $query->where('email', 'LIKE', '%'.$email.'%');
    }

    public function registrations(){
        //return $this->belongsTo('App\Models\EventUserInscription','user_id','user_id');
        return $this->hasMany('App\Models\EventUserInscription','user_id','user_id');   
    }
}
