<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventInscriptionLog extends Model {
	
	protected $table = 'event_inscriptions_log';
	
	public $timestamps = true;
	
	protected $guarded = array('id');

	protected $primaryKey = 'id';
		
	protected 
		$id,
		$text,
		$inscription_id,
		$by_user_id;
	
	public function scopeInscriptionId($query, $inscription_id){
		return $query->where('inscription_id', $inscription_id);
	}
	
	public function user(){
		return $this->belongsTo('App\Models\User','by_user_id','user_id');	
	}

	public function inscription(){
		return $this->belongsTo('App\Models\EventInscription','inscription_id','inscription_id');	
	}
}
