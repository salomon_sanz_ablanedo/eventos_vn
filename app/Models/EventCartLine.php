<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventCartLine extends Model {	

	protected $table = 'event_cart_lines';
	
	public $timestamps = true;
	
	protected $guarded = array('line_id');

	protected $primaryKey = 'line_id';

	protected $casts = [
		'is_pastor'         => 'boolean',
		'nif_no'          	=> 'boolean',
		'lodgement_dates'   => 'array',
		'lodgement_without_room'=> 'boolean',
		'reduced_mobility'		 => 'boolean',
		'meals' 						=> 'boolean',
		'meals_list'     	=> 'array',
		'meals_specials' 	=> 'boolean',
		'meals_picnic' 			=> 'boolean',
		'specials_details'  => 'boolean',
		'arrival_departure' => 'boolean',
		'bus'			    		  => 'boolean',
		'payment_charges'   => 'array',
		'nursery'   		=> 'boolean',
		'nursery_peto'   	=> 'boolean'
  ];

    protected $attributes = array(
			'nif_no'               => false,
			'is_pastor'            => false,
			'meals'                => false,
			'meals_specials'       => false,
			'meals_picnic'         => false,
		  'lodgement_without_room' => false,
		  'reduced_mobility' 		 => false,
			'specials_details'     => false,
			'specials_details_obs' => false,
			'arrival_departure'    => false,
			'bus'    			   			 => false,
			'nursery'              => false,
			'nursery_peto'         => false,
    );
		
	protected 
		$line_id,
		$cart_id,
		$public_id,
		$name,
		$surname,
		$genre,
		$birthday,
		$nif,
		$nif_no,
		$church_id,
		$event_role,
		$is_pastor = false,
		$lider_relationship_type,
		$lodgement_id,
		$lodgement_dates,
		$lodgement_without_room,
		$reduced_mobility,
		$meals,
		$meals_list,
		$meals_specials = false,
		$meals_specials_obs,
		$meals_picnic,
		$specials_details,
		$specials_details_obs,
		$studios,
		$sports,
		$nursery,
		$nursery_peto,
		$bus,
		$arrival_departure,
		$arrival_departure_obs,
		$payment_amount,
		$payment_charges;
	
	public function scopeEventId($query, $event_id){
		return $query->where($this->table.'.event_id', '=', $event_id);
	}

	public function scopeSession($query, $session){
		return $query->where($this->table.'.session', '=', $session);
	}

	public function cart(){
		return $this->belongsTo('App\Models\EventCart','cart_id','cart_id');
	}

	public function church(){
		return $this->belongsTo('App\Models\Church','id','church_id');
	}

	public function lodgement(){
		return $this->hasOne('App\Models\EventLodgement','id','lodgement_id');
	}

	public function setMealsSpecialsAttribute($value)
	{
	    $this->attributes['meals_specials'] = $value == true || $value == '1' ? true : false;
	}	

	public function setSpecialsDetailsAttribute($value)
	{
	    $this->attributes['specials_details'] = $value == true || $value == '1' ? true : false;
	}	

	// public function setIsPastorAttribute($value)
	// {
	//     $this->attributes['is_pastor'] = empty($value)? '0' : '1';
	// }	
}
