<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventMeal extends Model {

	protected $table = 'event_meals';
	
	public $timestamps = true;
	
	protected $guarded = array('id','event_id');
	
	protected $primaryKey = 'id';
		
	protected 
		$id,
		$event_id,
		$date,
		$type,
		$price,
		$meal_obs,
		$picnic_opt;
	
	public function scopeEventId($query, $event_id){
		return $query->where('event_id', '=', $event_id);
	}
}
