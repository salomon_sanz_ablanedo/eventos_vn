<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventInscription extends Model {

	const PAYMENT_CARD   = 'CARD';
	const PAYMENT_PAYPAL = 'PAYPAL';
	const PAYMENT_BANK   = 'BANK';
	const PAYMENT_CHURCH  = 'CHURCH';
	const PAYMENT_OTHER  = 'OTHER';

	public static $PAYMENT_DESCRIPTIONS = array(
		self::PAYMENT_CARD   => [
			'label' => 'Tarjeta débito/crédito',
			'info'  => ''
		],
		self::PAYMENT_PAYPAL => [
			'label' => 'Paypal',
			'info'  => ''
		],
		/* self::PAYMENT_CHURCH =>  [
			'label' => 'Pago en mi iglesia',
			'info'  => 'La aportación será en efectivo, depositando, un sobre cerrado en la ofrenda, indicando:
			ENCUENTRO + Nombre y apellidos del participante'
		], */
		self::PAYMENT_CHURCH =>  [
			'label' => 'Pago en mi iglesia',
			'info'  => 'Pida información en su Iglesia de como realizarla',
			//'info'  => 'La aportación deberá ser en efectivo dado al responsable de cada iglesia.'
		],
		self::PAYMENT_BANK   => [
			'label' => 'Ingreso/Transferencia bancaria',
			'info'  => ''
		],
		self::PAYMENT_OTHER  => [
			'label' => 'Otras formas de pago',
			'info'  => ''
		]
	);

	protected $table = 'event_inscriptions';
	
	public $timestamps = true;
	
	protected $guarded = array('inscription_id');

	protected $primaryKey = 'inscription_id';

	protected $casts = [
		'total_price' => 'float',
		'total_paid' => 'float',
    ];
		
	protected 
		$inscription_id,
		$public_id,
		$event_id,
		$contact_name,
		$contact_phone,
		$contact_email,
		$payment_method,
		$created_by_user,
		$total_price,
		$total_paid;
	
	public function scopeEventId($query, $event_id){
		return $query->where($this->table.'.event_id', '=', $event_id);
	}

	public function scopePublicId($query, $public_id){
		return $query->where('public_id', $public_id);
	}

	public function Event(){
		return $this->belongsTo('App\Models\Evento','event_id','id');
	}

	public function lines(){
		return $this->hasMany('App\Models\EventInscriptionLine','inscription_id','inscription_id');
	}

	public function notes(){
		return $this->hasMany('App\Models\EventInscriptionAnotation', 'inscription_id', 'inscription_id');
	}

	public function logs(){
		return $this->hasMany('App\Models\EventInscriptionLog','inscription_id','inscription_id')->orderBy('created_at','asc');
	}
}
