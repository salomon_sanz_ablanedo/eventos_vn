<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventInscriptionFree extends Model {

	protected $table = 'events_inscriptions_free';
	
	public $timestamps = true;
	
	protected $guarded = array('id');

	protected $primaryKey = 'id';
		
	protected 
		$id,
		$event_id,
		$nif,
		$name;
}
