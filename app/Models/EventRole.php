<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventRole extends Model {

	protected $table = 'event_roles';
	
	public $timestamps = false;
	
	protected $guarded = array('role_id');
	
	protected $primaryKey = 'role_id';
		
	protected 
		$role_id,
		$name,
		$show_in;

	const ROLE_CAMPISTA = 1;
	const ROLE_GORRA = 2;
	const ROLE_AYUDANTE_GORRA = 6;
}
