<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventInscriptionLineCharge extends Model{

	protected $table = 'event_inscriptions_lines_charges';
	
	protected $guarded = array('id');
	
	protected $primaryKey = 'id';
	
	protected $fillable = ['description','price','quantity','inscription_id','obs','manual_charge','promotion'];

	//protected $hidden = ['password'];
	
	public $timestamps = true;	

	protected $casts = [
		'promotion' => 'boolean',
    ];	
	
	protected
		$id,
		$description,
		$quantity,
		$price,
		$inscription_id,
		$obs,
		$manual_charge,
		$promotion;

	/*
	public function church()
	{
		return $this->hasOne('App\Models\Church','id','church_id');
	}
	*/

	public function scopeInscriptionLines($query, $id)
	{
		if (is_numeric($id))
			return $query->where('inscription_id', $id);
		else
			return $query->whereIn('inscription_id', $id);
	}

	//public func
}
