<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventCart extends Model {	

	protected $table = 'event_cart';
	
	public $timestamps = true;
	
	protected $guarded = array('cart_id');

	protected $primaryKey = 'cart_id';
		
	protected 
		$cart_id,
		$session,
		$public_id,
		$event_id,
		$payment_method,
		$total_paid,
		$created_by_user,
		$status,
		$contact_name,
		$contact_phone,
		$contact_email;
	
	public function scopeEventId($query, $event_id){
		return $query->where($this->table.'.event_id', '=', $event_id);
	}

	public function scopePublicId($query, $id){
		return $query->where('public_id',$id);
	}

	public function scopeSession($query, $session){
		return $query->where($this->table.'.session', '=', $session);
	}

	public function event(){
		return $this->hasOne('App\Models\Evento','id','event_id');
	}

	public function lines(){
		return $this->hasMany('App\Models\EventCartLine','cart_id','cart_id');
	}

	// public function charges(){
	// 	return $this->hasMany('\App\Models\UserCharge','inscription_id','registration_id')->orderBy('created_at','asc');
	// }
	
	// public function isPaid(){
	// 	return $this->paid;	
	// }	
}
