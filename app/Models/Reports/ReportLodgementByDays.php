<?php

namespace App\Models\Reports;

use DB, DateUtils;

class ReportLodgementByDays extends BaseEventReport
{
 	//const BASE_LAYOUT = 'layouts.admin.report';
	protected $layout = 'layouts.admin.report_table';
 	public $title 	  = 'Alojamiento por días';
 	public $note 	  = '';

	protected static $params_mapping = array(
		'order_by' 	   => 'Orden',
		'lodgement_id' => 'Alojamiento',
		'days'         => 'Fecha'
	);

	public function processData($params)
	{
		if (is_array($params['lodgement_id']))
			$params['lodgement_id'] = implode(',',$params['lodgement_id']);

		$this->params = $params;

		//dd($params);

		$q = 'SELECT 
			l.inscription_id,
			l.surname, 
			l.name, 
			TIMESTAMPDIFF( YEAR, l.birthday, "'.$this->event->date_from.'" ) AS age,
			churchs.city,
			DATE_FORMAT(event_inscriptions_lines_lodgements.date,"%d/%m/%Y") as date,
			events_external_lodgements.name as lugar
		FROM eventsportal.event_inscriptions_lines l
		INNER JOIN eventsportal.event_inscriptions i ON l.inscription_id = i.inscription_id
		INNER JOIN eventsportal.churchs ON l.church_id = churchs.id
		INNER JOIN eventsportal.events_external_lodgements ON l.lodgement_id = events_external_lodgements.id
		INNER JOIN eventsportal.event_inscriptions_lines_lodgements ON l.line_id = event_inscriptions_lines_lodgements.inscription_id
		WHERE i.event_id ='.$this->event->id.'
		AND date = "'. $params['days'].'" 
		AND l.lodgement_id IN('.$params['lodgement_id'].')';

		switch ($params['order_by'])
		{
			case 'iglesias':
				$q .= ' ORDER BY churchs.city ASC, surname ASC, name ASC';
			break;

			case 'apellidos':
				$q .= ' ORDER BY surname ASC, name ASC';
			break;

			case 'alojamiento':
				$q .= ' ORDER BY lugar ASC, name ASC, surname ASC';
			break;
		}		

		//DB::setFetchMode(\PDO::FETCH_ASSOC);

		$this->data_rows = DB::select($q);

		$this->data_headers = array('Insc.','Apellidos','Nombre','Edad','Iglesia', 'Fecha','Lugar');

		//dd($this->data_rows);
	}
}