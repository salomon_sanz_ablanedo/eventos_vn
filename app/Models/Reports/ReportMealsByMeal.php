<?php

namespace App\Models\Reports;

use DB;
use \Illuminate\Support\Arr;


class ReportMealsByMeal extends BaseEventReport
{
 	//const BASE_LAYOUT = 'layouts.admin.report';
	protected $layout = 'layouts.admin.report_table';
 	public $title = 'Inscripciones con comida';
 	public $note = '';
	protected static $params_mapping = array(
		'meals' => 'Comidas',
		'order_by' => 'Orden',
		'comen' => 'Comen'
	);

	public function processData($params)
	{	
		if (is_array($params['meals']))
			$params['meals'] = implode(',',$params['meals']);

		$this->params = $params;

		// necesario para las pulseras
		if ($this->params['meals'] == 'someday')
		{
			if (!in_array($this->event->type, ['CAMPAMENTO','PRECAMPAMENTO']))
			{
				$q = 'SELECT 
					l.line_id,
					l.inscription_id,
					l.public_id,
					l.surname, 
					l.name,
					l.nursery,
					TIMESTAMPDIFF( YEAR, l.birthday, "'.$this->event->date_from.'" ) AS age, 
					churchs.city,
					count(*) as total_meals,
					m.registration_date
					FROM eventsportal.event_inscriptions_lines l
					INNER JOIN eventsportal.event_inscriptions i ON l.inscription_id = i.inscription_id
					INNER JOIN eventsportal.churchs ON l.church_id = churchs.id
					INNER JOIN eventsportal.event_inscriptions_lines_meals m ON l.line_id = m.inscription_id
					INNER JOIN eventsportal.event_meals ON m.event_meal_id = event_meals.id
					WHERE 
					i.event_id='.$this->event->id .' 
					AND nursery=0
					GROUP BY l.line_id
					ORDER BY l.inscription_id ASC';

					$this->data_headers = array('Insc.','Código','Apellidos','Nombre','Edad','Iglesia','Comidas', 'F. marcaje');
			}
			else
			{
				$q = 'SELECT 
				l.inscription_id,
				l.surname, 
				l.name, 
				TIMESTAMPDIFF( YEAR, l.birthday, "'.$this->event->date_from.'" ) AS age, 
				churchs.city,
				if (l.meals_specials = 0, "no","si") as meals_specials,
				l.meals_specials_obs,
				m.event_inscriptions_lines_meals.registration_date
				FROM eventsportal.event_inscriptions_lines l
				INNER JOIN eventsportal.event_inscriptions i ON l.inscription_id = i.inscription_id
				INNER JOIN eventsportal.churchs ON l.church_id = churchs.id
				WHERE i.event_id='.$this->event->id .' 
				AND l.lodgement_id IS NOT NULL 
				GROUP BY l.line_id
				ORDER BY l.inscription_id ASC';

				die($q);

				$this->data_headers = array('Insc.','Apellidos','Nombre','Edad','Iglesia','Dieta esp.','obs.');
			}
		}
		else
		{
			$EventMeal = new \App\Models\EventMeal;
			$meal = $EventMeal::find($params['meals']);

			$this->params['meals'] = $meal->type .' '.$meal->date;

			// if ($params['comen'] == 'SI')
			// {
				$q = 'SELECT 
				l.inscription_id,
				l.surname, 
				l.name, 
				TIMESTAMPDIFF( YEAR, l.birthday, "'.$this->event->date_from.'" ) AS age, 
				churchs.city,
				if (l.meals_specials = 0, "no","si") as meals_specials,
				l.meals_specials_obs,
				event_meals.type,
				if (event_meals.picnic_opt = 0 OR l.meals_picnic = 0, "no","si") as picnic,
				DATE_FORMAT(event_meals.date,"%d/%m/%Y") as date,
				m.registration_date
				FROM eventsportal.event_inscriptions_lines l
				INNER JOIN eventsportal.churchs ON l.church_id = churchs.id
				INNER JOIN eventsportal.event_inscriptions_lines_meals m ON l.line_id = m.inscription_id
				INNER JOIN eventsportal.event_meals ON m.event_meal_id = event_meals.id
				WHERE m.event_meal_id IN('.$params['meals'].')';

				$this->data_headers = array('Insc.','Apellidos','Nombre','Edad','Iglesia','Dieta esp.','obs.','tipo','picnic','fecha','marcaje');

				// switch ($params['order_by'])
				// {
				// 	case 'apellidos':
				// 		$q .= ' ORDER BY churchs.city ASC, surname ASC, name ASC ';
				// 	break;

				// 	case 'inscripciones': case 'fecha':
				// 		$q .= ' ORDER BY churchs.city ASC, insc ASC, age DESC';
				// 	break;
				// }

			// }
			// else
			// {
			// 	$q = 'SELECT DISTINCT(users.user_id),
			// 	IF (event_inscriptions_users.parent_inscription IS NULL, event_inscriptions_users.registration_id, event_inscriptions_users.parent_inscription) as insc,
			// 	users.surname, 
			// 	users.name, 
			// 	TIMESTAMPDIFF( YEAR, users.birthday, "'.$this->event->date_from.'" ) AS age, 
			// 	churchs.city
			// 	FROM eventsportal.event_inscriptions_users
			// 	INNER JOIN eventsportal.users ON event_inscriptions_users.user_id = users.user_id 
			// 	INNER JOIN eventsportal.churchs ON users.church_id = churchs.id
			// 	INNER JOIN eventsportal.events ON event_inscriptions_users.event_id = events.id
			// 	WHERE
			// 	NOT EXISTS(SELECT * FROM event_meals_users WHERE event_meals_users.inscription_id = event_inscriptions_users.registration_id AND event_meals_users.event_meal_id IN('.$params['meals'].'))
			// 	AND events.id='.$this->event->id;

			// 	$this->data_headers = array('Insc.','Apellidos','Nombre','Edad','Iglesia');
			// }

			switch ($params['order_by'])
			{
				case 'apellidos':
					$q .= ' ORDER BY churchs.city ASC, surname ASC, name ASC, date ASC, type ASC';
				break;

				case 'inscripciones':
					$q .= ' ORDER BY churchs.city ASC, insc ASC, age DESC , date ASC, type ASC';
				break;

				case 'fecha':
					$q .= ' ORDER BY date ASC, type ASC, churchs.city ASC, surname ASC, name ASC';
				break;

				case 'marcaje':
					$q .= ' ORDER BY registration_date ASC';
				break;
			}
		}
	
		//DB::setFetchMode(\PDO::FETCH_ASSOC);
		$this->data_rows = DB::select($q);
	}

	final public function braceletOutput()
	{
		$id_collect = Arr::pluck($this->data_rows, 'line_id');
		return with(\App\Services\EventsService::getInstance())->getBraceletHtml($id_collect, false, false,'adults');
	}
}