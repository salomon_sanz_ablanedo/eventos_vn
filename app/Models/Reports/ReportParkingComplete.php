<?php

namespace App\Models\Reports;

use DB;

class ReportParkingComplete extends BaseEventReport
{
 	//const BASE_LAYOUT = 'layouts.admin.report';
	protected $layout = 'layouts.admin.report_table';
 	public $title = 'Parking evento completo';
 	public $note = '';
	protected static $params_mapping = array(
		'order_by' => 'Orden'
	);

	public function processData($params)
	{
		$this->params = $params;

		$q = 'SELECT 
			IF (event_inscriptions_users.parent_inscription IS NULL, event_inscriptions_users.registration_id, event_inscriptions_users.parent_inscription) as insc,
			users.surname, 
			users.name, 
			TIMESTAMPDIFF( YEAR, users.birthdate, "'.$this->event->date_from.'" ) AS age, 
			churchs.city,
			users.legal_id
		FROM eventsportal.event_inscriptions_users
		INNER JOIN eventsportal.users ON event_inscriptions_users.user_id = users.user_id 
		INNER JOIN eventsportal.churchs ON users.church_id = churchs.id
		WHERE event_inscriptions_users.event_id ='.$this->event->id.'
		AND event_inscriptions_users.parking=1';

		switch ($params['order_by'])
		{
			case 'iglesias':
				$q .= ' ORDER BY churchs.city ASC, surname ASC, name ASC';
			break;

			case 'apellidos':
				$q .= ' ORDER BY surname ASC, name ASC';
			break;
		}			

		//DB::setFetchMode(\PDO::FETCH_ASSOC);
		
		$this->data_rows = DB::select($q);

		$this->data_headers = array('Insc.','Apellidos','Nombre','Edad','Iglesia','DNI');
	}
}