<?php

namespace App\Models\Reports;

use App\Models\Evento;
use View, Exception, Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;


abstract class BaseEventReport {

	protected $layout; 				// layout que usará (obligatorio definir para htmlOutput)
 	public $event;  				// evento del informe
 	public $title;   			// nombre del informe
 	public $note;				// anotaciones sobre el informe
 	public $params; 				// parámetros de entrada
 	public $params_mapped;			// parámetros de entrada legibles
 	protected static $params_mapping;
 	public $data_headers;   		// nombre de los campos en orden
 	public $data_rows = []; 		// array de arrays en el mismo orden que los campos

 	const MANDADORY_FIELDS = array('layout','event','title');

	final public function __construct(\App\Models\Evento $event, $params){
		$this->event = $event;
		$this->params = $params;

		$this->checkMandatoryFields();
		$this->processData($params);
	}

	final private function checkMandatoryFields()
	{
		foreach(self::MANDADORY_FIELDS as $f)
		{
			if (empty($this->{$f}))
				throw new Exception('property "'.$f.'" is not defined, is mandatory to be defined in the class');
		}
	}

	abstract public function processData($params);

	final public function excelOutput()
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle(Str::limit($this->title,29));

		foreach($this->data_rows as &$row)
			$row = (array) $row;

		$this->data_rows = Arr::prepend($this->data_rows, $this->data_headers);

		$sheet->fromArray(
			$this->data_rows
		);


		// encabezados
		// $sheet->getCellsRange('A1:'.chr(ord('A') + count($this->data_headers)-1).'1', function($cells) {
		// 	$cells->setBackground('#000000');
		// 	$cells->setFont(array(
		// 		'size'       => '14',
		// 		'bold'       =>  true,
		// 	));
		// 	$cells->setFontColor('#ffffff');
		// 	$cells->setValignment('middle');
		// });

		//$sheet->setHeight(1, 30);
		//$sheet->row(1, $this->data_headers);

		# título
		// $sheet->prependRow(1, array(
		// 	$this->title .' - '.date('d/m/y')
		// ));

		// $sheet->setHeight(1, 50);

		// $sheet->cells('A1', function($cells) {
		// 	$cells->setFont(array(
		// 		'size'       => '18',
		// 		'bold'       =>  true,
		// 	));
		// 	$cells->setValignment('middle');
		// });

		// $sheet->mergeCells('A1:'.chr(ord('A') + count($this->data_headers)-1).'1');
		$filename = Str::slug($this->title);
		$writer = new Xlsx($spreadsheet);
		//$writer->save(.'.xlsx');

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); /*-- $filename is  xsl filename ---*/
		header('Cache-Control: max-age=0');
		
		$writer->save('php://output');
	}

	final public function htmlOutput($print = false)
	{
		if (empty($this->layout))
			throw new Exception('Report view not defined');

		$this->params_mapped = $this->params;

		foreach ($this->params as $key=>$value)
		{
			if (array_key_exists($key, $this::$params_mapping)){
				$this->params_mapped[$this::$params_mapping[$key]] = $value;
				unset($this->params_mapped[$key]);
			}
			else
				$this->params_mapped[$key] = $value;
		}

		return view($this->layout, array(
			'report' => $this,
			'print' => $print
		));
	}

	// needed to expor xls
	public function collection()
    {
        return collect($this->data_rows);
	}
	
	public function headings(): array
    {
        return $this->data_headers;
    }
}