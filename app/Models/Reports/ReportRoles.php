<?php

namespace App\Models\Reports;

use DB;

class ReportRoles extends BaseEventReport
{
 	//const BASE_LAYOUT = 'layouts.admin.report';
	protected $layout = 'layouts.admin.report_table';
 	public $title = 'Roles de evento';
 	public $note = '';
	protected static $params_mapping = array(
		'roles' => 'Roles de evento',
		'order_by' => 'Orden'
	);

	public function processData($params)
	{	
		if (is_array($params['roles']))
			$params['roles'] = implode('","',$params['roles']);

		$this->params = $params;

		$q = 'SELECT 
			IF (event_inscriptions_users.parent_inscription IS NULL, event_inscriptions_users.registration_id, event_inscriptions_users.parent_inscription) as insc,
			event_inscriptions_users.public_id,
			users.surname, 
			users.name, 
			TIMESTAMPDIFF( YEAR, users.birthdate, "'.$this->event->date_from.'" ) AS age, 
			churchs.city, 
			users.phone1, 
			users.phone2,
			event_inscriptions_users.event_role
		FROM eventsportal.event_inscriptions_users
		INNER JOIN eventsportal.users ON event_inscriptions_users.user_id = users.user_id 
		INNER JOIN eventsportal.churchs ON users.church_id = churchs.id
		WHERE event_id ='.$this->event->id. '
		AND event_inscriptions_users.event_role IN("'.$params['roles'].'")';

		switch ($params['order_by'])
		{
			case 'apellidos':
				$q .= ' ORDER BY surname ASC, name ASC';
			break;

			case 'iglesias':
				$q .= ' ORDER BY city ASC, role ASC, surname ASC, name ASC';
			break;

			case 'role':
				$q .= ' ORDER BY role ASC, city ASC, surname ASC, name ASC';
			break;
		}

		//\Config::set('database.fetch', \PDO::FETCH_ASSOC);
		//DB::setFetchMode(\PDO::FETCH_ASSOC);

		$this->data_rows = DB::select($q);

		$this->data_headers = array('Insc.','Ref.','Apellidos','Nombre','Edad','Iglesia','Tfn1.','Tfn2.','Rol');
	}
}