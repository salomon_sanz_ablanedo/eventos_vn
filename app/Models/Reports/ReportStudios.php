<?php

namespace App\Models\Reports;

use DB;

class ReportStudios extends BaseEventReport
{
 	//const BASE_LAYOUT = 'layouts.admin.report';
	protected $layout = 'layouts.admin.report_table';
 	public $title = 'Apuntados a talleres';
 	public $note = '';
	protected static $params_mapping = array(
		'order_by' => 'Orden',
		'studio_id' => 'Taller'
	);

	public function processData($params)
	{	
		$this->params = $params;

		$this->params['studio_id'] = explode('|',$params['studio_id'])[1];

		$q = 'SELECT 
			l.inscription_id,
			l.surname, 
			l.name, 
			TIMESTAMPDIFF( YEAR, l.birthday, "'.$this->event->date_from.'" ) AS age, 
			churchs.city, 
			IF(s1.id = '.$this->params['studio_id'].', s1.short_name, s2.short_name) AS studio
		FROM eventsportal.event_inscriptions_lines l
		INNER JOIN eventsportal.event_inscriptions i ON l.inscription_id = i.inscription_id
		INNER JOIN eventsportal.churchs ON l.church_id = churchs.id
		LEFT JOIN eventsportal.event_studios s1 ON (l.studio1 = s1.id)
		LEFT JOIN eventsportal.event_studios s2 ON (l.studio2 = s2.id)
		WHERE i.event_id ='.$this->event->id.'
		AND (l.studio1 = '.$this->params['studio_id'] .' OR l.studio2 = '.$this->params['studio_id'].')';

		switch ($params['order_by'])
		{
			case 'apellidos':
				$q .= ' ORDER BY surname ASC, name ASC';
			break;

			case 'iglesias':
				$q .= ' ORDER BY churchs.city ASC, surname ASC, name ASC';
			break;

			case 'importe_asc':
				$q .= ' ORDER BY total ASC, surname ASC, name ASC';
			break;

			case 'importe_desc':
				$q .= ' ORDER BY total DESC, surname ASC, name ASC';
			break;
		}

		//\Config::set('database.fetch', \PDO::FETCH_ASSOC);
		//DB::setFetchMode(\PDO::FETCH_ASSOC);

		$this->data_rows = DB::select($q);

		$this->data_headers = array('Insc.','Apellidos','Nombre','Edad','Iglesia','Taller');
	}
}