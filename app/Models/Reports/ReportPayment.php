<?php

namespace App\Models\Reports;

use DB;

class ReportPayment extends BaseEventReport
{
 	//const BASE_LAYOUT = 'layouts.admin.report';
	protected $layout = 'layouts.admin.report_table';
 	public $title = 'Balances de pago';
 	public $note = '';
	protected static $params_mapping = array(
		'payment_balance' => 'Balance de pago',
		//'order_by' => 'Orden'
	);

	public function processData($params)
	{	
		$this->params = $params;

		$q = 'SELECT 
			i.inscription_id,
			i.contact_name,
			i.contact_phone, 
			i.contact_email,
			churchs.city,
			SUM(IF(c.quantity > 0, c.quantity * c.price,0)) AS total_to_paid,
			SUM(IF(c.quantity < 0, c.quantity * c.price,0)) AS total_paid,
			SUM(c.quantity * c.price) as total
		FROM eventsportal.event_inscriptions_lines l
		INNER JOIN eventsportal.event_inscriptions i ON l.inscription_id = i.inscription_id
		INNER JOIN eventsportal.churchs ON l.church_id = churchs.id
		INNER JOIN eventsportal.event_inscriptions_lines_charges c ON c.inscription_id = l.line_id
		WHERE i.event_id ='.$this->event->id. ' GROUP BY i.inscription_id';

		switch ($params['payment_balance'])
		{
			case 'paid':
				$q .= ' HAVING total = 0';
			break;

			case 'unpaid':
				$q .= ' HAVING total > 0';
			break;

			case 'refundable':
				$q .= ' HAVING total < 0';
			break;
		}

		switch ($params['order_by'])
		{
			case 'apellidos':
				$q .= ' ORDER BY contact_name ASC';
			break;

			case 'iglesias':
				$q .= ' ORDER BY churchs.city ASC, contact_name ASC';
			break;

			case 'importe_asc':
				$q .= ' ORDER BY total ASC, contact_name ASC';
			break;

			case 'importe_desc':
				$q .= ' ORDER BY total DESC, contact_name ASC';
			break;
		}

		$this->data_rows = DB::select($q);

		$this->data_headers = array('Insc.','Nombre','Tfn.','Email','Iglesia','A pagar','Pagado','Saldo');
	}
}