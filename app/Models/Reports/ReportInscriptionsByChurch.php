<?php

namespace App\Models\Reports;

use DB;

class ReportInscriptionsByChurch extends BaseEventReport
{
 	//const BASE_LAYOUT = 'layouts.admin.report';
	protected $layout = 'layouts.admin.report_table';
 	public $title = 'Asistentes por iglesia';
 	public $note = 'En inscripciones de grupo, el id de inscripción mostrado es el de la inscripción padre';
	protected static $params_mapping = array(
		'church' => 'Iglesia/s',
		'order_by' => 'Orden'
	);

	public function processData($params)
	{	
		if (is_array($params['church']))
			$params['church'] = implode(',',$params['church']);

		$this->params = $params;
		$has_studios = count($this->event->studios) > 0;

		/****************
		/* DATA HEADERS */

		$data_headers = array('Insc.','Código B.','Apellidos','Nombre','Edad','Sexo','Iglesia', 'Dieta esp.','Precio');

		if ($has_studios)
		{
			$data_headers[] = 'Taller1';
			$data_headers[] = 'Taller2';
		}

		if ($this->event->type == 'CAMPAMENTO')
		{
			$data_headers[] = 'Observac.';
			$data_headers[] = 'Habitación';
			$data_headers[] = 'Rol';
			$data_headers[] = 'Gorra';
		}

		/***********
		/* SELECT */

		$SELECT = array(
			'l.inscription_id',
		    'l.public_id',
			$params['order_by'] == 'nombre' ? 'l.name' : 'l.surname', 
			$params['order_by'] == 'nombre' ? 'l.surname' : 'l.name',
			'TIMESTAMPDIFF( YEAR, l.birthday, "'.$this->event->date_from.'" ) AS age', 
			'l.genre',
			'churchs.city',
			'l.meals_specials_obs',
			'l.payment_amount'			
		);

		if ($has_studios)
		{
			$SELECT[] = 's1.short_name as name_studio1';
			$SELECT[] = 's2.short_name as name_studio2';
		}

		if ($this->event->type == 'CAMPAMENTO')
		{	
			$SELECT[] = 'l.specials_details_obs';
			$SELECT[] = 'l.lodgement_room';
			$SELECT[] = 'r.name as rol'; // role name
			$SELECT[] = 'CONCAT(l2.name, " ", l2.surname) as _monitor';

		}

		// do not use empty() because sometimes the bus is free and is included in the price
		if ($this->event->bus_cost !== null)
		{
			$data_headers[] = 'Autobús';
			$SELECT[] = 'l.bus';
		}

		/**********
		/* FROM */

		$FROM = array(
			'eventsportal.event_inscriptions_lines l',
			'INNER JOIN eventsportal.event_inscriptions i ON l.inscription_id = i.inscription_id',
			'INNER JOIN eventsportal.churchs ON l.church_id = churchs.id'
		);

		if ($has_studios)
		{
			$FROM[] = 'LEFT JOIN eventsportal.event_studios s1 ON (l.studio1 = s1.id)';
			$FROM[] = 'LEFT JOIN eventsportal.event_studios s2 ON (l.studio2 = s2.id)';
		}

		if ($this->event->type == 'CAMPAMENTO'){
			$FROM[] = ' LEFT JOIN eventsportal.event_inscriptions_lines l2 ON l.monitor = l2.line_id';
			$FROM[] = ' LEFT JOIN eventsportal.event_roles r ON l.event_role = r.role_id';
		}

		/**********
		/* WHERE */

		$WHERE = array(
			'i.event_id ='.$this->event->id
		);

		if ($params['church'] != 'ALL')
			$WHERE[] = 'l.church_id IN ('.$params['church'].')';

		$q = 'SELECT '.implode(', ', $SELECT) .' FROM '.implode(' ', $FROM) . ' WHERE '.implode(' AND ', $WHERE);

		switch ($params['order_by'] ) 
		{
			case 'apellidos':
				$q .= ' ORDER BY surname ASC, name ASC';
			break;

			case 'nombre':
				$q .= ' ORDER BY name ASC, surname ASC';
			break;

			case 'inscripciones':
				$q .= ' ORDER BY inscription_id ASC';
			break;
			
			case 'talleres':
				$q .= ' ORDER BY name_studio ASC, surname ASC, name ASC';
			break;

			case 'iglesias': default:
				$q .= ' ORDER BY churchs.city ASC, inscription_id ASC, age DESC';
			break;

			case 'roles':
				$q .= ' ORDER BY rol, surname ASC, name ASC';
			break;
		}

		//die($q);


		//\Config::set('database.fetch', \PDO::FETCH_ASSOC);
		//DB::setFetchMode(\PDO::FETCH_ASSOC);

		$this->data_rows = DB::select($q);
		$this->data_headers = $data_headers;
	}
}