<?php

namespace App\Models\Reports;

use DB;
use \Illuminate\Support\Arr;


class ReportNurseryByYear extends BaseEventReport
{
	public $layout = 'layouts.admin.report_table';
 	public $title = 'Guarderías - Listado por año de nacimiento';

	protected static $params_mapping = array(
		'year_from' => 'desde el año',
		'year_to'   => 'hasta el año',
		'nursery'  => 'guardería',
		'order_by' => 'orden'
	);

	public function processData($params)
	{
		$year_from = $params['year_from'];
		$year_to = $params['year_to'];

		$q = 'SELECT l.inscription_id, l.surname, l.name, YEAR(l.birthday) as year, churchs.city as church, i.contact_name, i.contact_phone, l.lider_relationship_type, IF(l.nursery = 1, "si", "no") AS _nursery,
			IF(nursery_peto = 1, "si", "no") AS _nursery_peto';

		// solo para brazaletes
		if ($params['output_type'] == 'bracelet')
			$q .= ', l.line_id';

		$q .= '	FROM eventsportal.event_inscriptions_lines l
		INNER JOIN eventsportal.event_inscriptions i ON l.inscription_id = i.inscription_id
		INNER JOIN eventsportal.churchs ON l.church_id = churchs.id
		WHERE i.event_id ='.$this->event->id.'
		AND YEAR(l.birthday) >= "'.$year_from.'" 
		AND YEAR(l.birthday) <= "'.$year_to.'"';

		if ($params['nursery'] == 'si')
			$q .= 'AND l.nursery = 1';			
		else if ($params['nursery'] == 'no')
			$q .= 'AND l.nursery = 0';			

		switch ($params['order_by'])
		{
			case 'apellidos':
				$q .= ' ORDER BY surname ASC, name ASC';
			break;

			case 'inscripciones':
				$q .= ' ORDER BY inscription_id ASC, surname ASC, name ASC';
			break;

			case 'año':
				$q .= ' ORDER BY year ASC, surname ASC, name ASC';
			break;

			case 'iglesias':
				$q .= ' ORDER BY city ASC, surname ASC, name ASC';
			break;
		}

		//DB::setFetchMode(\PDO::FETCH_ASSOC);
		$this->data_rows = DB::select($q);

		$this->data_headers = array('Insc.','Apellidos','Nombre','Año','Iglesia','Contacto','Teléfono','Relación','Guardería','Peto comprado');

		if ($params['output_type'] == 'bracelet')
			$this->data_headers[] = 'Id linea';
	}

	final public function braceletOutput(){
		$id_collect = Arr::pluck($this->data_rows, 'line_id');	

		//dd($id_collect);	
		return with(\App\Services\EventsService::getInstance())->getBraceletHtml($id_collect, false, false, 'kids');
	}
}