<?php

namespace App\Models\Reports;

use DB;

class ReportNurseryByAge extends BaseEventReport
{
 	//const BASE_LAYOUT = 'layouts.admin.report';
	protected $layout = 'layouts.admin.report_table';
 	public $title = 'Guarderías - Listado por edad';
 	public $note = 'La edad se calcula en base al primer día del evento';
	protected static $params_mapping = array(
		'age_from' => 'desde edad',
		'age_to'   => 'hasta edad',
		'nursery'  => 'guardería',
		'order_by' => 'orden'
	);

	public function processData($params)
	{	
		$edad_from = date('Y-m-d',strtotime('-'.$params['age_from'].' year',strtotime($this->event->date_from)));
		$edad_to = date('Y-m-d',strtotime('-'.($params['age_to']+1).' year',strtotime($this->event->date_from)));

		$q = 'SELECT l.surname, l.name, TIMESTAMPDIFF( YEAR, l.birthday, "'.$this->event->date_from.'") AS age, churchs.city as church, i.contact_name, i.contact_phone, l.lider_relationship_type, IF(nursery = 1, "si", "no") AS _nursery,
			IF(nursery_peto = 1, "si", "no") AS _nursery_peto

		FROM eventsportal.event_inscriptions_lines l
		INNER JOIN eventsportal.event_inscriptions i ON l.inscription_id = i.inscription_id
		INNER JOIN eventsportal.churchs ON l.church_id = churchs.id
		WHERE i.event_id ='.$this->event->id.'
		AND l.birthday <= "'.$edad_from.'" 
		AND l.birthday >= "'.$edad_to.'"';

		if ($params['nursery'] == 'si')
			$q .= 'AND l.nursery = 1';			
		else if ($params['nursery'] == 'no')
			$q .= 'AND l.nursery = 0';	

		switch ($params['order_by'])
		{
			case 'apellidos':
				$q .= ' ORDER BY surname ASC, name ASC';
			break;

			case 'inscripciones':
				$q .= ' ORDER BY inscription_id ASC, surname ASC, name ASC';
			break;

			case 'edad':
				$q .= ' ORDER BY age ASC, surname ASC, name ASC';
			break;

			case 'iglesias':
				$q .= ' ORDER BY city ASC, surname ASC, name ASC';
			break;
		}
		
		//DB::setFetchMode(\PDO::FETCH_ASSOC);
		
		$this->data_rows = DB::select($q);

		$this->data_headers = array('Apellidos','Nombre','Edad','Iglesia','Contacto','Teléfono','Relación','Guardería','Peto comprado');
	}
}