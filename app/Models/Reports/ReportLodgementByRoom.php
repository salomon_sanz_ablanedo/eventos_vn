<?php

namespace App\Models\Reports;

use DB, DateUtils;

class ReportLodgementByRoom extends BaseEventReport
{
 	//const BASE_LAYOUT = 'layouts.admin.report';
	protected $layout = 'layouts.admin.report_table';
 	public $title 	  = 'Alojamiento por habitación';
 	public $note 	  = '';

	protected static $params_mapping = array(
		'order_by' 	   => 'Orden',
		'lodgement_id' => 'Alojamiento',
		'days'         => 'Fecha'
	);

	public function processData($params)
	{
		if (is_array($params['lodgement_id']))
			$params['lodgement_id'] = 6;//implode(',',$params['lodgement_id']);

		$this->params = $params;

		$q = 'SELECT 
			l.inscription_id,
			l.surname, 
			l.name, 
			TIMESTAMPDIFF( YEAR, l.birthday, "'.$this->event->date_from.'" ) AS age,
			churchs.city,
			l.lodgement_room
		FROM eventsportal.event_inscriptions_lines l
		INNER JOIN eventsportal.event_inscriptions i ON l.inscription_id = i.inscription_id
		INNER JOIN eventsportal.churchs ON l.church_id = churchs.id
		WHERE i.event_id ='.$this->event->id.'
		AND l.lodgement_id = '. $params['lodgement_id'];

		switch ($params['filter_room'])
		{
			case 'sin':
				$q .= ' AND l.lodgement_room IS NULL';
			break;

			case 'con':
				$q .= ' AND l.lodgement_room IS NOT NULL';
			break;
		}

		if (!empty($params['desde']))
			$q .= ' AND i.created_at >= "'.DateUtils::standarDate2mysqlDate($params['desde']).' 00:00:00"';

		if (!empty($params['hasta']))
			$q .= ' AND i.created_at <= "'.DateUtils::standarDate2mysqlDate($params['hasta']).' 23:59:59"';

		switch ($params['order_by'])
		{
			case 'iglesias':
				$q .= ' ORDER BY churchs.city ASC, surname ASC, name ASC';
			break;

			case 'inscripcion':
				$q .= ' ORDER BY inscription_id ASC, surname ASC, name ASC';
			break;

			case 'apellidos':
				$q .= ' ORDER BY surname ASC, name ASC';
			break;

			case 'habitacion':
				$q .= ' ORDER BY lodgement_room ASC, surname ASC, name ASC';
			break;
		}		

		//DB::setFetchMode(\PDO::FETCH_ASSOC);

		$this->data_rows = DB::select($q);

		$this->data_headers = array('Insc.','Apellidos','Nombre','Edad','Iglesia','Habitación');

		//dd($this->data_rows);
	}
}