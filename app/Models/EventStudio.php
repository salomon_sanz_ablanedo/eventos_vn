<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventStudio extends Model 
{

	protected $table = 'event_studios';

	public $timestamps = false;

	protected $guarded = array('id','event_id');

	protected $primaryKey = 'id';

	protected 
		$id,
		$name,
		$short_name,
		$info,
		$event_id,
		$date;

	public function event(){
		return $this->belongsTo('App\Models\Evento', 'id', 'event_id');
	}

	public function eventInscription(){
		return $this->belongsToMany('App\Models\EventUserInscription', 'id', 'studio_id');
	}
}
