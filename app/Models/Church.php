<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Church extends Model {

	protected $table = 'churchs';
	
	public $timestamps = true;
	
	protected $guarded = array('id');
		
	protected 
		$id,
		$name,
		$city,
		$address,	
		$contact_phone,
		$contact_events;		
}
