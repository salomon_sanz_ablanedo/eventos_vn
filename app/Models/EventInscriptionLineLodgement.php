<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventInscriptionLineLodgement extends Model 
{

	protected $table = 'event_inscriptions_lines_lodgements';

	public $timestamps = false;

	protected $guarded = array('id');

	protected $primaryKey = 'id';

	protected 
		$id,
		$inscription_id,
		$lodgement_id,
		$date;


	public function inscription(){
		return $this->belongsTo('App\Models\EventInscriptionLine', 'line_id', 'inscription_id');
	}

	public function scopeInscriptionId($query, $id){
		return $query->where('inscription_id', $id);
	}
}
