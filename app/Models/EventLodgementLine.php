<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventLodgementLine extends Model 
{

	protected $table = 'events_external_lodgements_lines';

	public $timestamps = false;

	protected $guarded = array('lodgement_line_id');

	protected $primaryKey = 'lodgement_line_id';

	protected 
		$lodgement_line_id,
		$lodgement_id,
		$event_id;

	public function event(){
		return $this->belongsTo('App\Models\Evento', 'id', 'event_id');
	}

	public function lodgement(){
		return $this->hasOne('App\Models\EventLodgement', 'id', 'lodgement_id');
	}
}
