<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSport extends Model 
{

	protected $table = 'event_sports';

	public $timestamps = false;

	protected $guarded = array('sport_id','event_id');

	protected $primaryKey = 'sport_id';

	protected 
		$sport_id,
		$name,
		$event_id;

	public function event(){
		return $this->belongsTo('App\Models\Evento', 'id', 'event_id');
	}
}
