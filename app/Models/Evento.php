<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model 
{
	protected $table      = 'events';
	protected $guarded    = array('id');
	protected $primaryKey = 'id';
	public    $timestamps    = true;

	protected 
		$id,
		$type,
		$name,	
		$slogan,
		$slogan_versicule,
		$address,
		$latitude,
		$longitude,
		$date_from,
		$date_to,
		$date_inscription_start,
		$date_inscription_end,
		$date_reduced_inscription_end,
		$visible,
		$lodgement_cost,
		$external_lodgements,
		$lodgement_type,
		$IBAN_bank,
		$cost_inscription,
		$cost_inscription_reduced,
		$cost_inscription_reduced_policy,
		$cost_inscription_childs,
		$cost_parking, // parking completo
		$cost_parking_day, // parking cada día
		$cost_parking_obs, // coste parking completo
		$cost_parking_day_obs, // coste parking dia a dia
		$nursery,
		$nursery_cost,
		$bus_cost,
		$bus_info,
		$require_photo,
		$reduced_cost_event_id;
	
	public function inscriptions()
	{
		return $this->hasMany('App\Models\EventInscription','event_id','id');		
	}

	public function meals(){
		return $this->hasMany('App\Models\EventMeal','event_id','id')
			->orderBy('date','asc')
			->orderBy('type','asc');
	}

	public function studios()
	{
		return $this->hasMany('App\Models\EventStudio','event_id','id');		
	}

	public function sports()
	{
		return $this->hasMany('App\Models\EventSport','event_id','id');		
	}

	public function lodgements()
	{
		return $this->hasManyThrough(
            'App\Models\EventLodgement',
            'App\Models\EventLodgementLine',
            'event_id',
            'id',
            'id',
            'lodgement_id' // Local key on countries table...
        );
	}

	public function inscriptionlines(){
        return $this->hasManyThrough('App\Models\EventInscriptionLine', 'App\Models\EventInscription', 'event_id','inscription_id','id');
    }

}
