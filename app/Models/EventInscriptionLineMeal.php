<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventInscriptionLineMeal extends Model {

	protected $table = 'event_inscriptions_lines_meals';
	
	public $timestamps = false;
	
	//protected $guarded = array('event_meal_id');
		
	protected 
		$id,
		$event_meal_id,
		$inscription_id,
		$registration_date,
		$registered_by_user,
		$picnic;

	public function scopeEventMealId($query, $meal_id){
		return $query->where('event_meal_id', '=', $meal_id);
	}

	public function scopeUnregistered($query){
		return $query->whereNull('registration_date');
	}

	public function scopeInscriptionId($query, $id){
		return $query->where('inscription_id', '=', $id);
	}

	public function user_registrator(){
		return $this->belongsTo('\App\Models\User','registered_by_user','user_id');
	}

	public function meal(){
		return $this->hasOne('App\Models\EventMeal','id','event_meal_id');
	}
}
