<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventLodgementRoomPlace extends Model 
{
	protected $table = 'event_lodgement_rooms_places';

	public $timestamps = false;

	protected $guarded = array('place_id');

	protected $primaryKey = 'place_id';

	protected 
		$place_id,
		$room_id,
		$inscription_id;

	public function inscription(){
		return $this->hasOne('App\Models\EventInscriptionLine', 'line_id', 'inscription_id');
	}

	public function room(){
		return $this->belongsTo('App\Models\EventLodgementRoom', 'room_id', 'room_id');
	}

	public function scopeRoomId($q, $room)
	{
		return $q->where('room_id', $room);
	}

}
