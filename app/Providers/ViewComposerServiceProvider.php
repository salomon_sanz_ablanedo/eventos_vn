<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use FotiApi, FotiCart, FuLoc, Auth;

class ViewComposerServiceProvider extends ServiceProvider
{    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {       
        # Category Menu  
        app('events')->listen(
            'creating:*',
            function ($event, $view){
                \Config::push('assets.used_views', $view[0]->getName());
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}