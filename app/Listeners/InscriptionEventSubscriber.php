<?php 

namespace App\Listeners;

use App\Services\EmailService;
use App\Mail\InscriptionAmountChanged;
use Mail;

class InscriptionEventSubscriber
{
    /**
     * Handle user login events.
     */
    public function onPriceChanged($e){
        Mail::queue(new InscriptionAmountChanged($e->inscription));
    }


    public function subscribe($events)
    {
        $events->listen(
            'App\Events\InscriptionTotalPriceModified',
            'App\Listeners\InscriptionEventSubscriber@onPriceChanged'
        );
    }
}