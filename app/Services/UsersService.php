<?php

namespace App\Services;

use App\Models\User as User;

class UsersService
{
	private static $instance;

	const FAKE_DNI_PREFIX = '0FK';

	public static function getInstance()
    {
     	if ( !self::$instance instanceof self)
        	self::$instance = new self;
	    return self::$instance;
    }

    public function cleanLegalId($str){
    	return strtoupper(preg_replace("/[^A-Za-z0-9]/", '', $str));
    }

    public function findUserByDNI($legal_id)
    {
    	$legal_id = $this->cleanLegalId($legal_id);
    	return User::where('legal_id','LIKE','%'.$legal_id.'%')->get();
    }

    // validate both NIF/NIE 
    public function validateLegalId($dni)
    {
        if (strlen($dni) != 9 ||
            preg_match('/^[XYZ]?([0-9]{7,8})([A-Z])$/i', $dni, $matches) !== 1) {
            return false;
        }

        return true;
    }

    public function findUsersByString($str)
    {
    	$int = (int) trim($str);
    	
		if (ctype_digit(trim($str)) && !empty($int))
		{
			$users = User::find($str);
		}
		// si es un DNI (si tiene números)
		else if (preg_match_all( "/[0-9]/", $str) > 0)
		{
			$str = $this->cleanLegalId();
			$users = User::where('legal_id', 'LIKE', '%'.trim($str).'%');
		}
		// si es un nombre
		else
		{
			$users = User::where('surname', 'LIKE', '%'.$str.'%');
		}

		return $users->get();
    }
                                                                        
    public function generateFakeDNI($date, $name, $surname)
    {
    	// para que la entrada de date sirva yyyy-mm-dd tanto como dd/mm/yyyy o dd-mm-yyyy
    	if (!is_numeric(substr($date, 4,1)))
   			$date = \DateUtils::mysqlDateToStandarDate(str_replace('/', '-',$date));
        //die(self::FAKE_DNI_PREFIX.preg_replace("/[^0-9]/",'',$date).substr(trim($name),0,1).substr(trim($surname),0,1));
    	return self::FAKE_DNI_PREFIX.preg_replace("/[^0-9]/",'',$date).strtoupper(substr(trim($name),0,1).substr(trim($surname),0,1));
    	//0FK17121984SS
    }

    public function isFakeDNI($dni)
    {
    	return substr($dni, '0',strlen(self::FAKE_DNI_PREFIX)) == self::FAKE_DNI_PREFIX;
    }

}


