<?php

namespace App\Services;

use Mail;
use \InlineStyle\InlineStyle;
use \App\Mail\InscriptionCompleted;
use \App\Mail\PaymentReceived;

class EmailService
{
	private static $instance;

	public static function getInstance()
    {
     	if ( !self::$instance instanceof self)
        	self::$instance = new self;
	    return self::$instance;
    }

    public function sendHtmlEmail($text_email, $to, $subject)
    {
        if (app()->environment('development'))
            $to = 'djmon84@gmail.com';

        Mail::queue('layouts.blank_html_email', ['msg'=>$text_email], function ($message) use ($to,$subject){
            $message->subject($subject);
            $message->to($to);
            $message->replyTo('retiroiglesias@gmail.com');
        });
    }

    public function sendInscriptionConfirmation($inscription){
        Mail::queue(new InscriptionCompleted($inscription));
    }

    public function sendPaymentReceived($inscription){
        Mail::queue(new PaymentReceived($inscription));
    }

    /*
    public function sendHtmlEmail($html, $to, $subject)
    {
        $htmldoc = new InlineStyle($html);
        $htmldoc->applyStylesheet($htmldoc->extractStylesheets());
        $html = $htmldoc->getHTML();
        die($html);

        Mail::raw($html, ['user' => $user], function ($message) use ($to,$subject) {
            //$m->to($user->email, $user->name)->subject('Your Reminder!');
            $message->subject($subject);
            $message->to($to);
        });
    }
    */
}


