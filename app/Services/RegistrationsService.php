<?php

namespace App\Services;

use \DateUtils;
use \Illuminate\Support\Arr;
use \App\Services\EmailService;
use Session, Log;

class RegistrationsService
{
  	private static $instance;

  	public static function getInstance()
	{
	  if ( !self::$instance instanceof self)
	      self::$instance = new self;
	  return self::$instance;
	}

	// public function getUserInscriptionFormParams(
	// 	$event_id)
	// {
	// 	$Event = resolve('\App\Models\Evento');
	// 	$Church = resolve('\App\Models\Church');

	// 	$evento = $Event::with('studios','meals','lodgements','lodgements.lodgement','sports')->find($event_id);

	// 	$lodgement_dates = [];

	// 	$lodgement_main = null;

	// 	foreach($evento->lodgements as $l)
	// 	{
	// 		if ($l->lodgement->external == 0){
	// 			$lodgement_main = $l->lodgement;
	// 			break;
	// 		}
	// 	}

	// 	if (count($evento->lodgements) > 0)
	// 	{
	// 		$lodgements_total_nights = DateUtils::getDaysBetweenDates($evento->date_from, $evento->date_to);

	// 		for ($a = 0; $a < $lodgements_total_nights; $a++)
	// 		{
	// 			$day = date_format(date_add(date_create($evento->date_from), date_interval_create_from_date_string((string) $a .' days')), 'Y-m-d');

	// 			$lodgement_dates[] = array(
	// 				'date' 		=> $day,
	// 				'date_name' => DateUtils::getDayNameByDate($day) . ' '. DateUtils::getMonthDayFromDate($day)
	// 			);
	// 		}
	// 	}

	// 	$reg_service = \App\Services\RegistrationsService::getInstance();

	// 	$params = array(
	// 		'event' 		     => $evento,
	// 		'churchs' 			 => $Church::orderBy('city', 'ASC')->get(),
	// 		//'inscription_type' 	 => \Input::has('ind')?'individual':'group',
	// 		//'lodgement_dates' 	 => $lodgement_dates,
	// 		//'lodgement_main' 	 => $lodgement_main,
	// 		//'event_meals_by_day' => $meals_by_day,
	// 		'relations' 		 => $reg_service ->getAvailableRelations()
	// 	);

	// 	return $params;
	// }

	public function finishCartInscription(\App\Models\EventCart $cart)
	{
		$new_insc = $this->cart2Inscription($cart);
		$this->updateInscriptionPrice($new_insc);
		EmailService::getInstance()->sendInscriptionConfirmation($new_insc);

		//if (!in_array($cart->contact_email,['djmon84@gmail.com'])){
			$cart->status = 'CLOSED';
			$cart->save();
		//}

		// solo funciona para transferencia
		//if (!in_array($cart->contact_email,['djmon84@gmail.com']))
			Session::regenerate(true);

		return $new_insc;
	}

    public function getAvailableRelations()
    {
		$type = \DB::select(\DB::raw("SHOW COLUMNS FROM event_cart_lines WHERE Field = 'lider_relationship_type'") )[0]->Type;
		preg_match('/^enum\((.*)\)$/', $type, $matches);
		$enum = array();
		foreach( explode(',', $matches[1]) as $value )
		{
			$v = trim( $value, "'" );
		 	$enum = array_add($enum, $v, $v);
		}
		asort($enum);
		return $enum;
    }

    public function updateInscriptionPrice($insc)
    {
    	$lines = $insc->lines->pluck('line_id');    	

		$charges = \App\Models\EventInscriptionLineCharge::inscriptionLines($lines)->get();		

		$total_charges = 0;
		$total_paid    = 0;

		foreach($charges as $c)
		{
			if ($c->quantity > 0 || $c->promotion) // solo cargos
				$total_charges += $c->quantity * $c->price;
			else
				$total_paid += abs($c->quantity * $c->price);
		}
		//die(json_encode($total_charges));
		$insc->total_price = $total_charges;
		$insc->total_paid  = $total_paid;

		Log::info('total_charges: '.$total_charges);
		Log::info('total_paid' . $total_paid);
		$insc->save();
		return $insc;
    }

    public function legalIdExistsInInscription($legal, $event_id)
    {
    	if (!is_array($legal))
    		$legal = [$legal];

    	$legals = \DB::table('event_inscriptions_lines')
        	->join('event_inscriptions', 'event_inscriptions_lines.inscription_id', '=', 'event_inscriptions.inscription_id')
        	->where('event_inscriptions_lines.nif', $legal)
        	->where('event_inscriptions.event_id', $event_id)
        	->get();

    	// todo que pasa si son de otros eventos
    	if (count($legals) == 0)
    		return false;
    	else
    		return $legals->first();
    }

    public function legalIdExistsInCart($legal, $cart)
    {
    	if (!is_array($legal))
    		$legal = [$legal];

    	$legals = \App\Models\EventCartLine::whereIn('nif',$legal)->where('cart_id', $cart)->get();

    	// todo que pasa si son de otros eventos
    	if (count($legals) == 0)
    		return false;
    	else
    		return $legals;
    }

    public function calculateUserInscriptionCharges($insc, $event_id)
    {
		$event      = \App\Models\Evento::find($event_id);
		$Usercharge = new \App\Models\EventInscriptionLineCharge;
		$EventMeal  = new \App\Models\EventMeal;
		$charges    = [];
		$total      = 0;
		$meals      = [];

		if ($event->cost_inscription_childs_policy == '13_AT_EVENT')
		{
			$age = \DateUtils::getYearsDiff($insc->birthday, $event->date_from);
		}
		else
		{
			// 13_AT_EVENT_YEAR
			$age = \DateUtils::getYearsDiff($insc->birthday, explode('-', $event->date_from)[0].'-12-31');
		}

    	if (class_basename(get_class($insc)) == 'EventCartLine')
    	{
			$charge_inscription_id = null;

			if ($insc->meals && !empty($insc->meals_list))
				$meals = $EventMeal::whereIn('id',$insc->meals_list)->get();
    	}
    	else
    	{			            
            $charge_inscription_id = $insc->line_id;
            $meals = $EventMeal::whereIn('id',function($q) use ($insc){
                    $q->from('event_inscriptions_lines_meals')
                        ->selectRaw('event_meal_id')
                        ->where('inscription_id', '=', $insc->line_id);
            })->get();
    	}


		if ($event->cost_inscription > 0)
		{
			if ($event->lodgement_type == 'FULL')
			{
				if (!empty($insc->lodgement_id))
				{
					// comprobamos si es una persona VIP (no paga)

					$free = \App\Models\EventInscriptionFree::where('event_id',$event_id)->where('nif',$insc->nif)->get();

					if (count($free) > 0)
					{
						$charge_inscription = new $Usercharge([
							'description'    => 'Inscripción gratuita para pastor, responsable o profesor',
							'price'          => 0,
							'quantity'       => '1',
							'inscription_id' => $charge_inscription_id,
							'promotion'      => 0
						]);
					}
                    else if ($insc->lodgement_without_room && $event->type == 'CAMPAMENTO')
                    {
                        $charge_inscription = new $Usercharge([
							'description'    => 'Inscripción gratuita acompañante (bebé o niño)',
							'price'          => 0,
							'quantity'       => '1',
							'inscription_id' => $charge_inscription_id,
							'promotion'      => 0
						]);
                    }
					else if ($age < 13)
				    {
						$charge_inscription = new $Usercharge([
							'description'    => 'Inscripción infantil (0-12)',
							'price'          => $event->cost_inscription_childs,
							'quantity'       => '1',
							'inscription_id' => $charge_inscription_id,
							'promotion'      => 0
						]);
					}
                    else
                    {
						// TODO: comprobar si ya está apuntado el campamento para hcer descuento
						$charge_inscription = new $Usercharge([
							'description'    => 'Inscripción '.$event->type,
							'price'          => $event->cost_inscription,
							'quantity'       => '1',
							'inscription_id' => $charge_inscription_id,
							'promotion'      => 0
						]);
					}
				}
				else
				{
					$charge_inscription = new $Usercharge([
						'description'    => 'Inscripción '.$event->type . '(sin comida ni alojamiento)',
						'price'          => 0,
						'quantity'       => '1',
						'inscription_id' => $charge_inscription_id,
						'promotion'      => 0
					]);
				}
			}
			else
			{
				if ($age < 13)
				{
					$charge_inscription = new $Usercharge([
						'description'    => 'Inscripción infantil (0-12)',
						'price'          => $event->cost_inscription_childs,
						'quantity'       => '1',
						'inscription_id' => $charge_inscription_id,
						'promotion'      => 0
					]);
				}
				else
				{
					// según fecha, reducido o completo
					$created_time = null;

					if (empty($insc->created_at))
						$created_time = time();
					else
						$created_time = strtotime($insc->created_at);

					//date_timestamp_get(date_create($insc->created_at));


					if (
						!empty($event->cost_inscription_reduced) && (
							($event->cost_inscription_reduced_policy == 'DATE' && !empty($event->date_reduced_inscription_end) && $created_time < strtotime($event->date_reduced_inscription_end)) ||
							($event->cost_inscription_reduced_policy == 'FOREIGN' && $insc->church_id != 2)
						)
					)
					{
						$charge_inscription = new $Usercharge([
							'description'       => 'Inscripción reducida adulto (+13)',
							'price'             => $event->cost_inscription_reduced,
							'quantity'          => '1',
							'inscription_id'    => $charge_inscription_id,
							'promotion'      => 0
						]);
					}
					else
					{
						$charge_inscription = new $Usercharge([
							'description'    => 'Inscripción adulto (+13)',
							'price'          => $event->cost_inscription,
							'quantity'       => '1',
							'inscription_id' => $charge_inscription_id,
							'promotion'      => 0
						]);
					}
				}
			}
		}

		// calculate payment amount
		if (!empty($insc->parking))
		{
			$charge_parking = new $Usercharge([
				'description'    => 'Parking estándar',
				'price'          => $event->cost_parking,
				'quantity'       => '1',
				'inscription_id' => $charge_inscription_id,
				'promotion'      => 0
			]);
		}
		else if (!empty($insc->parking_days))
		{
			$charge_parking = new $Usercharge([
				'description'    => 'Parking por dia',
				'price'          => $event->cost_parking_day,
				'quantity'       => $insc->parking_days,
				'inscription_id' => $charge_inscription_id,
				'promotion'      => 0
			]);
		}

		if (isset($charge_inscription)) 
			$charges[] = $charge_inscription;

		if (isset($charge_parking)) 
			$charges[] = $charge_parking;

		if (!empty($meals))
		{
			foreach($meals as $m)
			{
				$charges[] = new $Usercharge([
					'description'    => substr($m->type .' ',2). \DateUtils::getDayNameByDate($m->date).' ('.\DateUtils::mysqlDateToStandarDate($m->date).')',
					'price'          => $m->price,
					'quantity'       => 1,
					'inscription_id' => $charge_inscription_id,
					'promotion'      => 0
				]);
			}
		}

		if (!empty($insc->lodgement_id) && !$insc->lodgement_without_room)
		{
			$lodgement = $insc->lodgement;

			if ($lodgement->external == 0 && $event->lodgement_cost > 0)
			{
				$nights = $insc->lodgement_dates;

				if (count($nights) > 0)
				{
					foreach ($nights as $n)
					{
						if (!is_object($n))
							$night_date = $n;
						else
							$night_date = $n->date;

						$charges[] = new $Usercharge([
							'description'    => 'alojamiento noche del ' . \DateUtils::getDayNameByDate($night_date).' ('.\DateUtils::mysqlDateToStandarDate($night_date).')',
							'price' 		 => $event->lodgement_cost,
							'quantity'		 => 1,
							'inscription_id' => $charge_inscription_id,
							'promotion'      => 0

						]);
					}
				}
				else
				{
					$charges[] = new $Usercharge([
						'description'    => 'Alojamiento y comida',
						'price' 		 => $age < 13 ? $event->lodgement_cost : $event->cost_inscription_childs ?? $event->lodgement_cost,
						'quantity'		 => 1,
						'inscription_id' => $charge_inscription_id,
						'promotion'      => 0
					]);
				}
			}
		}

		if ($age < 13 && $insc->nursery && $event->nursery_cost)
		{
			$charges[] = new $Usercharge([
				'description'    => 'Peto de guardería',
				'price' 		 => $event->nursery_cost,
				'quantity'		 => 1,
				'inscription_id' => $charge_inscription_id,
				'promotion'      => 0
			]);
		}

		if ($insc->bus)
		{
			$charges[] = new $Usercharge([
				'description'    => 'Autobús (ida y vuelta)',
				'price' 		 => $event->bus_cost,
				'quantity'		 => 1,
				'inscription_id' => $charge_inscription_id,
				'promotion'      => 0
			]);
		}

		foreach($charges as $c)
			$total += $c->quantity * $c->price;

		if ($event->type=='PRECAMPAMENTO' && $total > 0)
		{
			/* $charges[] = new $Usercharge([
				'description'    => 'Piscina e instalaciones',
				'price' 		 		 => 2,
				'quantity'		 	 => 1,
				'inscription_id' => $charge_inscription_id,
				'promotion'      => 0
			]);

			$total = $total + 2; */

			if ($event->reduced_cost_event_id)
			{ 
				//$event->cost_inscription : $event->cost_inscription_reduced
				$event_chained = \App\Models\Evento::find($event->reduced_cost_event_id);

				$res = \DB::table('event_inscriptions_lines')
								->join('event_inscriptions', 'event_inscriptions_lines.inscription_id', '=', 'event_inscriptions.inscription_id')
								->where('event_inscriptions_lines.nif', $insc->nif)
								->where('event_inscriptions.event_id', $event->reduced_cost_event_id)
								->get();

				$to_descount = 30;

				if ($total < 30)
					$to_descount = $total;

				if (count($res) > 0)
				{
					$charges[] = new $Usercharge([
						'description'    => 'Descuento promoción PRE + CAMPAMENTO',
						'price' 		 => $to_descount,
						'quantity'		 => -1,
						'inscription_id' => $charge_inscription_id,
						'promotion'      => 1
					]);
					$total = $total - $to_descount;

				}
			}
		}
		

		// if ($total == 102 || $total == 103)
		// {
		// 	$charges[] = new $Usercharge([
		// 		'description'    => 'Descuento promoción evento completo',
		// 		'price' 		 => 2,
		// 		'quantity'		 => -1,
		// 		'inscription_id' => $charge_inscription_id,
		// 		'promotion'      => 1
		// 	]);

		// 	$total -= 2;
		// }

    	return array(
    		'total'   => $total,
    		'charges' => $charges
    	);
    }

    public function registerInscriptionLog($inscription, $text, $action_user = null)
    {
    	$log = new \App\Models\EventInscriptionLog;
    	$log->inscription_id = $inscription;
    	$log->text = $text;

    	if (!empty($action_user) && is_numeric($action_user))
    		$log->by_user_id = $action_user;
    	else if (\Auth::check())
    		$log->by_user_id = \Auth::user()->user_id;

    	$log->save();
    }

    public function cart2Inscription(\App\Models\EventCart $cart)
    {
    	// inscription
		$params       = Arr::except($cart->toArray(), ['cart_id','session','status','lines','created_at','updated_at']);
		$inscription  = \App\Models\EventInscription::create($params);

		$params_meals      = [];
		$params_lodgements = [];
		$params_charges    = [];
		//dd($cart->lines);

		// inscription lines
    	foreach($cart->lines as $_line)
    	{
			$_line->inscription_id = $inscription->inscription_id;
			$line                 = $_line->toArray();
			$line['public_id'] 	  = $_line->public_id; //\App\Utils\StrUtils::readableRandom(5);
			$meals                = $line['meals_list'];
			$lodgement_dates      = $line['lodgement_dates'];
			$charges              = [];

    	$inscription_line = \App\Models\EventInscriptionLine::create(Arr::except($line, ['cart_id','line_id','payment_charges','meals_list','lodgement_dates','created_at','updated_at']));

    		//$inscription_line->public_id = $line['public_id'];

    		if (!empty($meals))
    		{
    			foreach($meals as $meal_id)
    			{
    				$params_meals[] = array(
    					'event_meal_id'  => $meal_id,
						'inscription_id' => $inscription_line->line_id,
					); 
    			}
    		}

    		if (!empty($line['lodgement_id']) && !empty($lodgement_dates))
    		{
    			foreach($lodgement_dates as $lodg_date)
    			{
    				$params_lodgements[] = array(
						'inscription_id' => $inscription_line->line_id,
    					'lodgement_id'   => $line['lodgement_id'],
    					'date'           => $lodg_date
					);
				}
    		}

    		$charges = $this->calculateUserInscriptionCharges($_line, $cart->event_id);

    		foreach($charges['charges'] as $charge){
					$charge->inscription_id = $inscription_line->line_id;
					$charge = $charge->toArray();
					$charge['manual_charge'] = 0;
					$params_charges[] = $charge;
				}
    	}

    	if ($cart->payment_method == 'CARD' && $cart->total_paid > 0)
    	{
    		$params_charges[] = array(
				'description'    => 'Pago ONLINE con tarjeta',
				'price'          => $cart->total_paid,
				'quantity'       => -1,
				'inscription_id' => $params_charges[0]['inscription_id'],
				'promotion'      => false,
				'manual_charge'  => 1
    		);
    	}

    	// meals
    	if (!empty($params_meals))
    		\App\Models\EventInscriptionLineMeal::insert($params_meals);

    	// lodgements
    	if (!empty($params_lodgements))
    		\App\Models\EventInscriptionLineLodgement::insert($params_lodgements);

    	// charges
    	if (!empty($params_charges))
    		\App\Models\EventInscriptionLineCharge::insert($params_charges);
    	
    	$inscription->public_id = $cart->public_id;
    	$inscription->save();


    	return $inscription;
    }

    public function storeUserPhoto($base64_img, $line)
    {
			$base64_img = substr($base64_img, 1+strrpos($base64_img, ','));
			file_put_contents($this->getUserPhotoPath($line), base64_decode($base64_img));	
		}
		
		public function removeUserPhoto($line){
			@unlink($this->getUserPhotoPath($line));
		}

		private function getUserPhotoPath($line){
			return public_path().'/uploads/users_photos/'.$line->public_id.'.jpeg';
		}
}