<?php

namespace App\Services;

class EventsService
{
	private static $instance;

	public static function getInstance()
    {
     	if ( !self::$instance instanceof self)
        	self::$instance = new self;
	    return self::$instance;
    }

    public function getBraceletHtml($id, $print = true, $auto_close = false, $size = 'kids')
    {
        if (!is_array($id))
            $id = [$id];

        $inscriptions = \App\Models\EventInscriptionLine::with(['church','inscription'])->whereIn('line_id', $id)->get();
        
        //$inscriptions->where('inscription_id','>','2175');
        //$inscriptions->where('inscription_id','<=','1800');
        //$inscriptions = $inscriptions->limit(20000)->get();

        $event_date_from = $inscriptions[0]->inscription->event->date_from;
        $print = true;

        return view('layouts.admin.print_label_bracelet', array(
            'size'            => $size,
            'event_date_from' => $event_date_from,
            'inscriptions' => $inscriptions,
            'print'        =>  $print,
            'auto_close'   =>   $auto_close
        ));
    }

    public function getIdentificatorHtml($id, $print = true, $auto_close = false)
    {
        if (!is_array($id))
            $id = [$id];

        $inscriptions = \App\Models\EventInscriptionLine::find($id);

    	return view('layouts.admin.print_label_identificator',array(
    		'inscriptions' => $inscriptions,
    		'print'       =>  $print,
    		'auto_close' => $auto_close
    	));
    }
}


