<?php

return [

    'used_views' => [],

    'roots' => [
        'layouts.base',
        'layouts.admin.base',
        'layouts.admin.naked',
    ],

    'fixed' => [
            
     ],

    'views' => [

        'layouts|admin|naked' => [
            ['css', 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin'],
            ['css', 'css/bootstrap.min.css'],
            ['css', 'css/nifty.min.css'],
            ['css', 'plugins/font-awesome/css/font-awesome.min.css'],
            ['css', 'plugins/morris-js/morris.min.css'],
            ['css', 'plugins/switchery/switchery.min.css'],
            ['css', 'plugins/bootstrap-select/bootstrap-select.min.css'],
            ['css', 'plugins/bootstrap-datepicker/bootstrap-datepicker.css'],
            ['css', 'css/demo/nifty-demo.min.css'],
            ['css', 'css/web.css'],
            ['js_header', 'js/jquery-3.3.1.min.js'],
            ['js_header', 'https://unpkg.com/axios/dist/axios.min.js'],
            ['js_header', 'js/nifty.js'],
            ['js_header', 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js'],
            ['js_header', 'js/bootstrap.min.js'],
            ['js_header', 'plugins/bootstrap-select/bootstrap-select.min.js'],
            ['js_header', 'plugins/bootstrap-datepicker/bootstrap-datepicker.js'],
            ['js_header', 'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.js'],
            ['js_footer', 'js/modal_fit_center.js'],
            ['js_footer', 'js/AjaxService.js?d=2170307'],
            ['js_blade',  'partials/common_js'],
        ],

        /************
        /* LAYOUTS */   

        'layouts|admin|base' => [
            ['css', 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin'],
            ['css', 'css/bootstrap.min.css'],
            ['css', 'css/nifty.min.css'],
            ['css', 'plugins/font-awesome/css/font-awesome.min.css'],
            ['css', 'plugins/morris-js/morris.min.css'],
            ['css', 'plugins/switchery/switchery.min.css'],
            ['css', 'plugins/bootstrap-select/bootstrap-select.min.css'],
            ['css', 'plugins/bootstrap-datepicker/bootstrap-datepicker.css'],
            ['css', 'css/demo/nifty-demo.min.css'],
            ['css', 'css/web.css'],
            ['js_header', 'https://unpkg.com/axios/dist/axios.min.js'],
            ['js_header', 'js/jquery-3.3.1.min.js'],
            ['js_header', 'js/nifty.js'],
            ['js_header', 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js'],
            ['js_header', 'js/bootstrap.min.js'],
            ['js_header', 'plugins/bootstrap-select/bootstrap-select.min.js'],
            ['js_header', 'plugins/bootstrap-datepicker/bootstrap-datepicker.js'],
            ['js_header', 'plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.js'],
            ['js_footer', 'js/modal_fit_center.js'],
            ['js_footer', 'js/AjaxService.js?d=2170307'],
            ['js_blade',  'partials/common_js'],
            /*
            <!--BootstrapJS [ RECOMMENDED ]-->
            <script src="{{asset('plugins/fast-click/fastclick.min.js')}}"></script>
            <script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
            <script src="{{asset('plugins/skycons/skycons.min.js')}}"></script> --}}
            */

        ],

        'partials|inscription_form' => [
            ['js_blade',  'partials/common_js'],
            // ['css', 'https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css'],
            // ['js_footer', 'https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.js'],
        ]
    ]    
];
