<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Web\EventController;
use App\Http\Controllers\API\EventCartController;
use App\Http\Controllers\API\EventInscriptionController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdminAjaxController;
use App\Http\Controllers\Admin\EventsController;
use App\Http\Controllers\Admin\InscriptionController;
use App\Http\Controllers\Admin\UsersController;

Auth::routes();

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);

Route::group(['prefix' => 'api'], function()
{
    Route::get('/eventcart/me', [EventCartController::class,'getCart'])->name('api.cart');    
    Route::post('/eventcart/me', [EventCartController::class,'postCart']);
    Route::get('/eventcart/me/line/{line?}', [EventCartController::class,'getCartLine'])->name('api.cart.line');
    Route::post('/eventcart/me/line/{line?}', [EventCartController::class,'postCartLine']);
    Route::delete('/eventcart/me/line/{line}', [EventCartController::class,'deleteCartLine']);
    
    Route::group(['middleware'=>'auth'], function()
    {
        Route::get('/inscription/line/{line}', [EventInscriptionController::class,'getInscriptionLine'])->name('api.inscription.line');
        Route::post('/inscription/line/{line?}', [EventInscriptionController::class,'postInscriptionLine']);
        Route::delete('/inscription/line/{line}', [EventInscriptionController::class,'deleteInscriptionLine']);
        Route::post('/inscription/line/{line}/move', [EventInscriptionController::class,'postInscriptionLineMove'])->name('api.inscription.move');
        Route::delete('/inscription', [EventInscriptionController::class,'deleteInscription'])->name('api.inscription.delete');
    });
});

Route::group(['prefix' => 'admin', ['middleware'=>'auth']], function()
{       
    // admin general
    Route::get('usuarios', [AdminController::class,'usuarios'])->name('admin.usuarios');
    Route::get('eventos',  [AdminController::class,'eventos'])->name('admin.eventos');
    Route::get('inicio',  [AdminController::class,'inicio'])->name('admin.inicio');

    Route::group(['prefix' => 'aj'], function()
    {
        Route::get('/{action}', [AdminAjaxController::class,'getAction'])->name('admin.ajax.get');
        Route::post('/{action}', [AdminAjaxController::class,'postAction'])->name('admin.ajax.post');
        Route::delete('/{action}', [AdminAjaxController::class,'deleteAction'])->name('admin.ajax.delete');
    });

    Route::group(['prefix' => 'eventos/{id}'], function()
    {
        Route::get('inicio' ,  [EventsController::class,'getInicio'])->name('admin.eventos.inicio');
        Route::get('inscribir', [EventsController::class,'getInscribir'])->name('admin.eventos.inscribir');
        Route::get('inscripciones', [EventsController::class,'getInscripciones'])->name('admin.eventos.inscripciones');
        Route::get('registros', [EventsController::class,'getRegistros'])->name('admin.eventos.registros');
        Route::get('registrosPost', [EventsController::class,'getRegistrosPost'])->name('admin.eventos.registros_post');
        Route::get('alojamiento', [EventsController::class,'getAlojamiento'])->name('admin.eventos.alojamiento');

        // AJAX
        Route::get('alojamiento_info', [EventsController::class,'getAlojamientoRoomFloorInfo'])->name('admin.eventos.alojamiento_info');
        Route::get('alojamiento_search', [EventsController::class,'getAlojamientoSearch'])->name('admin.eventos.alojamiento_search');
        Route::get('alojamiento_totals', [EventsController::class,'getAlojamientoTotals'])->name('admin.eventos.alojamiento_totals');
        Route::get('alojamiento_gorras', [EventsController::class,'getAlojamientoGorrasWithoutHelpers'])->name('admin.eventos.alojamiento_gorras_without_helpers');
        Route::post('alojamiento_assign', [EventsController::class,'postAlojamientoAssign'])->name('admin.eventos.alojamiento_assign');
        Route::post('alojamiento_unassign', [EventsController::class,'postAlojamientoUnassign'])->name('admin.eventos.alojamiento_unassign');
        Route::post('alojamiento_lock', [EventsController::class,'postAlojamientoLock'])->name('admin.eventos.alojamiento_lock');
        Route::post('alojamiento_unlock', [EventsController::class,'postAlojamientoUnlock'])->name('admin.eventos.alojamiento_unlock');
       
        Route::get('informes', [ EventsController::class,'getInformes'])->name('admin.eventos.informes');
        Route::post('informes', [ EventsController::class,'postInformes'])->name('admin.eventos.informes_post');
        Route::get('estadisticas', [EventsController::class,'getEstadisticas'])->name('admin.eventos.estadisticas');
        Route::get('configuracion', [EventsController::class,'getConfiguracion'])->name('admin.eventos.configuracion');
    });

    Route::group(['prefix' => 'inscripcion/{id}'], function()
    {
        Route::get('inicio', [InscriptionController::class,'getInicio'])->name('admin.inscripcion.inicio');
        Route::post('inicio', [InscriptionController::class,'postInicio'])->name('admin.inscripcion.inicio_post');
        Route::get('modify', [InscriptionController::class,'getModify'])->name('admin.inscripcion.modify');
        Route::post('modify', [InscriptionController::class,'postModify'])->name('admin.inscripcion.modify_post');
        Route::get('acciones', [InscriptionController::class,'getAcciones'])->name('admin.inscripcion.acciones');
        Route::post('acciones', [InscriptionController::class,'postAcciones'])->name('admin.inscripcion.acciones_post');
        Route::get('anotaciones', [InscriptionController::class,'getAnotaciones'])->name('admin.inscripcion.anotaciones');
        Route::post('anotaciones', [InscriptionController::class,'postAnotaciones'])->name('admin.inscripcion.anotaciones_post');
        Route::get('cargos', [InscriptionController::class,'getCargos'])->name('admin.inscripcion.cargos');
        Route::post('cargos', [InscriptionController::class,'postCargos'])->name('admin.inscripcion.cargos_post');
        Route::get('log', [InscriptionController::class,'getLog'])->name('admin.inscripcion.log');
        Route::get('comidas', [InscriptionController::class,'getMealsInfo'])->name('admin.inscripcion.meals_info');
    });

    Route::group(['prefix' => 'usuarios/{id}'], function()
    {
        Route::get('inicio', [UsersController::class,'getInicio'])->name('admin.usuario.inicio');
        Route::post('inicio', [UsersController::class,'postInicio'])->name('admin.usuarios.inicio_post');
        Route::get('inscripciones', [UsersController::class,'getInscripciones'])->name('admin.usuario.inscripciones');
        Route::get('acciones', [UsersController::class,'getAcciones'])->name('admin.usuario.acciones');
    });

    Route::post('/eventos',  [AdminController::class,'eventos_post'])->name('admin.eventos_post');
    Route::post('/usuarios',  [AdminController::class,'usuarios_post'])->name('admin.usuarios_post');
});

// WEB
Route::group(['prefix' => 'evento/{id}'], function()
{
    Route::get('informacion',  [EventController::class,'getInformacion'])->name('web.evento.informacion');
    Route::get('inscribirse', [EventController::class,'getInscribirse'])->name('web.evento.inscribirse');
});

Route::get('inscripcion-completada/{inscription_id}',[EventController::class,'getInscripcionCompletada'])->name('web.inscripcion_completada');

// payment
Route::get('ko',[EventController::class,'ko'])->name('web.inscripcion_ko');
Route::get('tpv_redirect', [EventController::class,'tpvRedirect'])->name('web.tpv_redirect');
Route::post('notification', [EventController::class,'notification'])->name('tpv_notification');

Route::get('imprimir',[EventController::class,'getImprimir'])->name('web.imprimir');
Route::get('/contacto',[EventController::class,'getContact'])->name('web.contact');
Route::get('/update-public', [EventController::class,'updatePublicId']);
Route::get('/',[EventController::class,'getIndex'])->name('web.index');

/* Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home'); */
