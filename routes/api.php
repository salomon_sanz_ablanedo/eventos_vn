<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/* use \App\Models\Church as Church;
use \App\Http\Resources\Church as ChurchResource; */

/* Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::get('/event/{id}', function (
    Request $request, 
    \App\Models\Evento $evento, 
    $id){
   $evento =  $evento::with(['meals','studios','lodgements'])->find($id);   
   return new \App\Http\Resources\Event($evento);    
});

Route::get('/church', function(    
    \App\Models\Church $Church
)
{
    return App\Http\Resources\Church::collection($Church::all());
});

// Route::get('/eventcart/me', function(){
//     die(Session::getId());
//     return new EventCartResource(EventCart::find(Session::getId()));
// });

// Route::post('/eventcart/me/line/{line_id}', ['as'=>'api.cart.line'], function(){
//     return new EventCartResource(EventCart::find(Session::getId()));
// });
